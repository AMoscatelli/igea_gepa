<?php 
//Da includere per la gestione della sessione
include('../php/config.inc.php');
include('../php/session.php');
include('../php/controller.php');

$action=0;

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $action = 1;
}


?>
<html>
   
   	<head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        
        <title>Igea - page name</title>
    
        <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <!-- Custom Css Select -->
		<link href="css/style.css" rel="stylesheet">
	</head>

   
   <body>
   
   <!--Main Navigation-->
	<header>
	    <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
    <!--/Main Navigation-->
    
    <!--Main Layout-->
    <main>
		<div id="spinner" style='height: 100%; width: 100%'></div>
        <div class="container" id="completed" style="visibility: hidden;">
        
			 <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
             <div class="card card-cascade wider reverse">
          
				<div class="card-body text-center">
     
                    <!--Sezione bottoni shares-->
                    <div class="social-counters ">

                        <!--Facebook-->
                        <button type="submit" name="source" value="cercaContab" class="btn btn-default" onclick="">Cerca</button>
                        <button type="button" class="btn btn-default" id="modificaBtn" onclick="">Modifica</button>
            			<button type="button" class="btn btn-default" id="salvaBtn" onclick="">Salva</button>
					</div>
                    

                </div>
            <!--Post data-->
            </div>
            </section>

            <!--Section: A-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">

                <!--Grid row-->
                <div class="row">

                </div>
               <!--Grid row-->

            </section>
            <!--/Section: A-->

            <hr class="mb-5 mt-4">

            <!--Section: B-->
            <section class="extra-margins pb-3 wow fadeIn" data-wow-delay="0.3s">

                <!--Grid row-->
                <div class="row">


                </div>
                <!--Grid row-->

            </section>
            <!--/Section: B-->

            <!--Section: C-->
            <section>

                <div class="jumbotron author-box px-5 py-4 text-center text-md-left wow fadeIn" data-wow-delay="0.3s">
                    <!--Name-->
                    <h4 class="font-weight-bold h4 text-center">Default Page</h4>
                    <hr>
                    <div class="row">
                	</div>

            </section>
            <!--/Section C-->

            <!--Section: D-->
  	        <section class="mb-4 pt-5 wow fadeIn" data-wow-delay="0.3s">

  	           
  	        </section>
            <!--/Section: D-->

            
        </div>

    </main>
    <!--Main Layout-->
      
   </body>
   <!--  SCRIPTS  -->
    <!-- JQuery -->
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom JS -->
    <script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
    <script type="text/javascript" src="../js/moment.js"></script>	
    <script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
    <!-- Menu Navigazione -->
    <script type="text/javascript" src="jsCustom/navigationSideBar.js"></script>
    <!-- Spinner -->
    <script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
   	<script>
   		$('#spinner').LoadingOverlay("show");
   		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
        new WOW().init();  
        $(document).ready(function () {
        	hideComponent('spinner');
        	showComponent('completed');
        	$('#spinner').LoadingOverlay("hide");  	
        });      
    </script>

</html>



