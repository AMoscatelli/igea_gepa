function initCharts(type, id, color, data) {

	if (type === "percent") {

		$('.min-chart#' + id).easyPieChart({
			barColor : color,// "#4caf50",
			onStep : function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			}
		});

	} else if (type === 'pie') {

		// pie
		var ctxP = document.getElementById("pieChart").getContext('2d');
		var myPieChart = new Chart(ctxP, {
			type : 'pie',
			data : {
				labels : [ "Red", "Green", "Yellow", "Grey", "Dark Grey" ],
				datasets : [ {
					data : [ 300, 50, 100, 40, 120 ],
					backgroundColor : [ "#F7464A", "#46BFBD", "#FDB45C",
							"#949FB1", "#4D5360" ],
					hoverBackgroundColor : [ "#FF5A5E", "#5AD3D1", "#FFC870",
							"#A8B3C5", "#616774" ]
				} ]
			},
			options : {
				responsive : true
			}
		});

	} else if (type === 'polar') {

		// polar
		var ctxPA = document.getElementById("polarChart").getContext('2d');
		var myPolarChart = new Chart(ctxPA, {
			type : 'polarArea',
			data : {
				labels : [ "Red", "Green", "Yellow", "Grey", "Dark Grey" ],
				datasets : [ {
					data : [ 300, 50, 100, 40, 120 ],
					backgroundColor : [ "rgba(219, 0, 0, 0.1)",
							"rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)",
							"rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)" ],
					hoverBackgroundColor : [ "rgba(219, 0, 0, 0.2)",
							"rgba(0, 165, 2, 0.2)", "rgba(255, 195, 15, 0.3)",
							"rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)" ]
				} ]
			},
			options : {
				responsive : true
			}
		});

	} else if (type === 'bar') {

		var ctxB = document.getElementById("barChart").getContext('2d');
		var myBarChart = new Chart(ctxB, {
			type : 'bar',
			data : {
				labels : [ "Tot Paz. in Carico", "Paz. in Trattamento",
						"Paz. in Attesa" ],
				datasets : [ {
					label : 'Pazienti',
					data : data,
					backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)' ],
					borderColor : [ 'rgba(255,99,132,1)',
							'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)' ],
					borderWidth : 1
				} ]
			},
			options : {
				scales : {
					yAxes : [ {
						ticks : {
							beginAtZero : true
						}
					} ]
				}
			}
		});

	} else if (type === 'doughnutChart') {

		// doughnut
		var ctxD = document.getElementById("doughnutChart").getContext('2d');
		var myLineChart = new Chart(ctxD, {
			type : 'doughnut',
			data : {
				labels : [ "Red", "Green", "Yellow", "Grey", "Dark Grey" ],
				datasets : [ {
					data : [ 300, 50, 100, 40, 120 ],
					backgroundColor : [ "#F7464A", "#46BFBD", "#FDB45C",
							"#949FB1", "#4D5360" ],
					hoverBackgroundColor : [ "#FF5A5E", "#5AD3D1", "#FFC870",
							"#A8B3C5", "#616774" ]
				} ]
			},
			options : {
				responsive : true
			}
		});

	}

}