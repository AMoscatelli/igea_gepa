<?php
require 'config.inc.php';
require 'DB.php';

session_start();

//controllo le chimate fatte da JQuery
if(isset($_POST['source'])){
    if(isset($_POST['source'])){
        $source = $_POST['source'];
        switch ($source) {
            case 'salvaSection':
                salvaSezione();
                break;
            case 'inviaSondaggio':
                inviaSondaggio();
                break;
            case 'checkSession':
                checkSession();
        }
    }
}


function salvaQuestionario($q1, $q2, $q3, $q4, $q5, $q6, $q7, $q8, $q9, $q10, $media, $centro){
    $DBa = DB::getInstance();
    $DBa->insert('esitoquestionario', [
        'idStruttura' => $centro,
        'dataValutazione' => date('Y-m-d H:i:s'),
        'q1'	=> $q1,
        'q2'	=> $q2,
        'q3'	=> $q3,
        'q4'	=> $q4,
        'q5'	=> $q5,
        'q6'	=> $q6,
        'q7'	=> $q7,
        'q8'	=> $q8,
        'q9'	=> $q9,
        'q10'	=> $q10,
        'media' => $media
    ]);

    
}

function getDati($dataStart,$dataEnd,$idStruttura){
    $DBa = DB::getInstance();
    
    if($dataStart!=""){
        if($idStruttura=="0"){
            $rows = $DBa->table('esitoquestionario')->where([['dataValutazione >=',$dataStart." 00:00"],['dataValutazione <=',$dataEnd." 23:59"]])->orderBy('dataValutazione','ASC')->get()->toJSON();
        } else {
            $rows = $DBa->table('esitoquestionario')->where([['dataValutazione >=',$dataStart." 00:00"],['dataValutazione <=',$dataEnd." 23:59"],['idStruttura',$idStruttura]])->orderBy('dataValutazione','ASC')->get()->toJSON();
        }
          
    } else {
        $rows = $DBa->table('esitoquestionario')->where([['idStruttura',$idStruttura]])->orderBy('dataValutazione','ASC')->get()->toJSON();
    }
    return $rows;
}

function getStrutture(){
    $DBa = DB::getInstance();
        $rows = $DBa->table('struttura')->get()->toJSON();
    return $rows;
}

function salvaSezione(){
    if(isset($_POST['sezione'])){
        $_SESSION['sezione'.$_POST['sezione']] = $_POST['voto'];
    }
    if(isset($_POST['centro'])){
        $_SESSION['centro'] = $_POST['centro'];
    }
    
}

function inviaSondaggio(){
    if($_SESSION['sezione1']!="10"){
        for($i=1;$i<11;$i++){
            if(isset($_SESSION['sezione'.$i]) & $_SESSION['sezione'.$i]!=""){
                $_SESSION['media'] = ($_SESSION['media'] + $_SESSION['sezione'.$i]);
            }
        }
        $_SESSION['media']=$_SESSION['media']/10;
        salvaQuestionario($_SESSION['sezione1'], $_SESSION['sezione2'], $_SESSION['sezione3'], $_SESSION['sezione4'], $_SESSION['sezione5'], $_SESSION['sezione6'], $_SESSION['sezione7'], $_SESSION['sezione8'], $_SESSION['sezione9'], $_SESSION['sezione10'], $_SESSION['media'], $_SESSION['centro']);
    }else{
        //    Rieti = 2
        //    Collevecchio 1    
        salvaQuestionario("5","5","5","5","5","5","5","5","5","5","5",$_SESSION['centro']);
    }
        
    
    resetSession();
}

function resetSession(){
    for($i=1;$i<11;$i++){
        if(isset($_SESSION['sezione'.$i])){
            $_SESSION['sezione'.$i] = "";
        }
    }
    
    $_SESSION['media'] = "";
    $_SESSION['timeout'] = "";
    $_SESSION['oldSession']="";
    $_SESSION['currentSession']="";
}


function checkSession(){
    
    if(isset($_SESSION['oldSession'])){
        if($_SESSION['oldSession']!=""){
            if($_SESSION['currentSection']==$_SESSION['oldSession']) {
                
            } else {
                $_SESSION['oldSession'] = $_SESSION['currentSection'];
                unset($_SESSION['timeout']);
            }
        } else {
            $_SESSION['oldSession'] = $_SESSION['currentSection'];
            unset($_SESSION['timeout']);
        }
    } else {
        $_SESSION['oldSession'] = $_SESSION['currentSection'];
        unset($_SESSION['timeout']);
    }
    
    // set timeout period in seconds
    $inactive = 49;
    $session_life = 0;
    
    // check to see if $_SESSION['timeout'] is set
    if(isset($_SESSION['timeout']) ) {
        $session_life = time() - $_SESSION['timeout'];
        if($session_life > $inactive){
            session_destroy();
            resetSession();
            $session_life = "KO";
        }
    } else {
        $_SESSION['timeout'] = time();
    }
    echo $session_life;
}

?>