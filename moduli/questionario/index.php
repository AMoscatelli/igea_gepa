<?php 

include('php/config.inc.php');
include('php/session.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Mondo Riabilitazione</title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../../css/mdb.min.css" rel="stylesheet">

  <style>

/* Required for full background image */

html,
body,
header,
#intro-section {
height: 100%;
background-image:url('../../images/sfondo.png');
background-repeat:no-repeat;
background-size:contain;
background-position:center;
}

.logo{
   margin-bottom: 293px;
}

@media (max-width: 740px) {
html,
body,
header,
#intro-section {
height: 100%;
background-image:url('../../images/sfondo.png');
background-repeat:no-repeat;
background-size:contain;
background-position:center;
}

.logo{
    margin-bottom: 293px;
}


}

.top-nav-collapse {
background-color: #563e91 !important;
}

.navbar:not(.top-nav-collapse) {
background: transparent !important;
}

@media (max-width: 991px) {
.navbar:not(.top-nav-collapse) {
background: #563e91 !important;
}
}

.rgba-gradient {
  background: -moz-linear-gradient(45deg, rgba(0, 148, 53, 0.05), rgba(130, 130, 130, 0.1) 100%);
  background: -webkit-linear-gradient(45deg, rgba(0, 148, 53, 0.05), rgba(130, 130, 130, 0.1) 100%);
  background: linear-gradient(to 45deg, rgba(0, 148, 53, 0.05), rgba(130, 130, 130, 0.1) 100%);
}

div#scritta {
    margin-bottom: 50px;
}

</style>

</head>

<body>


  <!-- Full Page Intro -->
  <div id="intro-section" class="view">

    <video class="video-intro" poster="https://mdbootstrap.com/img/Photos/Others/background.jpg" playsinline autoplay muted loop>
      <source src="https://mdbootstrap.com/img/video/animation.mp4" type="video/mp4">
    </video>

    <!-- Mask & flexbox options-->
    <div class="mask rgba-gradient d-flex justify-content-center align-items-center">

      <!-- Content -->
      <div class="container px-md-3 px-sm-0">

        <!--Grid row-->
        <div class="row wow fadeIn mt-5">

          <!--Grid column-->
          <div class="col-md-12 white-text text-center">

<!--             <h3 class="display-3 font-weight-bold white-text mb-0 pt-md-5 pt-5">Creative Agency</h3> --> 
			<div class="text-center logo mt-2" ><img src="../../images/logo.png" class="rounded mx-auto d-block animated fadeInDown" width="48%"></div>
<!--             <hr class="hr-light my-4 w-75"> -->
            <div id="scritta"><h4 class="subtext-header mt-2"><strong>Compilando questo breve sondaggio, ci aiuterai ad ottenere risultati migliori!</strong></h4></div>
          </div>
          
          <!--Grid column-->
        </div>
        
        
      
      
        	
        <!--Grid row-->
        <!-- bottone Inizia -->
				<div class="row animated fadeInUp">
					<div class="col-md-12 text-center">
						<a href="section1.php" class="btn btn-rounded btn-outline-green mt-5"><iclass="fas fa-home"></i> Inizia!</a>
					</div>
				</div>
		<!-- fine Inizia -->

		</div>
      <!-- Content -->

    </div>
    <!-- Mask & flexbox options-->

  </div>
  <!-- Full Page Intro -->



  <!--  SCRIPTS  -->
  <!-- JQuery -->
  <script type="text/javascript" src="../../js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../../js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../../js/mdb.min.js"></script>
  <script>
    new WOW().init();

  </script>
</body>

</html>
