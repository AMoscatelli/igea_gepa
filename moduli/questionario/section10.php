<?php 
include('php/config.inc.php');
include('php/controller.php');

$_SESSION['currentSection'] = "10";

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Mondo Riabilitazione</title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
  
  <!-- Bootstrap core CSS -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../../css/mdb.min.css" rel="stylesheet">

<style>

.font_custom{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 180%;
}


ul.stepper {
    counter-reset: section;
    overflow-y: auto;
    overflow-x: hidden;
    margin: 0em 0rem;
    padding: 0rem;
}

footer.page-footer {
    position: fixed;
    width: 100%;
    bottom: 0;
    color: #fff;
    z-index: 2000;
    right: 0;
}

.primary-color, ul.stepper li.active a .circle, ul.stepper li.completed a .circle {
    background-color: #e8f5e9!important;
}


ul.stepper li a .circle {
    font-weight: bold;
    text-decoration: strong;
    display: inline-block;
    color: #009739;
    font-stretch: 21;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    /* background: rgba(0,0,0,.38); */
    width: 1.75rem;
    height: 1.75rem;
    text-align: center;
    line-height: 1.7rem;
    margin-right: .5rem;
}

.attivo{
    background-color: #009735!important;
}

div#smile {
       margin-top: 13%;
}

div#scritta {
       margin-top: 7%;
}

</style>

</head>

<body>

<!-- As a heading -->
<nav class="navbar navbar-dark green lighten-5">
  <span class="navbar-brand">
  	<img src="../../images/logo.png" class="img-fluid">
</span>
</nav>


<div class="container">

<div id="scritta" class="text-center"><p class="font_custom">Il personale amministrativo fornisce informazioni esaurienti ed &eacute; efficiente nel risolvere eventuali problemi?</div>

<div class="row" id="smile">
	<div class="col"></div>
	<div class="col-2 wow fadeInUp"><img src="../../images/verybad.png" class="img-fluid smile" value="1"></div>
	<div class="col-2 wow fadeInUp" data-wow-delay="0.2s"><img src="../../images/bad.png" class="img-fluid smile" value="2"></div>
	<div class="col-2 wow fadeInUp" data-wow-delay="0.4s"><img src="../../images/neutral.png" class="img-fluid smile" value="3"></div>
	<div class="col-2 wow fadeInUp" data-wow-delay="0.6s"><img src="../../images/good.png" class="img-fluid smile" value="4"></div>
	<div class="col-2 wow fadeInUp" data-wow-delay="0.8s"><img src="../../images/verygood.png" class="img-fluid smile" value="5"></div>
	<div class="col"></div>
	
</div>
</div>

<!-- Footer -->
<footer class="page-footer font-small green lighten-2">

<!-- Horizontal Steppers -->
<div class="row">
  <div class="col-md-12">

    <!-- Stepers Wrapper -->
    <ul class="stepper stepper-horizontal" id="stepper">

      <!-- First Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">1</span>
        </a>
      </li>

      <!-- Second Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">2</span>
        </a>
      </li>
      <!-- Third Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">3</span>
        </a>
      </li>
      
            <!-- First Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">4</span>
        </a>
      </li>

      <!-- Second Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">5</span>
        </a>
      </li>
      <!-- Third Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">6</span>
        </a>
      </li>
      
            <!-- First Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">7</span>
        </a>
      </li>

      <!-- Second Step -->
      <li class="completed ">
        <a href="#!">
          <span class="circle">8</span>
        </a>
      </li>
      <!-- Third Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">9</span>
        </a>
      </li>
      
            <!-- First Step -->
      <li class="completed attivo">
        <a href="#!">
          <span class="circle">10</span>
        </a>
      </li>
      <li class="completed">
        <a href="#!">
          <span class="circle">End</span>
        </a>
      </li>
    </ul>
    <!-- /.Stepers Wrapper -->

  </div>
</div>
<!-- /.Horizontal Steppers -->

</footer>
<!-- Footer -->

  <!--  SCRIPTS  -->
  <!-- JQuery -->
  <script type="text/javascript" src="../../js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../../js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../../js/mdb.min.js"></script>
  <script>
    new WOW().init();


    $('.smile' ).click(function() {
    	var foto = this.currentSrc.split("/");

    	var section10;

	    switch (foto[foto.length-1]) {
	    
	    case 'verybad.png': //Admin
        	section10 = "1";
            break;
        case 'bad.png': //Admin
        	section10 = "2"
            break;
        case 'neutral.png': //Admin
        	section10 = "3"
            break;
        case 'good.png': //Admin
        	section10 = "4"
            break;
        case 'verygood.png': //Admin
        	section10 = "5"
            break;
   		}

   		// scrivo in sessione il risultato del questinario della Section 1

    	$.ajax({
    		type : 'post',
    		url : 'php/controller.php',
    		data : {
    			'source' : 'salvaSection',
    			'sezione': '10',
    			'voto': section10
    		},
    		success : function(response) {
        			window.location.href = 'final.php';
    		},
    		error : function() {
    			toastr.info("C'� stato un errore nel sistema, si prega di riprovare!");
    		}
    	});
    	    	  
    	});

	setInterval(function(){ 
    	$.ajax({
    		type : 'post',
    		url : 'php/controller.php',
    		data : {
    			'source' : 'checkSession',
    		},
    		success : function(response) {
        		if(response=="KO"){
        			window.location.href = 'index.php';	
        		}
    		},
    		error : function() {
    			toastr.info("C'� stato un errore nel sistema, si prega di riprovare!");
    		}
    	});

    	 }, 10000);

  </script>	
</body>

</html>
