<?php 
include('php/config.inc.php');
include('php/controller.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Mondo Riabilitazione</title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
  
  <!-- Bootstrap core CSS -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../../css/mdb.min.css" rel="stylesheet">

<style>

.font_custom{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 300%;
}


ul.stepper {
    counter-reset: section;
    overflow-y: auto;
    overflow-x: hidden;
    margin: 0em 0rem;
    padding: 0rem;
}

footer.page-footer {
    position: fixed;
    width: 100%;
    bottom: 0;
    color: #fff;
    z-index: 2000;
    right: 0;
}

.primary-color, ul.stepper li.active a .circle, ul.stepper li.completed a .circle {
    background-color: #e8f5e9!important;
}


ul.stepper li a .circle {
    font-weight: bold;
    text-decoration: strong;
    display: inline-block;
    color: #009739;
    font-stretch: 21;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    /* background: rgba(0,0,0,.38); */
    width: 1.75rem;
    height: 1.75rem;
    text-align: center;
    line-height: 1.7rem;
    margin-right: .5rem;
}

.attivo{
    background-color: #009735!important;
}

div#smile {
       margin-top: 13%;
}

div#scritta {
       margin-top: 17%;
}

</style>

</head>

<body>

<!-- As a heading -->
<nav class="navbar navbar-dark green lighten-5">
  <span class="navbar-brand">
  	<img src="../../images/logo.png" class="img-fluid">
</span>
</nav>


<div class="container">

	<div id="scritta" class="text-center"><p class="font_custom">Grazie per il tuo contributo!!</div>
	
	<div id="logo" class="text-center wow fadeInUp"><img src="../../images/logo.png" class="img-fluid"></div>
</div>

<!-- Footer -->
<footer class="page-footer font-small green lighten-2">

<!-- Horizontal Steppers -->
<div class="row">
  <div class="col-md-12">

    <!-- Stepers Wrapper -->
    <ul class="stepper stepper-horizontal" id="stepper">

      <!-- First Step -->
      <li class="completed">
        <a href="#!">
          <span class="circle">1</span>
        </a>
      </li>

      <!-- Second Step -->
<!--       <li class="active"> -->
<!--         <a href="#!"> -->
<!--           <span class="circle">2</span> -->
<!--           <span class="label">Second step</span> -->
<!--         </a> -->
<!--       </li> -->
      <!-- Third Step -->
      <li class="completed attivo">
        <a href="#!">
          <span class="circle">End</span>
        </a>
      </li>

    </ul>
    <!-- /.Stepers Wrapper -->

  </div>
</div>
<!-- /.Horizontal Steppers -->

</footer>
<!-- Footer -->

  <!--  SCRIPTS  -->
  <!-- JQuery -->
  <script type="text/javascript" src="../../js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../../js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../../js/mdb.min.js"></script>
  <script>
    new WOW().init();

	$.ajax({
		type : 'post',
		url : 'php/controller.php',
		data : {
			'source' : 'inviaSondaggio',
		},
		success : function(response) {
    		
			setTimeout(function(){
				window.location.href = 'index.php'; 
			}, 3000);
		},
		error : function() {
			toastr.info("C'� stato un errore nel sistema, si prega di riprovare!");
		}
	});

  </script>	
</body>

</html>
