var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "nomeCognome",
			label : "Nome e Cognome",
			align : "left",
			width : 120

		}, {
			name : "OreSett",
			label : "Ore Settimanali",
			align : "left",
			width : 70
		} ],
		width : width,
		iconSet : "fontAwesome",
		idPrefix : "g1_",
		caption : 'Cerca',
		data : obj,
		rownumbers : true,
		sortname : "invdate",
		sortorder : "desc",
		caption : "Utility --> Amministrativi",
		onSelectRow : function(rowid) {
			rowData = $(this).jqGrid("getLocalRow", rowid);

			if (rowData_old == rowData) {// doppio Click
				AMMINISTRATIVImodifica();
			} else {// Primo Click
				showComponent('eliminaBtn');
				showComponent('modificaBtn');
				rowData_old = rowData;
			}
		}
	});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function AMMINISTRATIVImodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	MODALAmministrativiEditAddModal('modal', 'Amministrativi: '
			+ rowData.nomeCognome,
			'Clicca su Salva per aggiornare i dati della Amministrativi.',
			'AMMINISTRATIVIsalvaAmministrativi(2)', 'Salva e esci', 2);
	$('#nomeCognome').val(rowData.nomeCognome);
	$('#oreSett').val(rowData.OreSett);
	$('#id').val(rowData.Id);
	rowData_old = 0;
}

function AMMINISTRATIVIsalvaAmministrativi(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaAmministrativo",
			'tableName' : 'amministrativi',
			'Id' : $('#id').val(),
			'nomeCognome' : $('#nomeCognome').val(),
			'oreSett' : $('#oreSett').val(),
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function AMMINISTRATIVIeliminaAsk() {

	doModal('modal', 'ELIMINA ' + rowData.nomeCognome, 'Stai per eliminare '
			+ rowData.nomCognome, 'AMMINISTRATIVIeliminaAmministrativi()',
			'ELMINA', '$("#centralModalSuccess").modal("toggle");', 'Annulla',
			4)

}
function AMMINISTRATIVIeliminaAmministrativi() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaAmministrativo",
			'tableName' : 'amministrativi',
			'Id' : rowData.Id
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function AMMINISTRATIVINew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALAmministrativiEditAddModal('modal',
			'Aggiungi un nuovo Amministrativo:',
			'Clicca su Salva per aggiungere un nuovo Amministrativo.',
			'AMMINISTRATIVIsalvaAmministrativi(1)', 'Salva e esci', 2);

}
