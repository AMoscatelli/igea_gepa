var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "Nome_Cognome",
							label : "Nome Cognome",
							align : "center"
						}, {
							name : "Qualifica",
							label : "Qualifica",
							align : "center"
						}, {
							name : "Specializzazione",
							label : "Specializzazione",
							align : "center"
						}, {
							name : "OreSett",
							label : "Ore Settimanali",
							align : "center"
						} ],
						width : width,

						iconSet : "fontAwesome",
						idPrefix : "g1_",
						data : obj,
						caption : 'Cerca',
						rownumbers : true,
						sortname : "invdate",
						sortorder : "desc",
						caption : "Utility --> Medici",
						onSelectRow : function(rowid) {
							rowData = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowData) {// doppio Click
								hideComponent('eliminaBtn');
								hideComponent('modificaBtn');
								MODALmediciEditAddModal(
										'modal',
										'Medico: ' + rowData.Nome_Cognome,
										'Clicca su Salva per aggiornare i dati del Medico.',
										'MEDICIsalvaMedico(2)', 'Salva e esci',
										2);
								$('#nome').val(rowData.Nome_Cognome);
								$('#ore').val(rowData.OreSett);
								$('#specializzazione').val(
										rowData.Specializzazione);
								$('#qualifica').val(rowData.Qualifica);
								$('#id').val(rowData.Id);
								rowData_old = 0;
							} else {// Primo Click
								showComponent('eliminaBtn');
								showComponent('modificaBtn');
								rowData_old = rowData;
							}
						}
					});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function MEDICImodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	$("#jqGrid").jqGrid("resetSelection");
	MODALmediciEditAddModal('modal', 'Medico: ' + rowData.Nome_Cognome,
			'Clicca su Salva per aggiornare i dati del Medico.',
			'MEDICIsalvaMedico(2)', 'Salva e esci', 2);
	$('#nome').val(rowData.Nome_Cognome);
	$('#ore').val(rowData.OreSett);
	$('#specializzazione').val(rowData.Specializzazione);
	$('#qualifica').val(rowData.Qualifica);
	$('#id').val(rowData.Id);
	rowData_old = 0;
}

function MEDICIsalvaMedico(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaMedico",
			'tableName' : 'medici',
			'Id' : $('#id').val(),
			'Nome_Cognome' : $('#nome').val(),
			'OreSett' : $('#ore').val(),
			'Specializzazione' : $('#specializzazione').val(),
			'Qualifica' : $('#qualifica').val(),
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function MEDICIeliminaMedicoAsk() {

	doModal('modal', 'ELIMINA ' + rowData.Nome_Cognome,
			'Stai per eliminare il medico ' + rowData.Nome_Cognome,
			'MEDICIeliminaMedico()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function MEDICIeliminaMedico() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaMedico",
			'tableName' : 'medici',
			'Id' : rowData.Id
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function MEDICINew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALmediciEditAddModal('modal', 'Aggiungi nuovo medico:',
			'Clicca su Salva per aggiungere il nuovo medico.',
			'MEDICIsalvaMedico(1)', 'Salva e esci', 2);

}
