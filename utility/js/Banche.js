var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "Banca",
			label : "Banca",
			align : "left",
			width : 120

		}, {
			name : "Agenzia",
			label : "Agenzia",
			align : "left",
			width : 70
		}, {
			name : "Via",
			label : "Indirizzo",
			align : "left",
			width : 70
		}, {
			name : "Iban",
			label : "Pazienti Ambul.",
			align : "center"
		} ],
		width : width,

		iconSet : "fontAwesome",
		idPrefix : "g1_",
		caption : 'Cerca',
		data : obj,
		rownumbers : true,
		sortname : "invdate",
		sortorder : "desc",
		caption : "Utility --> Banche",
		onSelectRow : function(rowid) {
			rowData = $(this).jqGrid("getLocalRow", rowid);

			if (rowData_old == rowData) {// doppio Click
				BANCHEmodifica();
			} else {// Primo Click
				showComponent('eliminaBtn');
				showComponent('modificaBtn');
				rowData_old = rowData;
			}
		}
	});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function BANCHEmodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	MODALBancheEditAddModal('modal', 'Banca: ' + rowData.Banca,
			'Clicca su Salva per aggiornare i dati della Banca.',
			'BANCHEsalvaBanca(2)', 'Salva e esci', 2);
	$('#banca').val(rowData.Banca);
	$('#agenzia').val(rowData.Agenzia);
	$('#via').val(rowData.Via);
	$('#CAP').val(rowData.CAP);
	$('#citta').val(rowData.Citta);
	$('#iban').val(rowData.Iban);
	$('#scadenza').val(rowData.Scadenza);
	$('#id').val(rowData.Id);
	rowData_old = 0;
}

function BANCHEsalvaBanca(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaBanca",
			'tableName' : 'banche',
			'Id' : $('#id').val(),
			'banca' : $('#banca').val(),
			'agenzia' : $('#agenzia').val(),
			'via' : $('#via').val(),
			'CAP' : $('#CAP').val(),
			'citta' : $('#citta').val(),
			'iban' : $('#iban').val(),
			'scadenza' : $('#scadenza').val(),
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function BANCHEeliminaBancaAsk() {

	doModal('modal', 'ELIMINA ' + rowData.Banca, 'Stai per eliminare la Banca '
			+ rowData.Banca, 'BANCHEeliminaBanca()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function BANCHEeliminaBanca() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaBanca",
			'tableName' : 'banche',
			'Id' : rowData.Id
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function BANCHENew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALBancheEditAddModal('modal', 'Aggiungi una nuova Banca:',
			'Clicca su Salva per aggiungere la nuova Banca.',
			'BANCHEsalvaBanca(1)', 'Salva e esci', 2);

}
