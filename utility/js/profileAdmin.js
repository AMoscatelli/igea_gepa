var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [
								{
									name : "nome_cognome",
									label : "Nome Cognome",
									align : "left",
									width : 180

								},
								{
									name : "email",
									label : "Email",
									align : "left"
								},
								{
									name : "username",
									label : "Ore Ambul.",
									align : "center",
									width : 60
								},
								{
									name : "role",
									label : "Ruolo utente",
									align : "center",
									width : 60,
									formatter : formatUserRole
								},
								{
									name : "notifiche",
									label : "Invia notifiche",
									align : "center",
									width : 60,
									formatter : formatNotifiche
								},
								{
									name : "pwd",
									label : "Password",
									align : "center",
									width : 60,
									formatter : function() {
										return "<img src='../images/key.png' alt='change pwd' />";
									}
								} ],
						width : width,
						onCellSelect : function(rowId, iCol, content, event) {
							// YOUR STUFF
							if (iCol == 6) {
								rowData = $(this).jqGrid("getLocalRow", rowId);

								MODALPwdChange(
										'modal',
										'Utente: ' + rowData.nome_cognome,
										'Clicca su Salva per modificare la password dell\'utente.',
										'PROFILEsalva(2)', 'Salva e esci', 2);

							}

						},
						iconSet : "fontAwesome",
						idPrefix : "g1_",
						caption : 'Cerca',
						data : obj,
						rownumbers : true,
						sortname : "invdate",
						sortorder : "desc",
						caption : "Utility --> Super User",
						onSelectRow : function(rowid) {
							rowData = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowData) {// doppio Click
								hideComponent('eliminaBtn');
								hideComponent('modificaBtn');
								MODALPROFILEEditAddModal(
										'modal',
										'Utente: ' + rowData.nome_cognome,
										'Clicca su Salva per aggiornare i dati dell\'utente.',
										'PROFILEsalva(2)', 'Salva e esci', 2);
								$('#nome').val(rowData.nome_cognome);
								$('#email').val(rowData.email);
								$('#username').val(rowData.username);
								$("#role select").val(2);
								$('#notifiche select').val(2);
								rowData_old = 0;
							} else {// Primo Click
								showComponent('eliminaBtn');
								showComponent('modificaBtn');
								rowData_old = rowData;
							}
						}
					});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function formatUserRole(cellvalue, options, rowObject) {

	switch (cellvalue) {
	case 1:
		return "Super User";
		break;
	case 2:
		return "Amministrazione";
		break;
	case 3:
		return "Dottore";
		break;
	case 4:
		return "Terapista";
		break;
	case 5:
		return "Utente";
		break;
	}
}

function formatNotifiche(cellvalue, options, rowObject) {

	switch (cellvalue) {
	case 0:
		return "No";
		break;
	case 1:
		return "Si";
		break;
	}
}

function PROFILEmodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	$("#jqGrid").jqGrid("resetSelection");
	MODALPROFILEEditAddModal('modal', 'User: ' + rowData.Nominativo,
			'Clicca su Salva per aggiornare i dati dell\'utente.',
			'PROFILEsalva(2)', 'Salva e esci', 2);
	$('#nome').val(rowData.nome_cognome);
	$('#email').val(rowData.email);
	$('#username').val(rowData.username);
	$('#role').val(rowData.role);
	$('#notifiche').val(rowData.notifiche);
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
	rowData_old = 0;
}

function PROFILEsalva(action) {

	if ($('#password').val() != $('#password1').val()) {
		toastr.error('Le password inserite non coincidono!');
		$('#password1').val("")
	} else {

		$('#centralModalSuccess').modal('toggle');
		$
				.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : "salvaProfile",
						'tableName' : 'profile',
						'Id' : rowData.id,
						'nome_cognome' : $('#nome').val() == undefined ? rowData.nome_cognome
								: $('#nome').val(),
						'email' : $('#email').val() == undefined ? rowData.email
								: $('#nome').val(),
						'username' : $('#username').val() == undefined ? rowData.username
								: $('#username').val(),
						'role' : $('#role').val() == undefined ? rowData.role
								: $('#role').val(),
						'notifiche' : $('#notifiche').val() == undefined ? rowData.notifiche
								: $('#notifiche').val(),
						'password' : $('#password').val(),
						'action' : action,
					},
					success : function(response) {
						location.reload();
					},
					error : function() {
						toastr.error('Opps! Si è verifiato un problema!');
					}
				});
	}
}
function PROFILEeliminaAsk() {

	doModal('modal', 'ELIMINA ' + rowData.Nominativo,
			'Stai per eliminare l\'utente ' + rowData.nome_cognome,
			'PROFILEelimina()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function PROFILEelimina() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaProfile",
			'tableName' : 'profile',
			'Id' : rowData.id
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function PROFILENew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALPROFILEEditAddModal('modal', 'Aggiungi nuovo utente:',
			'Clicca su Salva per aggiungere il nuovo utente.',
			'PROFILEsalva(1)', 'Salva e esci', 2);
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();

}
