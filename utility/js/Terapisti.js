var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "Nominativo",
							label : "Nome Cognome",
							align : "left",
							width : 180

						}, {
							name : "Specializzazione",
							label : "Specializzazione",
							align : "left"
						}, {
							name : "OreAmb",
							label : "Ore Ambul.",
							align : "center",
							width : 60
						}, {
							name : "PazAmb",
							label : "Pazienti Ambul.",
							align : "center",
							width : 60
						}, {
							name : "OreDom",
							label : "Ore Domic.",
							align : "center",
							width : 60
						}, {
							name : "PazDom",
							label : "Pazienti Domic..",
							align : "center",
							width : 60
						} ],
						width : width,

						iconSet : "fontAwesome",
						idPrefix : "g1_",
						caption : 'Cerca',
						data : obj,
						rownumbers : true,
						sortname : "invdate",
						sortorder : "desc",
						caption : "Utility --> Terapisti",
						onSelectRow : function(rowid) {
							rowData = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowData) {// doppio Click
								hideComponent('eliminaBtn');
								hideComponent('modificaBtn');
								MODALterapistiEditAddModal(
										'modal',
										'Terapista: ' + rowData.Nominativo,
										'Clicca su Salva per aggiornare i dati del Terapista.',
										'TERAPISTIsalvaTerapista(2)',
										'Salva e esci', 2);
								$('#nome').val(rowData.Nominativo);
								$('#specializzazione').val(
										rowData.Specializzazione);
								$('#oreAmb').val(rowData.OreAmb);
								$('#pazAmb').val(rowData.PazAmb);
								$('#oreDom').val(rowData.OreDom);
								$('#pazDdom').val(rowData.PazDom);
								$('#id').val(rowData.Id);
								rowData_old = 0;
							} else {// Primo Click
								showComponent('eliminaBtn');
								showComponent('modificaBtn');
								rowData_old = rowData;
							}
						}
					});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function TERAPISTImodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	$("#jqGrid").jqGrid("resetSelection");
	MODALterapistiEditAddModal('modal', 'Terapista: ' + rowData.Nominativo,
			'Clicca su Salva per aggiornare i dati del Terapista.',
			'TERAPISTIsalvaTerapista(2)', 'Salva e esci', 2);
	$('#nome').val(rowData.Nominativo);
	$('#specializzazione').val(rowData.Specializzazione);
	$('#oreAmb').val(rowData.OreAmb);
	$('#pazAmb').val(rowData.PazAmb);
	$('#oreDom').val(rowData.OreDom);
	$('#pazDdom').val(rowData.PazDom);
	$('#id').val(rowData.Id);
	rowData_old = 0;
}

function TERAPISTIsalvaTerapista(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaTerapista",
			'tableName' : 'terapista',
			'Id' : $('#id').val(),
			'Nominativo' : $('#nome').val(),
			'Specializzazione' : $('#specializzazione').val(),
			'oreAmb' : $('#oreAmb').val(),
			'pazAmb' : $('#pazAmb').val(),
			'pazDom' : $('#pazDom').val(),
			'oreDom' : $('#oreDom').val(),
			'username' : $('#username').val(),
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function TERAPISTIeliminaTerapistaAsk() {

	doModal('modal', 'ELIMINA ' + rowData.Nominativo,
			'Stai per eliminare il terapista ' + rowData.Nominativo,
			'TERAPISTIeliminaTerapista()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function TERAPISTIeliminaTerapista() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaTerapista",
			'tableName' : 'terapista',
			'Id' : rowData.Id
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function TERAPISTINew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALterapistiEditAddModal('modal', 'Aggiungi nuovo Terapista:',
			'Clicca su Salva per aggiungere il nuovo del Terapista.',
			'TERAPISTIsalvaTerapista(1)', 'Salva e esci', 2);

}
