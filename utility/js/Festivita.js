var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "DataFestivita",
							label : "Data Festività",
							align : "left",
							width : 180,
							formatter : 'date',
							formatoptions : {
								srcformat : 'Y-m-d',
								newformat : 'd/m/Y'
							},

						}, {
							name : "Commento",
							label : "Nome Festvità",
							align : "left"
						} ],
						width : width,

						iconSet : "fontAwesome",
						idPrefix : "g1_",
						caption : 'Cerca',
						data : obj,
						rownumbers : true,
						sortname : "invdate",
						sortorder : "desc",
						caption : "Utility --> Terapisti",
						onSelectRow : function(rowid) {
							rowData = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowData) {// doppio Click
								hideComponent('eliminaBtn');
								hideComponent('modificaBtn');
								MODALfestivitaEditAddModal(
										'modal',
										'Festività: ' + rowData.Commento,
										'Clicca su Salva per aggiornare la data della festività.\nATTENZIONE: Questa operazione ricalcolera la data di fine di tutti i progetti che includono questa festività',
										'FESTIVITAsalvaFestivita(2)',
										'Salva e esci', 2);
								$('#festivita').val(rowData.DataFestivita);
								$('#commento').val(rowData.Commento);
								rowData_old = 0;
							} else {// Primo Click
								showComponent('eliminaBtn');
								showComponent('modificaBtn');
								rowData_old = rowData;
							}
						}
					});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function FESTIVITAmodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	$("#jqGrid").jqGrid("resetSelection");
	MODALfestivitaEditAddModal('modal', 'Festività: ' + rowData.Commento,
			'Clicca su Salva per aggiornare la data della festività.',
			'FESTIVITAsalvaFestivita(2)', 'Salva e esci', 2);
	$('#festivita').val(rowData.DataFestivita);
	$('#commento').val(rowData.Commento);
	rowData_old = 0;
}

function FESTIVITAsalvaFestivita(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaFestivita",
			'tableName' : 'festivita',
			'DataFestivita' : $('#festivita').val(),
			'Commento' : $('#commento').val(),
			'Id' : rowData.id,
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function FESTIVITAeliminaFestivitaAsk() {

	doModal('modal', 'ELIMINA ' + rowData.Commento,
			'Stai per eliminare la Festività ' + rowData.Commento + ' ('
					+ formatCustomHTMLDate(rowData.DataFestivita, "/") + ')',
			'FESTIVITAeliminaFestivita()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function FESTIVITAeliminaFestivita() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaFestivita",
			'tableName' : 'festivita',
			'Id' : rowData.id,
			'dataEliminata' : rowData.DataFestivita
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function FESTIVITANew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALfestivitaEditAddModal(
			'modal',
			'Aggiungi una Festività:',
			'Clicca su Salva per aggiornare la data della festività.\nATTENZIONE: Questa operazione ricalcolera la data di fine di tutti i progetti che includono questa festività',
			'FESTIVITAsalvaFestivita(1)', 'Salva e esci', 2);

}
