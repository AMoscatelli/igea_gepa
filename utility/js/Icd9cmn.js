var obj = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "codice",
							label : "Codice ICD9 ",
							align : "center"
						}, {
							name : "descrizione",
							label : "Descrizione",
							align : "left"
						} ],
						width : width,

						iconSet : "fontAwesome",
						idPrefix : "g1_",
						data : obj,
						caption : 'Cerca',
						rownumbers : true,
						sortname : "invdate",
						sortorder : "desc",
						caption : "Utility --> Icd9CmN",
						onSelectRow : function(rowid) {
							rowData = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowData) {// doppio Click
								hideComponent('eliminaBtn');
								hideComponent('modificaBtn');
								MODALicd9cmnEditAddModal(
										'modal',
										'Codice ICD9: ' + rowData.codice,
										'Clicca su Salva per aggiornare il codice ICD9.',
										'ICD9CMNsalvaIcd9cmn(2)',
										'Salva e esci', 2);
								$('#codice').val(rowData.codice);
								$('#descrizione').val(rowData.descrizione);
								rowData_old = 0;
							} else {// Primo Click
								showComponent('eliminaBtn');
								showComponent('modificaBtn');
								rowData_old = rowData;
							}
						}
					});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function ICD9CMNmodifica() {
	hideComponent('eliminaBtn');
	hideComponent('modificaBtn');
	$("#jqGrid").jqGrid("resetSelection");
	MODALicd9cmnEditAddModal('modal', 'Codice ICD9: ' + rowData.codice,
			'Clicca su Salva per aggiornare il codice ICD9.',
			'ICD9CMNsalvaIcd9cmn(2)', 'Salva e esci', 2);
	$('#codice').val(rowData.codice);
	$('#descrizione').val(rowData.descrizione);
	rowData_old = 0;
}

function ICD9CMNsalvaIcd9cmn(action) {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "salvaIcd9cmn",
			'tableName' : 'icd9cmn',
			'Id' : rowData.ID1,
			'codice' : $('#codice').val(),
			'descrizione' : $('#descrizione').val(),
			'action' : action,

		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}
function ICD9CMNeliminaIcd9cmnAsk() {

	doModal('modal', 'ELIMINA Cod. ' + rowData.codice,
			'Stai per eliminare il codice ICD9 ' + rowData.codice,
			'ICD9CMNeliminaIcd9cmn()', 'ELMINA',
			'$("#centralModalSuccess").modal("toggle");', 'Annulla', 4)

}
function ICD9CMNeliminaIcd9cmn() {
	$('#centralModalSuccess').modal('toggle');
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "eliminaIcd9cmn",
			'tableName' : 'icd9cmn',
			'Id' : rowData.ID1
		},
		success : function(response) {
			location.reload();
		},
		error : function() {
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

}

function ICD9CMNNew() {
	$("#jqGrid").jqGrid("resetSelection");
	MODALicd9cmnEditAddModal('modal', 'Aggiungi nuovo codice ICD9:',
			'Clicca su Salva per aggiungere il nuovo codice ICD9.',
			'ICD9CMNsalvaIcd9cmn(1)', 'Salva e esci', 2);

}
