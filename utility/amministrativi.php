<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include('../php/config.inc.php');
include ('../php/controller.php');

?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - Utility - Amministrativi</title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- JqGrid Css -->
<link rel="stylesheet" href="../css/jquery-ui.min.css"> <!-- https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css -->
<link rel="stylesheet" href="../css/ui.jqgrid.min.css"> <!-- https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css -->
<script type="text/javascript">
	$.LoadingOverlay("show");
</script>
</head>


<body>

	<!--Main Navigation-->
	<header>
	    <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
    <!--/Main Navigation-->
	<!--Main Layout-->
	<main>

	<div class="container">

		<!--Section: Title-->
		<section class="pb-3 text-center ">
			<!--Section heading-->
			<h1 class="h1 py-1">Utility - Amministrativi</h1>
			<!--Section description-->
			<p class="grey-text mb-1">Manutenzione Tabella</p>
		</section>
		<!--/Section Title-->

		<!--Section: tool-->
		<section class="mb-4 pt-5 wow fadeIn" data-wow-delay="0.3s">
		
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<form method="post">
						<div class="row mt-4 text-center" id="row-lenght">
							<div class="col-md-3">
								<!-- <button type="button" class="btn btn-default" id="indietroBtn"    onclick=""><i class="fa fa-edit left"> INDIETRO</i></button> -->
							</div>
							<div class="col-md-12">
								<button type="button" class="btn btn-default" id="modificaBtn" 	onclick="AMMINISTRATIVImodifica();"><i class="fa fa-edit left"> MODIFICA</i></button>
								<button type="button" class="btn btn-default" id="eliminaBtn" 	onclick="AMMINISTRATIVIeliminaAsk();"><i class="fa fa-trash left"> ELIMINA</i></button>
								<button type="button" class="btn btn-default" id="nuovoBtn" 	onclick="AMMINISTRATIVINew();"><i class="fa fa-plus left"> NUOVA</i></button>
							</div>
						</div>
					</form>
				</div>
				<!--Post data-->
			</div>
		
		</section>
		<!--/Section: tool-->
		<!--Section: grid-->
		<section class="mb-4 pt-5 wow fadeIn" data-wow-delay="0.3s">
		
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<table id="jqGrid"></table>
				</div>
				<!--Post data-->
			</div>
		
		</section>
		<!--/Section: grid-->

	</div>
	
	</main>
	<!--Main Layout-->
	<div id="modal"></div>
</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- JQGRDI core files -->
<script	src="../js/jquery.jqgrid.min.js"></script> <!--  https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js-->
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>

<script type="text/javascript" src="js/Amministrativi.js"></script>
<script type="text/javascript" src="../jsCustom/modal.js"></script>	
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>

<!-- Menu Navigazione -->
    <script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>

<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<script>
		
		//initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>');
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		var obj = <?php echo controllerMethod('selectAll','amministrativi');?>;
		JQGRIDinitGrid(obj, '#jqGrid', $("#row-lenght").innerWidth()-30);
        new WOW().init();  
        $(document).ready(function () {
        	$('.collapsible-body').css({
        		"display" : "none"
        	});
        	$('#utilityMenuBody').css({
        		"display" : "block"
        	});
        	$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');
        	$.LoadingOverlay("hide");  	
        });
        hideComponent('eliminaBtn');
        hideComponent('modificaBtn');      
    </script>

</html>
































