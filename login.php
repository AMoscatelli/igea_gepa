<?php
   include("php/config.inc.php");
   include("php/controller.php");
//   include 'php/session.php';
   session_start();
   
   $login_error = 9;
   
   if(isset($_GET['result'])){
       $login_error = $_GET['result'];
   }
   if(isset($_SESSION['login_user'])){
       header("location: ".$baseurl."/index.php?result=1");//session still valid. go to Index.
       exit;
   }
   controller();
   
?>

<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Igea - Login page</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <style>

        .intro-2 {
            background: url("images/bg.jpg")no-repeat center center;
            background-size: cover;
        }
        .top-nav-collapse {
            background-color: #3f51b5 !important;
        }
        .navbar:not(.top-nav-collapse) {
            background: transparent !important;
        }
        @media (max-width: 768px) {
            .navbar:not(.top-nav-collapse) {
                background: #3f51b5 !important;
            }
        }

        .card {
            background-color: rgba(229, 228, 255, 0.2);
        }
        .md-form label {
            color: #ffffff;
        }
        h6 {
            line-height: 1.7;
        }
        @media (max-width: 740px) {
            .full-height,
            .full-height body,
            .full-height header,
            .full-height header .view {
                height: 750px;
            }
        }
        @media (min-width: 741px) and (max-height: 638px) {
            .full-height,
            .full-height body,
            .full-height header,
            .full-height header .view {
                height: 750px;
            }
        }

        .card {
            margin-top: 30px;
            /*margin-bottom: -45px;*/

        }

        .md-form input[type=text]:focus:not([readonly]),
        .md-form input[type=password]:focus:not([readonly]) {
            border-bottom: 1px solid #8EDEF8;
            box-shadow: 0 1px 0 0 #8EDEF8;
        }
        .md-form input[type=text]:focus:not([readonly])+label,
        .md-form input[type=password]:focus:not([readonly])+label {
            color: #8EDEF8;
        }

        .md-form .form-control {
            color: #fff;
        }

    </style>

</head>

<body>
	

    <!--Main Navigation-->
    <header>
       <!--Intro Section-->
        <section class="view intro-2">
            <div class="full-bg-img">
              <div class="mask rgba-stylish-strong flex-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-lg-5">
							<form action="" method="post">
                            <!--Form with header-->
                            <div class="card wow fadeIn" data-wow-delay="0.3s">
                                <div class="card-body">

                                    <!--Header-->
                                    <div class="form-header blue-gradient">
                                        <h3><i class="fa fa-user mt-2 mb-2"></i> IGEA - Accesso:</h3>
                                    </div>

                                    <!--Body-->
                                    <div class="md-form">
                                        <i class="fa fa-user prefix white-text"></i>
                                        <input type="text" id="username" name="username" class="form-control">
                                        <label for="orangeForm-name">Utente</label>
                                    </div>
                                    <div class="md-form">
                                        <i class="fa fa-lock prefix white-text"></i>
                                        <input type="password" id="password" name="password" class="form-control">
                                        <label for="orangeForm-email">Password</label>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" name="source" value="login" class="btn primary-color-dark btn-lg">Entra</button>
                                        <hr>
                                        <div class="inline-ul text-center d-flex justify-content-center">
                                            <a class="p-2 m-2 fa-lg tw-ic"><i class="fa fa-twitter white-text"></i></a>
                                            <a class="p-2 m-2 fa-lg li-ic"><i class="fa fa-linkedin white-text"> </i></a>
                                            <a class="p-2 m-2 fa-lg ins-ic"><i class="fa fa-instagram white-text"> </i></a>
                                        </div>
                                        <div id="notification" style="display: none">
                                        	<p class="white-text" id="notification_text">default Value</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--/Form with header-->
							</form>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </section>

    </header>
    <!--Main Navigation-->


    <!--  SCRIPTS  -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script type="text/javascript" src="jsCustom/htmlTool.js"></script>
    <script>
        new WOW().init();
        var result = <?php echo $login_error; ?>;
        if(result === 0){
        	showComponent('notification');
        	setComponentText('notification_text', 'Utente o Password errati!')
        }
        if(result === 1){
        	showComponent('notification');
        	setComponentText('notification_text', 'Sessione scaduta. Esegui il Login!')
        }
        if(result === 2){
        	showComponent('notification');
        	setComponentText('notification_text', 'Disconnessione avvenuta con Successo!')
        	toastr.success('Disconnessione avvenuta con Successo!');
        }  
    </script>
</body>
</html>
