/* includere nella pagina php:
 * include 'config.inc.php';
 * 
 * includere nella pagina php lato html:
	<!--Main Navigation-->
	<header>
	    <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>
	</header>
    <!--/Main Navigation--> 
 * 
 * lato JS:
 * initNavBar('<?php echo $baseurl;?>');
 */

function initNavBar(path, version, role) {

	var html = '<!-- Navigation Bar Start -->';
	html += '<div class="container">';
	html += '<a class="navbar-brand active" href="'
			+ path
			+ '"><i class="fa fa-home"></i><strong> IGEA - <span id="small-font">Ver. '
			+ version
			+ '</span></strong><span class="sr-only">(current)</span></a>';
	html += '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">';
	html += '<span class="navbar-toggler-icon"></span>';
	html += '</button>';
	html += '<div class="collapse navbar-collapse" id="navbarSupportedContent">';
	html += '<ul class="navbar-nav mr-auto">';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/anagrafica/anagrafica.php"><i class="fa fa-users"aria-hidden="true"></i> Anagrafica</a>';
	html += '</li>';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/progetto/progetto_sezA.php?result=1"><i class="fa fa-book" aria-hidden="true"></i> Progetti</a>';
	html += '</li>';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/contabilita/contabilita.php"><i class="fa fa-calculator" aria-hidden="true"></i> Contabilita</a>';
	html += '</li>';
	html += '<li class="nav-item dropdown">';
	html += '<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	html += '<i class="fa fa-th-list"></i> Liste attesa</a>';
	html += '<div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/lista_attesa/lista_attesa.php">Lista attesa</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/lista_attesa/lista_attesa.php?child=true">NPI</a>';
	html += '</div>';
	html += '</li>';
	html += '<li class="nav-item dropdown">';
	html += '<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	html += '<i class="fa fa-print"></i> Stampe</a>';
	html += '<div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/cartClinic/cartClinica.php">Cartella clinica</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/catrellaClin/sezioniSiar.php">Sezioni SIAR</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/catrellaClin/scadMensili.php">Scadenze mensili</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="'
			+ path
			+ '/catrellaClin/scadMensiliAttesa.php">Scadenze mensili attesa</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="'
			+ path
			+ '/catrellaClin/ctrlMensProgetti.php">Controllo mensile Progetti</a>';
	html += '</div>';
	html += '</li>';
	html += '<li class="nav-item dropdown">';
	html += '<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	html += '<i class="fa fa-print"></i> SIAR</a>';
	html += '<div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">Codifica SIAR</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">Elaborazioni SIAR</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">SIAR Anag. Apertura</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">SIAR Clinic. Apertura</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">SIAR Clinic. Chiusura</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/SIAR/cartellaClinica.php">SIAR Clinic. Aggiornamento</a>';
	html += '</div>';
	html += '</li>';
	html += '<li class="nav-item dropdown">';
	html += '<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	html += '<i class="fa fa-id-card-o" aria-hidden="true"></i> Utility</a>';
	html += '<div class="dropdown-menu dropdown-menu-right dropdown-info custom-width" aria-labelledby="navbarDropdownMenuLink-4">';
	html += '<div class="container">';
	html += '<div class="row">';
	html += '<div class="col-md-5 offset-md-1">';
	if (role == 1) {
		html += '<a class="dropdown-item waves-effect waves-light" href="'
				+ path + '/utility/profileAdmin.php">Utenti Amministratore</a>';
	}
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/utility/medici.php">Medici</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/utility/terapisti.php">Terapista</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Terapista Paz. Privati</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Terapista ORE</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Terapista ORE Per DATA</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/utility/amministrativi.php">Amministrativi</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Clienti Privati</a>';
	html += '<a class="dropdown-item waves-effect waves-light"  href="' + path
			+ '/utility/banche.php">Banche</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Codici ISTAT</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Area</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Operatori</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Strutture Provenienza</a>';
	html += '</div>';
	html += '<div class="col-md-5">';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/utility/icd9cmn.php">ICD9-CM</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Capitoli</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Main</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Gravita Compromissione</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Gravita Limitazione</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Gravita Alterazione</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Assistenza Richiesta</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Barriera</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">ICF Sub Facilitatore</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/utility/festivita.php">Festivita</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="#"><span class="badge indigo">Rette</a>';
	html += '</div>';
	html += '</div>';
	html += ' </div>';
	html += '</div>';
	html += '</li>';
	html += '</ul>';
	html += '<ul class="navbar-nav nav-flex-icons">';
	html += '<!-- <li class="nav-item vertical_line mr-1 ml-1"></li>  -->';
	html += '<li class="nav-item dropdown">';
	html += '<a class="nav-link dropdown-toggle waves-effect waves-light" id="loginUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	html += '<i class="fa fa-user"></i> Benvenuto </a>';
	html += '<div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">';
	html += '<a class="dropdown-item waves-effect waves-light" href="#">My account</a>';
	html += '<a class="dropdown-item waves-effect waves-light" href="' + path
			+ '/logout.php">Logout</a>';
	html += '</div>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</div>';
	html += '<!-- Navigation Bar End-->';
	$("#navBar").html(html);
}
