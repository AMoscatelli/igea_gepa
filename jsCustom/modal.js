function doModal(elementId, headTitle, formContent, jsFunction, btnText,
		jsNOFunction, btnNOText, typeMsg, customIcon) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';
	var icon = '<i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
		icon = '<i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
		icon = '<i class="fa fa-info fa-4x mb-3 animated rotateIn"></i>';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
		icon = '<i class="fa fa-warning fa-4x mb-3 animated rotateIn"></i>';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
		icon = '<i class="fa fa-exclamation-triangle fa-4x mb-3 animated rotateIn"></i>';
	}
	if (customIcon !== undefined) {
		icon = '<i class="fa ' + customIcon
				+ ' fa-4x mb-3 animated rotateIn"></i>';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += icon;
	html += '<p>' + formContent + '</p>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" onclick="' + jsNOFunction
			+ '" data-dismiss="modal">' + btnNOText + '</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show');
}

function MODALmediciEditAddModal(elementId, headTitle, formContent, jsFunction,
		btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-8">';
	html += '<label for="nome">Nome Cognome</label> ';
	html += '<input type="text" class="form-control text-center" id="nome" name="nome"></input>';
	html += '</div>';
	html += '<div class="col-4">';
	html += '<label for="ore">Ore</label> ';
	html += '<input type="number" class="form-control text-center" id="ore" name="ore"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<label for="qualifica">Qualifica:</label> ';
	html += '<input type="text" class="form-control text-center" id="qualifica" name="qualifica"></input>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<label for="specializzazione">Specializzazione:</label> ';
	html += '<input type="text" class="form-control text-center" id="specializzazione" name="specializzazione"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function MODALicd9cmnEditAddModal(elementId, headTitle, formContent,
		jsFunction, btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-12">';
	html += '<label for="codice">Codice</label> ';
	html += '<input type="text" class="form-control text-center" id="codice" name="codice"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-12">';
	html += '<label for="descrizione">Descrizione:</label> ';
	html += '<input type="text" class="form-control text-center" id="descrizione" name="descrizione"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function MODALfestivitaEditAddModal(elementId, headTitle, formContent,
		jsFunction, btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-gift fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="festivita">Festività;</label> ';
	html += '<input type="date" class="form-control  placeholder="DD/MM/YYYY" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4} text-center" id="festivita" name="festivita"></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="commento">Nome della Festivit&agrave;</label> ';
	html += '<input type="text" class="form-control text-center" id="commento" name="commento"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function MODALterapistiEditAddModal(elementId, headTitle, formContent,
		jsFunction, btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="nome">Nome Cognome</label> ';
	html += '<input type="text" class="form-control text-center" id="nome" name="nome"></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="username">Username:</label> ';
	html += '<input type="text" class="form-control text-center" id="username" name="username"></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="specializzazione">Specializzazione</label> ';
	html += '<input type="text" class="form-control text-center" id="specializzazione" name="specializzazione"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-6">';
	html += '<label for="oreAmb">Ore Ambul.:</label> ';
	html += '<input type="number" class="form-control text-center" id="oreAmb" name="oreAmb"></input>';
	html += '</div>';
	html += '<div class="col-6">';
	html += '<label for="pazAmb">Paz. Ambul.:</label> ';
	html += '<input type="number" class="form-control text-center" id="pazAmb" name="pazAmb"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-6">';
	html += '<label for="oreDom">Ore Domic.:</label> ';
	html += '<input type="number" class="form-control text-center" id="oreDom" name="oreDom"></input>';
	html += '</div>';
	html += '<div class="col-6">';
	html += '<label for="pazDom">Paz. Domic.:</label> ';
	html += '<input type="number" class="form-control text-center" id="pazDom" name="pazDom"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function MODALBancheEditAddModal(elementId, headTitle, formContent, jsFunction,
		btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-8">';
	html += '<label for="banca">Banca</label> ';
	html += '<input type="text" class="form-control text-center" id="banca" name="banca"></input>';
	html += '</div>';
	html += '<div class="col-4">';
	html += '<label for="agenzia">Agenzia:</label> ';
	html += '<input type="text" class="form-control text-center" id="agenzia" name="agenzia"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-12">';
	html += '<label for="via">Indirizzo:</label> ';
	html += '<input type="text" class="form-control text-center" id="via" name="via"></input>';
	html += '</div>';
	html += '<div class="col-6">';
	html += '<label for="CAP">CAP:</label> ';
	html += '<input type="number" class="form-control text-center" id="CAP" name="CAP"></input>';
	html += '</div>';
	html += '<div class="col-6">';
	html += '<label for="citta">Citta:</label> ';
	html += '<input type="text" class="form-control text-center" id="citta" name="citta"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-12">';
	html += '<label for="iban">Iban:</label> ';
	html += '<input type="text" class="form-control text-center" id="iban" name="iban"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function MODALAmministrativiEditAddModal(elementId, headTitle, formContent,
		jsFunction, btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="nomeCognome">Nome e Cognome</label> ';
	html += '<input type="text" class="form-control text-center" id="nomeCognome" name="nomeCognome"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-12">';
	html += '<label for="oreSett">Ore Settimanali:</label> ';
	html += '<input type="text" class="form-control text-center" id="oreSett" name="oreSett"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);
	$('#centralModalSuccess').modal('show')
}

function hideModal() {
	// Using a very general selector - this is because $('#modalDiv').hide
	// will remove the modal window but not the mask
	$('.modal.in').modal('hide');
}

function MODALPROFILEEditAddModal(elementId, headTitle, formContent,
		jsFunction, btnText, typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="nome">Nome Cognome:</label> ';
	html += '<input type="text" class="form-control text-center" id="nome" name="nome"></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="email">Indirizzo Email:</label> ';
	html += '<input type="email" class="form-control text-center" id="email" name="email"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-12">';
	html += '<label for="username">Username:</label> ';
	html += '<input type="text" class="form-control text-center" id="username" name="username"></input>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row mt-2 mr-2 ml-2">';
	html += '<div class="col-6">';
	html += '<label for="notifiche">Ricevi Notifche:</label> ';
	html += '<select class="mdb-select colorful-select dropdown-primary "	id="notifiche" name="notifiche" style="width: 100%">';
	html += '<option value="1" selected>Si</option>';
	html += '<option value="0">No</option>';
	html += '</select>';
	html += '</div>';
	html += '<div class="col-6">';
	html += '<label for="role">Ruolo:</label> ';
	html += '<select class="mdb-select colorful-select dropdown-primary"	id="role" name="role" style="width: 100%">';
	html += '<option value="5" selected>Utente</option>';
	html += '<option value="4">Terapista</option>';
	html += '<option value="3">Dottore</option>';
	html += '<option value="2">Amministrazione</option>';
	html += '<option value="1">Super User</option>';
	html += '</select>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);

	$('#centralModalSuccess').modal('show')

}

function MODALPwdChange(elementId, headTitle, formContent, jsFunction, btnText,
		typeMsg) {

	var type = 'modal-success';
	var btnType = 'btn-success';
	var btnOutline = 'btn-outline-success';

	if (typeMsg == 1) {
		type = 'modal-success';
		btnType = 'btn-success';
		btnOutline = 'btn-outline-success';
	} else if (typeMsg == 2) {
		type = 'modal-info';
		btnType = 'btn-info';
		btnOutline = 'btn-outline-info';
	} else if (typeMsg == 3) {
		type = 'modal-warning';
		btnType = 'btn-warning';
		btnOutline = 'btn-outline-warning';
	} else if (typeMsg == 4) {
		type = 'modal-danger';
		btnType = 'btn-danger';
		btnOutline = 'btn-outline-danger';
	}

	var html = '  <!-- Central Modal Medium Success -->';
	html += '<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify ' + type
			+ '" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead">' + headTitle + '</p>';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';
	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	html += '<i class="fa fa-user fa-4x mb-3 animated rotateIn"></i>';
	html += '<div class="col-12">';
	html += '<div class="row text-center mr-2 ml-2">';
	html += '<p class="card-text">';
	html += '<strong>' + formContent + '</strong>';
	html += '</p>';
	html += '</div>';
	html += '<div class="row mt-2  mr-1 ml-1">';
	html += '<div class="col-2" style="display:none">';
	html += '<label for="nome">Id</label> ';
	html += '<input type="text" class="form-control text-center" id="id" name="id" disabled></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="password">Inserisci la nuova password:</label> ';
	html += '<input type="password" class="form-control text-center" id="password" name="password"></input>';
	html += '</div>';
	html += '<div class="col-12">';
	html += '<label for="password1">Reinserisci la password:</label> ';
	html += '<input type="password" class="form-control text-center" id="password1" name="password1"></input>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" onclick="' + jsFunction + '" class="btn '
			+ btnType + '">' + btnText
	'+<i class="fa fa-diamond ml-1"></i></a>';
	html += '<a type="button" class="btn ' + btnOutline
			+ ' waves-effect" data-dismiss="modal">Chiudi</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Success-->';
	$("#" + elementId).html(html);

	$('#centralModalSuccess').modal('show')

}
