function DATEUTILgetAnno() {
	return (new Date()).getFullYear()
}
function DATEUTILgetMese() {
	return (new Date()).getMonth() + 1
}
function DATEUTILgetDateNow(millisecond=false) {
	if(millisecond){
		return new Date().getTime();
	}else{
		return (new Date());
	}
}


// funzione per calcolare giorni lavorativi
// console.log(moment("STRINGA_DATA").addWorkdays("NUMERO DI
// GIORNI","ARRAY_FESTIVITA'").format('DD-MM-YYYY'));
moment.fn.addWorkdays = function(days, festivita, show) {
	// definisco le variabili che conteggiano il numero di domeniche e le
	// festività
	var domeniche = 0;
	var feste = 0;

	// var feste_nazionali = [ "2018-03-01", "2018-03-04","2018-03-07" ];
	var feste_nazionali = festivita;
	//
	//
	// var festa = moment(festivita);
	var increment = days / Math.abs(days);
	var date = this.clone();// .add(Math.floor(Math.abs(days) / 6) * 7 *
	// increment,'days');
	var remaining = days-1;// % 6;
	while (remaining != 0) {
		date.add(increment, 'days');
		//console.log(date.format('YYYY-MM-DD'));
		if (date.isoWeekday() !== 7) {

			for (i = 0; i < feste_nazionali.length; i++) {

				var festa = moment(feste_nazionali[i]);
				var chiusura = false;
				if (date.format('YYYY-MM-DD') !== festa.format('YYYY-MM-DD')) {

				} else {
					feste = feste + 1;
					chiusura = true;
					break;
				}
			}

			if (!chiusura) {
				remaining -= increment;
			}

		} else {

			for (i = 0; i < feste_nazionali.length; i++) {

				var festa = moment(feste_nazionali[i]);
				var chiusura = false;
				if (date.format('YYYY-MM-DD') !== festa.format('YYYY-MM-DD')) {

				} else {
					feste = feste + 1;
					chiusura = true;
					break;
				}
			}

			if (!chiusura) {
				domeniche = domeniche + 1;
			}

		}
	}
	// }
	if (show == true) {
		$('#domenica').val(domeniche);
		$('#feste').val(feste);
	} 
	return date;
};

function formatISODate(val) {
	if(val!=null){
		val = val.split("/");
	
		var val1 = val[2] + "-" + val[1] + "-" + val[0];
	
		return val1;
	}
}



function formatCustomHTMLDate(val, split) {
	
	if(val!=null){
		if (val.includes("-")) {
			val = val.split("-");
	
			if (val[2].length > 2) {
				val[2] = val[2].substring(0, 2);
			}
	
			var val1 = val[2] + split + val[1] + split + val[0];
	
			return val1;
	
		} else {
	
			return val;
		}
	}
}

function getNomeMese(mese) {
	if (mese == "1") {
		return 'Gennaio';
	}
	if (mese == "2") {
		return 'Febbraio';
	}
	if (mese == "3") {
		return 'Marzo';
	}
	if (mese == "4") {
		return 'Aprile';
	}
	if (mese == "5") {
		return 'Maggio';
	}
	if (mese == "6") {
		return 'Giugno';
	}
	if (mese == "7") {
		return 'Luglio';
	}
	if (mese == "8") {
		return 'Agosto';
	}
	if (mese == "9") {
		return 'Settembre';
	}
	if (mese == "10") {
		return 'Ottobre';
	}
	if (mese == "11") {
		return 'Novembre';
	}
	if (mese == "12") {
		return 'Dicembre';
	}
}

function setDataEta(dataNascitaID, inputID) {
	// format data di nascita
	var old_date = formatISODate($('#' + dataNascitaID).val());

	// ricava età
	var birthDate = new Date(old_date);
	var ageDifMs = Date.now() - birthDate.getTime();
	var ageDate = new Date(ageDifMs); // miliseconds from epoch
	var eta = Math.abs(ageDate.getUTCFullYear() - 1970);
	$('#' + inputID).val(eta);
	return parseInt(eta);

}

function AdultoMinore(eta, inputID) {

	if (eta < 18) {
		$('#' + inputID).val('MINORE');
	} else {
		$('#' + inputID).val('ADULTO');
	}

}
