function JQGRIDexportGrid(data, div, fileName){
	
	$("#"+div).excelexportjs({
		
		  containerid: "dvjson",
		
		  datatype: 'json',
		
		  dataset: data,
		  
		  fileName: fileName,
		
		  columns: getColumns(data)    
		
		});

	
}