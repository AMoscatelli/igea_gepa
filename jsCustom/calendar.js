var calendar;

var ciccio = false;

function initCalendar(calendarEl){
		calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
        themeSystem: 'bootstrap',
        locale: 'it',
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        defaultDate: '2019-03-12',
        weekNumbers: true,
        navLinks: true, // can click day/week names to navigate views
        editable: true,    
        selectable: true,
        selectMirror: true,
        select: function(arg) {
            
        	//$('#date_event').val(formatCustomHTMLDate(moment(arg.start).format(), '/')).change;

        	if(ciccio==false){
        	    ciccio = true;
        	    //showEvent(calendar);
            }

          },
        eventLimit: true, // allow "more" link when too many events
        events: [
          {
            title: 'All Day Event',
            start: '2019-03-01'
          },
          {
            title: 'Long Event',
            start: '2019-03-07',
            end: '2019-03-10'
          }
        ]
      });

	calendar.render();
       
}

function mostraPazienti(){
	hideComponent('event_div');
	showComponent('lista_pazienti');
}


function showEvent(input,output){
	$( "#calendar_div" ).animate({
	    width: "60%"
	  }, 
	  {
		duration: 500,
		specialEasing: {
		      width: "linear"
		},
		complete: function() {
		    	calendar.render();
		}
		});
	
	showComponent('lista_pazienti');
		
		$( "#lista_pazienti" ).animate({
		    width: "37%"
		  }, 
		  {
			duration: 500,
			specialEasing: {
			      width: "linear"
			},
			complete: function() {

			}
		});
}


function hideEvent(calendarEl){
	
	$('#giorni_event').val("");
	$('#frequenza_event').val("0");
	
	$('#surname_event').val("");
	$('#name_event').val("");
	
	hideComponent('calendar_div');
	
	$( "#calendar_div" ).animate({
	    width: "100%",
	    height: "100%"
	  }, 
	  {
		duration: 500,
		specialEasing: {
		      width: "linear"
		},
		complete: function() {
				
		    	calendar.destroy();
		    	initCalendar(calendarEl);
		    	
		    	
		}
		});
	
	hideComponent('event_div');
	showComponent('calendar_div');

	ciccio = false;
}