/**
 * 
 */
function showComponent(componentID) {

	$("#" + componentID).show();

}

function hideComponent(componentID) {

	$("#" + componentID).hide();

}

function setComponentText(componentID, text) {

	$("#" + componentID).html(text);

}
function HTMLTOOLnumberOnly(element) {
	$('#' + element).keyup(function() {
		if (!isANumber($('#' + element).val(), true)) {
			$('#' + element).css('color', 'red');
			toastr.error('Non è stato inserito un numero valido!');
		} else {
			$('#' + element).css('color', 'black');
		}
	});
}

function HTMLTOOLnumberOnlyClass(classe) {

	$('.' + classe).each(function() {
		var element = this.id;

		$('#' + element).keyup(function() {
			if (!isANumber($('#' + element).val(), true)) {
				$('#' + element).css('color', 'red');
				toastr.error('Non è stato inserito un numero valido!');
			} else {
				$('#' + element).css('color', 'black');
			}
		});

	});
}


function HTMLTOOLvalidateTextNumber(sectionID) {
	$('#' + sectionID + ' input')
			.each(
					function() {
						if (this.id !== "") {
							$('#' + this.id)
									.keyup(
											function(e) {
												// controllo le input di tipo
												// TEXT
												if ($('#' + this.id).attr(
														'type') == "text") {
													if (($('#' + this.id).val())
															.includes("'")
															|| e.keyCode == 220) {
														$('#' + this.id).css(
																'color', 'red');
														toastr
																.error('Non è stato inserito un testo valido!');
													} else {
														$('#' + this.id).css(
																'color',
																'black');
													}
													// controllo le input di
													// tipo NUMBER
												} else if ($('#' + this.id)
														.attr('type') == "number") {
													if (!isANumber($(
															'#' + this.id)
															.val(), true)) {
														$('#' + this.id).css(
																'color', 'red');
														toastr
																.error('Non è stato inserito un testo valido!');
													} else {
														$('#' + this.id).css(
																'color',
																'black');
													}
												}
											});
						}

					});
}

// controllo che i campi obbligatori siano popolati prima dell'invio al DB
function HTMLTOOLareRequiredFilled(section, classID) {
	var ok = true;
	$('#' + section + ' input').each(function() {
		if (this.id !== "") {
			var classe = $('#' + this.id).attr("class");
			if (classe.includes(classID)) {
				if ($('#' + this.id).val() === "") {
					toastr.warning('Il campo ' + this.id + ' è obbligatoro!');
					ok = false;
				}
			}
		}
	});
	$('#' + section + ' select').each(
			function() {
				if (this.id !== "") {
					var classe = $('#' + this.id).attr("class");
					if (classe.includes(classID)) {
						if ($('#' + this.id).val() === "vuoto"
								|| $('#' + this.id).val() === "") {
							toastr.warning('Il campo ' + this.id
									+ ' è obbligatoro!');
							ok = false;
						}
					}
				}
			});
	return ok;
}

// Questa funzione crea le option per le select in base al component ID che
// viene fornito
function creaOption(componentID, json) {

	$('#' + componentID + ' select').each(
			function() {

				var keys = Object.keys(json[0]);

				for (a = 0; a < keys.length; a++) {

					if (this.id == keys[a]) {
						var campo = this.id;
						$('#' + campo).append(
								$("<option></option>").attr("value", "").text(
										"Seleziona"));

						for (var i = json.length - 1; i >= 0; i--) {
							$('#' + campo).append(
									$("<option></option>").attr("value",
											json[i].Id).text(json[i][campo]));
						}

					}

				}

			});

}

// quando si preme indietro resetto tutte le select!
function resetCampi() {
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').val('0').change();
	$('.mdb-select').material_select();
	resetSezioni();
}

function resetSezioni() {
	// resetto tutti gli input della sezione
	$('#sezioni input').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			$('#' + campo).val("");
		}
	});
	
	$('#sezioni textarea').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			$('#' + campo).val("");
		}
	});
	
	resetVariabili();
}

// funzione per il recupero dei valori degli elementi
function recuperaElementi(elemento, sezione, sezioneGoTo, action, durataProgetto, feste, domeniche,modInt,impRiab,freqSett) {
	
	
	if(sezione=="D") {
		salvaCheckBox();
	}
	
	if(sezione=="G"){
		salvaAccessi();
	}
	
	verificaTabelle_new();
	
	var result = {};
	var nome;
	var cognome;
	var action = action;
	
	if(sezione=="A"){
		var vecchioPr = $('#vecchioPr').val();
	}

	if (vecchioPr == "1") {
		action = "3";
	} 
// else {
// vecchioPr = "0";
// }

	// recupero il nomitavo del paziente
	$('#rowNominativo input').each(function() {
		if (this.id == "nominativo_cognome") {
			cognome = this.value;
		} else {
			nome = this.value;
		}
	});
	
	
		$('#' + elemento).find('td').each (function() {
				var nome_campo = this.id;
				if (nome_campo !== "" & this.innerText !== "") {
					if ($("#" + nome_campo).attr("class").includes("accessiPrev")) { 
						
					} else {
					result[this.id] = this.innerText;
					}
				}
		
				
		});

	
	// per ogni elemento input recupero il valore (dove non è vuoto)
	$('#' + elemento + ' input').each(function() {
		var campo = this.id;
		if (campo !== "" & this.value !== "") {
			
			if ($('#' + campo).is(':checkbox')) {
				if($("#" + campo).attr("class").includes("figProfIniz")) {
					
				} else {
						if($('#' + campo).prop('checked')==true){
							result[this.id] = 1;
						} else {
							result[this.id] = 0;
						}
				}
			} else 	if ($("#" + campo).attr("class").includes("dateFormat")) {
				result[this.id] = formatISODate(this.value);
				
			} else 	if ($("#" + campo).attr("class").includes("accessiPrev")) { 
				
			} else {
			// results.push({
			// id : campo,
			// value : this.value
				result[this.id] = specialChar(this.value,"up");
			// });
			}
		}
	});

	// per ogni elemento select recupero il valore (dove non è vuoto)
	$('#' + elemento + ' select').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result[this.id] = this.value;
			// });
		}
	});

	// per ogni elemento textarea recupero il valore (dove non è vuoto)
	$('#' + elemento + ' textarea').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result[this.id] = specialChar(this.value,"up");
			// });
		}
	});

	// popolo la variabile result con i valori dei campi non vuoti;
	// var data = JSON.stringify(result);

	if(sezione=="A"){
		
		var compilati = true;
		
		if($('#stato').val()!="") {
			
			
			
			$( ".check" ).each(function( index ) {
				
				 if(this.value==""){
					 compilati = false;
				 } 
				
			});
			
			if(compilati){
			
				$.ajax({
							type : 'post',
							url : '../php/controller.php',
							data : {
								'source' : 'mandaDati',
								'dati' : result,
								'nome' : nome,
								'cognome' : cognome,
								'sezione' : sezione,
								'action' : action,
								'vecchioPr' : vecchioPr,
								'durataProgetto': durataProgetto,
								'feste': feste,
								'domeniche': domeniche,
								'impRiab': impRiab,
								'modInt': modInt,
								'freqSett': freqSett
							},
							success : function(response) {
								window.location = "../progetto/progetto_sez" + sezioneGoTo
										+ ".php";
							},
							error : function() {
								toastr.error('Attenzione si &egrave; verificato un errore!!!');
							}
						});
			} else {
				toastr.error('Controlla i campi obbligatori!');
			}
		} else {
			toastr.error('Per poter proseguire è necessario impostare lo stato');
		}
	} else if(sezione=="E"){
		var checked = false
		if($('#CodprotOrtAusili').val()=="1"){
				$(".P").each(function() {
					if(this.checked==true){
						checked = true;
					} 
				});
				
				if(checked==true){
					$.ajax({
						type : 'post',
						url : '../php/controller.php',
						data : {
							'source' : 'mandaDati',
							'dati' : result,
							'nome' : nome,
							'cognome' : cognome,
							'sezione' : sezione
						},
						success : function(response) {
							window.location = "../progetto/progetto_sez" + sezioneGoTo
									+ ".php";
						},
						error : function() {
							alert("error");
						}
					});
				} else {
					toastr.error('Per poter proseguire è necessario selezionare almeno una voce');
				}
					
		} else if ($('#CodprotOrtAusili').val()=="2") {
			$.ajax({
				type : 'post',
				url : '../php/controller.php',
				data : {
					'source' : 'mandaDati',
					'dati' : result,
					'nome' : nome,
					'cognome' : cognome,
					'sezione' : sezione
				},
				success : function(response) {
					window.location = "../progetto/progetto_sez" + sezioneGoTo
							+ ".php";
				},
				error : function() {
					alert("error");
				}
			});
		} else {
			toastr.error('Controlla i campi obbligatori!');
		}
		
	} else if(sezione=="B"|sezione=="C"|sezione=="D"|sezione=="F"|sezione=="G"|sezione=="H"|sezione=="I"|sezione=="M"){
			
		var compilati = true;	
			
			$( ".check" ).each(function( index ) {
				 if(this.value==""){
					 compilati = false;
				 } 
			});
			
			if(compilati){
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : 'mandaDati',
						'dati' : result,
						'nome' : nome,
						'cognome' : cognome,
						'sezione' : sezione
					},
					success : function(response) {
						window.location = "../progetto/progetto_sez" + sezioneGoTo
								+ ".php";
					},
					error : function() {
						alert("error");
					}
				});
			} else {
				toastr.error('Controlla i campi obbligatori!');
			}
	
	} else {
		$.ajax({
			type : 'post',
			url : '../php/controller.php',
			data : {
				'source' : 'mandaDati',
				'dati' : result,
				'nome' : nome,
				'cognome' : cognome,
				'sezione' : sezione
			},
			success : function(response) {
				window.location = "../progetto/progetto_sez" + sezioneGoTo
						+ ".php";
			},
			error : function() {
				alert("error");
			}
		});
	}

}




// funzione per il salvataggio degli elementi
function salvaDati(elemento, sezione) {
		
		verificaTabelle_new();
		
		if(sezione=="D") {
			salvaCheckBox();
		}
		
		if(sezione=="G"){
			salvaAccessi();
		}
		
		var result = {};
		var nome;
		var cognome;
		var action = action;
		

		// recupero il nomitavo del paziente
		$('#rowNominativo input').each(function() {
			if (this.id == "nominativo_cognome") {
				cognome = this.value;
			} else {
				nome = this.value;
			}
		});
		
		
			$('#' + elemento).find('td').each (function() {
					var nome_campo = this.id;
					if (nome_campo !== "" & this.innerText !== "") {
						if ($("#" + nome_campo).attr("class").includes("accessiPrev")|$("#" + nome_campo).attr("class").includes("exclude")) { 
							
						} else {
						result[this.id] = this.innerText;
						}
					}
			
					
			});


		// per ogni elemento input recupero il valore (dove non è vuoto)
		$('#' + elemento + ' input').each(function() {
			var campo = this.id;
			if (campo !== "" & this.value !== "") {
				
				if ($('#' + campo).is(':checkbox')) {
						
					if($("#" + campo).attr("class").includes("figProfIniz")) {
						
					} else {
						if($('#' + campo).prop('checked')==true){
							result[this.id] = 1;
						} else {
							result[this.id] = 0;
					}
					}
				} else 	if ($("#" + campo).attr("class").includes("dateFormat")) {
					result[this.id] = formatISODate(this.value);
					
				} else 	if ($("#" + campo).attr("class").includes("accessiPrev")|$("#" + campo).attr("class").includes("exclude")) { 
					
				} else {
				// results.push({
				// id : campo,
				// value : this.value
				result[this.id] = specialChar(this.value,"up");
				// });
				}
			}
		});

		// per ogni elemento select recupero il valore (dove non è vuoto)
		$('#' + elemento + ' select').each(function() {
			var campo = this.id
			if (campo !== "" & this.value !== "") {
				// results.push({
				// id : campo,
				// value : this.value
				result[this.id] = this.value;
				// });
			}
		});

		// per ogni elemento textarea recupero il valore (dove non è vuoto)
		$('#' + elemento + ' textarea').each(function() {
			var campo = this.id
			if (campo !== "" & this.value !== "") {
				// results.push({
				// id : campo,
				// value : this.value
				result[this.id] = specialChar(this.value,"up");
				// });
			}
		});

		// popolo la variabile result con i valori dei campi non vuoti;
		// var data = JSON.stringify(result);
		
		if(sezione=="A") {
			
// if($('#codProg').val()!=''){
// var codiceProgetto = $('#codProg').val();
//
// } else {
// var codiceProgetto = '';
// }
			
			
			
			if($('#stato').val()!="") {
				
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : 'salvaDatiProgetto',
						'dati' : result,
						'sezione' : sezione
						// 'codiceProgetto': codiceProgetto
					},
					success : function(response) {
						// richiedo il codice progetto
						$.ajax({
							type : 'post',
							url : '../php/controller.php',
							data : {
								'source' : 'richiediCodProgetto',
							},
							success : function(response) {
								salvaCodProg(response[0].id);
								// $('#codProg').val(response[0].id);
								toastr.success('Progetto salvato con successo!');
							},
							error : function() {
								alert("error");
							}
						});
						
					},
					error : function() {
						alert("error");
					}
				});
				
			} else {
				toastr.error('Per poter salvare il progetto è necessario impostare lo stato');
			}
		} else {
			$.ajax({
				type : 'post',
				url : '../php/controller.php',
				data : {
					'source' : 'salvaDatiProgetto',
					'dati' : result,
					'sezione' : sezione
				},
				success : function(response) {
					// richiedo il codice progetto
					$.ajax({
						type : 'post',
						url : '../php/controller.php',
						data : {
							'source' : 'richiediCodProgetto',
						},
						success : function(response) {
							salvaCodProg(response[0].id);
							// $('#codProg').val(response[0].id);
							toastr.success('Progetto salvato con successo!');
						},
						error : function() {
							alert("error");
						}
					});
				},
				error : function() {
					alert("error");
				}
			});
		}
}



// funzione per riempire automticamente i campi
function riempiElementi(elemento, input, obj, valore2) {

	var valore = $('#vecchioPr').val();
	if (valore == undefined | valore == "") {
		if (valore2 !== "") {
			valore = valore2;
		} else {
			valore = "1";
		}
	}

	// questa parte del codice riguarda il riempimento delle input/select di
	// un progetto esistente sul db!
	var a = 0;
	
	if(input=="td"){
		$('#' + elemento).find('td').each (function() {
				var nome_campo = this.id;
				var keys = Object.keys(obj);
				
				for (a = 0; a < keys.length; a++) {
					if (nome_campo == keys[a]) {
						// $('#' + nome_campo).prop('disabled', true);
						$('#' + nome_campo).html(obj[this.id]);
					}
				}
				
		});
		
		calcolaPresenze();
	}
	
	
	
	$('#' + elemento + ' ' + input).each(
			function() {
				var keys = Object.keys(obj);
				var campo = this.id;
				for (a = 0; a < keys.length; a++) {
					if (this.id == keys[a]) {
						// introduco un controllo per l'input checkbox
						if ($('#' + campo).is(':checkbox')) {
// if (valore == "1") {
// $('#' + campo).prop('disabled', true);
// }
							if (obj[campo] == "1") {
								$('#' + campo).prop('checked', true);
							} else if(obj[campo] != ""){
								$('#' + campo).prop('checked', false);
							}
						} else {
							// fine controllo
							if(campo.includes("Dominio_")){
								
								var valore_index = campo.substring(campo.indexOf("_")+1,campo.length);
								
								$('#' + campo).val(obj[this.id]).change;
								
					        	switch (this.value){
				            	case "b": 
				                	$("#pat"+valore_index).css("background-color", "yellow");
				                	break;
				            	case "d":
				            		$("#pat"+valore_index).css("background-color", "green");
				                	break;
				            	case "e": 
				                	$("#pat"+valore_index).css("background-color", "red");
				                	break;
				            	case "s":
				            		$("#pat"+valore_index).css("background-color", "blue");
				                	break;
					        	}
				        	creaOption_pat2(query_ICF,valore_index,this.value);
				        	
				        	creaOption_qual(query_qual1,valore_index,this.value,"1");
				        	creaOption_qual(query_qual2,valore_index,this.value,"2");
				        	creaOption_qual(query_qual3,valore_index,this.value,"3");
								
							} else {
								
								$('#' + campo).val(specialChar(obj[this.id],"down")).change;
								$('#' + campo).val(specialChar(obj[this.id],"down"));
							}
							
// if (valore == "1") {
// $('#' + campo).prop('disabled', true);
// }
							if($("#" + keys[a]).attr("class")!=undefined){
							if ($("#" + keys[a]).attr("class").includes(
									"dateFormat")) {
								$('#' + campo).val(
										formatCustomHTMLDate($('#' + campo)
												.val(), '/')).change;
							}
						}
						}
					}
				}
				if (campo=='luogoResidenza'| campo=='comuneDiNascita'){
					$('#' + campo).prop('disabled', true);
				}
			});
	// }

	// reinizializzo le select!
	if ($('.search').hasClass("select2-hidden-accessible")){
		$('.search').select2('destroy');

		$('.search').select2();

	}

	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();

}

// questa funzione sblocca i campi quando si preme il tasto modifica!
function abilitaCampiCustom(sectionID) {

	$('#' + sectionID + ' input').each(
			function() {
				var campo = this.id;
				if (campo !== "") {
					if (campo == "codProg" | campo == "codAnag"
							| campo == "eta" | campo == "domenica"
							| campo == "feste") {
						// non faccio nulla perchè devono rimanere disabilitate
					} else {
						$('#' + campo).prop('disabled', false);
					}
				}
			});

	$('#' + sectionID + ' select').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	$('#' + sectionID + ' textarea').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	hideComponent('modificaBtn');
	showComponent('salvaBtn');
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
}

// questa funzione sblocca i campi quando si preme il tasto modifica!
function abilitaCampi() {

	$('#sezioni input').each(
			function() {
				var campo = this.id;
				if (campo !== "") {
					if (campo == "codProg" | campo == "codAnag"
							| campo == "eta" | campo == "domenica"
							| campo == "feste") {
						// non faccio nulla perchè devono rimanere disabilitate
					} else {
						$('#' + campo).prop('disabled', false);
					}
				}
			});

	$('#sezioni select').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	$('#sezioni textarea').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	hideComponent('modificaBtn');
	showComponent('salvaBtn');
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
}

// funzione per il recupero dei valori degli elementi
function pushElementi(elemento, obj) {
	// console.log(obj);
	// Nome e Cognome
	$('#nominativo_nome').val(obj[0].Nome);
	$('#nominativo_cognome').val(obj[0].Cognome);

	// valorizza input
	$('#' + elemento + ' input').each(function() {

		if (this.id !== "") {
			$('#' + this.id).val(obj[0][this.id]);
		}

	});
	// valorizza select
	$('#' + elemento + ' select').each(function() {

		if (this.id !== "") {
			$('#' + this.id).val(obj[0][this.id]);
		}

	});

	// valorizza textarea
	$('#' + elemento + ' textarea').each(function() {

		if (this.id !== "") {
			$('#' + this.id).val(obj[0][this.id]);
		}

	});

}

function salvaDatiSession(obj, nomeVariabile) {

	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : 'salvaDatiSession',
			'nomeVariabile' : nomeVariabile,
			'dati' : obj

		},
		success : function(response) {
		},
		error : function() {
			alert("error");
		}
	});

}




function verificaTabelle(sezione){
	
	var result_tab = {};
	
	if(sezione=="A") {
		elemento = "Table_VF";
	} else if(sezione=="B") {
		elemento = "Table_ES";
	} else if(sezione=="C") {
		elemento = "Table_IND";
	}else if(sezione=="E") {
		elemento = "Table_PAT1";
	}else {
		elemento="";
	}
	
	
	if(elemento!=""){

	// per ogni elemento input recupero il valore (dove non è vuoto)
	$('#' + elemento + ' input').each(function() {
		var campo = this.id;
		if (campo !== "" & this.value !== "") {

			result_tab[this.id] = specialChar(this.value,"up");

		}
	});

	// per ogni elemento select recupero il valore (dove non è vuoto)
	$('#' + elemento + ' select').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result_tab[this.id] = this.value;
			// });
		}
	});

	// per ogni elemento textarea recupero il valore (dove non è vuoto)
	$('#' + elemento + ' textarea').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result_tab[this.id] = specialChar(this.value,"up");
			// });
		}
	});	
	
	
	var contaRighe = $('.tab_count').length + 1;
		
		$.ajax({
			type : 'post',
			url : '../php/controller.php',
			data : {
				'source' : 'salvaTabelle',
				'nomeTabella' : elemento,
				'datiTabella' : result_tab,
				'righeTabella': contaRighe

			},
			success : function(response) {
			},
			error : function() {
				alert("error");
			}
		});

	}
	
}

function verificaTabelle_new(){
		
	
	$('.tabelle').each(function () {
		
	var result_tab = {};	

	var elemento = this.id;
	
	// per ogni elemento input recupero il valore (dove non è vuoto)
	$(this).find(':input').each(function() {
		var campo = this.id;
		if (campo !== "" & this.value !== "") {
			
			if ($('#' + campo).is(':checkbox')) {
				if($('#' + campo).prop('checked')==true){
					result_tab[this.id] = 1;
				} else {
					result_tab[this.id] = 0;
				}
			} else 	if ($("#" + campo).attr("class").includes("dateFormat")) {
				result_tab[this.id] = formatISODate(this.value);
				
			} else {
				result_tab[this.id] = specialChar(this.value,"up");
			}
		}
	});

	// per ogni elemento select recupero il valore (dove non è vuoto)
	$(this).find('select').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result_tab[this.id] = this.value;
			// });
		}
	});

	// per ogni elemento textarea recupero il valore (dove non è vuoto)
	$(this).find('textarea').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result_tab[this.id] = specialChar(this.value,"up");
			// });
		}
	});	
	
	if(elemento=="Table_PAT1"){
		var contaRighe = $('.tab_count_pat1').length + 1;
	} else  if(elemento=="Table_PAT2"){
		var contaRighe = $('.tab_count_pat2').length + 1;
	} else  if(elemento=="Table_PT1"){
		var contaRighe = $('.tab_count_pt1').length + 1;
	} else  if(elemento=="Table_PT2"){
		var contaRighe = $('.tab_count_pt2').length + 1;
	} else if(elemento=="Table_Day"){
		var contaRighe = $('.tab_count_day').length + 1;
	} else if(elemento=="Table_Cons"){
		var contaRighe = $('.tab_count_cons').length + 1;
	} else {
		var contaRighe = $('.tab_count').length + 1;
	}
	
		
		$.ajax({
			type : 'post',
			url : '../php/controller.php',
			data : {
				'source' : 'salvaTabelle',
				'nomeTabella' : elemento,
				'datiTabella' : result_tab,
				'righeTabella': contaRighe

			},
			success : function(response) {
			},
			error : function() {
				alert("error");
			}
		});
		
	});
}

// class capitalize per rendere la prima lettera maouscola e le altre minuscole
// in automatico
var connectives = {
	    das: true,
	    da: true,
	    dos: true,
	    do: true,
	    de: true,
	    e: true
	};

	function capitalize(str) {
	    return str
	        .split(" ")
	        .map(function(word) {
	            return !connectives[word.toLowerCase()]
	                ? word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
	                : word.toLowerCase();
	        })
	        .join(" ");
	};

	$(".capitalize").keyup(function() {
	    var cursorStart = this.selectionStart;
	    var cursorEnd = this.selectionEnd;
	    var capitalizedString = capitalize($(this).val());

	    $(this).val(capitalizedString);
	    this.setSelectionRange(cursorStart, cursorEnd);
	});


function specialChar(str,flow){
	
	var stringa = str;
	
			if(flow=="up"){
				searchStr = ["'",'"'];
				str_sub = ["ü","ä"]
			} else {
				searchStr = ["ü","ä"];
				str_sub = ["'",'"'];
			}
			
			for(w=0;w<searchStr.length;w++){
			
				if(typeof str=="string") {
			
				    var searchStrLen = searchStr[w].length;
					    if (searchStrLen == 0) {
					        return [];
					    }
				    var startIndex = 0, index, indices = [];
			
				    while ((index = str.indexOf(searchStr[w], startIndex)) > -1) {
				        indices.push(index);
				        startIndex = index + searchStrLen;
				    }
			
				    
				    for(h=0;h<indices.length;h++){
				    	stringa = stringa.replace(searchStr[w],str_sub[w]);
				    } 
			    
				} else {
					
				}
			
			}
		    
		    return stringa;
}

function salvaCodProg(response) {
	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : 'salvaCodProg',
			'codProg' : response

		},
		success : function(response) {
		},
		error : function() {
			alert("error");
		}
	});
}


function check_campi(stato,regAssist){
	

	switch(stato){
		case "1": // ATTESA
			$('.trattamento').each(function (index, value) {
				$('label[for='+$(this).attr('id')+']').removeClass( "prova" );
				$("#"+$(this).attr('id')).removeClass( "check" );
				});

			$('.attesa').each(function (index, value) {
				$('label[for='+$(this).attr('id')+']').addClass( "prova" );
				$("#"+$(this).attr('id')).addClass( "check" );
				});
			break;
		case "2": // AUTORIZZATO
			break;
		case "3": // TRATTAMENTO
			$('.attesa').each(function (index, value) {
				$('label[for='+$(this).attr('id')+']').removeClass( "prova" );
				$("#"+$(this).attr('id')).removeClass( "check" );
				});
			
			$('.trattamento').each(function (index, value) {
				$('label[for='+$(this).attr('id')+']').addClass( "prova" );
				$("#"+$(this).attr('id')).addClass( "check" );
				});
			if(regAssist=="2"){
				$('label[for=modTrasp]').addClass( "prova" );
				$("#modTrasp").addClass( "check" );
			}
			break;
		default:
			break;
	}

}
