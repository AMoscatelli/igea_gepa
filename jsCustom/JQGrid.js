var obj = [];
var rowData_old = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, tipoPaziente, width) {

	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "ID",
			label : "ID",
			align : "center"
		}, {
			name : "Cognome",
			label : "Cognome",
			align : "center"
		}, {
			name : "Nome",
			label : "Nome",
			align : "center"
		}, {
			name : "ImpRiab",
			label : "Mod.Intervento",
			align : "center"
		}, {
			name : "ModalitaIntervento",
			label : "Impegno",
			align : "center"
		}, {
			name : "Anno",
			label : "Anno",
			align : "center"
		}, {
			name : "Mese",
			label : "Mese",
			align : "center"
		}, {
			name : "DataInizio",
			label : "Data Inizio",
			align : "center",
			sorttype : "date",
			formatter : "date",
			formatoptions : {
				newformat : "d-M-Y"
			}
		}, {
			name : "DataFine",
			label : "Data Fine",
			align : "center",
			sorttype : "date",
			formatter : "date",
			formatoptions : {
				newformat : "d-M-Y"
			}
		}, {
			name : "GGPCTeor",
			label : "GGPCTeor",
			align : "center",
			template : "number"
		}, {
			name : "Ass80",
			label : "Ass80",
			align : "center",
			template : "number",
			formatoptions : {
				decimalPlaces : 0
			},
			editable : true
		}, {
			name : "Ass00",
			label : "Ass00",
			align : "center",
			template : "number",
			formatoptions : {
				decimalPlaces : 0
			},
			editable : true
		} ],
		width : width,

		iconSet : "fontAwesome",
		idPrefix : "g1_",
		data : obj,
		rownumbers : true,
		sortname : "invdate",
		sortorder : "desc",
		caption : "Indicare il Numero di assenze per i Pazienti."
	});
}

	if (ids1.length != 0 && ids.length != 0) {
		for (var i = ids.length; i >= 0; i--) {
			grid.jqGrid('editRow', ids[i]);
		}

		for (var i = ids1.length; i >= 0; i--) {
			grid1.jqGrid('editRow', ids1[i]);
		}
		$.LoadingOverlay("hide");
	} else {
		$.LoadingOverlay("hide");
		toastr.error('Opps! Si è verifiato un problema!');
	}
}

function JQGRIDsaveRows(GridName, GridName1) {
	var resid = [];
	var nonResid = [];
	var grid = $(GridName);
	var ids = grid.jqGrid('getDataIDs');
	var grid1 = $(GridName1);
	var ids1 = grid1.jqGrid('getDataIDs');

	if (ids1.length != 0) {
		for (var i = 0; i < ids1.length; i++) {
			grid1.jqGrid('saveRow', ids1[i]);
			nonResid[i] = grid1.jqGrid("getRowData", ids1[i]);
		}
	}
	if (ids.length != 0) {
		for (var i = 0; i < ids.length; i++) {
			grid.jqGrid('saveRow', ids[i]);
			resid[i] = grid.jqGrid("getRowData", ids[i]);
		}
	}

	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "saveConatbAssenze",
			'residenziale' : resid,
			'nonResidenziale' : nonResid
		},
		success : function(response) {
			$.LoadingOverlay("hide");
			toastr.success('Salvataggio avvenuto con Successo!');
		},
		error : function() {
			$.LoadingOverlay("hide");
			toastr.error('Opps! Si è verifiato un problema!');
		}
	});

	/*
	 * if(ids1.length!=0 && ids.length!=0){ for (var i = 0; i < ids.length; i++) {
	 * grid.saveRow(ids[i], false); var rowData = grid.jqGrid("getRowData",
	 * ids[i]); var query = 'UPDATE `presenze` SET `Ass80`=
	 * '+rowData.Ass80+',`Ass00`= '+rowData.Ass00+' WHERE `ID` = '+rowData.ID+'
	 * AND `Anno` = '+rowData.Anno+' AND `Mese` = '+rowData.Mese+'';
	 * JQGRDIupdateRow(query); }
	 * 
	 * 
	 * for (var i = 0; i < ids1.length; i++) { grid1.jqGrid('saveRow', ids1[i]); }
	 * }else{ alert('Nulla da Salvare'); }
	 */
}

function JQGRDIgetGrid(GridName) {
	var grid = $(GridName);
	var ids = grid.jqGrid('getGridParam', 'data');
	return ids;
}

function JQGRDIupdateRow(Query) {

	$.ajax({
		type : 'post',
		url : '../php/mysql/Query_Update.php',
		data : {
			'query' : Query
		},
		success : function(response) {
			console.log(response);
			// obj = JSON.parse(response);
			// showGrid(obj, GridName, tipoPaziente);
		},
		error : function() {
			alert("error");
		}
	});

}


// JQGrid Progetti --> Pazienti
function JQGRIDinitPazientiGrid(obj, GridName, width, query) {
	$grid = $(GridName);

	var click_count = 0;
	// definisco due valori per determinare se si tratta di un nuovo progetto o
	// la visualizzazione di uno vecchio
	$('#nuovoPr').val("0");
	$('#vecchioPr').val("0");

	$(GridName).jqGrid('GridUnload');

	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "CodAnagr",
							label : "Codice Anagrafica",
							align : "center",
							searchoptions : {
								// show search options
								sopt : [ "eq" ]
							// ge = greater or equal to, le = less or equal
							// to, eq = equal to
							}
						}, {
							name : "Nome",
							label : "Nome",
							align : "center"
						}, {
							name : "Cognome",
							label : "Cognome",
							align : "center"
						}, {
							name : "CodiceFiscale",
							label : "Codice Fiscale",
							align : "center"
						}, ],
						rowNum : 30,
						height : "auto",
						pager : "#jqGridPazientiPager",
						width : width,
						iconSet : "fontAwesome",
						caption : "Cerca:",
						idPrefix : "g1_",
						data : obj,
						rownumbers : true,
						sortname : "CodAnagr",
						sortorder : "asc",
						caption : "Selezionare il paziente desiderato",
						ignoreCase : true,
						onSelectRow : function(rowid) {

							var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;

							if (rowData_old == rowData) {

								hideComponent('gridPazienti');

								hideComponent('mostraPrBtn');

								showComponent('sezioni');
								
								showComponent('VF_section');

								showComponent('salvaBtn');

								showComponent('rowNominativo');

								$('#nominativo_nome').val(rowData.Nome);

								$('#nominativo_cognome').val(rowData.Cognome);

								$('#codAnag').val(rowData.CodAnagr);

								hideComponent('counterPr');

								rowData_old = 0;

								$('#nuovoPr').val("1");

							} else {

								click_count = 0;

								$('#nominativo_nome').val(rowData.Nome);

								$('#nominativo_cognome').val(rowData.Cognome);

								rowData_old = rowData;

								rowData = rowData.CodAnagr;

								showComponent('counterPr');

								$
										.ajax({
											type : 'post',
											url : '../php/controller.php',
											data : {
												'source' : "cercaProgetto",
												'whereValue' : rowData,
												'whereCond' : 'CodAnagr',
												'tableName' : 'progetto',
												// 'fieldName':
												// "p.CodProj,st.Stato,p.DataInizio,p.DataFine,p.DataStato,p.DurataProgetto,inte.Intervento,r.RegAssist,m.ModalitaIntervento,i.ImpRiab,me.Nome_Cognome,te1.Terapista,te2.Terapista,te3.Terapista",
												'fieldName' : "p.CodProj as codProg," +
														"st.Stato as stato," +
														"p.DataInizio as dataInizio," +
														"p.DataFine as dataFine," +
														"p.DataStato as dataStato," +
														"p.DurataProgetto as durataProgetto," +
														"inte.Intervento as intervento," +
														"r.RegAssist as regAssist," +
														"m.ModalitaIntervento as modInt," +
														"i.ImpRiab as impRiab," +
														"me.Nome_Cognome as medico," +
														"te.Nominativo as terapista1," +
														"te1.Nominativo as terapista2," +
														"te2.Nominativo as terapista3"
											},
											success : function(response) {

												var obj = JSON.parse(response);
												var a = 0;
												for (i = 0; i < obj.length; i++) {
													a = i + 1;

												}
												JQGRIDinitProgettiGrid(obj,
														'#grid2', $(
																"#rowButton")
																.innerWidth(),
														rowData, query);
												setComponentText('counterPr', a);

											},
											error : function() {
												toastr
														.error('Opps! Si è verifiato un problema!');
											}
										});

							}
						}

					});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridPazientiPager').css({
		"height" : "35px"
	});
}


// JQGrid Anagrafica --> Pazienti
function JQGRIDinitAnagraficaGrid(obj, GridName, width) {
	$grid = $(GridName);

	var click_count = 0;

	$(GridName).jqGrid('GridUnload');
	// "use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "CodAnagr",
			label : "Codice Anagrafica",
			align : "center",
			searchoptions : {
				// show search options
				sopt : [ "eq" ]
			// ge = greater or equal to, le = less or equal
			// to, eq = equal to
			}
		}, {
			name : "Nome",
			label : "Nome",
			align : "center"
		}, {
			name : "Cognome",
			label : "Cognome",
			align : "center"
		}, {
			name : "CodiceFiscale",
			label : "Codice Fiscale",
			align : "center"
		}, ],
		rowNum : 30,
		height : "auto",
		pager : "#jqGridAnagrPazientiPager",
		width : width,
		iconSet : "fontAwesome",
		caption : "Cerca:",
		idPrefix : "g1_",
		data : obj,
		rownumbers : true,
		sortname : "CodAnagr",
		sortorder : "asc",
		caption : "Selezionare il paziente desiderato",
		ignoreCase : true,
		onSelectRow : function(rowid) {

			var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;

			if (rowData_old == rowData) {

				// toastr.error('hai cliccato 2 volte');

				// hideComponent('gridPazienti');

				// hideComponent('mostraPrBtn');

				// showComponent('sezioni');

				// showComponent('rowNominativo');

				$('#nominativo_nome').val(rowData.Nome);

				$('#nominativo_cognome').val(rowData.Cognome);

				$('#codAnag').val(rowData.CodAnagr);

				// hideComponent('counterPr');

				rowData_old = 0;

			} else {

				click_count = 0;

				rowData_old = rowData;

				rowData = rowData.CodAnagr;

				// showComponent('counterPr');

				// $.ajax({
				// type: 'post',
				// url: '../php/controller.php',
				// data: {
				// 'source': "cercaProgetto",
				// 'whereValue': rowData,
				// 'whereCond': 'CodAnagr',
				// 'tableName': 'progetto',
				// 'fieldName':
				// "p.CodProj,p.Stato,p.DataInizio,p.DataFine,p.DataStato,p.DurataProgetto,inte.Intervento,r.RegAssist,m.ModalitaIntervento,i.ImpRiab,me.`Nome_Cognome`",
				// //'fieldName':
				// "p.CodProj,p.Stato,p.DataInizio,p.DataFine,p.DataStato,p.DurataProgetto,p.Intervento,r.RegAssist,m.ModalitaIntervento,i.ImpRiab",
				// },
				// success: function (response) {
				//							  
				// var obj = JSON.parse(response);
				// var a = 0;
				// for(i=0;i<obj.length;i++){
				// a = i + 1;
				//								 
				// }
				// JQGRIDinitProgettiGrid(obj,'#grid2',$("#rowButton").innerWidth(),rowData);
				// setComponentText('counterPr',a);
				//						
				// },
				// error: function () {
				// toastr.error('Opps! Si è verifiato un problema!');
				// }
				// });

			}
		}

	});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridAnagrPazientiPager').css({
		"height" : "35px"
	});
}
