/* includere nella pagina php:
 * include 'config.inc.php';
 * 
 * includere nella pagina php lato html:
	<!--Main Navigation-->
	<header>
	    <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>
	</header>
    <!--/Main Navigation--> 
 * 
 * lato JS:
 * initNavBar('<?php echo $baseurl;?>');
 */

function initNavBar(path, version, role) {

	var html = '<!-- SideNav slide-out button -->';
	html += '<a id="menu_button" href="#" data-activates="slide-out" class="btn btn-primary p-3 button-collapse"><i class="fas fa-bars"></i></a>';
	html += '<!-- Sidebar navigation -->';
	html += '<div id="slide-out" class="side-nav fixed wide sn-bg-1">';
	html += '<ul class="custom-scrollbar">';
	html += '<!-- Logo -->';
	html += '<li>';
	html += '<div>';
	html += '<a class="menuItem" href="'
			+ path
			+ '"><i class="fa fa-home"></i><strong> IGEA - <span id="small-font">Ver. '
			+ version
			+ '</span></strong><span class="sr-only">(current)</span></a>';
	html += '</div>';
	html += '<div class="logo-wrapper sn-ad-avatar-wrapper">';
	html += '<a class="menuItem" href="#"><img src="'
			+ path
			+ '/images/avatar.png" class="rounded-circle"><span>Benvenuto </span></a>';
	html += '</div>';
	html += '</li>';
	html += '<!--/. Logo -->';
	html += '<!-- Side navigation links -->';

	// Menu Anagrafica
	html += '<li>';
	html += '<ul class="collapsible collapsible-accordion">';
	html += '<li><a id="anagMenu" class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-users"></i> Anagrafica<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="anagraficaMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li><a href="'
			+ path
			+ '/anagrafica/anagrafica_detail.php?new=true" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Nuovo</span></a>';
	html += '</li>';
	html += '<li><a href="' + path
			+ '/anagrafica/anagrafica.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Cerca</span></a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';

	// Menu Progetti
	html += '<li><a class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-book"></i>Progetti<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="progettoMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li><a href="'
			+ path
			+ '/progetto/progetto_sezA.php?buttonPr=nuovo" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Nuovo</span></a>';
	html += '</li>';
	html += '<li><a href="' + path
			+ '/progetto/progetto_sezA.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Cerca</span></a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	// Menu contabilità
	html += '<li><a class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-calculator"></i> Contabilit&agrave;<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="contabilitaMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li><a href="' + path
			+ '/contabilita/contabilita.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Fatture</span></a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	// Lista attesa
	html += '<li><a id="listaMenu" class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-th-list"></i> Liste attesa<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="listaAttesaMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li>';
	html += '<a href="' + path
			+ '/lista_attesa/lista_attesa.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Adulti</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="'
			+ path
			+ '/lista_attesa/lista_attesa.php?child=true" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Bambini (NPI)</span>';
	html += '</a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	// Menu Stampe
	html += '<li><a id="stampeMenu" class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-print"></i> Stampe<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="stampeMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li>';
	html += '<a href="' + path
			+ '/cartClinic/cartClinica.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Cartella clinica</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/catrellaClin/sezioniSiar.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">SIAR</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/catrellaClin/scadMensili.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Scadenza mensili</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/catrellaClin/ctrlMensProgetti.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Controllo mensile</span>';
	html += '</a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	// Menu SIAR
	html += '<li><a id="siarMenu" class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-print"></i> SIAR<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="siarMenuBody" class="collapsible-body">';
	html += '<ul>';
	html += '<li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Codifica</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Elaborazioni</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Apertura Anag.</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Apertura Clinica</span>';
	html += '</a>';
	html += '</li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Chiusura Clinica</span>';
	html += '</a>';
	html += '</li>';
	html += '<a href="#" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Aggiornamento Clinica</span>';
	html += '</a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	// Menu Utility
	html += '<li><a id="utilityMenu" class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fa fa-toolbox"></i> Utility<i class="fas fa-angle-down rotate-icon"></i></a>';
	html += '<div id="utilityMenuBody" class="collapsible-body">';
	html += '<ul>';
	if (role == 1) {
		html += '<li>';
		html += '<a href="' + path
				+ '/utility/profileAdmin.php" class="waves-effect">';
		html += '<span class="sv-slim"></span>';
		html += '<span class="sv-normal">Gestione Utenti</span>';
		html += '</a>';
		html += '</li>';
	}
	html += '<li>';
	html += '<a href="' + path + '/utility/medici.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Medici</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/utility/terapisti.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Terapisti</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/utility/amministrativi.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Amministrativi</span>';
	html += '</a>';
	html += '</li>';
	html += '<a href="' + path + '/utility/banche.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Banche</span>';
	html += '</a>';
	html += '</li>';
	html += '<a href="' + path + '/utility/icd9cmn.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">ICD9-CM</span>';
	html += '</a>';
	html += '</li>';
	html += '<li>';
	html += '<a href="' + path
			+ '/utility/festivita.php" class="waves-effect">';
	html += '<span class="sv-slim"></span>';
	html += '<span class="sv-normal">Festivit&agrave;</span>';
	html += '</a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '</li>';
	html += '<li><a href="'
			+ path
			+ '/logout.php" id="toggle" class="waves-effect"><i class="sv-slim-icon fas fa-angle-double-left"></i> Disconnetti</a>';
	html += '</li>';
	html += '</ul>';
	html += '</li>';
	html += '<!--/. Side navigation links -->';
	html += '</ul>';
	html += '<div class="sidenav-bg rgba-blue-strong"></div>';
	html += '</div>';
	html += '<!--/. Sidebar navigation -->';

	$("#navBar").html(html);

	$('#menu_button').click(function() {
		$("#slide-out").css("transform", "translateX(0px)");
	});

	$('section').click(function() {
		$("#slide-out").css("transform", "translateX(-100%)");
	});
}
function initListener() {

	var anag = true;
	$('#anagMenu').click(function() {
		if (anag) {
			$('#anagMenuBody').css({
				"display" : "none"
			});
			anag = false;
		} else {
			$('#anagMenuBody').css({
				"display" : "block"
			});
			anag = true;
		}
	});
}
