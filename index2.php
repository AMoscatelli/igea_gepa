<?php 
//Da includere per la gestione della sessione
//include('php/config.php');
include('php/session.php');
include('php/config.inc.php');
include('php/function.php');
$index_result = 9;

if(isset($_GET['result'])){
    $index_result = $_GET['result'];
}

if($_SESSION['login_role']=='1'){

//recupero il parziale dei minori di 18 anni in attesa
$inAttesaChild = intval(getRowCount('lista_attesa_child_less_18_vista'));
//recupero il parziale dei maggiori di 14 anni in attesa
$inAttesaAdult = intval(getRowCount('lista_attesa_adult_over_18_vista'));
//recupero il numrrno totale dei pazienti in attesa
$inAttesa = $inAttesaAdult+$inAttesaChild;
//recupero il numero dei pazienti in trattamento
$inTrattamento = intval(getRowCount('paz_trattamento_vista'));
//Pazienti in Carico
$tot = $inAttesa+$inTrattamento;

$inTrattamentoPerc = intval(($inTrattamento/$tot)*100);
$inAttesaPer = 100-$inTrattamentoPerc;
$inAttesaChildPerc = intval(($inAttesaPer*($inAttesaChild/$inAttesa)*100)/100);
$inAttesaAdultPerc = $inAttesaPer-$inAttesaChildPerc;
}else{
    $inTrattamentoPerc = 0;
    $inAttesaPer = 0;
    $inAttesaChildPerc = 0;
    $inAttesaAdultPerc = 0;
    $inAttesa = 0;
    $inTrattamento = 0;
    $tot = 0;
    
}

?>
<html>
   
   <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Igea - HOME</title>

    <!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Table -->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
     <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- JQGrid -->
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/ui.jqgrid.min.css" rel="stylesheet">
    

	<!-- Calendar -->
	<link href='calendar/core/main.css' rel='stylesheet' />
    <link href='calendar/bootstrap/main.css' rel='stylesheet' />
    <link href='calendar/timegrid/main.css' rel='stylesheet' />
    <link href='calendar/daygrid/main.css' rel='stylesheet' />
    <link href='calendar/list/main.css' rel='stylesheet' />

        <!-- Custom Css Select -->
	<link href="css/style.css" rel="stylesheet">
	
	
    <style>
    
/*  da togliere il commento quando si vuole visualizzare gli eventi sulla sx
  #wrap {
    width: 1100px;
    margin: 0 auto;
  }

  #external-events {
    margin-top: 20%;
    float: left;
    width: 150px;
    padding: 0 10px;
    border: 1px solid #ccc;
    background: #eee;
    text-align: left;
  }

  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
  }

  #external-events .fc-event {
    margin: 10px 0;
    cursor: pointer;
  }

  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }

  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }

  #calendar {
    float: right;
    width: 900px;
  }
  
  */
    </style>

</head>


<body>

    <!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	    

	</header>
    <!--/Main Navigation-->

    <!--Main Layout-->
    
	<div class="container mt-5" id="container">
	    <!--Section: Calendar-->
    	<section>
        
        <div class="row" id="riga">
        	
            	<div id="calendar_div">
                	<div id='calendar'></div>
       			</div>
       			
       			<div id="event_div" style="width:420px; display:none" class="ml-3 mt-1" >
       			<!-- Collapsible element -->
                <!-- Card -->
                    <div class="card testimonial-card" id="collapseExample">
                    
                      <!-- Background color -->
                      <div class="card-up indigo lighten-1 event"></div>
                    
                      <!-- Avatar -->
                      <div class="avatar mx-auto white">
                        <img src="img/event.png" class="rounded-circle" alt="woman avatar">
                      </div>
                    
                      <!-- Content -->
                      <div class="card-body">
                                <!-- Titolo-->
                                <p><strong>Nuovo Appuntamento</strong></p>
                                
                                <hr class="hr_color">
                               
                                <!-- Date input-->
                                <label for="date_event">Data evento</label>
                                <div class="row d-flex justify-content-center">
	                                <input type="text" id="date_event" class="form-control event_form text-center hr_color" disabled>
                                </div>
                                
                                <hr class="hr_color">
                                
                                <!-- Nome input-->
                                <div class="row d-flex justify-content-center">
	                                    <!-- Grid column -->
                                        <div class="col">
                                          <!-- Default input -->
                                          <input type="text" class="form-control text-center hr_color" placeholder="Nome" id="name_event">
                                        </div>
                                        <!-- Grid column -->
                                    
                                        <!-- Grid column -->
                                        <div class="col">
                                          <!-- Default input -->
                                          <input type="text" class="form-control text-center hr_color" placeholder="Cognome" id="surname_event">
                                        </div>
                                        <!-- Grid column -->
                                </div>
                                
                                <hr class="hr_color">
                                
                                <!-- Frequenza e #giorni input-->
                                <div class="row d-flex justify-content-center">
	                                    <!-- Grid column -->
                                        <div class="col">
                                          <select class="browser-default custom-select text-center hr_color" id="frequenza_event">
                                              <option value="0" selected>Scegli la frequenza</option>
                                              <option value="1">1</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                            </select>
                                        </div>
                                        <!-- Grid column -->
                                    
                                        <!-- Grid column -->
                                        <div class="col">
                                          <!-- Default input -->
                                          <input type="text" class="form-control text-center hr_color" placeholder="numero di giorni" id="giorni_event">
                                        </div>
                                        <!-- Grid column -->
                                </div>
                                
                                <hr class="hr_color">
                                
                                 <div class="row float-right">
 										<button type="button" class="btn btn-outline-primary btn-rounded waves-effect" id="indietro" onclick="mostraPazienti();"><i class="fas fa-arrow-left" aria-hidden="true"></i></button>
                                		<button type="button" class="btn btn-outline-primary btn-rounded waves-effect">Crea</button>
                                		<button type="button" class="btn btn-outline-danger btn-rounded waves-effect" onclick="hideEvent(calendarEl);">Chiudi</button>
                                </div>
                      </div>
                    <!-- Card -->
                </div>
                <!-- / Collapsible element -->
                </div>
        
        		<div id="lista_pazienti" class="ml-3 mt-1" style="display:none">
                <!-- Table with panel -->
                <div class="card card-cascade narrower">
                
                  <!--Card image-->
                  <div class="view view-cascade gradient-card-header blue darken-3 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                    <a href="" class="white-text mx-3">Scegli paziente</a>
                  </div>
                  <!--/Card image-->
                
                  <div class="px-4 mb-2 mt-1">
                      <!--Table-->
                    <table id="pazienti_table" class="stripe text-center" style="width:100%"></table>
                      <!--Table-->                
                  </div>
                
                </div>
                <!-- Table with panel -->
        		</div>
          </div>
		</section>
		<!--/Section: Calendar-->
		<!--Section: KPI-->
    	<section>
    		<!-- <div class="mt-1" id="kpi"></div> -->
    		<div id="kpi" class="card card-cascade wider reverse" style="display:none">
				
						<div class="row mt-1 text-center mt-4 mb-4" id="row-lenght">
						
						<div class="col-md-4">
							<div class="text-center">
                                <span class="min-chart" id="trattamento" data-percent="<?php echo $inTrattamentoPerc; ?>"><span class="percent"></span></span>
                                <h5><p class="label white" style="color:green">Pazienti in trattamento <i class="fa fa-arrow-circle-up"></i></p></h5>
                            </div>                        	
                        </div>
                        <div class="col-md-4">
							<div class="text-center">
                                <span class="min-chart" id="adulti" data-percent="<?php echo $inAttesaAdultPerc; ?>"><span class="percent"></span></span>
                                <h5><span class="label white" style="color:orange">Adulti in attesa <i class="fa fa-arrow-circle-up"></i></span></h5>
                            </div>                        	
                        </div>
                        <div class="col-md-4">
							<div class="text-center">
                                <span class="min-chart" id="bambini" data-percent="<?php echo $inAttesaChildPerc; ?>"><span class="percent"></span></span>
                                <h5><span class="label white" style="color:orange">Bambini in attesa <i class="fa fa-arrow-circle-up"></i></span></h5>
                            </div>                        	
                        </div>
						<div class="col-md-6 mt-4 mb-4">
							<canvas id="barChart"></canvas>
						</div>
						<div class="col-md-6 mt-4 mb-4">
							<canvas id="polarChart"></canvas>
                    	</div>
                    	<div class="col-md-6 mt-4 mb-4">
							<canvas id="doughnutChart"></canvas>
                    	</div>
                    	<div class="col-md-6 mt-4 mb-4">
							<canvas id="pieChart"></canvas>
                    	</div>
                    	
						
				</div>
				<!--Post data-->
			</div>
		</section>
		<!--/Section: KPI-->
	</div>
    <!--/Main Layout-->
  	
</body>
    <!--  SCRIPTS  -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    
    <script type="text/javascript" src="js/jquery-ui-1.12.1.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src=" https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
   
    
    
    <!-- Calendar -->
    <script src='calendar/core/main.js'></script>
    <script src='calendar/core/locales-all.js'></script>
    <script src='calendar/interaction/main.js'></script>
    <script src='calendar/bootstrap/main.js'></script>
    <script src='calendar/daygrid/main.js'></script>
    <script src='calendar/timegrid/main.js'></script>
    <script src='calendar/list/main.js'></script>

    <script type="text/javascript" src='index_comp/KPI/kpi.js'></script>
    
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    
    <!-- Custom JavaScript -->
    <script type="text/javascript" src="jsCustom/htmlTool.js"></script>
    
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
 	<!-- Menu Navigazione -->
    <script type="text/javascript" src="jsCustom/navigationSideBar.js"></script>
    
    <!-- Loading spinner -->
    <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
    
    <!-- Custom JS -->
    <script type="text/javascript" src="jsCustom/calendar.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="jsCustom/dateUtil.js"></script>
    <script type="text/javascript" src="js/jquery.jqgrid.min.js"></script>
   
   	
   	<script>

 


   	
        var calendarEl = document.getElementById('calendar');
        
   		//Mostro lo Spinner
   		$.LoadingOverlay("show");
   		//Inizializzo MDBootstrap 
        new WOW().init();
      	//inizializzo la navBar
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
        var result = <?php echo $index_result; ?>;
        var user = 'Benvenuto <?php echo $_SESSION['login_user'];?>';
        var role = '<?php echo $_SESSION['login_role'];?>';
	
        if(result===1){//ho appena fatto login e quindi mostro il msg di Benvenuto
        	toastr.success('Benvenuto <?php echo $login_session; ?>!');
        }

    	var obj = <?php echo selectAll('anagrafico','Nome,Cognome');?>;

    	var result = [];

    	for(var i in obj){
    	    result.push([obj [i].Nome,obj[i].Cognome]);
    	}
	
		//var result1 = JSON.stringify(result);

        $(document).ready(function () {
        	// Show sideNav
           	$('.button-collapse').sideNav('show');
           	// Hide sideNav
           	$('.button-collapse').sideNav('hide');
           	
            $('#loginUser').html(user);
        	$.LoadingOverlay("hide");
    
    		var table = $('#pazienti_table').DataTable({
     	        data: result,
    	        columns: [
    	            { title: "Nome" },
    	            { title: "Cognome" }
    	        ],
        		"pageLength": 7,
        		select: true
        	 });

       	    table.on( 'select', function ( e, dt, type, indexes ) {
                var rowData = table.rows( indexes ).data().toArray();

                $('#name_event').val(rowData[0][0]);
                $('#surname_event').val(rowData[0][1]);
                
                hideComponent('lista_pazienti');
                showComponent('event_div');
            });

                    	
         });


        
		if(role > '1'){ //se non sono admin
			
			initCalendar(calendarEl);
			
        	
		}else{//se sono admin vedono i KPI

			showComponent('kpi');
			
			var data = [<?php echo $tot;?>,<?php echo $inTrattamento;?>,<?php echo $inAttesa;?>,]
			initCharts('percent','trattamento','#4caf50');
			initCharts('percent','adulti','#ffa500');
			initCharts('percent','bambini','#ffa500');
			initCharts('pie');
			initCharts('doughnutChart');
			initCharts('polar');
			initCharts('bar','','',data);
		}
    </script>

    
</html>