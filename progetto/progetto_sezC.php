<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$sezioneC = "";
$json_rowData="";
$arr=[];
$num_tabella = "";
$tab_sezC ="";

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['dataInizio'])){
            $dataInizio =  $section2['dataInizio'];
        } else {
            $dataInizio ="";
        }
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
        if(isset($section2['codProg'])){
            $codProj = $_SESSION['set_sectionA']['codProg'];
        } else  {
            $codProj = "";
        }
        
        
    }
}

$vecchioPr =  $_SESSION['vecchioPr'];

if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
        $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionC'])){
    $action=3;
}

if(isset($_SESSION['set_sectionC'])){
    if($_SESSION['set_sectionC']!==""){
        $section = $_SESSION['set_sectionC'];
        $sezioneC = json_encode((object)$section);
    }
}

if (isset($_SESSION['Table_IND'])) {
    if ($_SESSION['Table_IND'] !== "") {
        $tab = $_SESSION['Table_IND'];
        
        $tab_sezC = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_IND'])) {
            $num_tabella = $_SESSION['num_Table_IND'];
        }
        
    }
}

?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">

</head>

   
   <body>
   
   <!--Main Navigation-->
    <header>

		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>

    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','C');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','C','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezC" role="tabpanel">
                             
                            							<!-- Creazione grid della prima riga -->    
							<div class="row">
								<div class="col-6">
									<!-- Invalidit� input -->
									<label for="inviante">Inviante</label> 
									<select class="colorful-select dropdown-primary text-center search attesa trattamento" id="inviante" style="width:100%"></select>
								</div>
								
								<div class="col-6">
									<label for="altroInviante">Altro inviante</label>
									<input type="text" class="form-control text-center" id="altroInviante" disabled></input>
								</div>
								
							</div>
													       <!-- Fine prima riga -->
							<br>
							                         <!-- Creazione grid della seconda riga -->
							<div class="row">
							<div class="col-6">
								<label for="StruttureProvenienza">Strutture di provenienza</label>
								<select class="colorful-select dropdown-primary text-center border_custom_select search" id="StruttureProvenienza" style="width:100%"></select>
								</div>
								
								<div class="col-6">
									<label for="altroProv">Altre provenienze</label>
									<input type="text" class="form-control text-center" id="altroProv">
								</div>
 
<!-- 								<div> 
								 <input type="text" class="form-control text-center" id="nuovoPr" style="display:none">
								 <input type="text" class="form-control text-center" id="vecchioPr" style="display:none">
                                </div> -->
							</div>
													       <!-- Fine seconda riga -->
							<br>
							                         <!-- Creazione grid della terza riga -->
							<div class="row">
								<div class="col-3">
									<label for="condVita">Condizioni di vita</label>
									<select class="colorful-select dropdown-primary text-center border_custom_select search attesa trattamento" id="condVita" style="width:100%"></select>
								</div>
								
								<div class="col-4">
									<label for="altroCondVita">Altre condizioni di vita</label>
									<input type="text" class="form-control text-center" id="altroCondVita" disabled></input>
								</div>
								
								<div class="col-5">
									<label for="ricov12mesi">Ricovero ultimi 12 mesi</label>
									<select class="colorful-select dropdown-primary text-center border_custom_select search attesa trattamento" id="ricov12mesi" style="width:100%"></select>
								</div>
							</div>
							                         <!-- Fine terza riga -->
							<br>
							                         <!-- Creazione grid della quarta riga -->
							<div class="row">
								<div class="form-group col">
									<label for="perTrattamento">Periodo di trattamento</label>
									<textarea class="form-control" id="perTrattamento" rows="2"></textarea>
								</div>
                            </div> 
                                                     <!-- Fine quarta riga -->
							<br>
							                         <!-- Creazione grid della quinta riga -->
							<div class="row">
								<div class="form-group col">
									<label for="obRiabRagg">Obiettivi Riabilitativi Raggiunti</label>
									<textarea class="form-control" id="obRiabRagg" rows="1"></textarea>
								</div>
							
                            </div>  
                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->

		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="C_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Indicazioni</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" style="display: block;overflow-x: auto;white-space: nowrap; height:auto;" id="Table_IND">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataFAM">Data FAM</th>
															<th class="align-middle" id="IndicFam">IndicFAM</th>
															<th class="align-middle" id="DataScuo">DataScuo</th>
															<th class="align-middle" id="IndicScuo">IndicScuo</th>
															<th class="align-middle" id="OperatoreScuo">OperatoreScuo</th>
															<th class="align-middle" id="DataAltri">Data Altri</th>
															<th class="align-middle" id="IndicAltri">IndicAltri</th>
															<th class="align-middle" id="OperatoreAltri">OperatoreAltri</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count">
															<td class="align-middle">
																<input type="text" id="DataFam_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="IndicFam_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<input type="text" id="DataScuo_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="IndicScuo_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="OperatoreScuo_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<input type="text" id="DataAltri_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="IndicAltri_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="OperatoreAltri_1" rows="2"></textarea>
															</td>
														</tr>

														<tr>
															<td colspan="8"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>

<div style="height: 100px;"></div>

        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- JQGrid JS -->
<script 	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');
		
        new WOW().init();
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	var stato = '<?php echo $stato?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

		//effettuo la query per l'input inviante e creo la relativa option
        var query_inviante = <?php echo selectAll('inviante','Inviante as inviante,Id');?>;
		creaOption('sezioni',query_inviante);

		//effettuo la query per l'input strutture provenienza e creo la relativa option
        var query_strProvenienza = <?php echo selectAll('struttureprovenienza','StruttureProvenienza,Id');?>;
		creaOption('sezioni',query_strProvenienza);

		//effettuo la query per l'input condizioni di vita e creo la relativa option
        var query_CondCondiVita = <?php echo selectAll('condizionidivita','CondCondiVita as condVita,Id');?>;
		creaOption('sezioni',query_CondCondiVita);

		//effettuo la query per l'input ricovero ultimi 12 mesi e creo la relativa option
        var query_ric12Mesi = <?php echo selectAll('ricov12mesi','Ricov12mesi as ricov12mesi,Id');?>;
		creaOption('sezioni',query_ric12Mesi);


		

		
    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}


    		$('.mdb-select').material_select();
    		$('.search').select2();           

        	var tab_sezC = '<?php echo $tab_sezC; ?>';

        	var num_righe = '<?php echo $num_tabella; ?>';

    		var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    		var sezione = '<?php if($sezioneC!=""){echo $sezioneC;}else{echo "0";} ?>';
    		
    		 if(tab_sezC!=""){

	        		for(var i=2;i<num_righe;i++) {
	        			addNewRows();	
	        		}
	        		
	        		riempiElementi("C_section","input",jQuery.parseJSON(tab_sezC));

					riempiElementi("C_section","textarea",jQuery.parseJSON(tab_sezC));

					riempiElementi("C_section","select",jQuery.parseJSON(tab_sezC));
					
        		}
    		
    		if(rowData1!="0"){

        		//se la tabella non � stata precedentemente compilata, faccio la query!

           		if(tab_sezC=="") {
        		
    			//effettuo la query per la tabella relativa agli esami strumentali
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : 'cercaIndicazioni',
						'codProg': codProj
					},
					success : function(response) {
 						var exaStrum = jQuery.parseJSON(response);
						
						var countES = exaStrum.length;
						
						riempiValutazione("Table_IND", exaStrum, countES);
						
					},
					error : function() {
						alert("error");
					}
				});

           		}

           		
               	var obj = jQuery.parseJSON(rowData1);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

            	riempiElementi('sezioni','textarea',obj,"1");

           	}

    		if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

           	} 

		//inizializzo il datepicker
    	$('.datepicker').pickadate({

    		  today: 'Oggi',
    		  clear: '',
    		  close: 'Chiudi',
    		  format: 'dd/mm/yyyy'
        });


    	$('#condVita').change(function() {
    		if($('#condVita').val()=="8"){
    			$('#altroCondVita').prop('disabled', false);
    			$("#altroCondVita").addClass( "check" );
    			$('label[for=altroCondVita]').addClass( "prova" );
    		} else {
    			$('#altroCondVita').prop('disabled', true);
    			$("#altroCondVita").removeClass( "check" );
    			$('label[for=altroCondVita]').removeClass( "prova" );
    			$('#altroCondVita').val("");
    		}
    	});

    	$('#inviante').change(function() {
    		if($('#inviante').val()=="4"){
    			$('#altroInviante').prop('disabled', false);
    			$("#altroInviante").addClass( "check" );
    			$('label[for=altroInviante]').addClass( "prova" );
    		} else {
    			$('#altroInviante').prop('disabled', true);
    			$("#altroInviante").removeClass( "check" );
    			$('label[for=altroInviante]').removeClass( "prova" );
    			$('#altroInviante').val("");
    		}
    	});

    	//controllo lo stato e visualizzo i campi obbligatori in base al suo valore
 		if(vecchioPr=="0"){
			check_campi(stato);
 		}

   	
    	//chiudo lo spinner
    	$.LoadingOverlay("hide");  	

    });


 function addNewRows() {


      	  var rowCount = $('.tab_count').length + 1;
      		
      	   $('#Table_IND tr:last').after(


				'<tr id="'+rowCount+'" class="tab_count">'+
					'<td class="align-middle">'+
						'<input type="text" id="DataFam_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
					'</td>'+
					'<td class="align-middle">'+
						'<textarea class="form-control" id="IndicFam_'+rowCount+'" rows="2"></textarea>'+
					'</td>'+
					'<td class="align-middle">'+
						'<input type="text" id="DataScuo_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
					'</td>'+
					'<td class="align-middle">'+
						'<textarea class="form-control" id="IndicScuo_'+rowCount+'" rows="2"></textarea>'+
					'</td>'+
					'<td class="align-middle">'+
						'<textarea class="form-control" id="OperatoreScuo_'+rowCount+'" rows="2"></textarea>'+
					'</td>'+
					'<td class="align-middle">'+
						'<input type="text" id="DataAltri_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
					'</td>'+
					'<td class="align-middle">'+
						'<textarea class="form-control" id="IndicAltri_'+rowCount+'" rows="2"></textarea>'+
					'</td>'+
					'<td class="align-middle">'+
						'<textarea class="form-control" id="OperatoreAltri_'+rowCount+'" rows="2"></textarea>'+
					'</td>'+
      	 '</tr>');

      	   $('#Table_IND tr:last').after(
      			'<tr>'+
      			'<td colspan="8"></td>'+
      			'</tr>'
      	   );

      	//inizializzo il datepicker
       	$('.datepicker').pickadate({

       		  today: 'Oggi',
       		  clear: '',
       		  close: 'Chiudi',
       		  format: 'dd/mm/yyyy'
           }); 
      }

        
    </script>
</html>