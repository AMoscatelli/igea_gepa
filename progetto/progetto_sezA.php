<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

$action = 0;
$nome = "";
$codProg = "";
$cognome = "";
$cognome1 = "";
$nome1 = "";
$sezioneA = "";
$codiceProgetto = "";
$tab_sezA = "";
$jObj = "";
$codAnagr = "";
$num_tabella = "";
if(isset($_SESSION['vecchioPr'])){
    $vecchioPr = $_SESSION['vecchioPr'];
} else {
    $vecchioPr="";
}

if (isset($_GET['result'])) {
    $result = $_GET['result'];
    if ($result == "1") {
        cancella();
    }
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET['codAnag'])) {
        
        $nome = $_GET['nome'];
        $codAnagr = $_GET['codAnag'];
        $cognome = $_GET['cognome'];
        $action = 4;
        $_SESSION['set_sectionA'] = "";
    } else if (isset($_GET['CodProj'])) {
        $action = 2;
        $nome = '';
        $codProg = $_GET['CodProj'];
        $cognome = '';
    } else if (isset($_GET['buttonPr'])){
        $source = $_GET['buttonPr'];
        switch ($source) {
            case 'nuovo':
                $action = 1;
                $_SESSION['set_sectionA'] = "";
                cancella();
                break;
                
        }
        
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $source = $_POST['buttonPr'];
    switch ($source) {
        case 'nuovo':
            $action = 1;
            $_SESSION['set_sectionA'] = "";
            break;
        case 'cerca':
            $action = 2;
            $nome = $_POST['cerca_nome'];
            $codProg = $_POST['cerca_codProg'];
            $cognome = $_POST['cerca_cognome'];
            break;
    }
}

if (isset($_SESSION['set_sectionA'])) {
    if ($_SESSION['set_sectionA'] !== "") {
        $section = $_SESSION['set_sectionA'];
        
        $sezioneA = json_encode((object) $section);
        if (isset($section['codAnag'])) {
            $codAnagr = $section['codAnag'];
        }
        $nome1 = $_SESSION['nome'];
        $cognome1 = $_SESSION['cognome'];
        
        $codiceProgetto = $_SESSION['codiceProgetto'];
        //$vecchioPr = $_SESSION['vecchioPr'];
        // if($vecchioPr==1){
        $action = 3;
        // } else {
        // $action = 1;
        // }
    }
}

if (isset($_SESSION['Table_VF'])) {
    if ($_SESSION['Table_VF'] !== "") {
        $tab = $_SESSION['Table_VF'];
        
        $tab_sezA = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_VF'])) {
            $num_tabella = $_SESSION['num_Table_VF'];
        }
        
    }
}

?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="../css/jquery-ui.min.css">
<link rel="stylesheet"
	href="../css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />




</head>


<body>

	<!--Main Navigation-->
	<header>

		<!-- Standard TOP Nav BAR -->
	    <!-- <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav> -->
	    <div id="navBar"></div>
	</header>

	<!-- Definisco la modal di richiesta salvataggio Progetto utilizzando la classe modal.js -->
	<div id="salvaProgetto"></div>


	<!-- Definisco la modal per la ricerca del progetto -->

	<div class="modal fade show" id="centralModalWarningDemo" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel"padding-right: 17px;">
		<form method="post">
			<div class="modal-dialog modal-notify modal-info" role="document">
				<!--Content-->
				<div class="modal-content">
					<!--Header-->
					<div class="modal-header">
						<p class="heading">Cerca Progetto</p>

						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true" class="white-text">�</span>
						</button>
					</div>

					<!--Body-->
					<div class="modal-body">
						<div class="row">
							<div class="col-4">
								<span class="align-middle"> <img src="../img/lente.png"
									class="img-fluid z-depth-1-half rounded-circle modal_custom_cercaProgetto"></img>
								</span>
							</div>
							<div class="col-8">
								<div class="row text-center mr-2 ml-2">
									<p class="card-text">
										<strong>Per cercare il progetto, inserisci il nome o il
											cognome del paziente o il codice progetto</strong>
									</p>
								</div>
								<div class="row mt-2  mr-1 ml-1">
									<div class="col">
										<label for="cerca_nome">Nome</label> <input type="text"
											class="form-control text-center" id="cerca_nome"
											name="cerca_nome"></input>
									</div>
									<div class="col">
										<label for="cerca_nome">Cognome</label> <input type="text"
											class="form-control text-center" id="cerca_cognome"
											name="cerca_cognome"></input>
									</div>
								</div>
								<div class="row mt-2 mr-2 ml-2">
									<label for="cerca_codProg">Codice Progetto</label> <input
										type="text" class="form-control text-center"
										id="cerca_codProg" name="cerca_codProg"></input>
								</div>

							</div>
						</div>
					</div>


					<!--Footer-->
					<div class="modal-footer justify-content-center">

						<button type="submit" name="buttonPr"
							class="btn btn-info waves-effect waves-light" value="cerca">Cerca</button>
						<button type="button" class="btn btn-outline-info waves-effect"
							data-dismiss="modal">Chiudi</button>

					</div>
				</div>
				<!--/.Content-->
			</div>
		</form>
	</div>

	<!-- Fine MODAL -->

	<div class="container">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center">

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
						<form method="post">
							<!--Facebook-->
							<button type="button" class="btn btn-default" id="backBtn"
								onclick="gestisciIndietro();" style="display: none">
								<i class="fa fa-arrow-circle-left left"> INDIETRO</i>
							</button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
							<button type="submit" name="source" value=""
								class="btn btn-default" onclick="" style="display: none">
								<i class="fa fa-print left"> STAMPA MESE</i>
							</button>
							<button type="button" class="btn btn-default" id="salvaBtn"
								onclick="salvaDati('sezioni','A');"
								style="display: none">
								<i class="fa fa-save left"> SALVA</i>
							</button>
							<button type="button" class="btn btn-default" id="eliminaBtn"
								onclick="" style="display: none">
								<i class="fa fa-trash left"> ELIMINA</i>
							</button>
							<button type="button" class="btn btn-default" id="modificaBtn"
								onclick="abilitaCampi();" style="display: none">
								<i class="fa fa-edit left"> MODIFICA</i>
							</button>
							<button type="submit" name="buttonPr" value="nuovo"
								class="btn btn-default" id="nuovoPrBtn" onclick=""
								style="display: none">
								<i class="fa fa-plus left"> NUOVO PROGETTO</i>
							</button>
							<button type="button" name="buttonPr" class="btn btn-default"
								id="cercaPrBtn" onclick="$('#centralModalWarningDemo').modal();"
								style="display: none">
								<i class="fa fa-search left"> CERCA PROGETTO</i>
							</button>
							<button type="button" name="buttonPr" value="mostra"
								class="btn btn-default" id="mostraPrBtn"
								onclick="mostraProgetti();" style="display: none">
								<i class="fa fa-eye left"> MOSTRA PROGETTO</i>
							</button>
							<span class="counter" id="counterPr" style="display: none">0</span>
							<button type="button" name="buttonPr" value="anagr"
								class="btn btn-default" id="mostraAnagr"
								onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');"
								style="display: none">
								<i class="fa fa-eye left"> MOSTRA ANAGRAFICA</i>
							</button>
						</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>

		<!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="rowNominativo" style="display: none">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1"></div>
						<div class="col-2">
							<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-center"
								id="nominativo_cognome" readonly>
						</div>

						<div class="col-4">
							<input type="text" class="form-control text-center"
								id="nominativo_nome" readonly>
						</div>
						<div class="col-1"></div>
					</div>
				</div>
		
		</section>


		<!--Section: Sezioni-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			style="display: none" id="sezioni">
			<!--Grid row-->
			<div class="row">
				<div class="col-md-12">

					<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<!-- <li class="nav-item"><a class="nav-link waves-light active" href="#" role="tab">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#" onclick="mostraToast();" role="tab">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#" onclick="mostraToast();" role="tab">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#" onclick="mostraToast();" role="tab">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#" onclick="mostraToast();" role="tab">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#" onclick="mostraToast();" role="tab">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="mostraToast();" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="mostraToast();" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="mostraToast();" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="mostraToast();" role="tab" href="#">Cartella Clinica</a></li>
							 -->
							<li class="nav-item"><a class="nav-link waves-light active"
								href="#" role="tab">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#"
								onclick="recuperaElementi('sezioni','A','B','<?php echo $action ?>');"
								role="tab">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#"
								onclick="recuperaElementi('sezioni','A','C','<?php echo $action ?>');"
								role="tab">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#"
								onclick="recuperaElementi('sezioni','A','D','<?php echo $action ?>');"
								role="tab">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#"
								onclick="recuperaElementi('sezioni','A','E','<?php echo $action ?>');"
								role="tab">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" href="#"
								onclick="recuperaElementi('sezioni','A','F','<?php echo $action ?>');"
								role="tab">Sezione D Indici Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','A','G','<?php echo $action ?>',$('#durataProgetto').val(),$('#feste').val(),$('#domenica').val(),$('#modInt').val(),$('#impRiab').val(),$('#freqSett1').val());"
								role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','A','H','<?php echo $action ?>');"
								role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','A','I','<?php echo $action ?>');"
								role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','A','M','<?php echo $action ?>');"
								role="tab" href="#">Cartella Clinica</a></li>

						</ul>
					</div>

					<!-- Tab panels -->
					<div class="tab-content card">

						<!--Panel 1-->
						<div class="tab-pane fade in show active" id="progetto_sezA"
							role="tabpanel">

							<!-- Creazione grid della prima riga -->

							<div class="row">
								<div class="col">
									<!-- Codice progetto input -->
									<label for="codProg">Codice Progetto</label> <input type="text"
										class="form-control text-center" data-toggle="tooltip"
										data-placement="top"
										title="Il campo si compila automaticamente" id="codProg"
										readonly>
								</div>
								<div class="col">
									<!-- Vecchia chiave input -->
									<label for="oldKey">Vecchia Chiave</label> <input type="text"
										class="form-control text-center" id="oldKey" disabled>
								</div>
								<div class="col">
									<!-- Codice Anagrafica input -->
									<label for="codAnag">Codice Anagrafica</label> <input
										type="text" class="form-control text-center custom-yellow"
										id="codAnag" disabled>
								</div>
								<div class="col-2">
									<!-- Num Cart input -->
									<label for="NumCart">Num Cart</label> <input type="text"
										class="form-control text-center" id="NumCart" disabled>
								</div>
								<div class="col-2" id="statoDiv">
									<!-- Stato input -->
									<label for="stato">Stato</label> <select
										class="mdb-select colorful-select dropdown-primary text-center border_custom_select"
										id="stato" name="stato">
									</select>
								</div>
								<div class="col">
									<!-- Data Stato input -->
									<label for="dateStato">Data Stato</label> 
									<input type="text" id="dateStato" class="form-control datepicker text-center dateFormat custom-yellow trattamento">
								</div>

<!-- 								<div class="col"> -->
<!-- 									<div class="form-check checkbox-teal"> -->
<!-- 										<input type="checkbox" class="form-check-input" id="PdaAg" -->
<!-- 											disabled> <label class="form-check-label" for="PdaAg">PdaAg</label> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="col"> -->
<!-- 									<div class="form-check checkbox-teal"> -->
<!-- 										<input type="checkbox" class="form-check-input" id="PAgto" -->
<!-- 											disabled> <label class="form-check-label" for="PAgto">PAgto</label> -->
<!-- 									</div> -->
<!-- 								</div> -->
								<div>
									<input type="text" class="form-control text-center"
										id="nuovoPr" style="display: none"> <input type="text"
										class="form-control text-center" id="vecchioPr"
										style="display: none">
								</div>
							</div>

							<br>

							<!-- Creazione grid nota stato -->
							<div class="row">

								<div class="col">

									<!-- Esame obiettivo Generale textarea -->
									<div class="form-group">
										<label for="notaStato">Nota Stato</label>
										<textarea class="form-control" id="notaStato" rows="1"
											></textarea>
									</div>

								</div>

							</div>

							<br>

							<!-- Creazione grid della seconda riga -->


							<div class="row">
								<div class="col">
									<!-- Data Inizio input-->
									<label for="dataInizio">Data inizio</label> <input type="text"
										id="dataInizio"
										class="form-control datepicker text-center dateFormat enableTrue trattamento"
										>
								</div>
								<div class="col">
									<!-- Durata Progetto input -->
									<label for="durataProgetto">Durata Progetto</label> <input
										type="text" class="form-control text-center number-only trattamento"
										id="durataProgetto" >
								</div>
								<div class="col">
									<!-- Data Fine input -->
									<label for="dataFine">Data Fine</label> <input type="text"
										class="form-control text-center dateFormat custom-yellow trattamento"
										id="dataFine" disabled>
								</div>
								<div class="col-sm-3" id="interventoDiv">
									<!-- Intervento input -->
									<label for="intervento">Intervento</label> <select
										class="mdb-select colorful-select dropdown-primary border_custom_select attesa"
										id="intervento" ></select>
								</div>
								<div class="col" id="modIntDiv">
									<!-- Modalit� intervento input -->
									<label for="modInt">Modalit&agrave; intervento</label> <select
										class="mdb-select colorful-select dropdown-primary border_custom_select trattamento"
										id="modInt" ></select>
								</div>
							</div>

							<br>

							<!-- Creazione grid della terza riga -->
							<div class="row">

								<div class="col" id="regAssistDiv">
									<!-- Regassist input -->
									<label for="regAssist">Regassist</label> <select
										class="mdb-select colorful-select dropdown-primary border_custom_select attesa trattamento"
										id="regAssist" ></select>
								</div>

								<div class="col" id="impRiabDiv">
									<!-- Impegno Riabilitativo input -->
									<label for="impRiab">Impegno Riabilitativo</label> <select
										class="mdb-select colorful-select dropdown-primary border_custom_select trattamento"
										id="impRiab" ></select>
								</div>

								<div class="col-3" id="medicoDiv">
									<!-- Medico input -->
									<label for="medico">Medico</label> <select
										class="mdb-select colorful-select dropdown-primary border_custom_select attesa trattamento"
										id="medico" >
									</select>
								</div>
							</div>

							<br>

							<!-- Creazione grid della quarta riga -->
							<div class="row">

								<div class="col" id="terapista1Div">
									<!-- Terapista1 input -->
									<label for="terapista1">Terapista 1</label> <select
										class="colorful-select dropdown-primary border_custom_select search"
										style="width: 100%" id="terapista1"></select>
								</div>
								<div class="col-1">
									<label for="freqset1">Frequenza</label> <input type="text"
										class="form-control text-center" id="freqset1" min="0" max="6"
										value="0" >
								</div>
								<div class="col" id="terapista2Div">
									<label for="terapista2">Terapista 2</label> <select
										class="colorful-select dropdown-primary border_custom_select search"
										style="width: 100%" id="terapista2"></select>
								</div>
								<div class="col-1">
									<label for="freqset2">Frequenza</label> <input type="text"
										class="form-control text-center" id="freqset2" min="0" max="6"
										value="0" >
								</div>
								<div class="col" id="terapista3Div">
									<label for="terapista3">Terapista 3</label> <select
										class="colorful-select dropdown-primary border_custom_select search"
										style="width: 100%" id="terapista3"></select>
								</div>
								<div class="col-1">
									<label for="freqset3">Frequenza</label> <input type="text"
										class="form-control text-center number-only" id="freqset3"
										min="0" max="6" value="0">
								</div>
								<div class="col-1">
									<label for="freqSett1">Fr. Tot</label> <input type="text"
										class="form-control text-center custom-yellow trattamento" id="freqSett1"
										min="0" max="6" value="0" disabled>
								</div>

							</div>


							<br>

							<!-- Creazione grid della quinta riga -->
							<div class="row">

								<div class="col">

									<!-- Anamnesi textarea -->
									<div class="form-group">
										<label for="anamnesi">Anamnesi patologica prossima -
											descrizione dell'evento acuto, referti aperti, pratiche
											assicurative</label>
										<textarea class="form-control" id="anamnesi" rows="3" ></textarea>
									</div>

								</div>

							</div>

							<br>

							<!-- Creazione grid della sesta riga -->
							<div class="row">

								<div class="col">

									<!-- Esame obiettivo Generale textarea -->
									<div class="form-group">
										<label for="esameObiettivoG">Esame obbiettivo generale</label>
										<textarea class="form-control" id="esameObbiettivoG" rows="1" ></textarea>
									</div>

								</div>

							</div>

							<br>

							<!-- Creazione grid della settima riga -->
							<div class="row">

								<div class="col">

									<!-- Anamnesi textarea -->
									<div class="form-group">
										<label for="esameObiettivoS">Esame obbiettivo specialistico</label>
										<textarea class="form-control" id="esameObbiettivoS" rows="5"></textarea>
									</div>

								</div>

							</div>

							<br>

							<!-- Creazione grid dell'ottava riga -->
							<div class="row">

								<!-- Definisco colonne vuote per centrare gli altri div-->
								<div class="col"></div>

								<!-- Definisco le colonne centrate per Domeniche, Feste e Password-->
								<div class="col-sm-2">
									<!-- Domeniche input -->
									<label for="domenica" style="color: blue">Domeniche</label> <input
										type="text" class="form-control" id="domenica" disabled>
								</div>
								<div class="col-sm-2">
									<!-- Feste input -->
									<label for="feste">Feste</label> <input type="text"
										class="form-control" id="feste" disabled>
								</div>

								<div class="col-sm-2">
									<!-- Domeniche input -->
									<label for="password">Password</label> <input type="text"
										class="form-control" id="password" disabled>
								</div>

								<!-- Definisco colonne vuote per centrare gli altri div-->
								<div class="col"></div>

							</div>

							<!-- linea di divisione -->
							<hr class="mt-4">
							<!--  -->
						</div>


					</div>
				</div>


			</div>


		</section>
		
		
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="VF_section" style="display:none">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Valutazione Funzionale</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" style="display: block;overflow-x: auto;white-space: nowrap; height:auto;" id="Table_VF">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataVF">Data VF</th>
															<th class="align-middle" id="FaseVF">Fase VF</th>
															<th class="align-middle" id="MedTer">Medico/Terapista</th>
															<th class="align-middle" id="VF">Valutazione Funzionale</th>
															<th class="align-middle" id="Ob">Obiettivi</th>
															<th class="align-middle" id="TP">Tempi Previsti</th>
															<th class="align-middle" id="IntPro">Interventi proposti</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count">
															<td class="align-middle">
																<input type="text" id="DataVF_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<select class="colorful-select dropdown-primary text-center border_custom_select search" style="width: 100%" id="FaseVF_1">
																	<option value="" disabled selected>Seleziona</option>
																	<option value="1">Inizio Intervento</option>
																	<option value="2">Di verifica</option>
																	<option value="3">Finale</option>
																	<option value="4">Consulenza</option>
																</select>
															</td>
															<td class="align-middle">
																<div>
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Terapista_1"></select>
																</div>
																<div class="mt-2">
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Medico_1"></select>
																</div>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="ValutazioneFunzionale_1" rows="2" style="height:auto;"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="Obbiettivi_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="TempiPrevisti_1" rows="2"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="InterventiProposti_1" rows="2"></textarea>
															</td>
														</tr>

														<tr>
															<td colspan="7"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>

		<!--Section: GRID pazienti-->

		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="gridPazienti" style="display: none">
			<table id="grid1">
				<div id="jqGridPazientiPager"></div>
			</table>
		</section>

		<!--Section: GRID progetti-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="gridProgetti" style="display: none">
			<table id="grid2">
				<div id="jqGridProgettiPager"></div>
			</table>
		</section>

		<div style="height: 100px;"></div>

	</div>


</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- JQGrid JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- Modal JS -->
<script type="text/javascript" src="../jsCustom/modal.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>



<script>

var query_terapisti_vf;
var query_medico_vf;

		//inizializzo lo spinner
		$.LoadingOverlay("show");

		HTMLTOOLnumberOnlyClass("number-only");

		$('#nuovoPr').val("0");
		$('#vecchioPr').val("0");
		//inizializzo la navBar
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
        new WOW().init();
        $(document).ready(function () {
        	$('.collapsible-body').css({
        		"display" : "none"
        	});
        	$('#progettoMenuBody').css({
        		"display" : "block"
        	});
        	var action = <?php echo $action;?>; 

        	HTMLTOOLvalidateTextNumber('sezioni');
        	
        	showComponent('cercaPrBtn');
            showComponent('nuovoPrBtn');
            
            if(action==1){
            	hideComponent('cercaPrBtn');
                hideComponent('nuovoPrBtn');
                }
            var sezione = [];
            
			var query_festivita = <?php selectAll('festivita','DataFestivita');?>;
			var query_terapisti = <?php selectAll('terapista','Nominativo as terapista1,ID as Id');?>;
			var query_terapisti1 = <?php selectAll('terapista','Nominativo as terapista2,ID as Id');?>;
			var query_terapisti2 = <?php selectAll('terapista','Nominativo as terapista3,ID as Id');?>;
			query_terapisti_vf = <?php selectAll('terapista','Nominativo as terapista_vf,ID as Id');?>;
			var query_stato = <?php selectAll('stato','Stato as stato,Id');?>;
			var query_intervento = <?php selectAll('intervento','Intervento as intervento,Id');?>;
			var query_regAssist = <?php selectAll('regimeassistenziale','RegAssist as regAssist,Id');?>;
			var query_modInt = <?php selectAll('modalitaintervento','ModalitaIntervento as modInt,Id');?>;
			var query_impRiab = <?php selectAll('impegnoriabilitativo','ImpRiab as impRiab,Id');?>;
			var query_medico = <?php selectAll('medici','`Nome_Cognome` as medico,Id');?>;
			query_medico_vf = <?php selectAll('medici','`Nome_Cognome` as medico_vf,Id');?>;
			

			//creo le option per le select
			
        	//::TERAPISTI
			creaOption('sezioni',query_terapisti);
			creaOption('sezioni',query_terapisti1);
			creaOption('sezioni',query_terapisti2);

			//creo le option per la prima tabella della verifica funzionale
			creaOption_vf(query_terapisti_vf,"1","ter");
			creaOption_vf(query_medico_vf,"1","med");

			
			//::STATO
			creaOption('sezioni',query_stato);

			//::INTERVENTO
			creaOption('sezioni',query_intervento);

			//::REGIME ASSISTENZIALE
			creaOption('sezioni',query_regAssist);

			//::MODALITA D'INTERVENTO
			creaOption('sezioni',query_modInt);

			//::IMPEGNO RIABILITATIVO
			creaOption('sezioni',query_impRiab);

			//::IMPEGNO RIABILITATIVO
			creaOption('sezioni',query_medico);
			
			
			
			//---------------------
			
			$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');
			 
			//inizializzo le select
			$('.search').select2();
			$('.mdb-select').material_select();
			
			//inizializzo il datepicker
        	$('.datepicker').pickadate({

        		  today: 'Oggi',
        		  clear: '',
        		  close: 'Chiudi',
        		  format: 'dd/mm/yyyy'
            }); 
            //inizializzo i tooltip
        	$('[data-toggle="tooltip"]').tooltip();

        	//---------------------
        	
        	// quando viene aggiunto il valore della durata del progetto, automaticamente viene calcolata la data di fine progetto
        	$('#durataProgetto').change(function() {
        		if($('#dataInizio').val()==""){
        			toastr.error('Data di inizio non inserita!');
        		} else {
        		$('#dataFine').val(formatCustomHTMLDate(moment(formatISODate($('#dataInizio').val())).addWorkdays($('#durataProgetto').val(),returnArrayFromJSON(query_festivita),true).format('YYYY-MM-DD'),"/"));
        			if($('#stato').val()=="3"){
        				$('#dateStato').val($('#dataFine').val());
        			}
        		}
        		});

        	//quando la data di inizio viene cambiata, la data di fine progetto viene aggioranta (solo nel caso in cui la durata del progetto � definita)
        	$('#dataInizio').change(function() {
        		if($('#durataProgetto').val()!==""){
        		$('#dataFine').val(formatCustomHTMLDate(moment(formatISODate($('#dataInizio').val())).addWorkdays($('#durataProgetto').val(),returnArrayFromJSON(query_festivita),true).format('YYYY-MM-DD'),"/"));
            		if($('#stato').val()=="3"){
        				$('#dateStato').val($('#dataFine').val());
        			}
        		}
        	});

    		$('#terapista1').change(function(){
    			rimuoviDuplicati(this,["terapista2","terapista3"],[query_terapisti1,query_terapisti2]);
    		});
    		$('#terapista2').change(function(){
    			rimuoviDuplicati(this,["terapista1","terapista3"],[query_terapisti,query_terapisti2]);
    		});
    		$('#terapista3').change(function(){
    			rimuoviDuplicati(this,["terapista1","terapista2"],[query_terapisti,query_terapisti1]);
    		});

		$("#freqset1").bind('keyup mouseup', function () {
// 			if(parseInt($("#freqset1").val())>6){
// 				$("#freqset1").val("6");
// 			}
			var f1 = parseInt($("#freqset1").val()!=""?$("#freqset1").val():0);
            var f2 = parseInt($("#freqset2").val()!=""?$("#freqset2").val():0);
            var f3 = parseInt($("#freqset3").val()!=""?$("#freqset3").val():0);
            $('#freqSett1').val(f1+f2+f3); 
            if(parseInt($("#freqSett1").val())>6){
            	toastr.error('La frequenza settimanale non puo superare i 6 Giorni !!!');
            }           
		});

    	$("#freqset2").bind('keyup mouseup', function () {
//     		if(parseInt($("#freqset2").val())>6){
// 				$("#freqset2").val("6");
// 			}
        	var f1 = parseInt($("#freqset1").val()!=""?$("#freqset1").val():0);
            var f2 = parseInt($("#freqset2").val()!=""?$("#freqset2").val():0);
            var f3 = parseInt($("#freqset3").val()!=""?$("#freqset3").val():0);
            $('#freqSett1').val(f1+f2+f3);	
            if(parseInt($("#freqSett1").val())>6){
            	toastr.error('La frequenza settimanale non puo superare i 6 Giorni !!!');
            }
       	});
        
    	$("#freqset3").bind('keyup mouseup', function () {
//     		if(parseInt($("#freqset3").val())>6){
// 				$("#freqset3").val("6");
// 			}
        	var f1 = parseInt($("#freqset1").val()!=""?$("#freqset1").val():0);
            var f2 = parseInt($("#freqset2").val()!=""?$("#freqset2").val():0);
            var f3 = parseInt($("#freqset3").val()!=""?$("#freqset3").val():0);
            $('#freqSett1').val(f1+f2+f3);
            if(parseInt($("#freqSett1").val())>6){
            	toastr.error('La frequenza settimanale non puo superare i 6 Giorni !!!');
            }
        });	

    	//definisco una funzione temporanea che abilita solo determinati campi in funzione della scelta dell'input "STATO" (temporaneamente disabilitata)
    	
    	$('#stato').change(function(){

        	if($('#stato').val()=="1"){
        		$('#dateStato').val("");
        	} else if($('#stato').val()=="3") {
    			//ricavo la data di oggi
//     			var newDate = new Date();
//     			var anno = newDate.getFullYear();
//     			var mese = newDate.getMonth()+1;
//     			if(mese<"10"){
//     				mese = "0"+mese;
//     			}
//     			var giorno = newDate.getDate();
    			
//     			//assegno alla data stato il giorno oggi
//      			$('#dateStato').val(giorno +"/" +mese + "/"+anno);
            }

        	if($('#dataFine').val()!=""){
        		$('#dateStato').val($('#dataFine').val());
        	}
                	

    		check_campi($('#stato').val());
        	
     	});


       	//riempo i campi se gi� completati
        	 if(action==3){

            	$.LoadingOverlay("show");

                hideComponent('cercaPrBtn');
                hideComponent('nuovoPrBtn');

                var vecchioPr = '<?php echo $vecchioPr; ?>';

				if(vecchioPr!=""){
                	$('#vecchioPr').val(vecchioPr);
				}
                
        		var sezioneA = '<?php echo $sezioneA; ?>';

        		var tab_sezA = '<?php echo $tab_sezA; ?>';

        		var nome = '<?php echo $nome1; ?>';

        		var cognome = '<?php echo $cognome1; ?>';

        		var obj1 = jQuery.parseJSON(sezioneA);

        		var num_righe = '<?php echo $num_tabella; ?>';

        		var codiceProgetto = '<?php echo $codiceProgetto;?>';

				riempiElementi("sezioni","select",obj1);
        		
        		riempiElementi("sezioni","input",obj1);

        		riempiElementi("sezioni","textarea",obj1);
        		
        		var valFunz = tab_sezA;

        		for(var i=2;i<num_righe;i++) {
        			addNewRows();	
        		}

        		if(tab_sezA!=""){
        		riempiElementi("VF_section","input",jQuery.parseJSON(tab_sezA));

        		riempiElementi("VF_section","select",jQuery.parseJSON(tab_sezA));

        		riempiElementi("VF_section","textarea",jQuery.parseJSON(tab_sezA));
        		}

        		if(codiceProgetto!=""){
        			$('#codProg').val(codiceProgetto);
        		}

        		check_campi($('#stato').val());

				//----------------------------------------------//

        		$('#nominativo_cognome').val(cognome);

        		$('#nominativo_nome').val(nome);

        		var obj = <?php if($action==3){echo selectAll('anagrafico','CodAnagr,Nome,Cognome,CodiceFiscale');}else{echo "0";}?>;

        		var query_festivita1 = <?php selectAll('festivita','DataFestivita');?>;

        		var obj2 = <?php if($action==3){echo cercaPaziente('progetto','CodProj as codProg,st.Stato as stato_view,DataInizio as dataInizio,DataFine as dataFine, DataStato as dataStato','CodAnagr',$codAnagr);}else{echo "0";}?>;

        		var codAnagr = '<?php echo $codAnagr; ?>';

        	    JQGRIDinitPazientiGrid(obj, '#grid1', $("#rowButton").innerWidth(),query_festivita1,action);
            	
            	JQGRIDinitProgettiGrid(obj2,'#grid2',$("#rowButton").innerWidth(),codAnagr,query_festivita1);

            	if($('#dataInizio').val()!=""){
            		if($('#durataProgetto').val()!=""){
            			$('#dataFine').val(formatCustomHTMLDate(moment(formatISODate($('#dataInizio').val())).addWorkdays($('#durataProgetto').val(),returnArrayFromJSON(query_festivita),true).format('YYYY-MM-DD'),"/"));
            		}
            	};
            	
				showComponent('sezioni');
				showComponent('VF_section');
            	showComponent('rowNominativo');

            	//console.log(vecchioPr);
            	
            	if(vecchioPr=="1"){
                	showComponent('modificaBtn');
                } else {
        			showComponent('salvaBtn');
        		}

                showComponent('backBtn');
                //$('.mdb-select').material_select();

                $.LoadingOverlay("hide");
            }

        	//chiudo lo spinner
        	$.LoadingOverlay("hide");  	

        });

</script>


<!-- Datepicker files: lingua-->
<script type="text/javascript" src="../js/it_IT.js"></script>

<!-- Funzioni della pagina -->
<script>


//toast temporaneo
function mostraToast(){
	toastr.error('Funzionalit&agrave; momentaneamente non disponibile!');
}


	var action = <?php echo $action;?>; 
	//nella ricerca della modal se compilo un campo l'altro automaticamente si svuota
	$('#cerca_nome').keyup(function() {
	      
	             $('#cerca_codProg').val("");
	      
	 });

	$('#cerca_cognome').keyup(function() {
	      
        $('#cerca_codProg').val("");
 
});
	
	$('#cerca_codProg').keyup(function() {
	      
        $('#cerca_nome').val("");
        $('#cerca_cognome').val("");
 
	});
	// fine ricerca modal	

	if(action == 1){
    	
    	$.LoadingOverlay("show");

    	showComponent('backBtn');
    	showComponent('mostraPrBtn');
        hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
    	
		var obj = <?php if($action==1){echo selectAll('anagrafico','CodAnagr,Nome,Cognome,CodiceFiscale');}else{echo "0";}?>;

		var query_festivita1 = <?php selectAll('festivita','DataFestivita');?>;
    	
    	JQGRIDinitPazientiGrid(obj, '#grid1', $("#rowButton").innerWidth(),query_festivita1);

    	showComponent('gridPazienti');
    	
    	$.LoadingOverlay("hide");
    	
    } else if (action == 2) {

    	$.LoadingOverlay("show");

		var nome = "<?php echo $nome; ?>";

		var cognome = "<?php echo $cognome; ?>";

		var codProg1 = "<?php echo $codProg; ?>";

		if(nome==""&cognome==""&codProg1==""){

			toastr.error('Inserisci nome, cognome o numero progetto!!');

		} else {

		var query_festivita1 = <?php selectAll('festivita','DataFestivita');?>;

		var result = <?php

            if ($action == 2 & $nome != "" | $action == 2 & $codProg != "" | $action == 2 & $cognome != "") {
                cercaPazienteModal();
            } else {
                echo "0";
            }
            ?>;

		if(result.length>0){
			
				if(nome!==""|cognome!==""){
        
        		JQGRIDinitPazientiGrid(result, '#grid1', $("#rowButton").innerWidth(),query_festivita1);
        
        		showComponent('gridPazienti');
        
        		showComponent('mostraPrBtn');
        
        		showComponent('backBtn');
        
        		hideComponent('nuovoPrBtn');
        
        		hideComponent('cercaPrBtn');
        		
        		} else if (codProg1!==""){
        			
        		JQGRIDinitProgettiGrid(result,'#grid2',$("#rowButton").innerWidth(),result[0].codAnagr,query_festivita1,result[0].NomeP,result[0].CognomeP);
        		
        		showComponent('gridProgetti');
        
        		}
		} else {
				// se la query mi restituisce un risultato vuoto mostro il messaggio
				toastr.error('Nessun progetto trovato!');
		}
		}
		$.LoadingOverlay("hide");
		
    } else if(action==3){
        hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
    } else if(action==4){
    	hideComponent('gridPazienti');
		
		hideComponent('mostraPrBtn');
		
		showComponent('sezioni');

		showComponent('VF_section');
		
		showComponent('salvaBtn');
		
		showComponent('rowNominativo');
		
		$('#nominativo_nome').val("<?php echo $nome; ?>");
		
		$('#nominativo_cognome').val("<?php echo $cognome; ?>");
		
		$('#codAnag').val("<?php echo $codAnagr; ?>");
		
		hideComponent('counterPr');
		
		

		$('#nuovoPr').val("1");
    }


    function addNewRows() {


    	  var rowCount = $('.tab_count').length + 1;
    		
    	   $('#Table_VF tr:last').after(


			'<tr id="'+rowCount+'" class="tab_count">' +
			'<td class="align-middle">'+
				'<input type="text" id="DataVF_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
			'</td>'+
			'<td class="align-middle">'+
				'<select class="colorful-select dropdown-primary text-center border_custom_select search" style="width: 100%" id="FaseVF_'+rowCount+'">'+
					'<option value="" disabled selected>Seleziona</option>'+
					'<option value="1">Inizio Intervento</option>'+
					'<option value="2">Di verifica</option>'+
					'<option value="3">Finale</option>'+
					'<option value="4">Consulenza</option>'+
				'</select>'+
			'</td>'+
			'<td class="align-middle">'+
				'<div>'+
					'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Terapista_'+rowCount+'"></select>'+
				'</div>'+
				'<div class="mt-2">'+
					'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Medico_'+rowCount+'"></select>'+
				'</div>'+
			'</td>'+
			'<td class="align-middle">'+
				'<textarea class="form-control" id="ValutazioneFunzionale_'+rowCount+'" rows="2"></textarea>'+
			'</td>'+
			'<td class="align-middle">'+
				'<textarea class="form-control" id="Obbiettivi_'+rowCount+'" rows="2"></textarea>'+
			'</td>'+
			'<td class="align-middle">'+
				'<textarea class="form-control" id="TempiPrevisti_'+rowCount+'" rows="2"></textarea>'+
			'</td>'+
			'<td class="align-middle">'+
				'<textarea class="form-control" id="InterventiProposti_'+rowCount+'" rows="2"></textarea>'+
			'</td>'+
		'</tr>');


    	   $('#Table_VF tr:last').after(
				'<tr>'+
				'<td colspan="7"></td>'+
				'</tr>'
    	   );


    	 //creo le option per le select appena create!
			creaOption_vf(query_terapisti_vf,rowCount,"ter");
			creaOption_vf(query_medico_vf,rowCount,"med");
    	   
    	 //inizializzo le select
			$('#Terapista_'+rowCount).select2();
			$('#Medico_'+rowCount).select2();
			$('#FaseVF_'+rowCount).select2();

			//inizializzo il datepicker
        	$('#DataVF_'+rowCount).pickadate({

        		  today: 'Oggi',
        		  clear: '',
        		  close: 'Chiudi',
        		  format: 'dd/mm/yyyy'
            }); 
      }

    function resetVariabili(){
    	
    	$.ajax({
    		type : 'post',
    		url : '../php/controller.php',
    		data : {
    			'source' : 'cancellaVariabile'
    		},
    		success : function(response) {
        		//alert("resetEffettuato!");
    		},
    		error : function() {
    			alert("error");
    		}
    	});
    }



</script>


</html>