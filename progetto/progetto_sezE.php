<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$sezioneE = "";
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];
$vecchioPr =  $_SESSION['vecchioPr'];
$tab_pat1 = "";
$num_pat1 = "";
$tab_pat2 = "";
$num_pat2 = "";

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['dataInizio'])){
            $dataInizio =  $section2['dataInizio'];
        } else {
            $dataInizio ="";
        }
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
        if(isset($section2['codProg'])){
            $codProj = $_SESSION['set_sectionA']['codProg'];
        } else  {
            $codProj = "";
        }
        
        
    }
}

if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
        $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionE'])){
    $action=3;
}

if(isset($_SESSION['set_sectionE'])){
    if($_SESSION['set_sectionE']!==""){
        $section = $_SESSION['set_sectionE'];
        $sezioneE = json_encode((object)$section);
    }
}

if (isset($_SESSION['Table_PAT1'])) {
    if ($_SESSION['Table_PAT1'] !== "") {
        $tab = $_SESSION['Table_PAT1'];
        
        $tab_pat1 = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_PAT1'])) {
            $num_pat1 = $_SESSION['num_Table_PAT1'];
        }
        
    }
}

if (isset($_SESSION['Table_PAT2'])) {
    if ($_SESSION['Table_PAT2'] !== "") {
        $tab1 = $_SESSION['Table_PAT2'];
        
        $tab_pat2 = json_encode((object) $tab1);
        
        if(isset($_SESSION['num_Table_PAT2'])) {
            $num_pat2 = $_SESSION['num_Table_PAT2'];
        }
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>
   
   <!--Main Navigation-->
    <header>

		  <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>

    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','E');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','E','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezE" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
									<div class="col-2 text-left mt-2">
										<label for="CodprotOrtAusili">Cod. Prot. Ort. Ausili</label>
									</div>
									<div class="col-2">
										<!-- Date picker data valutazione iniziale -->
										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select attesa trattamento" id="CodprotOrtAusili">
										  <option value="" disabled selected>Seleziona</option>
                                          <option value="1">SI</option>
                                          <option value="2">NO</option>
                                         </select>
									</div>
								</div> 
								<br>                                           
                            <div class="row mt-3">								  
								  	<div class="col-4 text-left">
								  	<label><font size="2">Apparecchi per la funzione respiratoria</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P1" class="P" disabled> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>
								  	<div class="col-4 text-left">
								  	<label><font size="2">Ortesi per il tronco</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P6" class="P" disabled> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>

								  </div>
								<!-- Fine prima riga -->
								<!-- Creazione grid della seconda riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Carrozzine ed altri ausili per la deambulazione</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P2" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Sistemi posturali</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P7" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine seconda riga -->
								<!-- Creazione grid della terza riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Ausili addominali</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P3" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Apparecchi e ortesi per l'arto</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P8" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine terza riga -->
								<!-- Creazione grid della quarta riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Ausili per la comunicazione, informazione</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P4" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Ausili e protesi ottiche e acustiche</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P9" class="P" disabled> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine quarta riga -->
								<!-- Creazione grid della quinta riga -->
                            <div class="row mt-3">
								  
								  	<div class="col-4 text-left">
								  	<label><font size="2">Ausili per la cura e protezione personale</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P5" class="P" disabled> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>
								  	<div class="col-4 text-left">
								  	<label><font size="2">Altro</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="P10" class="P" disabled> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>

								  </div>
								<!-- Fine quinta riga -->  
													      
							<br>
							                         <!-- Creazione grid della riga progetto-->
							<!-- <div class="row">	
								<div>
								 <input type="text" class="form-control text-center" id="nuovoPr" style="display:none">
								 <input type="text" class="form-control text-center" id="vecchioPr" style="display:none">
								</div>
							</div>-->
											       <!-- Fine riga progetto-->
                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
               <!--Grid row-->

            </section>
            

			<!--Sezione tabella Patologie oggetto-->
			<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="E_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Patologie Oggetto o Altra patologie</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" id="Table_PAT1">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataInizioPatologia" style="color:red">Data Inizio Patologia</th>
															<th class="align-middle" id="CodICD9" style="color:red">Descrizione CodICD9</th>
															<th class="align-middle" id="CodICD9value">CodICD9</th>
															<th class="align-middle" id="AltraPatologia">Altra patologia</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count_pat1">
															<td class="align-middle">
																<input type="text" id="DataInizioPatologia_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="CodICD9_1"></select>
															</td>
															<td class="align-middle">
																<input type="text" class="form-control text-center" id="CodICD9value_1">
															</td>
															<td class="align-middle">
                                                                <div class="form-check">
                                                                  <input type="checkbox" class="form-check-input" id="AltraPatologia_1">
                                                                  <label class="form-check-label" for="AltraPatologia_1"> </label>
                                                                </div>
															</td>											
														</tr>

														<tr>
															<td colspan="4"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>

			<!--Sezione tabella Patologie oggetto 2-->
			<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="E1_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">ProfiloDisabN</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows_pat2()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->


										<!--  defisco le option -->
										<div class="mb-2 mt-2" id="dominio" style="display:none">
											<div>DOMINIO:</div>
											 <hr>
    										<!-- Default inline 1-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input dominio" id="b" name="inlineDefaultRadiosExample" value="b">
                                              <label class="custom-control-label" for="b">B - Funzioni Corporee</label>
                                            </div>
                                            
                                            <!-- Default inline 2-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input dominio" id="d" name="inlineDefaultRadiosExample" value="d">
                                              <label class="custom-control-label" for="d">D - Attivit&aacute; e Partecipazione</label>
                                            </div>
                                            
                                            <!-- Default inline 3-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input dominio" id="e" name="inlineDefaultRadiosExample" value="e">
                                              <label class="custom-control-label" for="e">E - Fattori Ambientali</label>
                                            </div>
                                            
                                            <!-- Default inline 4-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input dominio" id="s" name="inlineDefaultRadiosExample" value="s">
                                              <label class="custom-control-label" for="s">S - Strutture Corporee</label>
                                            </div>
                                            
                                               <hr>
                                        </div>
                                        
                                     	
										<!-- Div per la tabella per il dominio B -->
										<div class="px-4">

											<div class="table-wrapper" >
												<table class="table table-hover mb-4 text-center table-bordered tabelle" style="display: block;overflow-x: auto;white-space: nowrap; height:auto;"  id="Table_PAT2">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="Dom">Dominio</th>
															<th class="align-middle" id="ICF">ICF</th>
															<th class="align-middle" id="ICFQual1">ICF Qual1</th>
															<th class="align-middle" id="ICFQual2">ICF Qual2</th>
															<th class="align-middle" id="ICFQual3">ICF Qual3</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="pat1" class="tab_count_pat2">
															<td class="align-middle">
																<select class="colorful-select dropdown-primary border_custom_select search dominio" id="Dominio_1" style="width:100%">
                                    									<option value="null" disabled selected>Seleziona</option>
                                    									<option value="b">Funzioni Corporee</option>
                                                                        <option value="d">Attivit&aacute; e Partecipazione</option>
                                                                        <option value="e">Fattori Ambientali</option>
                                                                        <option value="s">Strutture Corporee</option>
                            									</select>
                            								</td>
															<td class="align-middle">
																<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICF_1"></select>
															</td>
															<td class="align-middle">
																<div>
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual1_1"></select>
																</div>
																<div class="mt-2">
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF1_1"></select>
																</div>
															</td>
															<td class="align-middle">
																<div>
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual2_1"></select>
																</div>
																<div class="mt-2">
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF2_1"></select>
																</div>
															</td>
															<td class="align-middle">
																<div>
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual3_1"></select>
																</div>
																<div class="mt-2">
																	<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF3_1"></select>
																</div>
															</td>
														</tr>

														<tr>
															<td colspan="5"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>
		
		<div style="height: 100px;"></div>

        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- PROGETTO CUSTOM JS per la sezioneE -->
<script type="text/javascript" src="../progetto/js/progetto_sezE.js"></script>
<!-- JQGrid JS -->
<script 	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		var query_codiICD9 = <?php selectAll('Icd9CmN','ID1 as Id,descrizione,codice');?>;

		var query_ICF = <?php selectAll('ICF','ICFDes,ICF');?>;

		var query_qual1 = <?php selectAll('Qual1','QualDes,Qual1,D,Id');?>;

		var query_qual2 = <?php selectAll('Qual2','QualDes,Qual2,D,Id');?>;

		var query_qual3 = <?php selectAll('Qual3','QualDes,Qual3,D,Id');?>;
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	var stato = '<?php echo $stato?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);
		
    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}

        creaOption_pat1(query_codiICD9,"1");

    		$('.mdb-select').material_select();
    		$('.search').select2();

        	var tab_pat1 = '<?php echo $tab_pat1; ?>';

        	var num_righe_pat1 = '<?php echo $num_pat1; ?>';

        	var tab_pat2 = '<?php echo $tab_pat2; ?>';

        	var num_righe_pat2 = '<?php echo $num_pat2; ?>';

    		var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "";} ?>';

    		var sezione = '<?php if($sezioneE!=""){echo $sezioneE;}else{echo "";} ?>';

    		if(tab_pat1!=""){
        		for(var i=2;i<num_righe_pat1;i++) {
        			addNewRows();	
        		}
        		
        		riempiElementi("E_section","input",jQuery.parseJSON(tab_pat1));

				riempiElementi("E_section","textarea",jQuery.parseJSON(tab_pat1));

				riempiElementi("E_section","select",jQuery.parseJSON(tab_pat1));
        	}
    		if(tab_pat2!=""){
        		for(var i=2;i<num_righe_pat2;i++) {
        			addNewRows_pat2();	
        		}
        		
        		riempiElementi("E1_section","input",jQuery.parseJSON(tab_pat2));

				riempiElementi("E1_section","textarea",jQuery.parseJSON(tab_pat2));

				riempiElementi("E1_section","select",jQuery.parseJSON(tab_pat2));
    		}

   		

    		if(rowData1!=""){        		

    			if(tab_pat1==""&tab_pat2==""){
	    			//effettuo la query per la tabella relativa al profilo disabilit�
	 				$.ajax({
	 					type : 'post',
	 					url : '../php/controller.php',
	 					data : {
	 						'source' : 'cercaProfilo',
	 						'codProg': codProj
	 					},
	 					success : function(response) {
	  						var exaStrum = jQuery.parseJSON(response);
							
	 						var countES = exaStrum.length;
							
	 						//riempiValutazione("Table_PAT2", exaStrum, countES);

	 		        		for(var i=1;i<countES;i++) {
	 		        			addNewRows_pat2();	
	 		        		}
	 		        		
	 		        		riempiTabellePAT("Table_PAT2", exaStrum, countES);
	 		        		

							},
	 					error : function() {
	 						alert("error");
	 					}
	 				});

	    			//effettuo la query per la tabella relativa alle patologie oggetto
	 				$.ajax({
	 					type : 'post',
	 					url : '../php/controller.php',
	 					data : {
	 						'source' : 'cercaPatologie',
	 						'codProg': codProj
	 					},
	 					success : function(response) {
	  						var exaStrum = jQuery.parseJSON(response);
							
	 						var countES = exaStrum.length;
							
	 						//riempiValutazione("Table_PAT1", exaStrum, countES);

	 			    		for(var i=1;i<countES;i++) {
	 		        			addNewRows();	
	 		        		}
	 		        		
	 			    		riempiTabellePAT("Table_PAT1", exaStrum, countES);

	 		    			//effettuo la query per la tabella relativa altre patologie
	 		 				$.ajax({
	 		 					type : 'post',
	 		 					url : '../php/controller.php',
	 		 					data : {
	 		 						'source' : 'cercaAltrePatologie',
	 		 						'codProg': codProj
	 		 					},
	 		 					success : function(response) {
	 		  						var exaStrum = jQuery.parseJSON(response);
	 								
	 		 						var countES1 = exaStrum.length;
	 								
	 		 						//riempiValutazione("Table_PAT1", exaStrum, countES);
	 		 						if(countES>=1){
	 		 					  		for(var i=countES;i<countES1+countES;i++) {
		 		 		        			addNewRows();	
		 		 		        		}
	 		 						} else {
	 		 							for(var i=i;i<countES1;i++) {
		 		 		        			addNewRows();	
		 		 		        		}
	 		 						}
	 		 			  
	 		 		        		
	 		 			    		riempiTabellePAT2("Table_PAT1", exaStrum, countES,countES1);
	 								
	 		 					},
	 		 					error : function() {
	 		 						alert("error");
	 		 					}
	 		 				});
							
	 					},
	 					error : function() {
	 						alert("error");
	 					}
	 				});

    			}
	        		

	               	var obj = jQuery.parseJSON(rowData1);
	    
	               	riempiElementi('sezioni','input',obj,"1");
	    
	               	riempiElementi('sezioni','select',obj,"1"); 

	               	riempiElementi('sezioni','textarea',obj,"1");              	

           	} 
    		
    		if (sezione!="") {

         		var obj = jQuery.parseJSON(sezione);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

           	} 

    		//inizializzo il datepicker
        	$('.datepicker').pickadate({

        		  today: 'Oggi',
        		  clear: '',
        		  close: 'Chiudi',
        		  format: 'dd/mm/yyyy'
            }); 
        	
    	//chiudo lo spinner
    	$.LoadingOverlay("hide"); 

    	if($('#CodprotOrtAusili').val()=="1"){
			$(".P").each(function() {
				$(this).prop('disabled', false);
			});
    	}

    	// se viene selezionati SI tutte le checkbox sono selezionabili
    	$('#CodprotOrtAusili').change(function(){
			if($('#CodprotOrtAusili').val()=="1"){
				$('#sezioni input').each(function() {
            				var campo = this.id;
            				if(campo!=""){
                				if($('#' + campo).is(':checkbox')){
            						$('#' + campo).prop('disabled', false);
            					}
            				}
        			});
        	} else {
        			$('#sezioni input').each(function() {
        				var campo = this.id;
        				if(campo!=""){
            				if($('#' + campo).is(':checkbox')){
        						$('#' + campo).prop('disabled', true);
        						$('#' + campo).prop('checked', false);
        					}
        				}
					});
        	}
		});

    	//controllo lo stato e visualizzo i campi obbligatori in base al suo valore
 		if(vecchioPr=="0"){
			check_campi(stato);
 		}

 		
		});

        $('.codice').change(function(){
            	var cella_id = this.id.substring(this.id.indexOf("_")+1,this.id.length);

            	$('#CodICD9value_'+cella_id).val(this.value);

            });

        //verifico che dominio � stato scelto e preparo le option in base al valore scelto
        $('#Dominio_1').change(function(){

        	switch (this.value){
            	case "b": 
                	$("#pat1").css("background-color", "yellow");
                	break;
            	case "d":
            		$("#pat1").css("background-color", "green");
                	break;
            	case "e": 
                	$("#pat1").css("background-color", "red");
                	break;
            	case "s":
            		$("#pat1").css("background-color", "blue");
                	break;
        	}
        	creaOption_pat2(query_ICF,"1",this.value);
        	creaOption_qual(query_qual1,"1",this.value,"1");
        	creaOption_qual(query_qual2,"1",this.value,"2");
        	creaOption_qual(query_qual3,"1",this.value,"3");
        	//alert('cambio');
        });

 
    </script>
</html>