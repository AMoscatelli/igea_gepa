<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];
$codProg="";
$sezioneF = "";
$vecchioPr =  $_SESSION['vecchioPr'];


if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
    $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionF'])){
    $action=3;
}

if(isset($_SESSION['set_sectionF'])){
    if($_SESSION['set_sectionF']!==""){
        $section = $_SESSION['set_sectionF'];
        $sezioneF = json_encode((object)$section);
    }
}

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['codAnag'])){
            $codAnagr =  $section2['codAnag'];
        } else {
            $codAnagr ="";
        }
        if(isset($section2['durataProgetto'])){
            $durataProgetto =  $section2['durataProgetto'];
        } else {
            $durataProgetto ="0";
        }
        if(isset($section2['dataInizio'])){
            $dataInizio =  $section2['dataInizio'];
        } else {
            $dataInizio ="0";
        }
        if(isset($section2['feste'])){
            $feste =  $section2['feste'];
        } else {
            $feste ="0";
        }
        if(isset($section2['domenica'])){
            $domeniche =  $section2['domenica'];
        } else {
            $domeniche ="0";
        }
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>
   
   <!--Main Navigation-->
    <header>

		  <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>

    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','F');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','F','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezF" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
								<div class="col">
									<!-- Indice di Barthell input -->
									<label for="BarthelInizio">Indice Barthell</label> 
									<input type="text" class="form-control text-center number-only trattamento" id="BarthelInizio">
								</div>
								<div class="col">
									<!-- SPMSQ input -->
									<label for="SPMSQInizio">SPMSQ</label> 
									<input type="text" class="form-control text-center trattamento" id="SPMSQInizio">
								</div>
								<div class="col">
									<!-- "SVAM_Lp" input -->
									<label for="SVAM_Lp">SVAMA_Lp</label> 
									<input type="text" class="form-control text-center trattamento" id="SVAM_Lp">
								</div>
								<div class="col">
									<!-- SPMSQ input -->
									<label for="SVAM_Lc">SVAMA_Lc</label> 
									<input type="text" class="form-control text-center trattamento" id="SVAM_Lc">
								</div>
								<div class="col">
									<!-- SVAM_AU input -->
									<label for="SVAM_AU">SVAMA_AU</label> 
									<input type="text" class="form-control text-center trattamento" id="SVAM_AU">
								</div>
								<div class="col">
									<!-- SVAM_V input -->
									<label for="SVAM_V">SVAMA_V</label> 
									<input type="text" class="form-control text-center trattamento" id="SVAM_V">
								</div>
							</div>
							<!-- fine prima riga, inizio seconda -->                                       
             				<div class="row mt-3">
								<div class="col">
								<!-- Scala codificata input -->
									<label for="scalaCod">Scala codificata</label>
								 	<select class="colorful-select dropdown-primary text-center border_custom_select search" id="scalaCod" style="width: 100%"></select>
								</div>
								<div class="col">
								<!-- Scala codificata input -->
									<label for="scalaCod1">Scala codificata1</label>
								 	<select class="colorful-select dropdown-primary text-center border_custom_select search" id="scalaCod1" style="width: 100%"></select>
								</div>
							</div>
							<!-- fine seconda riga, inizio terza --> 
							 <div class="row mt-3">
								<div class="col">
								<!-- Scala codificata input -->
									<label for="NomeAltraScalaInizio1">Nome Altra scala 1</label>
									<input type="text" class="form-control text-center" id="NomeAltraScalaInizio1">
									<!-- <select class="colorful-select dropdown-primary text-center border_custom_select search" id="NomeAltraScalaInizio1" style="width: 100%"></select> -->
								</div>
								<div class="col">
								<!-- Scala codificata input -->
									<label for="NomeAltraScalaInizio2">Nome altra scala 2</label>
									<input type="text" class="form-control text-center" id="NomeAltraScalaInizio2">
								 	<!--<select class="colorful-select dropdown-primary text-center border_custom_select search" id="NomeAltraScalaInizio2" style="width: 100%"></select>-->
								</div>
								<div class="col">
								<!-- Scala codificata input -->
									<label for="NomeAltraScalaInizio3">Nome altra scala 3</label>
									<input type="text" class="form-control text-center" id="NomeAltraScalaInizio3">
								 	<!--<select class="colorful-select dropdown-primary text-center border_custom_select search" id="NomeAltraScalaInizio3" style="width: 100%"></select>-->
								</div>
							</div>                                      
               
							<!-- fine terza riga, inizio quarta-->
							<div class="row mt-3">
								<div class="col-2">
								<!-- colonna vuota -->
								</div>
								<div class="col">
									<!-- "Durata" input -->
									<label for="DurataGG">Durata</label> 
									<input type="text" class="form-control text-center" id="DurataGG" disabled>
								</div>
								<div class="col">
									<!-- "Giorni effettivi" input -->
									<label for="GGEffettiviPC">Giorni effettivi PC</label> 
									<input type="text" class="form-control text-center" id="GGEffettiviPC" disabled>
								</div>
								<div class="col" style="display:none">
									<!-- "Giorni effettivi" input -->
									<label for="GGStimatiPC">Giorni stimati PC</label> 
									<input type="text" class="form-control text-center" id="GGStimatiPC" disabled>
								</div>
								
								<div class="col-2">
										<!-- Date picker data valutazione iniziale -->
										<label for="CicloSN">Ciclo SN</label>
										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="CicloSN">
											<option value="" disabled selected>Seleziona</option>
											<option value="1">SI</option>
											<option value="2">NO</option>
										</select>
								</div>
								<div class="col">
									<!-- "Num Cicli" input -->
									<label for="NumCicli">Num Cicli</label> 
									<input type="text" class="form-control text-center" id="NumCicli" disabled>
								</div>
								<div class="col-2">
								<!-- colonna vuota -->
								</div>
								
							</div>

							<!-- Fine quarta riga -->
								

                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
               </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->

<div style="height: 100px;"></div>



        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- JQGrid JS -->
<script 	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	var stato = '<?php echo $stato?>';

		$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}

    	
		//faccio la query per le scale codificate
    	var query_scalecodificate = <?php selectAll('scalacodificata','descrizione as scalaCod, codice as Id');?>;
    	var query_scalecodificate1 = <?php selectAll('scalacodificata','descrizione as scalaCod1, codice as Id');?>;

    	//query per trovare le festivitÓ
    	var query_festivita = <?php selectAll('festivita','DataFestivita');?>;

		//faccio la query per i nomi altre scale
    	var query_nomescala = <?php selectAll('scalavalaltra','scalaValAltra as NomeAltraScalaInizio1, ID as Id');?>;
    	var query_nomescala1 = <?php selectAll('scalavalaltra','scalaValAltra as NomeAltraScalaInizio2, ID as Id');?>;
    	var query_nomescala2 = <?php selectAll('scalavalaltra','scalaValAltra as NomeAltraScalaInizio3, ID as Id');?>;
    	var query_anagrafica = <?php selectWhere('anagrafico','*','CodAnagr',$codAnagr);?>;
		//creo le option per le select
		
    	//::Creo le option per le scale codificate
		creaOption('sezioni',query_scalecodificate);
		creaOption('sezioni',query_scalecodificate1);
		
		//::Creo le option per i nomi altre scale
		// creaOption('sezioni',query_nomescala);
		// creaOption('sezioni',query_nomescala1);
		// creaOption('sezioni',query_nomescala2);

    		$('.mdb-select').material_select();
    		$('.search').select2();

    		var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    		var sezione = '<?php if($sezioneF!=""){echo $sezioneF;}else{echo "0";} ?>';
    		
    		if(rowData1!="0"){

               	var obj = jQuery.parseJSON(rowData1);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 

    		if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);

        		riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 


	   	//controllo lo stato e visualizzo i campi obbligatori in base al suo valore
 		if(vecchioPr=="0"){
			check_campi(stato);
 		}

     		
    	//chiudo lo spinner
    	$.LoadingOverlay("hide"); 

		//rimuovo duplicati
		/*$('#NomeAltraScalaInizio1').change(function(){
			rimuoviDuplicati(this,["NomeAltraScalaInizio2","NomeAltraScalaInizio3"],[query_nomescala1,query_nomescala2]);
		});
		$('#NomeAltraScalaInizio2').change(function(){
			rimuoviDuplicati(this,["NomeAltraScalaInizio1","NomeAltraScalaInizio3"],[query_nomescala,query_nomescala2]);
		});
		$('#NomeAltraScalaInizio3').change(function(){
			rimuoviDuplicati(this,["NomeAltraScalaInizio1","NomeAltraScalaInizio2"],[query_nomescala,query_nomescala1]);
		});*/
		$('#scalaCod').change(function(){
			rimuoviDuplicati(this,["scalaCod1"],[query_scalecodificate1]);
		});
		$('#scalaCod1').change(function(){
			rimuoviDuplicati(this,["scalaCod"],[query_scalecodificate]);
		});
		// --- fine rimozione duplicati ---

		$('#CicloSN').change(function(){
			if($('#CicloSN').val()=="1"){
				$('#NumCicli').prop('disabled', false);
			} else {
				$('#NumCicli').prop('disabled', true);
				$('#NumCicli').val("");
			}
		});

		$('#BarthelInizio').change(function(){
			if($('#BarthelInizio').val()<1){
				$('#BarthelInizio').val("0");

				
			} else {

			}
		});

		//controllo la data di nascita per vedere se maggiorenne

			var data_nascita = query_anagrafica[0].DataDiNascita.split(" ");

			var differenza_giorni = days_between(data_nascita[0]);

			if(differenza_giorni<18){
				$('#BarthelInizio').val("XXX");
				$('#SPMSQInizio').val("XX");
				$('#SVAM_Lc').val("X");
				$('#SVAM_Lp').val("X");
				$('#SVAM_AU').val("X");
				$('#SVAM_V').val("X");
				
				$('#BarthelInizio').prop( "disabled", true );
				$('#SPMSQInizio').prop( "disabled", true );
				$('#SVAM_Lc').prop( "disabled", true );
				$('#SVAM_Lp').prop( "disabled", true );
				$('#SVAM_AU').prop( "disabled", true );
				$('#SVAM_V').prop( "disabled", true );
			}

			
			$('#DurataGG').val(<?php echo $durataProgetto?>);

			//$('#GGEffettiviPC').val(<?php echo $durataProgetto?>-<?php echo $feste?>-<?php echo $domeniche?>);
			$('#GGEffettiviPC').val(Math.round(days_between('<?php echo $dataInizio?>')*365));
			//$('#GGStimatiPC').val(Math.round(days_between('<?php echo $dataInizio?>')*365));
		});


    
        
		//funzione che calcola la differenza tra la data di nascita e oggi
        function days_between(date1) {

        	var today = new Date();
        	var dd = String(today.getDate()).padStart(2, '0');
        	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        	var yyyy = today.getFullYear();

        	today = mm + '/' + dd + '/' + yyyy;
        	

        	// The number of milliseconds in one day
        	var ONE_DAY = 1000 * 60 * 60 * 24;

        	var date1_conv = new Date(date1);
        	var date2_conv = new Date(today);

        	// Convert both dates to milliseconds
        	var date1_ms = parseInt(date1_conv.getTime());
        	var date2_ms = parseInt(date2_conv.getTime());

        	// Calculate the difference in milliseconds
        	var difference_ms = Math.abs(date1_ms - date2_ms);

        	// Convert back to days and return
        	//console.log(difference_ms/ONE_DAY/365);
        	return (difference_ms/ONE_DAY/365);

        	}
  
    </script>
</html>