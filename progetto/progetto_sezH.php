<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$sezioneH = "";
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];

if(isset($_SESSION['set_sectionA']['codProg'])){
    $codProj = $_SESSION['set_sectionA']['codProg'];
} else {
    $codProj = "";
}

//tabella diaria
$tab_sezH = "";
$num_tabella = "";


//tabella consulenza
$tab_sezH1 = "";
$num_tabella1 = "";

$vecchioPr =  $_SESSION['vecchioPr'];


if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
    $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionH'])){
    $action=3;
}

if(isset($_SESSION['set_sectionH'])){
    if($_SESSION['set_sectionH']!==""){
        $section = $_SESSION['set_sectionH'];
        $sezioneH = json_encode((object)$section);
    }
}


if (isset($_SESSION['Table_Day'])) {
    if ($_SESSION['Table_Day'] !== "") {
        $tab = $_SESSION['Table_Day'];
        
        $tab_sezH = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_Day'])) {
            $num_tabella = $_SESSION['num_Table_Day'];
        }
        
    }
}

if (isset($_SESSION['Table_Cons'])) {
    if ($_SESSION['Table_Cons'] !== "") {
        $tab = $_SESSION['Table_Cons'];
        
        $tab_sezH1 = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_Cons'])) {
            $num_tabella1 = $_SESSION['num_Table_Cons'];
        }
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>

   
   <!--Main Navigation-->
    <header><!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','H');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
			</div>
		</section>
		
<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','H','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezH" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
    							<div class="col-2">
    							</div>
								<div class="col-2">
									<label for="SospRicoveroNG">SospRicoveroNG</label> 
									<input type="number" class="form-control text-center" id="SospRicoveroNG">
								</div>
								<div class="col-2">
									<label for="SospRicovero100">SospRicovero100</label> 
									<input type="number" class="form-control text-center" id="SospRicovero100">
								</div>
								<div class="col-2">
									<label for="SospRicovero80">SospRicovero100</label> 
									<input type="number" class="form-control text-center" id="SospRicovero80">
								</div>
								<div class="col-2">
									<label for="SospRicovero60">SospRicovero60</label> 
									<input type="number" class="form-control text-center" id="SospRicovero60">
								</div>
    							<div class="col-2">
    							</div>
							</div>
							
                            <hr class="mt-4">
							
							<!-- fine prima riga, inizio seconda -->
							<div class="row mt-3">
    							<div class="col-2">
    							</div>
								<div class="col-2">
									<label for="SospAssenzaTempNG">SospAssenzaTempNG</label> 
									<input type="number" class="form-control text-center" id="SospAssenzaTempNG">
								</div>
								<div class="col-2">
									<label for="SospAssenzaTemp100">SospAssenzaTemp100</label> 
									<input type="number" class="form-control text-center" id="SospAssenzaTemp100">
								</div>
								<div class="col-2">
									<label for="SospAssenzaTemp80">SospAssenzaTemp80</label> 
									<input type="number" class="form-control text-center" id="SospAssenzaTemp80">
								</div>
								<div class="col-2">
									<label for="SospAssenzaTemp60">SospAssenzaTemp60</label> 
									<input type="number" class="form-control text-center" id="SospAssenzaTemp60">
								</div>
    							<div class="col-2">
    							</div>
							</div>
							
							<hr class="mt-4"> 
						
														<!-- riga inserimento parametri -->
							<div class="row mt-3">
									<div class="col-2">
									<label for="SoggEstivoSN">SoggEstivoSN</label>
										<!-- Soggiorno Estivo-->
										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="SoggEstivoSN">
										  <option value="" disabled selected>Seleziona</option>
                                          <option value="1">SI</option>
                                          <option value="2">NO</option>
                                         </select>
									</div>
									<div class="col-4">
									<label for="SoggEstivoOrg">SoggEstivoOrg</label> 
										<!-- Soggiorno Estivo Org-->
										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="SoggEstivoOrg"></select>
									</div>
									<div class="col-2">
										<label for="ASoggEstivoSN">ASoggEstivoSN</label> 
										<!-- Altro Soggiorno Estivo-->
										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="ASoggEstivoSN">
										  <option value="" disabled selected>Seleziona</option>
                                          <option value="1">SI</option>
                                          <option value="2">NO</option>
                                         </select>
									</div>
    								<div class="col-2">
    									<label for="SoggEstivoAltro">SoggEstivoAltro</label> 
    									<input type="text" class="form-control text-center" id="SoggEstivoAltro">
    								</div>
    								<div class="col-2">
    									<label for="SoggEstivoDurata">SoggEstivoDurata</label> 
    									<input type="text" class="form-control text-center" id="SoggEstivoDurata">
    								</div>
							</div> 
							
							<hr class="mt-4">
							
							<!--Fine riga -->
							
							<div class="row">
								<div class="form-group col">
									<label for="AspettativeAssistito">Aspettative Assistito</label>
									<textarea class="form-control" id="AspettativeAssistito" rows="3"></textarea>
								</div>
                            </div>
                            
                            <hr class="mt-4">
                            
                            <!--Fine Riga -->
							
							<div class="row">
								<div class="form-group col">
									<label for="AspettativeFamiliari">Aspettative Familiari</label>
									<textarea class="form-control" id="AspettativeFamiliari" rows="3"></textarea>
								</div>
                            </div>        


                   		 </div>      
								

                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
       
               <!--Grid row-->
               
               	</section>

            <!--Section: Blog v.4-->
            
                        <!-- sezione tabella -->
            		<section class="text-center section-blog-fw mt-3 pb-3 wow fadeIn" id="Day_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Diaria</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows_Day()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper table-responsive" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" id="Table_Day">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataDay">Data</th>
															<th class="align-middle" id="Diaria">Motivo</th>
															<th class="align-middle" id="Diaria1"></th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count_day">
															<td class="align-middle">
																<input type="text" id="DataDiaria_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="Diaria_1"></select>
															</td>
															<td class="align-middle">
																<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="Diaria1_1">
																    <OPTION value="">Seleziona</OPTION >
                                                                    <OPTION value="1">Iniziale</OPTION >
                                                                    <OPTION value="2">Intermedia</OPTION >
                                                                    <OPTION value="3">Finale</OPTION >
                                                                </select>
															</td>
														</tr>

														<tr>
															<td colspan="7"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>


                        <!-- sezione tabella -->
            		<section class="text-center section-blog-fw mt-3 pb-3 wow fadeIn" id="Cons_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Consulenze</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows_Cons()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper table-responsive" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" id="Table_Cons">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataCons">Data</th>
															<th class="align-middle" id="MotivoCons">Motivo</th>
															<th class="align-middle" id="Consulenza">Consulenza</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count_cons">
															<td class="align-middle">
																<input type="text" id="Data_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<input type="text" class="form-control text-center" id="Motivo_1">
															</td>
															<td class="align-middle">
																<input type="text" class="form-control text-center" id="Consulenza_1">
															</td>
														</tr>

														<tr>
															<td colspan="7"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>
	<div style="height: 100px;"></div>


     </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- PROGETTO CUSTOM JS per la sezioneH -->
<script type="text/javascript" src="../progetto/js/progetto_sezH.js"></script>
<!-- JQGrid JS -->
<script type="text/javascript" src="../js/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<script type="text/javascript" src="../progetto/js/rowData.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();

        var query_soggiorni_estivi = <?php selectAll('SoggiornoEstivo','SoggiornoEstivo as SoggEstivoOrg,Id');?>;
    	creaOption('sezioni',query_soggiorni_estivi);

    	var query_diaria = <?php selectAll('ACCESSI','Acesso,Id');?>;
    	creaOption_day(query_diaria,"1");
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var vecchioPr = '<?php echo $vecchioPr; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}
    	
		$('.mdb-select').material_select();
		$('.search').select2();
       	
		//tabella diaria
		var tab_sezH = '<?php echo $tab_sezH; ?>';
		var num_righe = '<?php echo $num_tabella; ?>';
		//tabella consulenze
		var tab_sezH1 = '<?php echo $tab_sezH1; ?>';
        var num_righe1 = '<?php echo $num_tabella1; ?>';

        var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    	var sezione = '<?php if($sezioneH!=""){echo $sezioneH;}else{echo "0";} ?>';

		if(tab_sezH!=""){

    		for(var i=2;i<num_righe;i++) {
    			addNewRows_Day();	
    		}
    		
    		riempiElementi("Day_section","input",jQuery.parseJSON(tab_sezH));

			riempiElementi("Day_section","textarea",jQuery.parseJSON(tab_sezH));
			
		}

		if(tab_sezH1!=""){

    		for(var i=2;i<num_righe1;i++) {
    			addNewRows_Cons();	
    		}
    		
    		riempiElementi("Cons_section","input",jQuery.parseJSON(tab_sezH1));

			riempiElementi("Cons_section","textarea",jQuery.parseJSON(tab_sezH1));
			
		}
    		
    	if(rowData1!="0"){

           		if(tab_sezH=="") {
        		
    			//effettuo la query per la tabella relativa alla diaria
				
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : 'cercaDiaria',
						'codProg': codProj
					},
					success : function(response) {
 						var Diaria = jQuery.parseJSON(response);
						
						var countDay = Diaria.length;
						
						riempiTabellaDiaria("Table_Day", Diaria, countDay);
						
					},
					error : function() {
						alert("error");
					}
				});

           		}

           		if(tab_sezH1=="") {
            		
        			//effettuo la query per la tabella relativa alla diaria
    				
    				$.ajax({
    					type : 'post',
    					url : '../php/controller.php',
    					data : {
    						'source' : 'cercaConsulenza',
    						'codProg': codProj
    					},
    					success : function(response) {
     						var Consulenza = jQuery.parseJSON(response);
    						
    						var countCons = Consulenza.length;
    						
    						riempiTabellaDiaria("Table_Cons", Consulenza, countCons);
    						
    					},
    					error : function() {
    						alert("error");
    					}
    				});

               		}

               	var obj = jQuery.parseJSON(rowData1);

               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

        } 

    	if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);

        		riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

        } 

		//inizializzo il datepicker
    	$('.datepicker').pickadate({

    		  today: 'Oggi',
    		  clear: '',
    		  close: 'Chiudi',
    		  format: 'dd/mm/yyyy'
        });
      		
    	//chiudo lo spinner
    	$.LoadingOverlay("hide");
	
		});


  
    </script>
</html>