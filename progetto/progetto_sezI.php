<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];
$codProg="";
$sezioneI = "";

$vecchioPr =  $_SESSION['vecchioPr'];

if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
    $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionI'])){
    $action=3;
}

if(isset($_SESSION['set_sectionI'])){
    if($_SESSION['set_sectionI']!==""){
        $section = $_SESSION['set_sectionI'];
        $sezioneI = json_encode((object)$section);
    }
}

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
        if(isset($section2['codProg'])){
            $codProj = $_SESSION['set_sectionA']['codProg'];
        } else  {
            $codProj = "";
        }
        
        
    }
}

if(isset($_SESSION['set_sectionF'])){
    if($_SESSION['set_sectionF']!==""){
        $section3 = $_SESSION['set_sectionF'];
        $sezioneF = json_encode((object)$section3);
        
        if(isset($section3['SVAM_Lc'])){
            $SVAM_Lc = $_SESSION['set_sectionF']['SVAM_Lc'];
        } else  {
            $SVAM_Lc = "";
        }
        
        if(isset($section3['SVAM_Lp'])){
            $SVAM_Lp = $_SESSION['set_sectionF']['SVAM_Lp'];
        } else  {
            $SVAM_Lp = "";
        }
        
        if(isset($section3['SVAM_AU'])){
            $SVAM_AU = $_SESSION['set_sectionF']['SVAM_AU'];
        } else  {
            $SVAM_AU = "";
        }
        
        if(isset($section3['SVAM_V'])){
            $SVAM_V = $_SESSION['set_sectionF']['SVAM_V'];
        } else  {
            $SVAM_V = "";
        }
        
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>

   
   <!--Main Navigation-->
    <header><!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	 </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','I');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
			</div>
		</section>
		
<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','I','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezI" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
								<div class="col-12 text-center" style="color:red">
								<font>Sezione F. CONCLUSIONE DEL PROGETTO RIABILITATIVO</font>
								</div>
							</div>
							
                            <hr class="mt-4">
							
							<!-- fine prima riga, inizio seconda -->
							<div class="row mt-3">
    							<div class="col-3">
    							</div>
								<div class="col-2" style="color:red">
									Data conclusione del progetto:
								</div>
								<div class="col-1">
    							</div>
								<div class="col-2 mt-2">
									<input type="text" id="DataConclusione" class="form-control datepicker text-center dateFormat">
								</div>
								<div class="col-3">
    							</div>
							</div>
							
							<hr class="mt-4"> 
						
														<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-1">
    							</div>
								<div class="col-7 text-left">
									Risultato raggiunto (riduzione della disabilit&aacute; trattata):
								</div>
								<div class="col-4">
    							</div>
							</div>
							<!-- valutazione finale indice di Barthel -->
							<div class="row mt-3">
								<div class="col-2">
    							</div>
								<div class="col-6 mt-2 text-left" style="color:red">
									Scheda valutazione capacit&aacute; funzionali di base (Indice di Barthel):
								</div>
								<div class="col-2">
								<input type="text" class="form-control text-center" id="Barthelfine">
    							</div>
								<div class="col-2">
    							</div>
    						</div>
    						<!-- valutazione finale aspetto cognitivo-->  
							<div class="row mt-3">
								<div class="col-2">
    							</div>
								<div class="col-6 mt-2 text-left" style="color:red">
									Scheda valutazione aspetto cognitivo (S.P.M.S.Q.):
								</div>
								<div class="col-2">
								<input type="text" class="form-control text-center" id="SPSMSQfine">
    							</div>
								<div class="col-2">
    							</div>
    						</div>
    						<!-- valutazione finale indice di Barthel -->  
							<div class="row mt-3">
								<div class="col-2">
    							</div>
								<div class="col-6 mt-2 text-left">
									Scheda valutazione aspetti sensoriali e di comunicazione (da SVAMA):
								</div>
								<div class="col-4">
    							</div>
    						</div>
    						<div class="row mt-3">
    							<div class="col-2">
    							</div>
    							                                <!-- Lc -->
								<div class="col-2">
								<label for="SVAM_Lcfine">Lc</label>
								<input type="text" class="form-control text-center trattamento" id="SVAM_Lcfine" disabled>
    							</div>
    							    							<!-- Lp -->
								<div class="col-2">
								<label for="SVAM_Lpfine">Lp</label>
								<input type="text" class="form-control text-center trattamento" id="SVAM_Lpfine" disabled>
    							</div>
    							    							<!-- U -->
								<div class="col-2">
								<label for="SVAM_AUfine">U</label>
								<input type="text" class="form-control text-center trattamento" id="SVAM_AUfine" disabled>
    							</div>
    							    							<!-- V -->
								<div class="col-2">
								<label for="SVAM_Vfine">V</label>
								<input type="text" class="form-control text-center trattamento" id="SVAM_Vfine" disabled>
    							</div>
    							<div class="col-2">
    							</div>
							</div>
							<hr class="mt-4">
							
							<!--Fine riga -->
							<div class="row">
							    <div class="col-2">
    							</div>
    							<div class="col-3">
									<label for="ProtOrtAusiliFine">CodprotOrtAusili</label>
									<!-- Altro Soggiorno Estivo-->
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select trattamento" id="ProtOrtAusiliFine">
										<option value="" disabled selected>Seleziona</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
									</select>
								</div>
								<div class="col-7">
    							</div>
							</div>
                         	<div id="codProt" style="display:none">
							<div class="row mt-4">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Apparecchi per la funzione respiratoria</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P1f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>  
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Carrozzine ed altri ausili per la deambulazione</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P2f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Ausili addominali</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P3f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Ausili per comunicazione, informazione</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P4f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Ausili per la cura e protezione personale</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P5f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<!-- fine prime 5 checkbox -->
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Ortesi per il tronco</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P6f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>  
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Sistemi posturali</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P7f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Apparecchi e ortesi per l'arto</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P8f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Ausili e protesi ottiche e acustiche</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P9f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							
							<div class="row mt-3">
							 		
        							 		<div class="col-2">
            								</div>
        									<div class="col-4 text-left">
        										<label><font size="2">Altro</font></label>
        									</div>
        									<div class="col-2">
        										<!-- Switch -->
        										<div class="switch blue-white-switch">
        											<label> Off <input type="checkbox" id="P10f" class="P"><span class="lever"></span> On </label>
        										</div>
        									</div>
							</div>
							</div>       
                            
						    <hr class="mt-4">
                            
                            														<!-- Motivo conclusioni -->
							<div class="row mt-4">
								<div class="col-1">
    							</div>
								<div class="col-7 text-left" style="color:red">
									Motivo della conclusione: 
								</div>
								<div class="col-4">
    							</div>
							</div>
                                                                                    <!--Fine Riga -->
                                                                                    
                                                                                    <!-- Scelta conclusioni -->
							<div class="row mt-3">
								<div class="col-2">
    							</div>
								<div class="col-7">
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="MotivoConclusione"></select> 
								</div>
								<div class="col-3">
    							</div>
							</div>
                                                                                    <!--Fine Riga -->
                                                                                    
                             <hr class="mt-4">
                            
                   		 </div>      
								

                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
               </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->


<div style="height: 100px;"></div>


        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- JQGrid JS -->
<script type="text/javascript" src="../js/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<script type="text/javascript" src="../progetto/js/rowData.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

		//inizializzo il datepicker
    	$('.datepicker').pickadate({

    		  today: 'Oggi',
    		  clear: '',
    		  close: 'Chiudi',
    		  format: 'dd/mm/yyyy'
        }); 

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var vecchioPr = '<?php echo $vecchioPr; ?>';

    	var stato = '<?php echo $stato;?>';

    	var SVAM_Lc = '<?php echo $SVAM_Lc;?>';

    	var SVAM_Lp = '<?php echo $SVAM_Lp;?>';

    	var SVAM_AU = '<?php echo $SVAM_AU;?>';

    	var SVAM_V = '<?php echo $SVAM_V;?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}    	

        	//effettuo la query e creo le option per i soggiorni estivi
        	
        	var query_conclusione = <?php selectAll('MotivoConclusione','MotivoConclusione,Id');?>;

			creaOption('sezioni',query_conclusione);

			//fine creazione option

    		$('.mdb-select').material_select();
    		$('.search').select2();

        	var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    		var sezione = '<?php if($sezioneI!=""){echo $sezioneI;}else{echo "0";} ?>';
    		
    		if(rowData1!="0"){

               	var obj = jQuery.parseJSON(rowData1);

               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 

    		if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);

        		riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 


    	verificaOption('ProtOrtAusiliFine','codProt');


		if(vecchioPr=="0"){
			check_campi(stato);
 		}

 		
    	//chiudo lo spinner
    	$.LoadingOverlay("hide");

    	$('#ProtOrtAusiliFine').change(function(){
			if($('#ProtOrtAusiliFine').val()=="1"){
				showComponent('codProt');
			} else {
				hideComponent('codProt');
			}
    	});

    	if(SVAM_Lc!=""){
        	$('#SVAM_Lcfine').val(SVAM_Lc);
    	}

    	if(SVAM_Lp!=""){
        	$('#SVAM_Lpfine').val(SVAM_Lp);
    	}

    	if(SVAM_AU!=""){
        	$('#SVAM_AUfine').val(SVAM_AU);
    	}

    	if(SVAM_V!=""){
        	$('#SVAM_Vfine').val(SVAM_V);
    	}

    	
    	
		});


  
    </script>
</html>