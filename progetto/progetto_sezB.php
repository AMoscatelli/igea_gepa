<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$sezioneB = "";
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];

if(isset($_SESSION['set_sectionA']['codProg'])){
    $codProj = $_SESSION['set_sectionA']['codProg'];
} else {
    $codProj = "";
}

$tab_sezB = "";
$num_tabella = "";

$vecchioPr =  $_SESSION['vecchioPr'];

if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
        $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionB'])){
    $action=3;
}

if(isset($_SESSION['set_sectionB'])){
    if($_SESSION['set_sectionB']!==""){
        $section = $_SESSION['set_sectionB'];
        $sezioneB = json_encode((object)$section);
    }
}

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['dataInizio'])){
            $dataInizio =  $section2['dataInizio'];
        } else {
            $dataInizio ="";
        }
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
        if(isset($section2['regAssist'])){
            $regAssist =  $section2['regAssist'];
        } else {
            $regAssist ="";
        }
        
        
    }
}

if (isset($_SESSION['Table_ES'])) {
    if ($_SESSION['Table_ES'] !== "") {
        $tab = $_SESSION['Table_ES'];
        
        $tab_sezB = json_encode((object) $tab);
        
        if(isset($_SESSION['num_Table_ES'])) {
            $num_tabella = $_SESSION['num_Table_ES'];
        }
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="../css/jquery-ui.min.css">
<link rel="stylesheet"
	href="../css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>
   
   <!--Main Navigation-->
    <header>

		  <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>

    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','B');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#" >Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','B','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezB" role="tabpanel">
                             
                            							<!-- Creazione grid della prima riga -->    
							<div class="row">
								<div class="col-2">
									<!-- Invalidit� input -->
									<label for="invalidita">Invalidit&agrave;</label> 
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select attesa trattamento" id="invalidita"></select>
								</div>
								<div class="col-1">
									<!-- Percentuale invalidit� input -->
									<label for="percInvalidita">%</label> 
									<input type="text" class="form-control text-center" id="percInvalidita" placeholder="0 %" disabled>
								</div>
								<div class="col-4">
									<!-- Modalit� di trasporto input -->
									<label for="modTrasp">Modalit&agrave; trasporto</label> 
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="modTrasp"><font color="red"></font></select>
								</div>
								<div class="col">
									<!-- Percentuale invalidit� input -->
									<label for="altraModTrasp">Altra modalit&agrave;</label> 
									<input type="text" class="form-control text-center" id="altraModTrasp" disabled>
								</div>
								<div class="col-1">
									<!-- Modalit� di In sostegno input -->
									<label for="InSostegno">In sostegno</label> 
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="InSostegno">
        									<option value="" disabled selected>-</option>
        									<option value="1">SI</option>
                                            <option value="2">NO</option>
									</select>
								</div>
								<div class="col-1">
									<!-- Modalit� di Servizio assistenza El input -->
									<label for="ServAssEl">ServAssEl</label> 
									<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select attesa trattamento" id="ServAssEl">
        									<option value="" disabled selected>-</option>
        									<option value="1">SI</option>
                                            <option value="2">NO</option>
									</select>
								</div>
							</div>
													       <!-- Fine prima riga -->
							<br>
							                         <!-- Creazione grid della seconda riga -->
							<div class="row">
                        		<div class="col-2 mt-2">
									<!-- Data input-->
									<label for="DataProgIni">Data</label> 
									<input type="text" id="DataProgIni" class="form-control datepicker text-center dateFormat">
								</div>
								<div class="form-group col-10">
									<label for="PropProgEquipeRiabIniz">Proposta Progetto dell'EQUIPE RIABILITATIVA (iniziale)</label>
									<textarea class="form-control" id="PropProgEquipeRiabIniz" rows="2"></textarea>
								</div>
                            </div>
							                         <!-- Creazione grid della terza riga -->
							<div class="row">
                        		<div class="col-2 mt-2">
									<!-- Data input-->
									<label for="DataProgInt">Data</label> 
									<input type="text" id="DataProgInt" class="form-control datepicker text-center dateFormat">
								</div>
								<div class="form-group col-10">
									<label for="prProgFin">Proposta Progetto dell'EQUIPE RIABILITATIVA (intermedia)</label>
									<textarea class="form-control" id="PropProgEquipeRiabInter" rows="2"></textarea>
								</div>
                            </div> 
							                         <!-- Creazione grid della quarta riga -->
							<div class="row">
								<div class="form-group col">
									<label for="EsamiEmatochimici">Esami Ematochimici</label>
									<textarea class="form-control" id="EsamiEmatochimici" rows="1"></textarea>
								</div>
                            </div>  
							                         <!-- Creazione grid della quinta riga -->
							<div class="row">
								<div class="form-group col">
									<label for="Terapiafarmacologica">Terapia Farmacologica in atto</label>
									<textarea class="form-control" id="Terapiafarmacologica" rows="2"></textarea>
								</div>
                            </div>                                                          
                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->
            
            
            <!-- sezione tabella -->
            		<section class="text-center section-blog-fw mt-3 pb-3 wow fadeIn" id="ES_section">
		
									<!-- creo la tabella per l'inserimento della valutazione Funzionale -->
							<div class="row">

								<div class="col-12">
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
											<a class="white-text mx-3 text-center">Esami Strumentali</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="addNewRows()">
													<i class="fas fa-plus mt-0"></i>
												</button>

											</div>
										</div>
										<!--/Card image-->




										<div class="px-4">

											<div class="table-wrapper table-responsive" >
												<!--Table-->
												<table class="table table-hover mb-4 text-center table-bordered tabelle" id="Table_ES">

													<!--Table head-->
													<thead>
														<tr>
															<th class="align-middle" id="DataES">Data ES</th>
															<th class="align-middle" id="TipoES">Tipo ES</th>
															<th class="align-middle" id="RefertoES">Referto ES</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_count">
															<td class="align-middle">
																<input type="text" id="EsStrData_1" class="form-control datepicker text-center dateFormat enableTrue">
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="EsStrTipo_1" rows="2" style="height:auto;"></textarea>
															</td>
															<td class="align-middle">
																<textarea class="form-control" id="EsStrReferto_1" rows="2"></textarea>
															</td>
														</tr>

														<tr>
															<td colspan="7"></td>
														</tr>

													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
								</div>

							</div>
		</section>


<div style="height: 100px;"></div>


        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- JQGrid JS -->
<script 	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');
		
        new WOW().init();
        $(document).ready(function () {
		
        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	var stato = '<?php echo $stato?>';

    	var regAssist = '<?php echo $regAssist?>';

    	//controllo se si tratta di un progetto nuovo (action == 1) o se si tratta di un progetto scaricato dal db (action == 3)
    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

		//effettuo la query per l'input invalidit� e creo la relativa option
        var query_invalidita = <?php echo selectAll('invalidita','Invalidita as invalidita,Id');?>;
		creaOption('sezioni',query_invalidita);
		
        //effettuo la query per l'input modalit� di trasporto e creo la relativa option
        var query_modTrasp = <?php echo selectAll('modalitaditrasporto','ModaTrasporto as modTrasp,Id');?>;
		creaOption('sezioni',query_modTrasp);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}


    		$('.mdb-select').material_select();
    		$('.search').select2();
            

        	var tab_sezB = '<?php echo $tab_sezB; ?>';

        	var num_righe = '<?php echo $num_tabella; ?>';

    		var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    		var sezione = '<?php if($sezioneB!=""){echo $sezioneB;}else{echo "0";} ?>';

           	var dataIniziale = '<?php echo $dataInizio?>';
            
           	//$('#DataProgIni').val(formatCustomHTMLDate(dataIniziale,"/"));  

    		//verifico se la tabella � valorizzata

    		if(tab_sezB!=""){

        		for(var i=2;i<num_righe;i++) {
        			addNewRows();	
        		}
        		
        		riempiElementi("ES_section","input",jQuery.parseJSON(tab_sezB));

				riempiElementi("ES_section","textarea",jQuery.parseJSON(tab_sezB));
				
    		}
    		
    		if(rowData1!="0"){

           		if(tab_sezB=="") {
        		
    			//effettuo la query per la tabella relativa agli esami strumentali
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : 'cercaEsamiStrumentali',
						'codProg': codProj
					},
					success : function(response) {
 						var exaStrum = jQuery.parseJSON(response);
						
						var countES = exaStrum.length;
						
						riempiValutazione("Table_ES", exaStrum, countES);
						
					},
					error : function() {
						alert("error");
					}
				});

           		}

               	var obj = jQuery.parseJSON(rowData1);

    			riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

               	var dataIniziale = '<?php echo $dataInizio?>';
               
               	//$('#DataProgIni').val(formatCustomHTMLDate(dataIniziale,"/"));        	

           	} 

            if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('sezioni','textarea',obj,"1");

           	}



		//inizializzo il datepicker
    	$('.datepicker').pickadate({

    		  today: 'Oggi',
    		  clear: '',
    		  close: 'Chiudi',
    		  format: 'dd/mm/yyyy'
        }); 
    	
    	//chiudo lo spinner


    	//controllo lo stato e visualizzo i campi obbligatori in base al suo valore
 		
 		if(vecchioPr=="0"){
 			check_campi(stato,regAssist);
 		}

 		if($('#invalidita').val()=="1"|$('#invalidita').val()=="2"){
 			$('label[for=percInvalidita').removeClass("prova");
 			$("#percInvalidita").removeClass( "check" );
 		}
    	
    	$.LoadingOverlay("hide");  	

    });



//prendo il valore dell'input % invalidit� e aggiungo il simbolo percentuale
$('#percInvalidita').change(function() {
	var valore = $('#percInvalidita').val();
	$('#percInvalidita').val(valore + " %");
});

$('#modTrasp').change(function() {
	if($('#modTrasp').val()=="5"){
		$('#altraModTrasp').prop('disabled', false);
		$('label[for=altraModTrasp').addClass("prova");
		$("#altraModTrasp").addClass( "check" );
	} else {
		$('#altraModTrasp').prop('disabled', true);
		$('label[for=altraModTrasp').removeClass("prova");
		$("#altraModTrasp").removeClass( "check" );
		$('#altraModTrasp').val("");
	}
});

$('#invalidita').change(function() {
	if($('#invalidita').val()=="3"){
		$('#percInvalidita').prop('disabled', false);
		$('label[for=percInvalidita').addClass("prova");
		$("#percInvalidita").addClass( "check" );
	} else {
		$('label[for=percInvalidita').removeClass("prova");
		$("#percInvalidita").removeClass( "check" );
		$('#percInvalidita').prop('disabled', true);
		$('#percInvalidita').val("");
	}
});




function addNewRows() {


	  var rowCount = $('.tab_count').length + 1;
		
	   $('#Table_ES tr:last').after(


		'<tr id="'+rowCount+'" class="tab_count">' +
    		'<td class="align-middle">'+
    		'<input type="text" id="EsStrData_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
        	'</td>'+
        	'<td class="align-middle">'+
        		'<textarea class="form-control" id="EsStrTipo_'+rowCount+'" rows="2" style="height:auto;"></textarea>'+
        	'</td>'+
        	'<td class="align-middle">'+
        		'<textarea class="form-control" id="EsStrReferto_'+rowCount+'" rows="2"></textarea>'+
        	'</td>'+

		'</tr>');


	   $('#Table_ES tr:last').after(
			'<tr>'+
			'<td colspan="7"></td>'+
			'</tr>'
	   );

		//inizializzo il datepicker
  	$('#EsStrData_'+rowCount).pickadate({

  		  today: 'Oggi',
  		  clear: '',
  		  close: 'Chiudi',
  		  format: 'dd/mm/yyyy'
      }); 
}


    </script>
</html>