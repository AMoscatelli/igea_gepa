 <?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$sezioneD = "";
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$vecchioPr = "";
$json_rowData="";
$arr=[];
$tab_sezD ="";
$vecchioPr = $_SESSION['vecchioPr'];

if(isset($_SESSION['set_sectionA'])){
    if($_SESSION['set_sectionA']!==""){
        $section2 = $_SESSION['set_sectionA'];
        $sezioneA = json_encode((object)$section2);
        
        if(isset($section2['stato'])){
            $stato =  $section2['stato'];
        } else {
            $stato ="";
        }
        
        if(isset($section2['dataInizio'])){
            $dataInizio =  $section2['dataInizio'];
        } else {
            $dataInizio ="";
        }
        
        if(isset($section2['codProg'])){
            $codProj = $_SESSION['set_sectionA']['codProg'];
        } else  {
            $codProj = "";
        }
        
        
    }
}
if(isset($_SESSION['rowData'])){
    $rowData = $_SESSION['rowData'];
    if($rowData!=""){
        $json_rowData = json_encode($rowData);
    }
}


if(isset($_SESSION['set_sectionD'])){
    $action=3;
    if($_SESSION['set_sectionD']!==""){
        $section = $_SESSION['set_sectionD'];
        $sezioneD = json_encode((object)$section);
    }
}

if (isset($_SESSION['FigureProfInizio'])) {
    if ($_SESSION['FigureProfInizio'] !== "") {
        $tab = $_SESSION['FigureProfInizio'];
        
        $tab_sezD = json_encode((object) $tab);
        
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>
   
   <!--Main Navigation-->
    <header>

		  <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>

    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaDati('sezioni','D');" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','G','<?php echo $action ?>');" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','D','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezD" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
									<div class="col-2 text-left mt-2">
										<label for="DataValutaz">Data valutazione iniziale</label>
									</div>
									<div class="col-2">
										<!-- Date picker data valutazione iniziale -->
										<input type="text" id="DataValutaz" class="form-control datepicker text-center dateFormat trattamento">
									</div>
								</div>
								<br>                                                
                            <div class="row mt-3">
								  
								  	<div class="col-4 text-left">
								  	<label><font size="2">Fisiatra</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Fisiatra" class="figProfIniz"> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>
								  	<div class="col-4 text-left">
								  	<label><font size="2">Terapista della Neuro e Psicomotricit&agrave; dell'et&agrave; evolutiva</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="TerapistadellaNeuroePsicomotricitadelletaevolutiva" class="figProfIniz"> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>

								  </div>
								<!-- Fine prima riga -->
								<!-- Creazione grid della seconda riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Neuropsichiatra Infantile</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="NeuropsichiatraInfantile" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Logopedista</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Logopedista" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine seconda riga -->
								<!-- Creazione grid della terza riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Medico di altre specialit&agrave;</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Medicodialtraspecialita" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Educatore Professionale</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Educatoreprofessionale" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine terza riga -->
								<!-- Creazione grid della quarta riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Psicologo</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Psicologo" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Ortottista-assistente di oftalmologia</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Ortottista-assistentedioftalmologia" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine quarta riga -->
								<!-- Creazione grid della quinta riga -->
                            <div class="row mt-3">
								  
								  	<div class="col-4 text-left">
								  	<label><font size="2">Assistente Sociale</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="AssistenteSociale" class="figProfIniz"> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>
								  	<div class="col-4 text-left">
								  	<label><font size="2">Podologo</font></label>
								  	</div>
								  	<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="podologo" class="figProfIniz"> 
											<span class="lever"></span> On
											</label>
										</div>
									</div>

								  </div>
								<!-- Fine quinta riga -->
								<!-- Creazione grid della sesta riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Infermiere</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Infermiere" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Terapista della riabilitazione Psichiatrica</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="terapistadellariabilitazionePsichiatrica" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine sesta riga -->
								<!-- Creazione grid della settima riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Fisioterapista</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Fisioterapista" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Altro</font></label>
									</div>
									<div class="col-2">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Altro" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine settima riga -->
								<!-- Creazione grid della ottava riga -->
								<div class="row mt-3">

									<div class="col-4 text-left">
										<label><font size="2">Terapista Occupazionale</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="TerapistaOccupazionale" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>
									<div class="col-4 text-left">
										<label><font size="2">Altro 1</font></label>
									</div>
									<div class="col-2 text-right">
										<!-- Switch -->
										<div class="switch blue-white-switch">
											<label> Off <input type="checkbox" id="Altro1" class="figProfIniz"> <span
												class="lever"></span> On
											</label>
										</div>
									</div>

								</div>
								<!-- Fine ottava riga -->
								<br>
								<div class="row mt-3">
									<div class="col-2 text-left mt-2">
										<label for="MedAltraSpecialita"><font size="2">Medico altra specialit&agrave;</font></label>
									</div>
									<div class="col-3">
										<select class="mdb-select colorful-select dropdown-primary border_custom_select" id="MedAltraSpecialita" disabled></select>
									</div>
								</div>    
													      
							<br>
							                         <!-- Creazione grid della riga progetto-->
							<!-- <div class="row">	
								<div>
								 <input type="text" class="form-control text-center" id="nuovoPr" style="display:none">
								 <input type="text" class="form-control text-center" id="vecchioPr" style="display:none">
								</div>
							</div> -->
											       <!-- Fine riga progetto-->
							<br>
							                       
                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->


<div style="height: 100px;"></div>


        </div>

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<script type="text/javascript" src="../progetto/js/progetto_sezD.js"></script>
<!-- JQGrid JS -->
<script 	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

		var stato = '<?php echo $stato?>';

		var dataInizio = '<?php  echo $dataInizio?>';

		//effettuo la query per l'input ricovero ultimi 12 mesi e creo la relativa option
        var query_medAltraSpecialita= <?php echo selectAll('medaltraspecialita','MedAltraSpecialita,Id');?>;
		creaOption('sezioni',query_medAltraSpecialita);


    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}

    		$('.mdb-select').material_select();
    		$('.search').select2();

    		var tab_sezD = '<?php echo $tab_sezD; ?>';

    		var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "";} ?>';

    		var sezione = '<?php if($sezioneD!=""){echo $sezioneD;}else{echo "";} ?>';

    		//if(rowData1!='""'&rowData1!=''){
    		
    		 if(tab_sezD!=""){
       		
        		riempiElementi("sezioni","input",jQuery.parseJSON(tab_sezD));
				
    		}
    		
    		if(rowData1!=""){

           		if(tab_sezD=="") {
            		
        			//effettuo la query per la tabella relativa agli esami strumentali
    				$.ajax({
    					type : 'post',
    					url : '../php/controller.php',
    					data : {
    						'source' : 'cercaFigureProfIniz',
    						'codProg': codProj
    					},
    					success : function(response) {
     						var exaStrum = jQuery.parseJSON(response);
    						
    						//var countES = exaStrum.length;
    						
    						//riempiValutazione("Table_IND", exaStrum, countES);
    						
    						if(exaStrum!="") {

    							riempiElementi('sezioni','input',exaStrum[0],"1");

    						}
    						
    					},
    					error : function() {
    						alert("error");
    					}
    				});

               	}

               	var obj = jQuery.parseJSON(rowData1);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 

    		if (sezione!="") {

        		var obj = jQuery.parseJSON(sezione);
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	}

    	//se viene abilitato il flag medico altra specialitÓ si abilita la voce del menu select
    	$('#Medicodialtraspecialita').change(function() {
			if(this.checked==true){
					$('.mdb-select').material_select('destroy');
					$('#MedAltraSpecialita').prop('disabled', false);
					$('label[for=MedAltraSpecialita]').addClass( "prova" );
					$("#MedAltraSpecialita").addClass( "check" );
					$('.mdb-select').material_select();
			} else {
				$('.mdb-select').material_select('destroy');
				$('label[for=MedAltraSpecialita]').removeClass( "prova" );
				$("#MedAltraSpecialita").removeClass( "check" );
				$('#MedAltraSpecialita').prop('disabled', true);
				$('#MedAltraSpecialita').val("0");
				$('.mdb-select').material_select();
			}
		});   

		if($('#Medicodialtraspecialita')[0].checked==true){
		
				$('.mdb-select').material_select('destroy');
				$('label[for=MedAltraSpecialita]').addClass( "prova" );
				$("#MedAltraSpecialita").addClass( "check" );
				$('#MedAltraSpecialita').prop('disabled', false);
				$('.mdb-select').material_select();
		} 



		//inizializzo il datepicker
    	$('.datepicker').pickadate({

    		  today: 'Oggi',
    		  clear: '',
    		  close: 'Chiudi',
    		  format: 'dd/mm/yyyy'
        });

    	//controllo lo stato e visualizzo i campi obbligatori in base al suo valore
 		if(vecchioPr=="0"){
 	 		$('#DataValutaz').val(formatCustomHTMLDate(dataInizio,"/"));
			check_campi(stato);
			
 		}
    	
    	//chiudo lo spinner
    	$.LoadingOverlay("hide");  	

    });


        
    </script>
</html>