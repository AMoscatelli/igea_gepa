<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

//session_start();

$action = $_SESSION['action'];
$nome1 =  $_SESSION['nome'];
$cognome1 =  $_SESSION['cognome'];
$json_rowData="";
$arr=[];
$codProg="";
$sezioneG = "";
$vecchioPr =  $_SESSION['vecchioPr'];

if(isset($_SESSION['rowData'])){
    
    $rowData = $_SESSION['rowData'];//==""?undefined:$_SESSION['rowData'];
    if($rowData!=""){
    $json_rowData = json_encode($rowData);
    }
    //$codProg = $rowData['codProg'];
}

if(isset($_SESSION['set_sectionG'])){
    $action=3;
}

if(isset($_SESSION['set_sectionG'])){
    if($_SESSION['set_sectionG']!==""){
        $section = $_SESSION['set_sectionG'];
        $sezioneG = json_encode((object)$section);
    }
}
?>

<html>
   
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />



</head>

   
   <body>
   
   <!-- Central Modal Small -->
   
 
<div class="modal fade" id="modalElimina" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Body-->
                        <div class="modal-body">
                            Vuoi davvero eliminare la selezione?
                        </div>
                        <input id="indexVal" style="display:none"></input>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light" data-dismiss="modal">Chiudi</button>
                            <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" onclick='cancellaRiga($("#indexVal").val(),"interventiPres","gridInt")'>Elimina</button>
                        </div>
                    </div>
                    <!--/.Content-->
                </div>
</div>

   
   <!--Main Navigation-->
    <header>
		  <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
    </header>
    
    <!--Main Layout-->
    <main>

        <div class="container">
        		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center" >

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
					<form method="post">
						<!--Facebook-->
						<button type="button" class="btn btn-default" id="backBtn"
							onclick="gestisciIndietro();" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
						<button type="submit" name="source" value=""
							class="btn btn-default" onclick="" style="display: none">
							<i class="fa fa-print left"> STAMPA MESE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="" style="display:none"><i class="fa fa-save left">  SALVA</i></button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="" style="display:none"><i class="fa fa-trash left">  ELIMINA</i></button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="abilitaCampi();" style="display:none"><i class="fa fa-edit left">  MODIFICA</i></button>
						<button type="submit" name="buttonPr" value="nuovo" class="btn btn-default" id="nuovoPrBtn"
							onclick=""><i class="fa fa-plus left">  NUOVO PROGETTO</i></button>
						<button type="button" name="buttonPr" class="btn btn-default" id="cercaPrBtn"
							onclick="$('#centralModalWarningDemo').modal();"><i class="fa fa-search left">  CERCA PROGETTO</i></button>
						<button type="button" name="buttonPr" value="mostra" class="btn btn-default" id="mostraPrBtn"
							onclick="mostraProgetti();" style="display:none"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
							<span class="counter" id="counterPr" style="display:none">0</span>
						<button type="button" name="buttonPr" value="anagr" class="btn btn-default" id="mostraAnagr"
							onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');" style="display:none"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>
	
        <!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1">
    					</div>
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-4">
    					<input type="text" class="form-control text-center" id="nominativo_nome" readonly>
						</div>
    					<div class="col-1">
    					</div>
				</div>
			</div>
			</div>
		</section>
		
		<!--Section: Blog v.4-->
            <section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="sezioni">

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
						
						<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','A','<?php echo $action ?>');" role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','B','<?php echo $action ?>');" role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','C','<?php echo $action ?>');" role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','D','<?php echo $action ?>');" role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','E','<?php echo $action ?>');" role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','F','<?php echo $action ?>');" role="tab" href="#">Sezione D Indici  Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light active" role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','H','<?php echo $action ?>');" role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','I','<?php echo $action ?>');" role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light" onclick="recuperaElementi('sezioni','G','M','<?php echo $action ?>');" role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>
                        
                        <!-- Tab panels -->
                        <div class="tab-content card">
                        
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="progetto_sezG" role="tabpanel">
                             
                            							
                           <div class="col-12">
                                                    <!-- Creazione grid della prima riga -->
							<div class="row">
    							<div class="col-3">
    							</div>
    								<div class="col-6">
    										<!-- Date picker data valutazione iniziale -->
    										<label for="interventiPres">Interventi (in presenza dell'utente)</label>
    										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="interventiPres">
    											<option value="" disabled selected>Seleziona</option>
    											<option value="1">Valutazione ed osservazione</option>
    											<option value="2">Riabilitazione logopedica</option>
    											<option value="3">Riabilitazione cardiologica</option>
    											<option value="4">Riabilitazione respiratoria</option>
    											<option value="5">Riabilitazione neuro-motoria</option>
    											<option value="6">Riabilitazione psicomotoria</option>
    											<option value="7">Riabilitazione cognitiva/neuropsicologica</option>
    											<option value="8">Terapia psicologica</option>
    											<option value="9">Terapia occupazionale</option>
    											<option value="10">Intervento ortottico</option>
    											<option value="11">Intervento educativo</option>
    											<option value="12">Intervento infermieristico</option>
    										</select>
    								</div>
    							<div class="col-3">
    							</div>
							</div>

							
							<!-- riga nascosta contenente la grid -->
							<div class="row mt-3" style="display:none" id="rowInterventiPres">
								<div class="col-2">
								</div>
								<div class="col-2">
									<!-- Num Cart input -->
									<label for="accessiPrev">Accessi previsti</label> 
									<input type="number" class="form-control text-center number-only" id="accessiPrev">
								</div>
								<div class="col-4">
									<!-- Num Cart input -->
									<label for="interventi">Interventi</label> 
									<input type="text" class="form-control text-center" id="interventi" disabled>
								</div>
								<div class="col-2">
									<!-- Num Cart input -->
									<label for="accessiEff">Accessi effettivi</label> 
									<input type="number" class="form-control text-center number-only" id="accessiEff">
								</div>
								<div class="col-1">
								<a class="btn-floating btn-sm blue-gradient" onclick="mostraRiga('intPres','gridInt','accPrev','int','accEff',$('#accessiPrev').val(),$('#interventi').val(),$('#accessiEff').val(),$('#interventiPres').find('option:selected').val(),'interventiPres');" id="buttonGrid"><i class="fa fa-plus"></i></a>
								</div>
								<div class="col-1">
								</div>
							
							</div>
							
							<!-- Creo un div nascosto per tenere conto delle righe create -->
							<div style="display:none">
							<input id="1_righeCreate"></input>
							</div>
							
							<!-- Grid nascosta con parametri inseriti -->
							<div id="gridInt"></div>     
							
							
                            <hr class="mt-4">
							
							<!-- fine prima riga, inizio seconda -->
							<div class="row mt-3">
    							<div class="col-3">
    							</div>
    								<div class="col-6">
    										<!-- Date picker data valutazione iniziale -->
    										<label for="interventiNoPres">Interventi (in assenza dell'utente)</label>
    										<select class="mdb-select colorful-select dropdown-primary text-center border_custom_select" id="interventiNoPres">
    											<option value="" disabled selected>Seleziona</option>
    											<option value="13">Elaborazione/revisione progetto</option>
    											<option value="14">Counseling</option>
    											<option value="15">Riunione d'equipe</option>
    											<option value="16">Adempimento di cui alla L. n 104/92 per l'integrazione scolastica</option>
    										</select>
    								</div>
    							<div class="col-3">
    							</div>
							</div>
							
														<!-- riga nascosta per inserimento parametri -->
							<div class="row mt-3" style="display:none" id="rowInterventiNoPres">
								<div class="col-2">
								</div>
								<div class="col-2">
									<!-- Num Cart input -->
									<label for="accessiPrev">Accessi previsti</label> 
									<input type="number" class="form-control text-center number-only" id="accessiNoPrev">
								</div>
								<div class="col-4">
									<!-- Num Cart input -->
									<label for="interventi">Interventi</label> 
									<input type="text" class="form-control text-center" id="interventiNo" disabled>
								</div>
								<div class="col-2">
									<!-- Num Cart input -->
									<label for="accessiEff">Accessi effettivi</label> 
									<input type="number" class="form-control text-center number-only" id="accessiNoEff">
								</div>
								<div class="col-1">
								<a class="btn-floating btn-sm blue-gradient" onclick="mostraRiga('intNoPres','gridNoInt','accNoPrev','Noint','accNoEff',$('#accessiNoPrev').val(),$('#interventiNo').val(),$('#accessiNoEff').val(),$('#interventiNoPres').find('option:selected').val(),'interventiNoPres');" id="buttonNoGrid"><i class="fa fa-plus"></i></a>
								</div>
								<div class="col-1">
								</div>
							
							</div>
							
							<!-- riga nascosta contenente la grid -->
							<div id="gridNoInt"></div>   
							
							<hr class="mt-4"> 
							
							<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="Chiu">TerNoFeste</label> 
									<input type="number" class="form-control text-center" id="Chiu">
								</div>
								<div class="col-2">
									<label for="VisiteMed">Val Med</label> 
									<input type="number" class="form-control text-center" id="VisiteMed">
								</div>
								<div class="col-2">
									<label for="ValTer">Val Ter</label> 
									<input type="number" class="form-control text-center" id="ValTer">
								</div>
								<div class="col-2">
									<label for="InterMed">Val Psic</label> 
									<input type="number" class="form-control text-center" id="InterMed">
								</div>
								<div class="col-2">
									<label for="CounsTer">Couns Ter</label> 
									<input type="number" class="form-control text-center" id="CounsTer">
								</div>
								<div class="col-2">
									<label for="CounsMed">Couns Med</label> 
									<input type="number" class="form-control text-center" id="CounsMed">
								</div>
							</div> 
							
														<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-3"></div>
								<div class="col-2">
									<label for="ElabTer">Elab Ter</label> 
									<input type="number" class="form-control text-center" id="ElabTer">
								</div>
								<div class="col-2">
									<label for="ElabMed">Elab Med</label> 
									<input type="number" class="form-control text-center" id="ElabMed">
								</div>
								<div class="col-2">
									<label for="ValMed">Videat</label> 
									<input type="number" class="form-control text-center" id="ValMed">
								</div>
								<div class="col-3"></div>
							</div> 
							
							<hr class="mt-4"> 
	
							<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="Fisiatra">Fisiatra</label> 
									<input type="number" class="form-control text-center" id="Fisiatra">
								</div>
								<div class="col-2">
									<label for="OsInTer">OsInTer</label> 
									<input type="number" class="form-control text-center" id="OsInTer">
								</div>
								<div class="col-2">
									<label for="ValTest">SomTestMed</label> 
									<input type="number" class="form-control text-center" id="ValTest">
								</div>
								<div class="col-2">
									<label for="SomTestTer">SomTestTdr</label> 
									<input type="number" class="form-control text-center" id="SomTestTer">
								</div>
								<div class="col-2">
									<label for="ValTdr">ValTdr</label> 
									<input type="number" class="form-control text-center" id="ValTdr">
								</div>
								<div class="col-2">
									<label for="Psicol">ValPsicol</label> 
									<input type="number" class="form-control text-center" id="Psicol">
								</div>
							</div>
	
							<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="NeurPsi">ValNeurCogn</label> 
									<input type="number" class="form-control text-center" id="NeurPsi">
								</div>
								<div class="col-2">
									<label for="Npi">Npi</label> 
									<input type="number" class="form-control text-center" id="Npi">
								</div>
								<div class="col-2">
									<label for="Ortop">Ortopedico</label> 
									<input type="number" class="form-control text-center" id="Ortop">
								</div>
								<div class="col-2">
									<label for="Neuro">Neurologo</label> 
									<input type="number" class="form-control text-center" id="Neuro">
								</div>
								<div class="col-2">
									<label for="Fonia">Foniatra</label> 
									<input type="number" class="form-control text-center" id="Fonia">
								</div>
								<div class="col-2">
									<label for="Otor">Otorino</label> 
									<input type="number" class="form-control text-center" id="Otor">
								</div>
							</div>
	
							<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="Ocul">Oculista</label> 
									<input type="number" class="form-control text-center" id="Ocul">
								</div>
								<div class="col-2">
									<label for="TecAusili">TecAusili</label> 
									<input type="number" class="form-control text-center" id="TecAusili">
								</div>
								<div class="col-2">
									<label for="Terapie">Terapie</label> 
									<input type="number" class="form-control text-center" id="Terapie">
								</div>
								<div class="col-2">
									<label for="InterTer">InterEduc</label> 
									<input type="number" class="form-control text-center" id="InterTer">
								</div>
								<div class="col-2">
									<label for="IntInferm">IntInfermier</label> 
									<input type="number" class="form-control text-center" id="IntInferm">
								</div>
								<div class="col-2">
									<label for="ElabDirS">ElabDirS</label> 
									<input type="number" class="form-control text-center" id="ElabDirS">
								</div>
							</div>
							
														<!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="RequTer">REquTer</label> 
									<input type="number" class="form-control text-center" id="RequTer">
								</div>
								<div class="col-2">
									<label for="RequMed">REquMed</label> 
									<input type="number" class="form-control text-center" id="RequMed">
								</div>
								<div class="col-2">
									<label for="CounsFamM">CounsfamMed</label> 
									<input type="number" class="form-control text-center" id="CounsFamM">
								</div>
								<div class="col-2">
									<label for="CounsInsM">CounsInsMed</label> 
									<input type="number" class="form-control text-center" id="CounsInsM">
								</div>
								<div class="col-2">
									<label for="CounsEstM">CounsEstMed</label> 
									<input type="number" class="form-control text-center" id="CounsEstM">
								</div>
								<div class="col-2">
									<label for="CounsFam">CounsfamTer</label> 
									<input type="number" class="form-control text-center" id="CounsFam">
								</div>
							</div>
							
	                        <!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="CounsIns">CounsInsTer</label> 
									<input type="number" class="form-control text-center" id="CounsIns">
								</div>
								<div class="col-2">
									<label for="CounsEst">CounsEstTer</label> 
									<input type="number" class="form-control text-center" id="CounsEst">
								</div>
								<div class="col-2">
									<label for="ElabIniz">elabinizTer</label> 
									<input type="number" class="form-control text-center" id="ElabIniz">
								</div>
								<div class="col-2">
									<label for="VerInt">elabIntTer</label> 
									<input type="number" class="form-control text-center" id="VerInt">
								</div>
								<div class="col-2">
									<label for="ElabFine">elabFineTer</label> 
									<input type="number" class="form-control text-center" id="ElabFine">
								</div>
								<div class="col-2">
									<label for="MedIniz">elabMedIniz</label> 
									<input type="number" class="form-control text-center" id="MedIniz">
								</div>
							</div>
							
                            <!-- riga inserimento parametri -->
							<div class="row mt-3">
								<div class="col-2">
									<label for="MedInt">elabMedInt</label> 
									<input type="number" class="form-control text-center" id="MedInt">
								</div>
								<div class="col-2">
									<label for="MedFine">elabMedFine</label> 
									<input type="number" class="form-control text-center" id="MedFine">
								</div>
								<div class="col-2">
									<label for="AdemTer">AdemTer</label> 
									<input type="number" class="form-control text-center" id="AdemTer">
								</div>
								<div class="col-2">
									<label for="AdemMed">AdemMed</label> 
									<input type="number" class="form-control text-center" id="AdemMed">
								</div>
								<div class="col-2">
									<label for="PsiMed">TerPsic</label> 
									<input type="number" class="form-control text-center" id="PsiMed">
								</div>
								<div class="col-2">
									<label for="Ortot">IntervOrtot</label> 
									<input type="number" class="form-control text-center" id="Ortot">
								</div>
							</div>
							
							<hr class="mt-4"> 

                    </div>      
								

                            <!--/.Panel 1-->
                        
                        </div>
                    </div>
                </div>
               </div>
               </div>
               <!--Grid row-->

            </section>
            <!--Section: Blog v.4-->

    </main>
     
      
   </body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<!-- JQGrid JS -->
<script type="text/javascript" src="../js/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<script type="text/javascript" src="../progetto/js/rowData.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var vecchioPr = '<?php echo $vecchioPr ?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}

    		$('.mdb-select').material_select();
    		$('.search').select2();

            var vecchioPr = '<?php echo $vecchioPr ?>';

        	var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "0";} ?>';

    		var sezione = '<?php if($sezioneG!=""){echo $sezioneG;}else{echo "0";} ?>';
    		
    		if(rowData1!="0"){

               	var obj = jQuery.parseJSON(rowData1);

               	creaRighe(obj,"gridInt","interventiPres");
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} else if (sezione!="0") {

        		var obj = jQuery.parseJSON(sezione);

        		creaRighe(obj,"gridInt","interventiPres");
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

           	} 

    	//chiudo lo spinner
    	$.LoadingOverlay("hide");
        
    	// se viene selezionati SI tutte le checkbox sono selezionabili
    	$('#interventiPres').change(function(){
    		showComponent("buttonGrid");
    		showComponent('rowInterventiPres');
    		$('#interventi').val($('#interventiPres option:selected').text());
    		$('#accessiPrev').val("0");
    		$('#accessiEff').val("0");
    		
    	});

     	$('#interventiNoPres').change(function(){
    		showComponent('rowInterventiNoPres');
    		showComponent("buttonNoGrid");
    		$('#interventiNo').val($('#interventiNoPres option:selected').text());
    		$('#accessiNoPrev').val("0");
    		$('#accessiNoEff').val("0");
    		
    	});
	
		});


  
    </script>
</html>