<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

// session_start();

$action = $_SESSION['action'];
$nome1 = $_SESSION['nome'];
$cognome1 = $_SESSION['cognome'];
$json_rowData = "";
$arr = [];
$sezioneG = "";
$tab_sezG ="";
$vecchioPr = $_SESSION['vecchioPr'];

// recupero i dati della durata progetto per il calcolo dello standard
$durataProgetto = $_SESSION['durataProgetto'];
$feste = $_SESSION['feste'];
$domeniche = $_SESSION['domeniche'];

// recupero i dati della modalit� di intervento
$impRiab = $_SESSION['impRiab'];
$modInt = $_SESSION['modInt'];
$freqSett = $_SESSION['freqSett'];

if(isset($_SESSION['set_sectionA']['codProg'])){
    $codProj = $_SESSION['set_sectionA']['codProg'];
} else {
    $codProj = "";
}

if (isset($_SESSION['rowData'])) {
    
    $rowData = $_SESSION['rowData']; // ==""?undefined:$_SESSION['rowData'];
    if ($rowData != "") {
        $json_rowData = json_encode($rowData);
    }
    // $codProg = $rowData['codProg'];
}

if (isset($_SESSION['set_sectionG'])) {
    $action = 3;
}

if (isset($_SESSION['set_sectionG'])) {
    if ($_SESSION['set_sectionG'] !== "") {
        $section = $_SESSION['set_sectionG'];
        $sezioneG = json_encode((object) $section);
    }
}

if (isset($_SESSION['AccessiPrev'])) {
    if ($_SESSION['AccessiPrev'] !== "") {
        $tab = $_SESSION['AccessiPrev'];
        
        $tab_sezG = json_encode((object) $tab);
        
    }
}
?>

<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - PROGETTI</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />


<style type="text/css">
.tr-color-selected {
	background-color: hsla(196, 77%, 55%, 0.3) !important;
	transition: all .2s ease-in;
}
</style>


</head>


<body>

	<!-- Central Modal Small -->


	<div class="modal fade" id="modalElimina" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<!--Content-->
			<div class="modal-content">
				<!--Body-->
				<div class="modal-body">Vuoi davvero eliminare la selezione?</div>
				<input id="indexVal" style="display: none"></input>
				<!--Footer-->
				<div class="modal-footer">
					<button type="button"
						class="btn btn-secondary btn-sm waves-effect waves-light"
						data-dismiss="modal">Chiudi</button>
					<button type="button"
						class="btn btn-primary btn-sm waves-effect waves-light"
						onclick='cancellaRiga($("#indexVal").val(),"interventiPres","gridInt")'>Elimina</button>
				</div>
			</div>
			<!--/.Content-->
		</div>
	</div>


	<!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>

	<!--Main Layout-->
	<main>

	<div class="container">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center">

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
						<form method="post">
							<!--Facebook-->
							<button type="button" class="btn btn-default" id="backBtn"
								onclick="gestisciIndietro();" style="display: none">
								<i class="fa fa-arrow-circle-left left"> INDIETRO</i>
							</button>
							<!-- onclick="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
							<button type="submit" name="source" value=""
								class="btn btn-default" onclick="" style="display: none">
								<i class="fa fa-print left"> STAMPA MESE</i>
							</button>
							<button type="button" class="btn btn-default" id="salvaBtn"
								onclick="salvaDati('sezioni','G');" style="display: none">
								<i class="fa fa-save left"> SALVA</i>
							</button>
							<button type="button" class="btn btn-default" id="eliminaBtn"
								onclick="" style="display: none">
								<i class="fa fa-trash left"> ELIMINA</i>
							</button>
							<button type="button" class="btn btn-default" id="modificaBtn"
								onclick="abilitaCampi();" style="display: none">
								<i class="fa fa-edit left"> MODIFICA</i>
							</button>
							<button type="submit" name="buttonPr" value="nuovo"
								class="btn btn-default" id="nuovoPrBtn" onclick="">
								<i class="fa fa-plus left"> NUOVO PROGETTO</i>
							</button>
							<button type="button" name="buttonPr" class="btn btn-default"
								id="cercaPrBtn" onclick="$('#centralModalWarningDemo').modal();">
								<i class="fa fa-search left"> CERCA PROGETTO</i>
							</button>
							<button type="button" name="buttonPr" value="mostra"
								class="btn btn-default" id="mostraPrBtn"
								onclick="mostraProgetti();" style="display: none">
								<i class="fa fa-eye left"> MOSTRA PROGETTO</i>
							</button>
							<span class="counter" id="counterPr" style="display: none">0</span>
							<button type="button" name="buttonPr" value="anagr"
								class="btn btn-default" id="mostraAnagr"
								onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');"
								style="display: none">
								<i class="fa fa-eye left"> MOSTRA ANAGRAFICA</i>
							</button>
						</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>

		<!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="rowNominativo">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-1"></div>
						<div class="col-2">
							<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-center"
								id="nominativo_cognome" readonly>
						</div>

						<div class="col-4">
							<input type="text" class="form-control text-center"
								id="nominativo_nome" readonly>
						</div>
						<div class="col-1"></div>
					</div>
				</div>
			</div>
		</section>

		<!--Section: Blog v.4-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="sezioni">

			<!--Grid row-->
			<div class="row">
				<div class="col-md-12">

					<!-- Nav tabs -->
					<div class="tabs-wrapper">
						<ul class="nav classic-tabs tabs-cyan" role="tablist">
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','A','<?php echo $action ?>');"
								role="tab" href="#">Sezione A</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','B','<?php echo $action ?>');"
								role="tab" href="#">Sezione B</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','C','<?php echo $action ?>');"
								role="tab" href="#">Sezione C</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','D','<?php echo $action ?>');"
								role="tab" href="#">Sezione D Valut.Iniz.</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','E','<?php echo $action ?>');"
								role="tab" href="#">Sezione D Prof.Disab.</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','F','<?php echo $action ?>');"
								role="tab" href="#">Sezione D Indici Dis.</a></li>
							<li class="nav-item"><a class="nav-link waves-light active"
								role="tab" href="#">D - E Interventi</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','H','<?php echo $action ?>');"
								role="tab" href="#">E Sospensioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','I','<?php echo $action ?>');"
								role="tab" href="#">F Conclusioni</a></li>
							<li class="nav-item"><a class="nav-link waves-light"
								onclick="recuperaElementi('sezioni','G','M','<?php echo $action ?>');"
								role="tab" href="#">Cartella Clinica</a></li>
						</ul>
					</div>

					<!-- Tab panels -->
					<div class="tab-content card">

						<!--Panel 1-->
						<div class="tab-pane fade in show active" id="progetto_sezG"
							role="tabpanel">


							<div class="row">
								<div class="col-1"></div>
								<div class="col-2">
									<label for="terapie">Terapie</label> <input type="text"
										class="form-control text-center" id="terapie" value="0" disabled> </input>
								</div>
								<div class="col-2">
									<label for="terapieNoFeste">Terapie No Feste</label> <input
										type="text" class="form-control text-center"
										id="terapieNoFeste" value="0"> </input>
								</div>
								<div class="col-3">
									<label for="standard">STANDARD</label> <input type="text"
										class="form-control text-center" id="standard" value="0"> </input>
								</div>
								<div class="col-1 align-middle">
									<i class="fas fa-check fa-3x animated rotateIn"
										style="color: green; display: none" id="ok"></i> <i
										class="fas fa-exclamation fa-3x animated rotateIn"
										style="color: orange; display: none" id="warning"></i> <i
										class="fas fa-exclamation-triangle fa-3x animated rotateIn"
										style="color: red; display: none" id="ko"></i>
								</div>
								<div class="col-3"></div>

							</div>

							<!-- linea di divisione -->
							<hr class="mt-4">
							<!--  -->



							<!-- Riga con tabella con accessi in presenza dell'utente -->
							<div class="row">
								<div class="col-6">
									<!-- Table with panel -->
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

											<a class="white-text mx-3 text-center">Accessi in presenza
												dell'utente</a>
										</div>
										<!--/Card image-->

										<div class="px-4">

											<div class="table-wrapper table-editable">
												<!--Table-->
												<table class="table table-hover mb-0 table-sm text-center">

													<!--Table head-->
													<thead>
														<tr>
															<th class="th-sm">#Accessi previsti</th>
															<th class="th-lg">Accessi</th>
															<th class="th-sm">#Accessi effettivi</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="1" class="tab_sx">
															<td id="ValutazioneOsservazioneNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Valutazione ed osservazione</td>
															<td>0</td>
														</tr>
														<tr id="2" class="tab_sx">
															<td id="RiabilitazioneLogopedicaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione logopedica</td>
															<td>0</td>
														</tr>
														<tr id="3" class="tab_sx">
															<td id="RiabilitazioneCardiologicaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione cardiologica</td>
															<td>0</td>
														</tr>
														<tr id="4" class="tab_sx">
															<td id="RiabilitazioneRespiratoriaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione respiratoria</td>
															<td>0</td>
														</tr>
														<tr id="5" class="tab_sx">
															<td id="RiabilitazioneNeuromorotiaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione neuro-motoria</td>
															<td>0</td>
														</tr>
														<tr id="6" class="tab_sx">
															<td id="RiabilitazionePsicomotoriaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione psicomotoria</td>
															<td>0</td>
														</tr>
														<tr id="7" class="tab_sx">
															<td id="RiabilitazioneCognitivaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione cognitiva/neuropsicologica</td>
															<td>0</td>
														</tr>
														<tr id="8" class="tab_sx">
															<td id="TerapiaPsicologicaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Terapia psicologica</td>
															<td>0</td>
														</tr>
														<tr id="9" class="tab_sx">
															<td id="TerapiaOccupazionaleNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riabilitazione occupazionale</td>
															<td>0</td>
														</tr>
														<tr id="10" class="tab_sx">
															<td id="InterventoOrtotticoNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Intervento ortottico</td>
															<td>0</td>
														</tr>
														<tr id="11" class="tab_sx">
															<td id="InterventoEducativoNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Intervento educativo</td>
															<td>0</td>
														</tr>
														<tr id="12" class="tab_sx">
															<td id="IntInfermieristico_prev" class="accessiPrev">0</td>
															<td>Intervento infermieristico</td>
															<td>0</td>
														</tr>
													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
									<!-- Table with panel -->
								</div>



								<!-- Tabella per gli input del progetto riepilogativi -->

								<div class="col-6">
									<!-- Table with panel -->
									<div class="card card-cascade narrower ml-2"
										style="display: none" id="card_riepilogo">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

											<a class="white-text mx-3 text-center">Dettaglio accessi</a>

											<div>
												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="abilita_campi_tabella_presenza()">
													<i class="fas fa-pencil-alt mt-0"></i>
												</button>

												<button type="button"
													class="btn btn-outline-white btn-rounded btn-sm px-2"
													onclick="disabilita_campi_tabella_presenza()">
													<i class="fas fa-save mt-0"></i>
												</button>
											</div>

										</div>
										<!--/Card image-->

										<div class="px-4">

											<div class="table-wrapper table-editable">
												<!--Table-->
												<table class="table table-hover mb-0 table-sm text-center" id="table_dx">

													<tbody>
														<tr class="1 tab_dx" style="display: none">
															<td>Valutazione Terapista</td>
															<td class="num_tab_dx" id="ValTdr">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Valutazione Logopedica</td>
															<td class="num_tab_dx" id="ValLogo">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Valutazione Psicologica</td>
															<td class="num_tab_dx" id="Psicol">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Valutazione Neuro-Cognitiva</td>
															<td class="num_tab_dx" id="NeurPsi">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Valutazione Neuro-Psichiatria infantile</td>
															<td class="num_tab_dx" id="Npi">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Osservazione interna Terapista</td>
															<td class="num_tab_dx" id="OsInTer">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Somma Test Medico</td>
															<td class="num_tab_dx" id="ValTest">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Somma Test Terapista</td>
															<td class="num_tab_dx" id="SomTestTer">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Fisiatra</td>
															<td class="num_tab_dx" id="Fisiatra">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Neurologo</td>
															<td class="num_tab_dx" id="Neuro">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Foniatra</td>
															<td class="num_tab_dx" id="Fonia">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Oculista</td>
															<td class="num_tab_dx" id="Ocul">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Otorino</td>
															<td class="num_tab_dx" id="Otor">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Ortopedico</td>
															<td class="num_tab_dx" id="Ortop">0</td>
														</tr>
														<tr class="1 tab_dx" style="display: none">
															<td>Tecar ausiliare</td>
															<td class="num_tab_dx" id="TecAusili">0</td>
														</tr>
														<tr class="2 tab_dx" style="display: none">
															<td>Riabilitazione logopedica</td>
															<td class="num_tab_dx" id="RiabiLogo">0</td>
														</tr>
														<tr class="3 tab_dx" style="display: none">
															<td>Riabilitazione cardiologica</td>
															<td class="num_tab_dx " id="RiabiCardio">0</td>
														</tr>
														<tr class="4 tab_dx" style="display: none">
															<td>Riabilitazione respiratoria</td>
															<td class="num_tab_dx " id="RiabiRespi">0</td>
														</tr>
														<tr class="5 tab_dx" style="display: none">
															<td>Riabilitazione neuro-motoria</td>
															<td class="num_tab_dx " id="RiabiNeuro">0</td>
														</tr>
														<tr class="6 tab_dx" style="display: none">
															<td>Riabilitazione psicomotoria</td>
															<td class="num_tab_dx " id="RiabiPsico">0</td>
														</tr>
														<tr class="7 tab_dx" style="display: none">
															<td>Riabilitazione cognitiva/neuropsicologica</td>
															<td class="num_tab_dx " id="RiabiCogni">0</td>
														</tr>
														<tr class="8 tab_dx" style="display: none">
															<td>Terapia psicologica</td>
															<td class="num_tab_dx" id="PsiMed">0</td>
														</tr>
														<tr class="10 tab_dx" style="display: none">
															<td>Intervento ortottico</td>
															<td class="num_tab_dx" id="Ortot">0</td>
														</tr>
														<tr class="11 tab_dx" style="display: none">
															<td>Intervento educativo</td>
															<td class="num_tab_dx" id="InterTer">0</td>
														</tr>
														<tr class="12 tab_dx" style="display: none">
															<td>Intervento infermieristico</td>
															<td class="num_tab_dx" id="IntInferm">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling familiare Medico</td>
															<td class="num_tab_dx" id="CounsFamM">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling interno Medico</td>
															<td class="num_tab_dx" id="CounsInsM">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling esterno Medico</td>
															<td class="num_tab_dx" id="CounsEstM">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling interno Terapista</td>
															<td class="num_tab_dx" id="CounsIns">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling esterno Terapista</td>
															<td class="num_tab_dx" id="CounsEst">0</td>
														</tr>
														<tr class="14 tab_dx" style="display: none">
															<td>Counseling familiare Terapista</td>
															<td class="num_tab_dx" id="CounsFam">0</td>
														</tr>
														<tr class="15 tab_dx" style="display: none">
															<td>Riunione d'equipe Terapisti</td>
															<td id="RequTer" class="num_tab_dx">0</td>
														</tr>
														<tr class="15 tab_dx" style="display: none">
															<td>Riunione d'equipe Medici</td>
															<td id="RequMed" class="num_tab_dx">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione iniziale Terapista</td>
															<td class="num_tab_dx" id="ElabIniz">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione intervento Terapista</td>
															<td class="num_tab_dx" id="VerInt">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione finale Terapista</td>
															<td class="num_tab_dx" id="ElabFine">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione iniziale Medico</td>
															<td class="num_tab_dx" id="MedIniz">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione intervento Medico</td>
															<td class="num_tab_dx" id="MedInt">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione finale Medico</td>
															<td class="num_tab_dx" id="MedFine">0</td>
														</tr>
														<tr class="13 tab_dx" style="display: none">
															<td>Elaborazione Direttore Sanitario</td>
															<td class="num_tab_dx" id="ElabDirS">0</td>
														</tr>
														<tr class="16 tab_dx" style="display: none">
															<td>Adempimento Terapista</td>
															<td id="AdemTer" class="num_tab_dx">0</td>
														</tr>
														<tr class="16 tab_dx" style="display: none">
															<td>Adempimento Medico</td>
															<td id="AdemMed" class="num_tab_dx">0</td>
														</tr>
													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
									<!-- Table with panel -->
								</div>
							</div>

							<!-- Riga con tabella con accessi in assenza dell'utente -->
							<div class="row mt-2">
								<div class="col-6">
									<!-- Table with panel -->
									<div class="card card-cascade narrower ml-2">

										<!--Card image-->
										<div
											class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

											<a class="white-text mx-3 text-center">Accessi in assenza
												dell'utente</a>

										</div>
										<!--/Card image-->

										<div class="px-4">

											<div class="table-wrapper">
												<!--Table-->
												<table class="table table-hover mb-0 table-sm text-center">

													<!--Table head-->
													<thead>
														<tr>
															<th class="th-sm">#Accessi previsti</th>
															<th class="th-lg">Accessi</th>
															<th class="th-sm">#Accessi effettivi</th>
														</tr>
													</thead>
													<!--Table head-->

													<!--Table body-->
													<tbody>
														<tr id="13" class="tab_sx">
															<td id="ElaborazioneRevisioneProgettiNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Elaborazione/revisione progetto</td>
															<td>0</td>
														</tr>
														<tr id="14" class="tab_sx">
															<td id="CounselingNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Counseling</td>
															<td>0</td>
														</tr>
														<tr id="15" class="tab_sx">
															<td id="RiunioneEquipeNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Riunione d'equipe</td>
															<td>0</td>
														</tr>
														<tr id="16" class="tab_sx">
															<td id="AdempIntegrScolasticaNumeroAccessi_prev" class="accessiPrev">0</td>
															<td>Adempimento di cui alla L. n 104/92 per
																l'integrazione scolastica</td>
															<td>0</td>
														</tr>
													</tbody>
													<!--Table body-->
												</table>
												<!--Table-->
											</div>

										</div>

									</div>
									<!-- Table with panel -->

								</div>
							</div>





						</div>
					</div>
				</div>
			</div>
			<!--Grid row-->

		</section>
		<!--Section: Blog v.4-->
		<div style="height: 100px;"></div>
	
	</main>




</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="../progetto/js/progetto.js"></script>
<script type="text/javascript" src="../progetto/js/progetto_sezG.js"></script>
<!-- JQGrid JS -->
<script type="text/javascript" src="../js/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="../progetto/js/JQGrid.js"></script>
<script type="text/javascript" src="../progetto/js/rowData.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>

<!-- MDB table init -->
<script type="text/javascript" src="../js/addons/datatables.min.js"></script>
<script type="text/javascript" src="../js/addons/mdb-editor.js"></script>
<!-- fine init table -->


<script>
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		//inizializzo la NavBar
		
		var path = '<?php echo $baseurl;?>';
		
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        new WOW().init();
        
        $(document).ready(function () {

        var nome = '<?php echo $nome1; ?>';

    	var cognome = '<?php echo $cognome1; ?>';

    	var action = '<?php echo $action; ?>';

    	var durataProgetto = '<?php echo $durataProgetto; ?>';

    	var feste = '<?php echo $feste; ?>';

    	var domeniche = '<?php echo $domeniche; ?>';

    	var impRiab = '<?php echo $impRiab; ?>';

    	var modInt = '<?php echo $modInt; ?>';

    	var freqSett = '<?php echo $freqSett; ?>';
    	
    	var vecchioPr = '<?php echo $vecchioPr; ?>';

    	var codProj = '<?php echo $codProj;?>';

    	$('#nominativo_cognome').val(cognome);

		$('#nominativo_nome').val(nome);

    	//inizializzo le select
		hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
		
        showComponent('backBtn');

		if(vecchioPr=="1"){
        	showComponent('modificaBtn');
		} else {
			showComponent('salvaBtn');	
		}

    		$('.mdb-select').material_select();
    		$('.search').select2();

    		var tab_sezG = '<?php echo $tab_sezG;?>';
    		
        	var rowData1 = '<?php if($json_rowData!=""){echo $json_rowData;}else{echo "";} ?>';

    		var sezione = '<?php if($sezioneG!=""){echo $sezioneG;}else{echo "";} ?>';

       		 if(tab_sezG!=""){
            		
         		riempiElementi("sezioni","input",jQuery.parseJSON(tab_sezG));
    				
     		}

       	 	//calcolo le terapie (con feste incluse ed ecluse)
     		if(durataProgetto!=""&freqSett!=""&domeniche!=""){
     			//feste incluse
     			$('#terapie').val(Math.round(parseInt(freqSett)*(parseInt(durataProgetto)+parseInt(domeniche))/7));

     			//feste escluse
     			$('#terapieNoFeste').val(Math.round(parseInt(freqSett)*(parseInt(durataProgetto)+parseInt(domeniche)+parseInt(feste))/7));
     		}
    		
    		if(rowData1!=""){

           		if(tab_sezG=="") {
            		
        			//effettuo la query per la tabella relativa agli esami strumentali
    				$.ajax({
    					type : 'post',
    					url : '../php/controller.php',
    					data : {
    						'source' : 'cercaAccessiPrev',
    						'codProg': codProj
    					},
    					success : function(response) {
     						var exaStrum = jQuery.parseJSON(response);
    						
    						if(exaStrum!="") {

    							riempiTabelleAccessi('sezioni',exaStrum[0]);

    						}
    						
    					},
    					error : function() {
    						alert("error");
    					}
    				});

               	}

               	var obj = jQuery.parseJSON(rowData1);

               	creaRighe(obj,"gridInt","interventiPres");
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('table_dx','td',obj,"1");

           	} 

       

            if (sezione!="") {

        		var obj = jQuery.parseJSON(sezione);

        		creaRighe(obj,"gridInt","interventiPres");
    
               	riempiElementi('sezioni','input',obj,"1");
    
               	riempiElementi('sezioni','select',obj,"1");

               	riempiElementi('table_dx','td',obj,"1");

			} 


    	//al fine di calcolare il coefficiente � necessario impostare durata progetto, modalit� e impegno dell'intervento 
    	if (durataProgetto==""){
        	if(impRiab==""&modInt==""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare la durata progetto, modalit&agrave; e impegno d'intervento!");
        	} else if (impRiab!=""&modInt==""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare la durata progetto, modalit&agrave; d'intervento!");
        	} else if (impRiab==""&modInt!=""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare la durata progetto e impegno d'intervento!");
        	} else {
    		toastr.error('Per calcolare il coefficiente &egrave; necessario impostare la durata progetto!');
        	}
    	} else {
        	if(impRiab==""&modInt==""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare la modalit&agrave; e impegno d'intervento!");
        	} else if (impRiab!=""&modInt==""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare la modalit&agrave; d'intervento!");
        	} else if (impRiab==""&modInt!=""){
        		toastr.error("Per calcolare il coefficiente &egrave; necessario impostare l'impegno d'intervento!");
        	} else {
        		calcolaPresenze(modInt,impRiab,freqSett,durataProgetto,feste,domeniche);
            } 
    	}

    	calcolaPresenze(modInt,impRiab,freqSett,durataProgetto,feste,domeniche);
    	
      		
    	//chiudo lo spinner
    	$.LoadingOverlay("hide");

    	//funzione che controlla la selezione e la visualizzazione delle righe della tabella/e
        $("tbody tr").click(function () {

    		var classe = this.className;


            if(classe=="tab_sx") {
                
            $('.tr-color-selected').removeClass('tr-color-selected');
            $(this).addClass("tr-color-selected");
            $('#card_riepilogo').show()
            
            for(i=1;i<17;i++){
                if (i==this.id) {
                    $('.'+i + '.tab_dx').val(this.outerText.substring(0,2));
            		$('.'+i + '.tab_dx').show();
                } else {
                	$('.'+i + '.tab_dx').hide();
                }
            }

            } else {
                
            }
        });

        $("#terapieNoFeste").on("change paste keyup", function() {
        	calcolaStandard(modInt,impRiab,freqSett,durataProgetto,feste,domeniche);
        });

		//funzione che verifica che il valore inserito. Se viene inserita una lettera, viene mostrato un toast.
        $(".num_tab_dx").keypress(function (e) {
            //se il tasto digitato non � un numero ritorna l'errore
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               toastr.error('Non � stato inserito un numero valido!');
                      return false;
           } 
          });

        $(".num_tab_dx").focusout(function(){
            	this.innerText=parseInt(this.innerText);
        	 	calcolaPresenze(modInt,impRiab,freqSett,durataProgetto,feste,domeniche);
        	});

    });


  
    </script>
</html>