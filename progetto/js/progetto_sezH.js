function creaOption_day(json,index) {

		var prova = $('#Diaria_'+index);
		
		prova.append($("<option></option>").attr("value", "").text("Seleziona"));


		for (var i = json.length - 1; i >= 0; i--) {
			prova.append(
					//$("<option></option>").attr("value", json[i].Acesso).text(json[i]["Acesso"]));
					$("<option></option>").attr("value", json[i].Id).text(json[i]["Acesso"]));
		}

}

//funzione che aggiunge righe alla tabella Diaria

function addNewRows_Day() {


	  var rowCount = $('.tab_count_day').length + 1;
		
	   $('#Table_Day tr:last').after(

			'<tr id="'+rowCount+'" class="tab_count_day">'+
				'<td class="align-middle">'+
					'<input type="text" id="DataDiaria_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="Diaria_'+rowCount+'"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="Diaria1_'+rowCount+'">'+
					    '<OPTION value="">Seleziona</OPTION >'+
                        '<OPTION value="1">Iniziale</OPTION >'+
                        '<OPTION value="2">Intermedia</OPTION >'+
                        '<OPTION value="3">Finale</OPTION >'+
                    '</select>'+
             '</td>'+
           '</tr>'
	 
	);

	   $('#Table_Day tr:last').after(
			'<tr>'+
			'<td colspan="7"></td>'+
			'</tr>'
	   );

	//inizializzo le select
	creaOption_day(query_diaria,rowCount);
	$('#Diaria_'+rowCount).select2();
	$('#Diaria1_'+rowCount).select2();
	
		//inizializzo il datepicker
	$(".datepicker").pickadate({

		  today: 'Oggi',
		  clear: '',
		  close: 'Chiudi',
		  format: 'dd/mm/yyyy'
    });
}


//funzione che aggiunge 

function addNewRows_Cons() {


	  var rowCount = $('.tab_count_cons').length + 1;
		
	   $('#Table_Cons tr:last').after(

			'<tr id="'+rowCount+'" class="tab_count_cons">'+
				'<td class="align-middle">'+
					'<input type="text" id="Data_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
				'</td>'+
				'<td class="align-middle">'+
					'<input type="text" class="form-control text-center" id="Motivo_'+rowCount+'">'+
				'</td>'+
				'<td class="align-middle">'+
					'<input type="text" class="form-control text-center" id="Consulenza_'+rowCount+'">'+
				'</td>'+
			'</tr>'
	 
	);

	   $('#Table_Cons tr:last').after(
			'<tr>'+
			'<td colspan="7"></td>'+
			'</tr>'
	   );

	
		//inizializzo il datepicker
	$(".datepicker").pickadate({

		  today: 'Oggi',
		  clear: '',
		  close: 'Chiudi',
		  format: 'dd/mm/yyyy'
    });
}



function riempiTabellaDiaria(elemento, response, countVF) {
	
	for(i=0;i<countVF;i++) {
			
		$('#' + elemento + ' input').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;

					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i][campo2],"/")).change;
							} else {
									
								
									//$('#' + campo).val(response[i][campo2]).change;
								$('#' + campo).val(specialChar("§", response[i][campo2],"'")).change;
								}
								
							}
						
						}

		});
		
		$('#' + elemento + ' select').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i][campo2],"/")).change;
							} else {
								$('#' + campo).val(response[i][campo2]).change;
								
							}
						
						}
					}
		});
		
	}	
		
	// reinizializzo le select!
	if ($('.search').hasClass("select2-hidden-accessible")){
		$('.search').select2('destroy');

		$('.search').select2();

	}

	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();
	
	}
