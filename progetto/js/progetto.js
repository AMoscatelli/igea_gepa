var modalShow = false;

function gestisciIndietro() {

	var windowLoc = ($(location).attr('pathname')).split("/");

	if (windowLoc[windowLoc.length - 1] != "progetto_sezA.php") {
		var url = path + '/progetto/progetto_sezA.php';
		window.location = url;
	}

	if ($('#gridPazienti').is(":visible")) {
		// quando la grid pazienti è visibile si torna alla card di creazione
		hideComponent('gridPazienti');
		hideComponent('mostraPrBtn');
		hideComponent('backBtn');
		hideComponent('salvaBtn');
		hideComponent('eliminaBtn');
		showComponent('nuovoPrBtn');
		showComponent('cercaPrBtn');
		hideComponent('counterPr');

	} else if ($('#gridProgetti').is(":visible")) {
		// quando la grid pazienti è visibile si torna alla card di creazione
		showComponent('gridPazienti');
		hideComponent('gridProgetti');
		showComponent('mostraPrBtn');
		hideComponent('mostraAnagr');
		// showComponent('counterPr');
		hideComponent('rowNominativo');
		hideComponent('counterPr');
		$('#grid1').jqGrid('resetSelection');

	} else if ($('#sezioni').is(":visible")) {
		var stato = $('#stato').prop('disabled');
		if (stato == false && !modalShow) {
			doModal("salvaProgetto", "Salva Progetto",
					"Vuoi salvare le modifiche?", "salva()", "Salva",
					"gestisciIndietro()", "Esci senza salvare", 2, "fa-save");
			modalShow = true;// $('#salvaProgetto').modal();
		} else {
			// rinizializzo la variabile che mostra la modal
			modalShow = false;

			// quando la grid sezioni è visibile si torna alla tabella pazienti
			// se è un nuovo progetto, altrimenti si torna alla grid pazienti
			if ($('#vecchioPr').val() == "1") {
				hideComponent('sezioni');
				hideComponent('VF_section');
				showComponent('gridProgetti');
				showComponent('rowNominativo');
				hideComponent('salvaBtn');
				hideComponent('modificaBtn');
				resetSezioni("A");
				$('#vecchioPr').val("0");
			} else {
				hideComponent('sezioni');
				hideComponent('VF_section');
				showComponent('gridPazienti');
				showComponent('mostraPrBtn');
				showComponent('counterPr');
				hideComponent('rowNominativo');
				hideComponent('salvaBtn');
				hideComponent('modificaBtn');
				hideComponent('counterPr');
				$('#grid1').jqGrid('resetSelection');
				resetCampi();
				// $('.mdb-select').material_select();
				$('#nuovoPr').val("0");

			}
		}
	}
}

// funzione che in caso di select con stessi campi, una volta selezionato il
// campo in 1, lo disabilita nelle altre select
function rimuoviDuplicati(campo, array, json) {

	var vari = [];
	for (var i = 0; i < array.length; i++) {
		$('#' + array[i]).children().filter(function() {
			return $(this).attr("value");
		}).each(function() {
			$(this).prop('disabled', false);
		});

		vari[i] = $('#' + array[i]).find("option:selected").val();
	}

	for (var i = 0; i < array.length; i++) {
		$('#' + array[i]).find('option[value="' + campo.value + '"]').prop(
				'disabled', true);
		for (var a = 0; a < vari.length; a++) {
			if (vari[a] != "") {
				$('#' + array[i]).find('option[value="' + vari[a] + '"]').prop(
						'disabled', true);
			}
		}
		$('#' + array[i]).select2('destroy').select2();
	}
}

// funzione per visualizzare la grid Interventi con presenza utente
function mostraGridIntervento() {
	initGrid($("#rowNominativo").innerWidth());
	showComponent('gridInterventiPrev');
	AddRow();
}

// funzione per visualizzare la grid Interventi con assenza utente
function mostraGridInterventoNo() {
	// initGrid();
	initGridNo($("#rowNominativo").innerWidth());
	showComponent('gridInterventiNoPrev');
	AddRow2();
}

function verificaOption(nome, visualizza) {
	if ($('#' + nome).val() == "1") {
		showComponent(visualizza);
	}
}

// abilita i campi del progett in attesa e scrive in session lo stato del
// progetto per
// gestire i campi nelle altre sezioni
function attesa() {
	// abilito i capi
	$('#dateStato').prop('disabled', false);
	$('#intervento').prop('disabled', false);
	$('#regAssist').prop('disabled', false);
	$('#notaStato').prop('disabled', false);
	// disabilito quelli che non servono
	$('#oldKey').prop('disabled', true);
	$('#numCart').prop('disabled', true);
	$('#PdaAg').prop('disabled', true);
	$('#PAgto').prop('disabled', true);
	$('#dataInizio').prop('disabled', true);
	$('#dataInizio').css('background-color', '#e9ecef');
	$('#durataProgetto').prop('disabled', true);
	$('#modInt').prop('disabled', true);
	$('#impRiab').prop('disabled', true);
	$('#medico').prop('disabled', true);
	$('#freqset1').prop('disabled', true);
	$('#freqset2').prop('disabled', true);
	$('#freqset3').prop('disabled', true);
	$('#anamnesi').prop('disabled', true);
	$('#esameObbiettivoG').prop('disabled', true);
	$('#esameObbiettivoS').prop('disabled', true);

}

function deceduto() {
	// disabilito quelli che non servono
	$('#dateStato').prop('disabled', true);
	$('#intervento').prop('disabled', true);
	$('#regAssist').prop('disabled', true);
	$('#notaStato').prop('disabled', true);
	$('#oldKey').prop('disabled', true);
	$('#numCart').prop('disabled', true);
	$('#PdaAg').prop('disabled', true);
	$('#PAgto').prop('disabled', true);
	$('#dataInizio').prop('disabled', true);
	$('#dataInizio').css('background-color', '#e9ecef');
	$('#durataProgetto').prop('disabled', true);
	$('#modInt').prop('disabled', true);
	$('#impRiab').prop('disabled', true);
	$('#medico').prop('disabled', true);
	$('#freqset1').prop('disabled', true);
	$('#freqset2').prop('disabled', true);
	$('#freqset3').prop('disabled', true);
	$('#anamnesi').prop('disabled', true);
	$('#esameObbiettivoG').prop('disabled', true);
	$('#esameObbiettivoS').prop('disabled', true);

}

function sospeso() {
	// disabilito quelli che non servono
	$('#dateStato').prop('disabled', true);
	$('#intervento').prop('disabled', true);
	$('#regAssist').prop('disabled', true);
	$('#notaStato').prop('disabled', true);
	$('#oldKey').prop('disabled', true);
	$('#numCart').prop('disabled', true);
	$('#PdaAg').prop('disabled', true);
	$('#PAgto').prop('disabled', true);
	$('#dataInizio').prop('disabled', true);
	$('#dataInizio').css('background-color', '#e9ecef');
	$('#durataProgetto').prop('disabled', true);
	$('#modInt').prop('disabled', true);
	$('#impRiab').prop('disabled', true);
	$('#medico').prop('disabled', true);
	$('#freqset1').prop('disabled', true);
	$('#freqset2').prop('disabled', true);
	$('#freqset3').prop('disabled', true);
	$('#anamnesi').prop('disabled', true);
	$('#esameObbiettivoG').prop('disabled', true);
	$('#esameObbiettivoS').prop('disabled', true);

}

function autorizzato() {
	// disabilito quelli che non servono
	$('#dateStato').prop('disabled', true);
	$('#intervento').prop('disabled', true);
	$('#regAssist').prop('disabled', true);
	$('#notaStato').prop('disabled', true);
	$('#oldKey').prop('disabled', true);
	$('#numCart').prop('disabled', true);
	$('#PdaAg').prop('disabled', true);
	$('#PAgto').prop('disabled', true);
	$('#dataInizio').prop('disabled', true);
	$('#dataInizio').css('background-color', '#e9ecef');
	$('#durataProgetto').prop('disabled', true);
	$('#modInt').prop('disabled', true);
	$('#impRiab').prop('disabled', true);
	$('#medico').prop('disabled', true);
	$('#freqset1').prop('disabled', true);
	$('#freqset2').prop('disabled', true);
	$('#freqset3').prop('disabled', true);
	$('#anamnesi').prop('disabled', true);
	$('#esameObbiettivoG').prop('disabled', true);
	$('#esameObbiettivoS').prop('disabled', true);

}

function trattamento() {
	// abilito i capi
	$('#dateStato').prop('disabled', false);
	$('#intervento').prop('disabled', false);
	$('#regAssist').prop('disabled', false);
	$('#notaStato').prop('disabled', false);

	$('#oldKey').prop('disabled', false);
	$('#numCart').prop('disabled', false);
	$('#PdaAg').prop('disabled', false);
	$('#PAgto').prop('disabled', false);
	$('#dataInizio').prop('disabled', false);
	$('#dataInizio').css('background-color', '#e0f5c7');
	$('#durataProgetto').prop('disabled', false);
	$('#modInt').prop('disabled', false);
	$('#impRiab').prop('disabled', false);
	$('#medico').prop('disabled', false);
	$('#freqset1').prop('disabled', false);
	$('#freqset2').prop('disabled', false);
	$('#freqset3').prop('disabled', false);
	$('#anamnesi').prop('disabled', false);
	$('#esameObbiettivoG').prop('disabled', false);
	$('#esameObbiettivoS').prop('disabled', false);

}


//funzione che calcola la somma di tutte il dettaglio delle presenze (progetto_sezG)
function calcolaPresenze(modInt,impRiab,freqSett,durataProgetto,feste,domeniche){
	
	//calcolo la somma per gli accessi previsti per le riunioni d'equipe
	$('#RiunioneEquipeNumeroAccessi_prev').html(parseInt($('#RequTer').html())+parseInt($('#RequMed').html()));
	
	//calcolo la somma per gli accessi previsti per l'adempimento integrazione scolastica
	$('#AdempIntegrScolasticaNumeroAccessi_prev').html(parseInt($('#AdemTer').html())+parseInt($('#AdemMed').html()));
	
	//calcolo la somma per gli accessi previsti per l'elaborazione
	$('#ElaborazioneRevisioneProgettiNumeroAccessi_prev').html(parseInt($('#ElabIniz').html())+parseInt($('#VerInt').html())+parseInt($('#ElabFine').html())+parseInt($('#MedIniz').html())+parseInt($('#MedInt').html())+parseInt($('#MedFine').html())+parseInt($('#ElabDirS').html()));
	
	//calcolo la somma per gli accessi previsti per il counseling
	$('#CounselingNumeroAccessi_prev').html(parseInt($('#CounsFamM').html())+parseInt($('#CounsInsM').html())+parseInt($('#CounsEstM').html())+parseInt($('#CounsIns').html())+parseInt($('#CounsEst').html())+parseInt($('#CounsFam').html()));
	
	//calcolo la somma per gli accessi previsti per l'intervento educativo
	$('#InterventoEducativoNumeroAccessi_prev').html(parseInt($('#InterTer').html()));
	
	//calcolo la somma per gli accessi previsti per l'intervento ortottico
	$('#InterventoOrtotticoNumeroAccessi_prev').html(parseInt($('#Ortot').html()));

	//calcolo la somma per gli accessi previsti per la terapia psicologica
	$('#TerapiaPsicologicaNumeroAccessi_prev').html(parseInt($('#PsiMed').html()));
	
	//calcolo la somma per gli accessi previsti per l'intervento infermieristico
	$('#IntInfermieristico_prev').html(parseInt($('#IntInferm').html()));
	
	//calcolo la somma per gli accessi previsti per le riabilitazioni
	$('#RiabilitazioneLogopedicaNumeroAccessi_prev').html(parseInt($('#RiabiLogo').html()));
	$('#RiabilitazioneCardiologicaNumeroAccessi_prev').html(parseInt($('#RiabiCardio').html()));
	$('#RiabilitazioneRespiratoriaNumeroAccessi_prev').html(parseInt($('#RiabiRespi').html()));
	$('#RiabilitazioneNeuromorotiaNumeroAccessi_prev').html(parseInt($('#RiabiNeuro').html()));
	$('#RiabilitazionePsicomotoriaNumeroAccessi_prev').html(parseInt($('#RiabiPsico').html()));
	$('#RiabilitazioneCognitivaNumeroAccessi_prev').html(parseInt($('#RiabiCogni').html()));
	
	//calcolo la somma per gli accessi previsti per la valutazione
	$('#ValutazioneOsservazioneNumeroAccessi_prev').html(parseInt($('#ValTdr').html())+parseInt($('#ValLogo').html())+parseInt($('#Psicol').html())+parseInt($('#NeurPsi').html())+parseInt($('#Npi').html())+parseInt($('#OsInTer').html())+parseInt($('#SomTestTer').html())+parseInt($('#Fonia').html())+parseInt($('#Neuro').html())+parseInt($('#Ocul').html())+parseInt($('#Otor').html())+parseInt($('#Fisiatra').html())+parseInt($('#Ortop').html())+parseInt($('#TecAusili').html())+parseInt($('#ValTest').html()));

	calcolaStandard(modInt,impRiab,freqSett,durataProgetto,feste,domeniche);
}

function calcolaStandard(modInt,impRiab,freqSett,durataProgetto,feste,domeniche){
	
	// calcolo il totale delle presenze relative ai terapisti
	var ValTer = parseInt($('#ValTdr').text()) + parseInt($('#ValLogo').text()) + parseInt($('#SomTestTer').text());
	
	var CounsTer = parseInt($('#CounsIns').text()) + parseInt($('#CounsEst').text()) + parseInt($('#CounsFam').text());
	
	var ElabTer = parseInt($('#ElabIniz').text()) + parseInt($('#VerInt').text()) + parseInt($('#ElabFine').text());
	
	var totTer = ValTer + CounsTer + ElabTer + parseInt($('#terapieNoFeste').val()) + parseInt($('#Ortot').text()) + parseInt($('#RequTer').text()) + parseInt($('#AdemTer').text()) + parseInt($('#InterTer').text());
	
	// calcolo il totale delle presenze relative ai medici
	
	var ValMed = parseInt($('#OsInTer').text()) + parseInt($('#ValTest').text()) + parseInt($('#Npi').text()) + parseInt($('#Ortop').text()) + parseInt($('#Neuro').text()) + parseInt($('#Fonia').text()) + parseInt($('#Ocul').text()) + parseInt($('#Otor').text()) + parseInt($('#Fisiatra').text()) + parseInt($('#TecAusili').text());
	
	var CounsMed = parseInt($('#CounsInsM').text()) + parseInt($('#CounsEstM').text()) + parseInt($('#CounsFamM').text());
	
	var ElabMed = parseInt($('#MedIniz').text()) + parseInt($('#MedInt').text()) + parseInt($('#MedFine').text());
		
	var totMed = ValMed + parseInt($('#Psicol').text()) + parseInt($('#NeurPsi').text()) + CounsMed + ElabMed + parseInt($('#ElabDirS').text()) + parseInt($('#RequMed').text()) + parseInt($('#PsiMed').text()) + parseInt($('#AdemMed').html());
	
	//calcolo il numero totali di accessi
	
	// var totAcc = totTer + totMed;
	
	var totAcc = $('#terapieNoFeste').val();
	
	// calcolo lo standard considerando il numero totale degli accessi diviso il numero totale di giorni. 4 indica la frequenza settimanale.
	
	var giorniTot = parseInt(durataProgetto) + parseInt(feste) + parseInt(domeniche);
	
	var standard = 7*totAcc/giorniTot;
	
	
	
	$('#standard').val(Math.round(standard * 100) / 100);
	
	if (standard!=0) {
	
		mostraKPI(standard, modInt, impRiab, freqSett);
	
	}
	
}

function mostraKPI(standard, modInt, impRiab, freqSett){
	
	if(modInt=="1") {
		
		if(parseInt(impRiab)==1){
			if(compreso(standard,9.4,10.4)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,8.9,9.4)|compreso(standard,10.4,10.9)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		} else if(parseInt(impRiab)==2){
			if(compreso(standard,5.1,5.9)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,4.6,5.1)|compreso(standard,5.9,6.4)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		} else if(parseInt(impRiab)==3){
			if(compreso(standard,1.9,2.4)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,1.4,1.9)|compreso(standard,2.4,2.9)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		}
		
	} else if (modInt=="2"){
		
		if(parseInt(impRiab)==1){
			if(compreso(standard,5.1,5.9)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,4.6,5.1)|compreso(standard,5.9,6.4)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		} else if(parseInt(impRiab)==2){
			if(compreso(standard,4.25,5.25)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,3.75,4.25)|compreso(standard,5.25,5.75)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		} else if(parseInt(impRiab)==3){
			if(compreso(standard,1.35,2.35)){
				$('#ok').show();
				$('#ko').hide();
				$('#warning').hide();
			} else if (compreso(standard,0.85,1.35)|compreso(standard,2.35,2.85)){
				$('#ok').hide();
				$('#ko').hide();
				$('#warning').show();
			} else {
				$('#ok').hide();
				$('#ko').show();
				$('#warning').hide();
			}
		}
		
	} else {
		
	}
}

function abilita_campi_tabella_presenza() {

	$('#table_dx').find('td').each(function() {
		var nome_campo = this.id;
		$('.num_tab_dx').prop('contenteditable',true)

	});
}

function disabilita_campi_tabella_presenza() {

	$('#table_dx').find('td').each(function() {
		var nome_campo = this.id;
		$('.num_tab_dx').prop('contenteditable',false)

	});
}

//funzione che restituisce il numero se compreso nel range indicato
function compreso(x, min, max) {
	  if (x >= min && x <= max) {
		  return true;
	  } else {
		  return false;
	  }
	}

function riempiValutazione(elemento, response, countVF) {
	

	
	for(var i=0;i<countVF;i++) {
		
		if(i!=0) {
			addNewRows();
		}

		
		
		$('#' + elemento + ' input').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i][campo2],"/")).change;
							} else {
								//$('#' + campo).val(specialChar("§", response[i][campo2],"'")).change;
								//
								if(response[i][campo2].includes("ü") | response[i][campo2].includes("ä")){
									$('#' + campo).val(specialChar(response[i][campo2],"down")).change;
								} else {
									$('#' + campo).val(response[i][campo2]).change;
								}
								
							}
						
						}
					}
		});
	
		$('#' + elemento + ' textarea').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;
					
					console
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							//console.log(a);
							campo2 = campo.substring(0, campo.indexOf("_"));
							
							//$('#' + campo).val(specialChar("§", response[i][campo2],"'"));
							//$('#' + campo).val(specialChar(response[i][campo2],"down"));
							
							if(response[i][campo2]!=undefined){
								if(response[i][campo2].includes("ü") | response[i][campo2].includes("ä")){
									$('#' + campo).val(specialChar(response[i][campo2],"down")).change;
								} else {
									$('#' + campo).val(response[i][campo2]);
								}
							}
							
	
						}
					}
		});
		
		$('#' + elemento + ' select').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							
							campo2 = campo.substring(0, campo.indexOf("_"));
							
							$('#' + campo).val(response[i][campo2]).change;
							
							
	
						}
					}
		});
	
	}
	// reinizializzo le select!
	if ($('.search').hasClass("select2-hidden-accessible")){
		$('.search').select2('destroy');

		$('.search').select2();

	}

	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();
	
	}
		

//creo le opzioni per quanto riguarda le select relative ai terapisti per la verifica funzionale!
function creaOption_vf(json,index,persona) {

	if(persona=="med") {
		var prova = $('#Medico_'+index);
		
		prova.append($("<option></option>").attr("value", "").text("Seleziona"));


		for (var i = json.length - 1; i >= 0; i--) {
			prova.append(
					$("<option></option>").attr("value", json[i].Id).text(
							json[i]["medico_vf"]));
		}
		
	} else if(persona=="ter") {
		
		var prova = $('#Terapista_'+index);
		
		prova.append($("<option></option>").attr("value", "").text("Seleziona"));


		for (var i = json.length - 1; i >= 0; i--) {
			prova.append(
					$("<option></option>").attr("value", json[i].Id).text(
							json[i]["terapista_vf"]));
		}
	}
}		