function creaOption_pat1(json,index) {

		var prova = $('#CodICD9_'+index);
		
		prova.append($("<option></option>").attr("value", "").text("Seleziona"));


		for (var i = json.length - 1; i >= 0; i--) {
			prova.append(
					$("<option></option>").attr("value", json[i].codice).text(
							json[i]["codice"]+" | "+json[i]["descrizione"]));
		}

}


//creo le option per la tabella PAT2 in base alla lettera scelta
function creaOption_pat2(json,index,letter) {

	
	var prova = $('#ICF_'+index);

	prova.find('option').remove();
	
	prova.append($("<option></option>").attr("value", "").text("Seleziona"));


	for (var i = json.length - 1; i >= 0; i--) {
		
		if(json[i].ICF.includes(letter)){
		prova.append(
				$("<option></option>").attr("value", json[i].ICF).text(
						json[i]["ICF"] + " | " + json[i]["ICFDes"]));
		}
	}
	
	//$('.search').select2('destroy');
	//$('.search').select2();

}

function creaOption_qual(json,index,letter,indexqual) {

	
	var prova = $('#ICFQual'+indexqual+'_'+index);
	
	var prova2 = $('#ICFQualF'+indexqual+'_'+index);

	prova.find('option').remove();
	prova2.find('option').remove();
	
	prova.append($("<option></option>").attr("value", "").text("Seleziona"));
	prova2.append($("<option></option>").attr("value", "").text("Seleziona"));

	//creo le option per ICFQualx
	for (var i = json.length - 1; i >= 0; i--) {
		
		if(json[i].D.includes(letter)){
		
		var qual = "Qual"+indexqual;
			
			if(indexqual=="1"){	
				prova.append($("<option></option>").attr("value", json[i].Qual1).text(json[i][qual] + " | " + json[i]["QualDes"]));
			} else if(indexqual=="2") {
				prova.append($("<option></option>").attr("value", json[i].Qual2).text(json[i][qual] + " | " + json[i]["QualDes"]));
			} else {
				prova.append($("<option></option>").attr("value", json[i].Qual3).text(json[i][qual] + " | " + json[i]["QualDes"]));
			}
		}
	}
	
	//creo le option per ICFQualFx
	for (var i = json.length - 1; i >= 0; i--) {
		
		if(json[i].D.includes(letter)){
			var qual = "Qual"+indexqual;
		prova2.append(
					$("<option></option>").attr("value", json[i].qual).text(json[i][qual] + " | " + json[i]["QualDes"]));
		}
	}


}

//funzione che aggiunge righe alla tabella PAT1

function addNewRows() {


	  var rowCount = $('.tab_count_pat1').length + 1;
		
	   $('#Table_PAT1 tr:last').after(


		'<tr id="'+rowCount+'" class="tab_count_pat1">'+
			'<td class="align-middle">'+
				'<input type="text" id="DataInizioPatologia_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
			'</td>'+
			'<td class="align-middle">'+
				'<select class="colorful-select dropdown-primary border_custom_select search codice" style="width: 100%" id="CodICD9_'+rowCount+'"></select>'+
			'</td>'+
			'<td class="align-middle">'+
				'<input type="text" class="form-control text-center" id="CodICD9value_'+rowCount+'">'+
			'</td>'+
			'<td class="align-middle">'+
            '<div class="form-check">'+
              '<input type="checkbox" class="form-check-input" id="AltraPatologia_'+rowCount+'">'+
              '<label class="form-check-label" for="AltraPatologia_'+rowCount+'"> </label>'+
            '</div>'+
			'</td>'+													
		'</tr>'
	);

	   $('#Table_PAT1 tr:last').after(
			'<tr>'+
			'<td colspan="4"></td>'+
			'</tr>'
	   );

	//inizializzo le select
	creaOption_pat1(query_codiICD9,rowCount);
	$('#CodICD9_'+rowCount).select2();
	
		//inizializzo il datepicker
	$(".datepicker").pickadate({

		  today: 'Oggi',
		  clear: '',
		  close: 'Chiudi',
		  format: 'dd/mm/yyyy'
    });

    $('.codice').change(function(){
    	var cella_id = this.id.substring(this.id.indexOf("_")+1,this.id.length);

    	$('#CodICD9value_'+cella_id).val(this.value);

    }); 
}

//funzione che aggiunge righe alla tabella PAT2

function addNewRows_pat2() {


	  var rowCount = $('.tab_count_pat2').length + 1;
		
	   $('#Table_PAT2 tr:last').after(


			'<tr id="pat'+rowCount+'" class="tab_count_pat2">'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary border_custom_select search dominio" id="Dominio_'+rowCount+'"  style="width:100%">'+
							'<option value="null" disabled selected>Seleziona</option>'+
							'<option value="b">Funzioni Corporee</option>'+
		                    '<option value="d">Attivit&aacute; e Partecipazione</option>'+
		                    '<option value="e">Fattori Ambientali</option>'+
		                    '<option value="s">Strutture Corporee</option>'+
					'</select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICF_'+rowCount+'"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'</div>'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual1_'+rowCount+'"></select>'+
					'</div>'+
					'<div class="mt-2">'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF1_'+rowCount+'"></select>'+
					'</div>'+
				'</td>'+
				'<td class="align-middle">'+
					'</div>'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual2_'+rowCount+'"></select>'+
					'</div>'+
					'<div class="mt-2">'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF2_'+rowCount+'"></select>'+
					'</div>'+
				'</td>'+
				'<td class="align-middle">'+
					'</div>'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQual3_'+rowCount+'"></select>'+
					'</div>'+
					'<div class="mt-2">'+
						'<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="ICFQualF3_'+rowCount+'"></select>'+
					'</div>'+
				'</td>'+
			'</tr>'
	);

	   $('#Table_PAT2 tr:last').after(
			'<tr>'+
			'<td colspan="5"></td>'+
			'</tr>'
	   );

	//inizializzo le select
	$('#Dominio_'+rowCount).select2();
	$('#ICF_'+rowCount).select2();
	$('#ICFQual1_'+rowCount).select2();
	$('#ICFQualF1_'+rowCount).select2();
	$('#ICFQual2_'+rowCount).select2();
	$('#ICFQualF2_'+rowCount).select2();
	$('#ICFQual3_'+rowCount).select2();
	$('#ICFQualF3_'+rowCount).select2();

	
	//creo le option per la riga appena creata in base alla lettera scelta
	$('#Dominio_'+rowCount).change(function(){
    	switch (this.value){
    	case "b": 
        	$("#pat"+rowCount).css("background-color", "yellow");
        	break;
    	case "d":
    		$("#pat"+rowCount).css("background-color", "green");
        	break;
    	case "e": 
        	$("#pat"+rowCount).css("background-color", "red");
        	break;
    	case "s":
    		$("#pat"+rowCount).css("background-color", "blue");
        	break;
	}
		creaOption_pat2(query_ICF,rowCount,this.value);
		
    	creaOption_qual(query_qual1,rowCount,this.value,"1");
    	creaOption_qual(query_qual2,rowCount,this.value,"2");
    	creaOption_qual(query_qual3,rowCount,this.value,"3");
	});
	
	creaOption_pat2(query_ICF,rowCount,$('input[name=inlineDefaultRadiosExample]:checked', '#dominio').val());
	
	
}


function riempiTabellePAT(elemento, response, countVF) {
	
	for(i=0;i<countVF;i++) {
			
		$('#' + elemento + ' input').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;

					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i][campo2],"/")).change;
							} else {
									
								
									//$('#' + campo).val(response[i][campo2]).change;
								$('#' + campo).val(specialChar("§", response[i][campo2],"'")).change;
								}
								
							}
						
						}

		});
		
		$('#' + elemento + ' select').each(
				function() {
					var keys = Object.keys(response[i]);
					var campo = this.id;
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i+1]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i][campo2],"/")).change;
							} else {
								if(campo.includes("Dominio_")){
									
									var valore_index = campo.substring(campo.indexOf("_")+1,campo.length);
									
									$('#' + campo).val(response[i][campo2]).change;
									
						        	switch (this.value){
					            	case "b": 
					                	$("#pat"+valore_index).css("background-color", "yellow");
					                	break;
					            	case "d":
					            		$("#pat"+valore_index).css("background-color", "green");
					                	break;
					            	case "e": 
					                	$("#pat"+valore_index).css("background-color", "red");
					                	break;
					            	case "s":
					            		$("#pat"+valore_index).css("background-color", "blue");
					                	break;
						        	}
					        	creaOption_pat2(query_ICF,valore_index,this.value);
					        	
					        	creaOption_qual(query_qual1,valore_index,this.value,"1");
					        	creaOption_qual(query_qual2,valore_index,this.value,"2");
					        	creaOption_qual(query_qual3,valore_index,this.value,"3");
					        	
									
								} else if (campo.includes("CodICD9_")){

									var valore_index = campo.substring(campo.indexOf("_")+1,campo.length);
									
									$('#' + campo).val(response[i][campo2]).change;

						            $('#CodICD9value_'+valore_index).val(this.value);

								} else {
									$('#' + campo).val(response[i][campo2]).change;
								}
								
							}
						
						}
					}
		});
		
	}	
		
	// reinizializzo le select!
	if ($('.search').hasClass("select2-hidden-accessible")){
		$('.search').select2('destroy');

		$('.search').select2();

	}

	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();
	
	}


function riempiTabellePAT2(elemento, response, countVF,countVF2) {
	
	for(i=countVF+1;i<countVF+countVF2+1;i++) {
			
		$('#' + elemento + ' input').each(
				function() {
					var keys = Object.keys(response[i-(countVF+1)]);
					var campo = this.id;
					if(campo=="AltraPatologia_"+[i]){
						$('#' + campo).prop('checked', true);
					}
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i-(countVF+1)][campo2],"/")).change;
							} else {

									//$('#' + campo).val(response[i-(countVF+1)][campo2]).change;
									$('#' + campo).val(specialChar("§", response[i-(countVF+1)][campo2],"'")).change;
									
								}
								
							}
						
						}

		});
		
		$('#' + elemento + ' select').each(
				function() {
					var keys = Object.keys(response[i-(countVF+1)]);
					var campo = this.id;
					for (a = 0; a < keys.length; a++) {
						if (this.id == keys[a]+'_'+[i]) {
							campo2 = campo.substring(0, campo.indexOf("_"));
								// fine controllo
							if ($("#" + campo).attr("class").includes("dateFormat")) {
								$('#' + campo).val(formatCustomHTMLDate(response[i-(countVF+1)][campo2],"/")).change;
							} else {
								if(campo.includes("Dominio_")){
									
									var valore_index = campo.substring(campo.indexOf("_")+1,campo.length);
									
									$('#' + campo).val(response[i-(countVF+1)][campo2]).change;
									
						        	switch (this.value){
					            	case "b": 
					                	$("#pat"+valore_index).css("background-color", "yellow");
					                	break;
					            	case "d":
					            		$("#pat"+valore_index).css("background-color", "green");
					                	break;
					            	case "e": 
					                	$("#pat"+valore_index).css("background-color", "red");
					                	break;
					            	case "s":
					            		$("#pat"+valore_index).css("background-color", "blue");
					                	break;
						        	}
					        	creaOption_pat2(query_ICF,valore_index,this.value);
					        	
					        	creaOption_qual(query_qual1,valore_index,this.value,"1");
					        	creaOption_qual(query_qual2,valore_index,this.value,"2");
					        	creaOption_qual(query_qual3,valore_index,this.value,"3");
					        	
									
								} else if (campo.includes("CodICD9_")){

									var valore_index = campo.substring(campo.indexOf("_")+1,campo.length);
									
									$('#' + campo).val(response[i-(countVF+1)][campo2]).change;

						            $('#CodICD9value_'+valore_index).val(this.value);

								} else {
									$('#' + campo).val(response[i-(countVF+1)][campo2]).change;
								}
								
							}
						
						}
					}
		});
		
	}	
		
	// reinizializzo le select!
	if ($('.search').hasClass("select2-hidden-accessible")){
		$('.search').select2('destroy');

		$('.search').select2();

	}

	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();
	
	}