//funzione che aggiunge righe alla tabella PT1

function addNewRows() {


	  var rowCount = $('.tab_count_pt1').length + 1;
		
	   $('#Table_PT1 tr:last').after(


				'<tr id="pt'+rowCount+'" class="tab_count_pt1">'+
				'<td class="align-middle">'+
					'<input type="text" id="DATA_'+rowCount+'" class="form-control datepicker text-center dateFormat enableTrue">'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" style="width: 100%" id="FASE_'+rowCount+'">'+
						'<option value="" disabled selected>Seleziona</option>'+
						'<option value="Programma Terapeutico">Programma Terapeutico</option>'+
						'<option value="Aggiornamento Programma Terapeutico">Aggiornamento Programma Terapeutico</option>'+
					'</select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" style="width: 100%" id="Periodo_'+rowCount+'">'+
						'<option value="" disabled selected>Seleziona</option>'+
						'<option value="Breve Termine">Breve Termine</option>'+
						'<option value="Medio Termine">Medio Termine</option>'+
						'<option value="Lungo Termine">Lungo Termine</option>'+
					'</select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<textarea class="form-control" id="Procedure_'+rowCount+'" rows="2"></textarea>'+
				'</td>'+
				'<td class="align-middle">'+
					'<textarea class="form-control" id="Modalita_'+rowCount+'" rows="2"></textarea>'+
				'</td>'+
				'<td class="align-middle">'+
					'<textarea class="form-control" id="Tempi_'+rowCount+'" rows="2"></textarea>'+
				'</td>'+
				'<td class="align-middle">'+
					'<textarea class="form-control" id="Operatore_'+rowCount+'" rows="2"></textarea>'+
				'</td>'+
			'</tr>'
	);

	   $('#Table_PT1 tr:last').after(
			'<tr>'+
			'<td colspan="7"></td>'+
			'</tr>'
	   );

	//inizializzo le select
	//creaOption_pat1(query_codiICD9,rowCount);
	$('#FASE_'+rowCount).select2();
	$('#Periodo_'+rowCount).select2();
	
	//inizializzo il datepicker
	$(".datepicker").pickadate({

		  today: 'Oggi',
		  clear: '',
		  close: 'Chiudi',
		  format: 'dd/mm/yyyy'
    });
}

//funzione che aggiunge righe alla tabella PAT2

function addNewRows_pt2() {


	  var rowCount = $('.tab_count_pt2').length + 1;
		
	   $('#Table_PT2 tr:last').after(


			'<tr id="pt'+rowCount+'" class="tab_count_pt2">'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="Area_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<textarea class="form-control" id="Obbiettivo_'+rowCount+'" rows="2" style="height:auto;"></textarea>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="OperatoreM_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="ObbiettivoAtteso_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="OperatoreN_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="ObbiettivoAggiornato_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
				'<td class="align-middle">'+
					'<select class="colorful-select dropdown-primary text-center border_custom_select search" id="RisultatiRaggiunti_'+rowCount+'" style="width:100%"></select>'+
				'</td>'+
			'</tr>'
	);

	   $('#Table_PT2 tr:last').after(
			'<tr>'+
			'<td colspan="7"></td>'+
			'</tr>'
	   );
	   
	//creo le option
	creaOption_pt1(query_area,rowCount,"Area");
	creaOption_pt1(query_operatore,rowCount,"OperatoreM");
	creaOption_pt1(query_operatoreN,rowCount,"OperatoreN");
	creaOption_pt1(query_obbiettivoAtteso,rowCount,"ObbiettivoAtteso");
	creaOption_pt1(query_obbiettivoAggiornato,rowCount,"ObbiettivoAggiornato");
	creaOption_pt1(query_risultatoAtteso,rowCount,"RisultatoAtteso");
	creaOption_pt1(query_risultatoRaggiunto,rowCount,"RisultatiRaggiunti");

	//inizializzo le select
//	$('#Area_'+rowCount).select2();
//	$('#OperatoreM_'+rowCount).select2();
//	$('#ObbiettivoAtteso_'+rowCount).select2();
//	$('#OperatoreN_'+rowCount).select2();
//	$('#ObbiettivoAggiornato_'+rowCount).select2();
//	$('#RisultatiRaggiunti_'+rowCount).select2();
}


function creaOption_pt1(json,index,element) {

	var prova = $('#'+element+'_'+index);
	prova.append($("<option></option>").attr("value", "").text("Seleziona"));

	for (var i = json.length - 1; i >= 0; i--) {
			prova.append($("<option></option>").attr("value", json[i].Id).text(json[i][element]));
	}
	
	prova.select2();

}