var obj = [];
var rowData_old = 0;

// JQGrid Progetti --> Pazienti
function JQGRIDinitPazientiGrid(obj, GridName, width, query, action) {
	$grid = $(GridName);

	var click_count = 0;
	if (action == "3") {

	} else {
		// definisco due valori per determinare se si tratta di un nuovo
		// progetto o la visualizzazione di uno vecchio
		$('#nuovoPr').val("0");
		$('#vecchioPr').val("0");
	}

	$(GridName).jqGrid('GridUnload');

	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "CodAnagr",
							label : "Codice Anagrafica",
							align : "center",
							searchoptions : {
								// show search options
								sopt : [ "eq" ]
							// ge = greater or equal to, le = less or equal to,
							// eq = equal to
							}
						}, {
							name : "Nome",
							label : "Nome",
							align : "center"
						}, {
							name : "Cognome",
							label : "Cognome",
							align : "center"
						}, {
							name : "CodiceFiscale",
							label : "Codice Fiscale",
							align : "center"
						}, ],
						rowNum : 30,
						height : "auto",
						pager : "#jqGridPazientiPager",
						width : width,
						iconSet : "fontAwesome",
						caption : "Cerca:",
						idPrefix : "g1_",
						data : obj,
						rownumbers : true,
						sortname : "CodAnagr",
						sortorder : "asc",
						caption : "Selezionare il paziente desiderato",
						ignoreCase : true,
						onSelectRow : function(rowid) {

							var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;

							if (rowData_old == rowData) {

								hideComponent('gridPazienti');

								hideComponent('mostraPrBtn');

								showComponent('sezioni');

								showComponent('VF_section');

								showComponent('salvaBtn');

								showComponent('rowNominativo');

								$('#nominativo_nome').val(rowData.Nome);

								$('#nominativo_cognome').val(rowData.Cognome);

								$('#codAnag').val(rowData.CodAnagr);
								
								var dataAnno = new Date;

								$('#NumCart').val(rowData.CodAnagr + "/" + dataAnno.getFullYear());

								hideComponent('counterPr');

								rowData_old = 0;

								$('#nuovoPr').val("1");

							} else {

								click_count = 0;

								$('#nominativo_nome').val(rowData.Nome);

								$('#nominativo_cognome').val(rowData.Cognome);

								rowData_old = rowData;

								rowData = rowData.CodAnagr;

								showComponent('counterPr');

								$
										.ajax({
											type : 'post',
											url : '../php/controller.php',
											data : {
												'source' : "cercaProgetto",
												'whereValue' : rowData,
												'whereCond' : 'CodAnagr',
												'tableName' : 'progetto',
												'fieldName' : "p.CodProj as codProg,"
														+ "st.Stato as stato_view,"
														+ "p.Stato as stato,"
														+ "p.DataInizio as dataInizio,"
														+ "p.DataFine as dataFine,"
														+ "p.DataStato as dateStato,"
														+ "p.DurataProgetto as durataProgetto,"
														+ "p.Intervento as intervento,"
														+ "p.RegAssist as regAssist,"
														+ "p.ModInt as modInt,"
														+ "p.ImpRiab as impRiab,"
														+ "p.Medico as medico,"
														+ "p.Terapista1 as terapista1,"
														+ "p.Terapista2 as terapista2,"
														+ "p.Terapista3 as terapista3,"
														+ "p.AnamnesiPatologicaprossima as anamnesi,"
														+ "p.EsameObbiettivoSpecialistico as esameObbiettivoS,"
														+ "p.EsameObbiettivoGenerale as esameObbiettivoG,"
														+ "p.Invalidita as invalidita,"
														+ "p.InvaliditaPerc as percInvalidita,"
														+ "p.ModaTrasporto as modTrasp,"
														+ "p.altraModaTrasporto as altraModTrasp,"
														+ "p.Ricov12mesi as ricov12mesi,"
														+ "p.CodProvenienza as strProvenienza,"
														+ "p.altroprov as altroProv,"
														+ "p.CodCondiVita as condVita,"
														+ "p.CodInviante as inviante,"
														+ "p.altrocondivita as altroCondVita,"
														+ "p.Altroinv as altroInviante,"
														+ "p.PeriodoTrattamento as perTrattamento,"
														+ "p.ObbiettiviRiabilitativiRaggiunti as obRiabRagg,"
														+ "p.freqsett1 as freqSett1,"
														+ "p.CodprotOrtAusili,"
														+ "p.ScalaCodificata as scalaCod,"
														+ "p.ScalaCodificata1 as scalaCod1,"
														+ "p.*"

											},
											success : function(response) {

												var obj = JSON.parse(response);
												var a = 0;
												for (i = 0; i < obj.length; i++) {
													a = i + 1;

												}

												JQGRIDinitProgettiGrid(obj,
														'#grid2', $(
																"#rowButton")
																.innerWidth(),
														rowData, query);
												setComponentText('counterPr', a);

											},
											error : function() {
												toastr
														.error('Opps! Si è verificato un problema!');
											}
										});

							}
						}

					});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridPazientiPager').css({
		"height" : "35px"
	});
}

function mostraProgetti() {

	var counter = $("#counterPr").text();

	if (counter != 0) {
		showComponent('gridProgetti');
		showComponent('rowNominativo');
		hideComponent('gridPazienti');
		showComponent('mostraAnagr');
		hideComponent('mostraPrBtn');
		hideComponent('counterPr');
		rowData_old = 0;
	} else {
		toastr
				.error('Non hai selezionato un paziente oppure non ci sono progetti associati al paziente!');
	}
}

function JQGRIDinitProgettiGrid(obj, GridName, width, codAnagr, query, nome,
		cognome) {
	$grid = $(GridName);
	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "codProg",
							label : "Codice Progetto",
							align : "center",
							searchoptions : {
								// show search options
								sopt : [ "eq" ]
							// ge = greater or equal to, le = less or equal to,
							// eq = equal to
							}
						}, {
							name : "stato_view",
							label : "Stato",
							align : "center"
						}, {
							name : "dataInizio",
							label : "Data Inizio",
							align : "center"
						}, {
							name : "dataFine",
							label : "Data Fine",
							align : "center"
						}, {
							name : "dateStato",
							label : "Data Stato",
							align : "center"
						}, ],
						rowNum : 30,
						height : "auto",
						pager : "#jqGridProgettiPager",
						width : width,
						iconSet : "fontAwesome",
						caption : "Cerca:",
						idPrefix : "g1_",
						data : obj,
						rownumbers : true,
						sortname : "CodProj",
						sortorder : "asc",
						caption : "Progetti associati",
						ignoreCase : true,
						onSelectRow : function(rowid) {

							var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;

							if (rowData_old == rowData) {

								$
										.ajax({
											type : 'post',
											url : '../php/controller.php',
											data : {
												'source' : 'cercaValutazioneFunzionale',
												'codProg' : rowData.codProg
											},
											success : function(response) {
												var valFunz = jQuery
														.parseJSON(response);

												var countVF = valFunz.length;

												riempiValutazione("Table_VF",valFunz, countVF);

											},
											error : function() {
												alert("error");
											}
										});

								$('#vecchioPr').val("1");

								hideComponent('gridProgetti');

								hideComponent('mostraPrBtn');

								hideComponent('mostraAnagr');

								hideComponent('cercaPrBtn');

								hideComponent('nuovoPrBtn');

								showComponent('sezioni');

								showComponent('VF_section');

								showComponent('backBtn');

								showComponent('modificaBtn');

								if (nome !== undefined) {

									$('#nominativo_nome').val(nome);

									$('#nominativo_cognome').val(cognome);
								}

								showComponent('rowNominativo');

								// scrivo il valore nell'input Codice Anagrafica
								$('#codAnag').val(codAnagr);

								// compilo i vari input (input, select e
								// textarea) in base ai dati forniti da rowData
								riempiElementi("sezioni", "input", rowData);

								riempiElementi("sezioni", "select", rowData);

								riempiElementi("sezioni", "textarea", rowData);

								salvaDatiSession(rowData, "rowData");

								// calcolo il numero di domeniche e festività
								formatCustomHTMLDate(moment(rowData.dataInizio)
										.addWorkdays(
												$('#durataProgetto').val(),
												returnArrayFromJSON(query),
												true).format('YYYY-MM-DD'));

								rowData_old = 0;

							} else {

								click_count = 0;

								rowData_old = rowData;

							}
						}
					});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid("filterToolbar", {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridProgettiPager').css({
		"height" : "35px"
	});
}