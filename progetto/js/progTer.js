
var prevElem;

function initProgTerGrid(width,obj) {
	       $("#progTer").jqGrid({
						data : obj,
						width : width - 60,
						datatype : "local",
						page : 1,
						colModel : [
								{
									label : 'Id',
									name : 'Id',
									key : true,
									hidden : true
								},
								{
									label : 'DATA',
									name : 'DATA',
									width : '90',
									align : 'center',
									editable : true,
									edittype : "text",
									editoption: {
										dataInit : function (elem) {
											
											}
									}

								},
								{
									label : 'FASE',
									name : 'FASE',
									editable : true,
									edittype : "select",
									width : '170',
									align : 'center',
									editoptions : {
										value : "ProgrammaTerapeutico:Programma Terapeutico;Aggiornamento Programma Terapeutico:Aggiornamento Programma Terapeutico",
										dataInit : function(elem, id) {
											$('#'+id.rowId+'_DATA').addClass('datepicker');
											$('#'+id.rowId+'_DATA').focus(function() {
												init();

											});
											$(elem).addClass('search');
											$(elem).css("width", "100%");
											$('.search').select2();
											prevElem = elem
							
										}
									}

								},
								{
									label : 'Periodo',
									name : 'Periodo',
									editable : true,
									edittype : "select",
									width : '120',
									align : 'center',
									editoptions : {
										value : "Breve Termine:Breve Termine;Medio Termine: Medio Termine;Lungo Termine:Lungo Termine",
										dataInit : function(elem, id) {
											$(elem).addClass('search');
											$(elem).css("width", "100%");
											$('.search').select2();
											prevElem = elem
										}
									}
								}, {
									label : 'Procedure',
									name : 'Procedure',
									editable : true,
									width : '160',
									align : 'center'
								}, {
									label : 'Modalita',
									name : 'Modalita',
									editable : true,
									edittype : "text",
									width : '90',
									align : 'center'
								}, {
									label : 'Tempi',
									name : 'Tempi',
									editable : true,
									edittype : "text",
									width : '80',
									align : 'center'
								}, {
									label : 'Operatore',
									name : 'Operatore',
									editable : true,
									edittype : "text",
									width : '310',
									align : 'center'
								} ],
						onSelectRow : editRow,
						viewrecords : true,
						rowNum : 10,
						onCellSelect: function(id,e,c,d){
							init();
						},
						pager : "#jqGridPagerTer"
					});

	$('#jqGridPagerTer').css({
		"height" : "35px"
	});
}

var lastSelection;
var rowData;
var id_selected;

function salvaRiga(){
    	var grid = $("#progTer");
        rowData = grid.jqGrid("getLocalRow", lastSelection);
        //prendo tutti i valori della riga
        var fase = $('#'+lastSelection+'_FASE').find("option:selected").val();
        var periodo = $('#'+lastSelection+'_Periodo').find("option:selected").val();
        var data = $('#'+lastSelection+'_DATA').val();
        var procedure = $('#'+lastSelection+'_Procedure').val();
        var modalita = $('#'+lastSelection+'_Modalita').val();
        var tempi = $('#'+lastSelection+'_Tempi').val();
        var operatore = $('#'+lastSelection+'_Operatore').val();
        
        grid.jqGrid('restoreRow', lastSelection);
        
        rowData.FASE = fase;
        rowData.Periodo = periodo;
        rowData.DATA = data;
        rowData.Procedure = procedure;
        rowData.Modalita = modalita;
        rowData.Tempi = tempi;
        rowData.Operatore = operatore;
        
        grid.jqGrid('setRowData', lastSelection, rowData);
        
        grid.jqGrid('resetSelection');
        
        lastSelection = null;

}


function editRow(id) {
	
		id_selected = id;
		if (id && id !== lastSelection) {
    	   
             var grid = $("#progTer");
             rowData = grid.jqGrid("getLocalRow", lastSelection);
             //prendo tutti i valori della riga
             var fase = $('#'+lastSelection+'_FASE').find("option:selected").val();
             var periodo = $('#'+lastSelection+'_Periodo').find("option:selected").val();
             var data = $('#'+lastSelection+'_DATA').val();
             var procedure = $('#'+lastSelection+'_Procedure').val();
             var modalita = $('#'+lastSelection+'_Modalita').val();
             var tempi = $('#'+lastSelection+'_Tempi').val();
             var operatore = $('#'+lastSelection+'_Operatore').val();
             
             grid.jqGrid('restoreRow', lastSelection);
             
             rowData.FASE = fase;
             rowData.Periodo = periodo;
             rowData.DATA = data;
             rowData.Procedure = procedure;
             rowData.Modalita = modalita;
             rowData.Tempi = tempi;
             rowData.Operatore = operatore;
             
             grid.jqGrid('setRowData', lastSelection, rowData);
             
             grid.jqGrid('editRow', id, {
                    keys : true,
                    focusField : 5
             });
             if(lastSelection==id){
             init();
             }
             lastSelection = id;
       }
}

var inizializzata = false;

$(window).blur(function() {
	var grid = $("#progTer");
    rowData = grid.jqGrid("getLocalRow", lastSelection);
    //prendo tutti i valori della riga
    var fase = $('#'+lastSelection+'_FASE').find("option:selected").val();
    var periodo = $('#'+lastSelection+'_Periodo').find("option:selected").val();
    var data = $('#'+lastSelection+'_DATA').val();
    var procedure = $('#'+lastSelection+'_Procedure').val();
    var modalita = $('#'+lastSelection+'_Modalita').val();
    var tempi = $('#'+lastSelection+'_Tempi').val();
    var operatore = $('#'+lastSelection+'_Operatore').val();
    
    grid.jqGrid('restoreRow', lastSelection);
    
    rowData.FASE = fase;
    rowData.Periodo = periodo;
    rowData.DATA = data;
    rowData.Procedure = procedure;
    rowData.Modalita = modalita;
    rowData.Tempi = tempi;
    rowData.Operatore = operatore;
    
    grid.jqGrid('setRowData', lastSelection, rowData);
    
    grid.jqGrid('resetSelection');
    
    lastSelection = null;
});


function init(){
	
	if(inizializzata){
	
	$('.datepicker').pickadate({
		  today: 'Oggi',
		  clear: '',
		  close: 'Chiudi',
		  format: 'dd/mm/yyyy',
		  showon: 'focus'
			  
    });	
	}
	inizializzata = true;
}


function addRow(elemento){

	 var row=GenerateNewRow(elemento);
	    var newRowId = $.jgrid.randId("new");
	    jQuery("#"+elemento).jqGrid('addRowData',newRowId,row);
	
}


function GenerateNewRow(elemento)
{
   var length=jQuery("#"+elemento).jqGrid('getGridParam','records');
   var newid=length+1;
   
   var row={
       isdirty:false,
       Id: newid,
       FASE: "",
       Periodo: "",
       DATA: "06/08/2018",
       Procedure: "",
       Modalita: "",
       Tempi: "",
       Operatore: "",
       };
   
   return row;        
}

function removeRow(elemento){
	if(id_selected==undefined){
		toastr.warning('Selezionare la riga da eliminare!');
	} else {
	jQuery("#"+elemento).jqGrid('delRowData',id_selected);
		toastr.success('Riga eliminata correttamente!');
		id_selected=undefined;
	}
}
