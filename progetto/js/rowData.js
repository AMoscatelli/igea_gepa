var valore_old = "";

function mostraRiga(divVis,grid,accPrev,int,accEff,accPrevVal,intVal,accEffVal,index,sel){
	
	if($('#'+divVis).is(":visible")){
		$('#'+grid).append(aggiungiRiga(accPrev,int,accEff,index));
		$('#'+accPrev+index).val(accPrevVal);
		$('#'+int+index).val(intVal);
		$('#'+accEff+index).val(accEffVal);
		$('#'+index).val(index);
		hideComponent("buttonGrid");
		hideComponent("buttonNoGrid");
	} else {
		$('#'+grid).html(initGrid(divVis));
		$('#'+grid).append(aggiungiRiga(accPrev,int,accEff,index));
		hideComponent("buttonGrid");
		hideComponent("buttonNoGrid");
		
		$('#'+accPrev+index).val(accPrevVal);
		$('#'+int+index).val(intVal);
		$('#'+accEff+index).val(accEffVal);
		$('#'+index).val(index);
	}

	
	var selezionato = $('#'+sel).find("option:selected").val();
	$('#'+sel).find('option[value="'+selezionato+'"]').prop('disabled',true);
	$('#'+sel).material_select('destroy');
	$('#'+sel).material_select();
	
	if(valore_old!="") {
		valore_old = valore_old + "|" + index;
	} else {
		valore_old = index;
	}
	
	$('#1_righeCreate').val(valore_old);
}

function initGrid(div){
	
	var html = "<!-- riga nascosta per inserimento parametri -->";
	html += '<div class="row justify-content-md-center mt-3" id="'+div+'">';
	html += '<div class="col col-lg-2 custom-grid">Accessi previsti</div>';
	html += '<div class="col col-lg-4 custom-grid">Interventi</div>';
	html += '<div class="col col-lg-2 custom-grid">Accessi effettuati</div>';
	html += '<div class="col col-lg-1 custom-grid">Action</div>';
	html += '</div>';
	
	return html
}

function aggiungiRiga(accPrev,int,accEff,index,accPrevVal,intVal,accEffVal){
	var html = "<!-- riga nascosta per inserimento parametri -->";
	html += '<div class="row justify-content-md-center" id="row'+index+'">';
	html += '<div class="col col-lg-2 custom-gridlight"><input type="text" class="form-control custom-form text-center value" id="'+accPrev+index+'" disabled></input></div>';
	html += '<div class="col col-lg-4 custom-gridlight"><input type="text" class="form-control custom-form text-center value" id="'+int+index+'" disabled></input></div>';
	html += '<div class="col col-lg-2 custom-gridlight"><input type="text" class="form-control custom-form text-center value" id="'+accEff+index+'"  disabled></input></div>';
	html += '<div class="col col-lg-1 custom-gridlight"><img class="custom-image" src="../img/trash.png" width="40%" id="" onclick="mostraPopup('+index+')"></img></div>';
	html += '</div>';
	
	return html
}


function cancellaRiga(numRiga,sel){
	if(parseInt(numRiga)>12){
	 sel="interventiNoPres";
	 $('#modalElimina').modal('hide');
	 $('#row'+numRiga).remove();
	 $('#'+sel).find('option[value="'+numRiga+'"]').prop('disabled',false);
	 $('#'+sel).material_select('destroy');
	 $('#'+sel).material_select();
	} else {
		 $('#modalElimina').modal('hide');
		 $('#row'+numRiga).remove();
		 $('#'+sel).find('option[value="'+numRiga+'"]').prop('disabled',false);
		 $('#'+sel).material_select('destroy');
		 $('#'+sel).material_select();
	}
}


function mostraPopup(index){
	$("#indexVal").val(index);
	$('#modalElimina').modal();
}



function creaRighe(obj,grid,divVis){
	var righeCreate = [];
	$('#sezioni input').each(function() {
				var keys = Object.keys(obj);
				var campo = this.id;
				for (a = 0; a < keys.length; a++) {
					if (this.id == keys[a]) {
						if(this.id=="1_righeCreate"){
							righeCreate = obj[this.id].split("|");
						}
					}
				}
	});

	for (g = 0; g < righeCreate.length; g++) {
		
		if(g>"0"){
			
			$('#'+grid).append(aggiungiRiga('accPrev','int','accEff',righeCreate[g]));
		} else {
			$('#'+grid).html(initGrid(divVis));
			$('#'+grid).append(aggiungiRiga('accPrev','int','accEff',righeCreate[g]));
		}
	}
}

