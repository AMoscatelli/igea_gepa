<?php
require 'config.inc.php';
require 'db_connection.php';


$filename = 'IGEA_db_Structure.sql'; //How to Create SQL File Step : url:http://localhost/phpmyadmin->detabase select->table select->Export(In Upper Toolbar)->Go:DOWNLOAD .SQL FILE
$op_data = '';
$lines = file($filename);
foreach ($lines as $line)
{
    if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
    {
        continue;
    }
    $op_data .= $line;
	
    if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
    {
		//print $op_data."\n new row \n";
		$result = mysqli_query($conn, $op_data);
        $op_data = '';
    }
}
//Response Agenda
$trigger1 = "CREATE TRIGGER `sendMail` AFTER INSERT ON `agenda` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (999999999); END;";
//Response CategoriaT
$trigger2 = "CREATE TRIGGER `AddResponseCategoriaT` AFTER INSERT ON `CategoriaT` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";
//Response DopoScuola
$trigger3 = "CREATE TRIGGER `AddResponseDoposcuola` AFTER INSERT ON `dopoScuola` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";
//Response Paziente
$trigger4 = "CREATE TRIGGER `AddResponsePazienteAdd` AFTER INSERT ON `paziente` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.idPaziente); END;";
//Response Paziente DELETE
$trigger5 = "CREATE TRIGGER `Delete_Join_Add_Response` AFTER DELETE ON `paziente` FOR EACH ROW BEGIN DELETE from joinPazienteTariffaTerapista WHERE joinPazienteTariffaTerapista.idPaziente = old.idPaziente; DELETE FROM response; INSERT INTO response (id) VALUES (old.idPaziente); END;";
//Response Role
$trigger6 = "CREATE TRIGGER `AddResponseRole` AFTER INSERT ON `Role` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";
//Response Stanza
$trigger7 = "CREATE TRIGGER `AddResponseStanza` AFTER INSERT ON `Stanza` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";
//Response Tariffa
$trigger8 = "CREATE TRIGGER `AddResponseTariffa` AFTER INSERT ON `Tariffa` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";
//Response Terapisti
$trigger9 = "CREATE TRIGGER `AddResponseTerapisti` AFTER INSERT ON `Terapisti` FOR EACH ROW BEGIN DELETE FROM response; INSERT INTO response (id) VALUES (new.ID); END;";


$result = mysqli_query($conn, $trigger1);
$result = mysqli_query($conn, $trigger2);
$result = mysqli_query($conn, $trigger3);
$result = mysqli_query($conn, $trigger4);
$result = mysqli_query($conn, $trigger5);
$result = mysqli_query($conn, $trigger6);
$result = mysqli_query($conn, $trigger7);
$result = mysqli_query($conn, $trigger8);
$result = mysqli_query($conn, $trigger9);

mysqli_close($conn);
header("Location: ../login.html");

?>



