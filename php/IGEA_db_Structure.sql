SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `eidon` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eidon`;

DROP TABLE IF EXISTS `addOn`;
CREATE TABLE `addOn` (
  `googleBasic` tinyint(1) NOT NULL,
  `googleAdvanced` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE TABLE `addOn`;

INSERT INTO `addOn` (`googleBasic`, `googleAdvanced`) VALUES
(0,0);

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `idPaziente` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `nota` longtext NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `eventoCancellato` tinyint(1) NOT NULL,
  `idTerapista` int(5) NOT NULL,
  `idDopoScuola` int(11) NOT NULL,
  `idTariffa` int(5) NOT NULL,
  `idStanza` int(5) NOT NULL,
  `ricorrenza` varchar(255) NOT NULL,
  `gruppoRicorrenza` int(11) NOT NULL,
  `stato` int(1) NOT NULL COMMENT '0 = da confermare, 1 = confermato, 2 = assente, 3 = fatturato',
  `now` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `CategoriaT`;
CREATE TABLE `CategoriaT` (
  `ID` int(11) NOT NULL,
  `tipoCategoria` varchar(255) NOT NULL,
  `quota` varchar(10) NOT NULL,
  `percentuale` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `dopoScuola`;
CREATE TABLE `dopoScuola` (
  `ID` int(11) NOT NULL,
  `idTerapista` int(11) NOT NULL,
  `idStanza` int(11) NOT NULL,
  `idTariffa` int(11) NOT NULL,
  `nomeDopoScuola` varchar(200) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `lun` varchar(11) NOT NULL,
  `mar` varchar(11) NOT NULL,
  `mer` varchar(11) NOT NULL,
  `gio` varchar(11) NOT NULL,
  `ven` varchar(11) NOT NULL,
  `sab` varchar(11) NOT NULL,
  `dom` varchar(11) NOT NULL,
  `confermato` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Info`;

CREATE TABLE `info` (
  `ID` int(11) NOT NULL,
  `licenceNumber` int(10) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `activationDate` date NOT NULL,
  `expirationDate` date NOT NULL,
  `mostrato` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `info` (`ID`, `licenceNumber`, `owner`, `activationDate`, `expirationDate`, `mostrato`) VALUES
(1, now(), 'RagSoc', now(), DATE_ADD(now(), INTERVAL 1 MONTH), 0);

DROP TABLE IF EXISTS `joinPazienteDopoScuola`;

CREATE TABLE `joinPazienteDopoScuola` (
  `IdPaziente` int(11) NOT NULL,
  `idDopoScuola` int(11) NOT NULL,
  `presenza` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `joinPazienteTariffaTerapista`;
CREATE TABLE `joinPazienteTariffaTerapista` (
  `idPaziente` int(11) NOT NULL,
  `idTariffa` int(11) NOT NULL,
  `idTerapista` int(11) NOT NULL,
  `idStanza` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `paziente`;
CREATE TABLE `paziente` (
  `idPaziente` int(11) NOT NULL,
  `nomeCognome` varchar(255) NOT NULL,
  `eta` int(3) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nota` text NOT NULL,
  `sesso` varchar(1) NOT NULL,
  `Patologia` varchar(255) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `response`;
CREATE TABLE `response` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
  `ID` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE TABLE `Role`;

INSERT INTO `Role` (`ID`, `role`) VALUES
(1, 'admin'),
(2, 'user');

DROP TABLE IF EXISTS `Stanza`;
CREATE TABLE `Stanza` (
  `ID` int(11) NOT NULL,
  `stanza` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Tariffa`;
CREATE TABLE `Tariffa` (
  `ID` int(11) NOT NULL,
  `tariffa` double(6,2) NOT NULL,
  `custom` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Terapisti`;
CREATE TABLE `Terapisti` (
  `ID` int(11) NOT NULL,
  `nomeCognome_t` varchar(255) NOT NULL,
  `idStanza` int(11) DEFAULT NULL,
  `idTariffa` int(11) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  `user` varchar(50) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1: admin, 2: user, 3:amministrazione',
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE TABLE `Terapisti`;

INSERT INTO `Terapisti` (`ID`, `nomeCognome_t`, `idStanza`, `idTariffa`, `color`, `user`, `pwd`, `role`, `idCategoria`) VALUES
(1, 'Administrator', NULL, NULL, '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 0);

ALTER TABLE `addOn`
  ADD PRIMARY KEY (`googleAdvanced`);

  ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `CategoriaT`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `dopoScuola`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `info`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `joinPazienteDopoScuola`
  ADD PRIMARY KEY (`IdPaziente`,`idDopoScuola`);

  ALTER TABLE `joinPazienteTariffaTerapista`
  ADD PRIMARY KEY (`idPaziente`,`idTariffa`,`idTerapista`);

  ALTER TABLE `paziente`
  ADD PRIMARY KEY (`idPaziente`);

  LTER TABLE `Role`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `Stanza`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `Tariffa`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `Terapisti`
  ADD PRIMARY KEY (`ID`);

  ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  ALTER TABLE `CategoriaT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  ALTER TABLE `dopoScuola`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  ALTER TABLE `info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  ALTER TABLE `paziente`
  MODIFY `idPaziente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  ALTER TABLE `Role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

  ALTER TABLE `Stanza`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  ALTER TABLE `Tariffa`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

  ALTER TABLE `Terapisti`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
