<?php
include 'DB.php';
include 'custom_class_DB/progetto.php';
include 'config.inc.php';
include 'fpdf.php';
include 'MysqliDb.php';
include 'custom_class_DB/anagrafica.php';


/*
 *  Esempio Uso della DB Connection MysqliDb
 *  $db = MysqliDb::getInstance();
    $result = $db->jsonBuilder()->get($tableName);
 * 
 * */

function getRowCount($tableName){
    $DBa = DB::getInstance();
    $DBa->table($tableName)->select('*')->get();
    $count = $DBa->count();
    return $count;
}

function getRette(){
    
    $DBa = DB::getInstance();
    echo $DBa->table('retta')->select('m.ModalitaIntervento, imp.ImpRiab, r.RegaAssist,r.PrezzoEuro,r.AssenzaEuro')->SQLString('AS r JOIN regimeassistenziale AS ras ON r.RegaAssist = ras.Id JOIN impegnoriabilitativo AS imp ON r.ImpRiab = imp.Id JOIN modalitaintervento AS m ON r.ModInt = m.Id')->get()->toJSON();
    
}

function login(){
    // username and password sent from form
    $DBa = DB::getInstance();
    $myusername = prepareSrtringMysql($_POST['username']);
    $mypassword = prepareSrtringMysql($_POST['password']);
    $password = md5($mypassword);
    
    
    $rows = $DBa->table('profile')->where('username',$myusername)->where('password',$password)->get();
    //$rows = $DBa->table('profile')->get();
    $sql = $DBa->getSQL();
    if($rows[0]!=null){
        $rows = json_decode($rows, true);
        $active = $rows[0]['idProfile'];
        $role = $rows[0]['role'];
        $profile = $rows[0]['nome_cognome'];
    //}
    //if($rows != null) {
        $config = $DBa->table('configcont')->get();
        $config = json_decode($config, true);
        $codCentro = $config[0]['CodCentro'];
        $codStruttura = $config[0]['CodiceStruttura'];
        //session_regenerate_id(true);
        $_SESSION['login_profile'] = $profile;
        $_SESSION['login_user'] = $myusername;
        $_SESSION['login_role'] = $role;
        $_SESSION['info_centro'] = $config;
        $_SESSION['cod_centro'] = $codCentro;
        $_SESSION['cod_struttura'] = $codStruttura;
        $_SESSION['login_id_logged'] = $active;
        $request = floatval(str_replace('.', '',$_SERVER['REQUEST_TIME_FLOAT']));
        $request= substr($request, 0, 11);
        $_SESSION['session_started'] = $request;

        
        if(isset($_SESSION['url_redirect'])){
            $url=$_SESSION['url_redirect'];
            $index = stripos($url, "/", 1);
            $url = substr($url, $index, strlen($url));
            header("location: .".$url);
        }else{
            header("location: index.php?result=1");
        }
        
        
    }else {
        print false;
        header("location: login.php?result=0");
        
    }
    
}

function ricaricaPresenze(){
   
    $anno = $_POST['selectAnno'];
    $mese = $_POST['selectMese'];
    //$GGPCTeor = ($datafine - $dataInizio) - ($domenicheComprese + $festivitaComprese);//dato che va cacolato creando specifica funzione
    //$Ass80;
    //$Ass00;
    
    $STH = retriveCampiPresenze();
    $STH1 = retriveValoriPresenze($anno, $mese);
    //array per Update;
    
    
    
    $DBa = DB::getInstance();
    $DBa->delete('presenze')->where([['Anno',$anno],['Mese',$mese]])->exec();
    
    for($i=0 ; $i<sizeof($STH1); $i++){
        $DBa->insert('presenze',$STH1[$i]);
        $query = $DBa->getSQL();
    }
    
}

function cercaContabResNonRes($regimeAssistenziale) {
    // username and password sent from form
    
    $anno = $_POST['selectAnno'];
    $mese = $_POST['selectMese'];
    
    $DBa = DB::getInstance();
    $rows = $DBa->table('presenze')->select('m.ModalitaIntervento, imp.ImpRiab, ras.RegAssist, pr.ID, a.Cognome, a.Nome, pr.Anno, pr.Mese, pr.DataInizio, pr.DataFine, pr.GGPCTeor, pr.Ass80, pr.Ass00, p.DurataProgetto,p.GGEffettiviPC, a.CodiceFiscale, a.ASL, a.distretto, p.CodProj')->SQLString("AS pr LEFT JOIN progetto AS p ON p.CodProj = pr.CodProj JOIN anagrafico AS a ON p.CodAnagr = a.CodAnagr JOIN regimeassistenziale AS ras ON p.Regassist = ras.Id JOIN impegnoriabilitativo AS imp ON p.ImpRiab = imp.Id JOIN modalitaintervento AS m ON p.ModInt = m.Id WHERE pr.Anno = '".$anno."' AND pr.Mese = '".$mese."' AND p.Regassist = '".$regimeAssistenziale."' ORDER BY a.Cognome, a.Nome")->get()->toJSON();
    //$rows = $DBa->SQLString("SELECT m.ModalitaIntervento, imp.ImpRiab, ras.RegAssist, pr.ID, a.Cognome, a.Nome, pr.Anno, pr.Mese, pr.DataInizio, pr.DataFine, pr.GGPCTeor, pr.Ass80, pr.Ass00, pa.CodICD9, p.DurataProgetto,p.GGEffettiviPC, a.CodiceFiscale, a.ASL, a.distretto, p.CodProj FROM PRESENZE AS pr LEFT JOIN progetto AS p ON p.CodProj = pr.CodProj LEFT JOIN patologieoggetto AS pa ON p.CodProj = pa.CodProj JOIN anagrafico AS a ON p.CodAnagr = a.CodAnagr JOIN regimeassistenziale AS ras ON p.Regassist = ras.Id JOIN impegnoriabilitativo AS imp ON p.ImpRiab = imp.Id JOIN modalitaintervento AS m ON p.ModInt = m.Id WHERE pr.Anno = '".$anno."' AND pr.Mese = '".$mese."' AND p.Regassist = '".$regimeAssistenziale."' ORDER BY a.Cognome, a.Nome")->execStringSQL()->toJSON();
    print $rows;
   
}

function saveConatbAssenze(){
    
    $residenziale = $_POST['residenziale'];
    $nonResidenziale = $_POST['nonResidenziale'];
    
    $DB = DB::getInstance();
    
    $val = $residenziale[0]['Ass80'];
    
    for($i = 0; $i < sizeof($residenziale); $i++){
        $DB->update('presenze',
            ['Ass80' => $residenziale[$i]['Ass80'],
                'Ass00' => $residenziale[$i]['Ass00']])
                ->where('ID','=',$residenziale[$i]['ID'])->exec();
        $query = $DB->getSQL();
    }
    for($i = 0; $i < sizeof($nonResidenziale); $i++){
        $DB->update('presenze',
            ['Ass80' => $nonResidenziale[$i]['Ass80'],
                'Ass00' => $nonResidenziale[$i]['Ass00']])
                ->where('ID','=',$nonResidenziale[$i]['ID'])->exec();
        $query = $DB->getSQL();
    } 
    
}


/* Funzione per la query di SELECT ALL. I parametri in ingresso sono il nome della tabella e i nomi dei campi. In caso di pi� campi basta inserire la virgola tra i fieldName. i.e. ("NomeFestivita,NomeTerapista")*/
function selectAll($tableName,$fieldName){
    
    $DBa = DB::getInstance();
    if($tableName=="festivita"){
        $result = $DBa->table($tableName)->select($fieldName)->orderBy("DataFestivita","DESC")->get()->toJSON();
    }else if($tableName=="response"){
        
        $result = $DBa->table($tableName)->get();
        
        $result1 = $result->toArray();
        
        session_start();
        
        $_SESSION['codiceProgetto'] = $result1[0]['id'];
        
    } else {
        $result = $DBa->table($tableName)->select($fieldName)->get()->toJSON();
    }
    
    echo $result;

}

function getPresenze($mese,$anno){
    if($mese<10){
        $mese = "0".$mese;
    }
    $inizio = date($anno."-".$mese."-01");
    $fine = date($anno."-".$mese."-t");
    $DBa = DB::getInstance();
    $resultPresenze = $DBa->table('presenze')->select('*')->where('Anno','=',$anno)->Where('Mese','=',$mese)->get()->toArray();
    $resultNuove = $DBa->table('progetto')->select('CodProj,DataInizio,DataFine')->where('DataInizio','<',"'".$fine."'")->Where('DataSTato','>',"'".$inizio."'")->where("stato",">","2")->get()->toArray();
    $trovato = false;
    if(sizeof($resultPresenze)!=0){
            //ricontrollo se tra i progett � cambiato qualcosa
        for($i=sizeof($resultPresenze)-1; $i>0; $i--){
            
            for($k=0;$k<sizeof($resultNuove);$k++){
                // trovo il progetto e quindi aggiorno i valori con i nuovi nel caso fossero cambiati
                if($resultPresenze[$i]['CodProj']==$resultNuove[$k]['CodProj']){
                    $resultPresenze[$i]['DataInizio'] = $resultNuove[$k]['DataInizio'];
                    $resultPresenze[$i]['DataFine'] = $resultNuove[$k]['DataFine'];
                    if(date($resultNuove[$k]['DataInizio'])<=date($inizio) && date($resultNuove[$k]['DataFine'])>=date($fine)){
                        //il progetto inizia prima e finisce dopo il mese oggetto di contabilita
                        $giorni=contaGGPC_Mese($mese, $anno);
                        
                    }else if(date($resultNuove[$k]['DataInizio'])>=date($inizio) && date($resultNuove[$k]['DataFine'])>=date($fine)){
                        //il progetto inizia dopo il primo giorno del mese e finisce dopo il mese oggetto di contabilita
                        $giorni=contaGGPC_BtweenDate(date($resultNuove[$k]['DataInizio']), $fine);
                        
                    }else if(date($resultNuove[$k]['DataInizio'])>=date($inizio) && date($resultNuove[$k]['DataFine'])<=date($fine)){
                        //il progetto inizia dopo il primo giorno del mese e finisce prime della fine del mese oggetto di contabilita
                        $giorni=contaGGPC_BtweenDate(date($resultNuove[$k]['DataInizio']), date($resultNuove[$k]['DataFine']));
                        
                    }else if(date($resultNuove[$k]['DataInizio'])<=date($inizio) && date($resultNuove[$k]['DataFine'])<=date($fine)){
                        //il progetto inizia prima del primo giorno del mese e finisce prima della fine del mese oggetto di contabilita
                        $giorni=contaGGPC_BtweenDate($inizio, date($resultNuove[$k]['DataFine']));
                        
                    }else{
                        
                        $giorni=9999;
                        
                    }
                    
                    $resultPresenze[$i]['GGPCTeor'] = $giorni;
                    $trovato=true;
                    break;
                }else{
                    $trovato = false;
                }
                
            }
            if(!$trovato){
                //se non trovo nella nouva query un progetto significa che � stata modificata
                //la data o il progetto nn esiste piu pertanto elimino la riga
                array_splice($resultPresenze, $i, 1);
            }
            
        }
        $DBa->delete('presenze')->where('Anno','=',$anno)->Where('Mese','=',$mese)->exec();
        for($i=0;$i<sizeof($resultNuove);$i++){
            $DBa->insert('presenze',$resultPresenze[$i]);
        }
        //$DBa->insert('presenze',$resultPresenze);        
    }else{
        for($i=0;$i<sizeof($resultNuove);$i++){
            $resultNuove[$i]['Anno']=$anno;
            $resultNuove[$i]['Mese']=$mese;
            $DBa->insert('presenze',$resultNuove[$i]);
        }
    }
    
    controllerMethod('cercaContabDomAmb','3');
    
}

function contaGGPC_BtweenDate($start, $end){
    
    $GGPC = 0;
    
    $sunday = intval(contaDomeniche_BtweenDate($start, $end));
    
    $start1 = new DateTime($start);
    $end1 = new DateTime($end);
    $days = $start1->diff($end1, true)->days;
    $days++;//aggiungo un giorno perche la differenza tra date non considera il giorno di inizio
    
    $GGPC = $days-$sunday-contaFestivita_BtweenDate($start,$end);
    
    
    return $GGPC;
    
}

function contaDomeniche_BtweenDate($start, $end){
    $start = new DateTime($start);
    $end = new DateTime($end);
    $days = $start->diff($end, true)->days;
    
    $sundays = intval($days / 7) + ($start->format('N') + $days % 7 >= 7);
    
    return $sundays;
}

function contaFestivita_BtweenDate($start, $end){
        
    $DBa = DB::getInstance();
    $feste = $DBa->table('festivita')->select('DataFestivita')->where('DataFestivita','>=', "'".$start."'")->where('DataFestivita','<=',"'".$end."'")->get()->toArray();
    
    return sizeof($feste);
    
}


function contaGGPC_Mese($mese, $anno){
    
    $GGPC = 0;
    
    $sunday = intval(contaDomeniche_Mese($mese, $anno));
        
    $days = cal_days_in_month(CAL_GREGORIAN, intval($mese), intval($anno));
    
    $GGPC = $days-$sunday-contaFestivita_Mese($mese,$anno);   
    
    
    return $GGPC;
    
}

function contaDomeniche_Mese($mese, $anno){
    $fromdt=date($anno."-".$mese."-01");
    $todt=date($anno."-".$mese."-t");
    
    $num_sundays='';
    for ($i = 0; $i < ((strtotime($todt) - strtotime($fromdt)) / 86400); $i++)
    {
        if(date('l',strtotime($fromdt) + ($i * 86400)) == 'Sunday')
        {
            $num_sundays++;
        }
    }
    
    return $num_sundays;
}

function contaFestivita_Mese($mese, $anno){
    $fromdt=date($anno."-".$mese."-01");
    $todt=date($anno."-".$mese."-t");
    
    $DBa = DB::getInstance();
    $feste = $DBa->table('festivita')->select('DataFestivita')->where('DataFestivita','>=', "'".$fromdt."'")->where('DataFestivita','<=',"'".$todt."'")->get()->toArray();
    
    return sizeof($feste);
    
}

function selectWhere($tableName,$fieldName,$whereCond,$whereValue){
    $DBa = DB::getInstance();
    $result = $DBa->table($tableName)->where($whereCond,'=', $whereValue)->get()->toJSON();
    $query = $DBa->getSQL();
    echo $result;
}

function selectWhere1($tableName,$fieldName,$whereCond,$whereValue){
    $DBa = DB::getInstance();
    $result = $DBa->table($tableName)->select($fieldName)->where($whereCond,'=', $whereValue)->get()->toJSON();
    $query = $DBa->getSQL();
    echo $result;
}

function selectFieldWhere($tableName,$fieldName,$whereCond,$whereValue){
    $DBa = DB::getInstance();
    $result = $DBa->table($tableName)->select($fieldName)->where($whereCond,'=', $whereValue)->get()->toJSON();
    $query = $DBa->getSQL();
    echo $result;
}

function cercaProgetto(){
    $whereValue = $_POST['whereValue'];
    if(!is_Array($whereValue)){
        $DBa = DB::getInstance();
        $tableName = $_POST['tableName'];
         $fieldName = $_POST['fieldName'];
        $whereCond = $_POST['whereCond'];
        $whereValue = $_POST['whereValue'];
        // query buona
        $result = $DBa->table($tableName)->select($fieldName)->SQLString(' AS p JOIN stato as st ON p.Stato = st.Id WHERE '.$whereCond.' = '.$whereValue.' ORDER BY p.CodProj DESC')->get()->toJSON();
        $query = $DBa->getSQL();
        print $result;
    }
}


//Funzione che fa la select di tutti i campi di un determinato paziente con le JOIN
function cercaPaziente($tableName,$fieldName,$whereCond,$whereValue){
    if(!is_Array($whereValue)){
        $DBa = DB::getInstance();      
        $result = $DBa->table($tableName)->select($fieldName)->SQLString(' AS p JOIN stato as st ON p.Stato = st.Id WHERE '.$whereCond.' = '.$whereValue.' ')->get()->toJSON();
        $query = $DBa->getSQL();
        return $result;
    }
}

//Funzione che fa la select di tutti i campi di un determinato paziente senza le JOIN
function cercaPaziente1($tableName,$fieldName,$whereCond,$whereValue){
    //session_start();
    $_SESSION['codiceProgetto'] = $whereValue;
    
    if(!is_Array($whereValue)){
        $DBa = DB::getInstance();
        if($tableName=="figureprofinizio"|$tableName=="interventiprevisti") {
            $result = $DBa->table($tableName)->where($whereCond,'=', $whereValue)->get()->toJSON();
        } else {
             $result = $DBa->table($tableName)->where($whereCond,'=', $whereValue)->get()->toJSON();
        }

        $query = $DBa->getSQL();
        
        if($tableName=="Consulenze"|$tableName=="Diaria"|$tableName=="ValutazioneFunzionale"|$tableName=="EsamiStrumentali"|$tableName=="Indicazioni"|$tableName=="ProfiloDisabN"|$tableName=="PatologieOggetto"|$tableName=="figureprofinizio"|$tableName=="altrepatologie"|$tableName=="interventiprevisti"){
            echo $result;
        } else {
            return $result;
        }
    }
}

//istat csv
function getISTATcsv($data){
    $file = fopen($data, 'r');
    //$array=json_decode($array, true);
    while (($line = fgetcsv($file)) !== FALSE) {
        //$line is an array of the csv elements
        $a='58091';
        //$key = 'IstatResidenza';
        //$a = $array[$key];
        $b=explode( ';',$line[0]);
        if ($a==$b[4]){
            return $b[5];
        }
    }
    fclose($file);
}

function cercaPazienteModal(){
    $nome = isset($_POST['cerca_nome']) ? $_POST['cerca_nome'] : '';
    $cognome = isset($_POST['cerca_cognome']) ? $_POST['cerca_cognome'] : '';
    $codProg = isset($_POST['cerca_codProg']) ? $_POST['cerca_codProg'] : $_GET['CodProj'];
    $DBa = DB::getInstance();
    
    if($nome!==""&$cognome==""){
        $result = $DBa->table('anagrafico')->select('CodAnagr,Nome,Cognome,CodiceFiscale')->where('Nome','like', "'%".$nome."%'")->get()->toJSON();
    } else if($cognome!==""&$nome==""){
        $result = $DBa->table('anagrafico')->select('CodAnagr,Nome,Cognome,CodiceFiscale')->where('Cognome','like', "'%".$cognome."%'")->get()->toJSON();
    }else if($nome!==""&$cognome!==""){
        $result = $DBa->table('anagrafico')->select('CodAnagr,Nome,Cognome,CodiceFiscale')->SQLString(' WHERE Nome= "'.$nome.'" AND Cognome="'.$cognome.'"')->get()->toJSON();
    }else if($codProg!==""){
    $fieldName = "p.CodProj as codProg,p.codAnagr,".
                    "p.Stato as stato,". 
                    "ana.Nome as NomeP,". 
                    "ana.Cognome as CognomeP,".  
                    "st.Stato as stato_view,". 
                    "p.DataInizio as dataInizio,".  
                    "p.DataFine as dataFine,".  
                    "p.DataStato as dataStato,".  
                    "p.VecchiaChiave as oldKey,".
                    "p.freqsett1 as freqSett1,".
                    "p.NumCart as numCart,".
                    "p.DurataProgetto as durataProgetto,".  
                    "p.Intervento as intervento,".  
                    "p.RegAssist as regAssist,".  
                    "p.ModInt as modInt,".  
                    "p.ImpRiab as impRiab,".  
                    "p.Medico as medico,".  
                    "p.Terapista1 as terapista1,".  
                    "p.Terapista2 as terapista2,". 
                    "p.Terapista3 as terapista3,".
                    "p.AnamnesiPatologicaprossima as anamnesi,".
                    "p.EsameObbiettivoSpecialistico as esameObbiettivoS,".
                    "p.EsameObbiettivoGenerale as esameObbiettivoG,".
                    "p.Invalidita as invalidita,".
                    "p.InvaliditaPerc as percInvalidita,".
                    "p.ModaTrasporto as modTrasp,".
                    "p.altraModaTrasporto as altraModTrasp,".
                    "p.Ricov12mesi as ricov12mesi,".
                    "p.CodProvenienza as strProvenienza,".
                    "p.altroprov as altroProv,".
                    "p.CodCondiVita as condVita,".
                    "p.CodInviante as inviante,".
                    "p.altrocondivita as altroCondVita,".
                    "p.Altroinv as altroInviante,".
                    "p.PeriodoTrattamento as perTrattamento,".
                    "p.ObbiettiviRiabilitativiRaggiunti as obRiabRagg,".
                    "p.ScalaCodificata as scalaCod,".
                    "p.ScalaCodificata1 as scalaCod1,".
                    "p.*";
                        
    $whereCond = 'CodProj';
    $whereValue = $codProg;
    
    //definisco il valore del codice progetto;
    $_SESSION['codiceProgetto'] = $codProg;
    
    $result = $DBa->table('progetto')->select($fieldName)->SQLString(' AS p LEFT JOIN regimeassistenziale as r ON p.Regassist = r.Id LEFT JOIN anagrafico as ana ON p.codAnagr = ana.CodAnagr LEFT JOIN modalitaintervento as m ON p.ModInt = m.Id LEFT JOIN impegnoriabilitativo as i ON p.ImpRiab = i.Id LEFT JOIN intervento as inte ON p.Intervento = inte.Id LEFT JOIN medici as me ON p.Medico = me.Id JOIN stato as st ON p.Stato = st.Id LEFT JOIN terapista as te ON p.Terapista1 = te.Id LEFT JOIN terapista as te1 ON p.Terapista2 = te1.Id LEFT JOIN terapista as te2 ON p.Terapista3 = te2.Id WHERE '.$whereCond.' = '.$whereValue.' ')->get()->toJSON();
    }
    $query = $DBa->getSQL();
    print $result;
}

function prepareSrtringMysql($var){
    $forbidden = array("'", "[", "]", "=", "%");
    $var = str_replace($forbidden, "", $var);
    return $var;
}

function salvaTabelle(){
    session_start();
    $nomeTabella = $_POST['nomeTabella'];
    
    $_SESSION[$nomeTabella] = $_POST['datiTabella'];
    $_SESSION['num_'.$nomeTabella] = $_POST['righeTabella'];
}


function salva(){
    session_start();
    $sezione = $_POST['sezione'];
    if($sezione=="A"){
        $_SESSION['set_sectionA'] = isset($_POST['dati'])?$_POST['dati']:"";
        $_SESSION['vecchioPr'] = isset($_POST['vecchioPr'])?$_POST['vecchioPr']:"";
                
        //recupero la durata del progetto per il calcolo dello standard
        $_SESSION['durataProgetto'] = isset($_POST['durataProgetto'])?$_POST['durataProgetto']:"";
        $_SESSION['feste'] = isset($_POST['feste'])?$_POST['feste']:"";
        $_SESSION['domeniche'] = isset($_POST['domeniche'])?$_POST['domeniche']:"";
        $_SESSION['modInt'] = isset($_POST['modInt'])?$_POST['modInt']:"";
        $_SESSION['impRiab'] = isset($_POST['impRiab'])?$_POST['impRiab']:"";
        $_SESSION['freqSett'] = isset($_POST['freqSett'])?$_POST['freqSett']:"";
    } else if($sezione=="B"){
        $_SESSION['set_sectionB'] = isset($_POST['dati'])?$_POST['dati']:"";
    } else if($sezione=="C"){
        $_SESSION['set_sectionC'] = isset($_POST['dati'])?$_POST['dati']:"";
    } else if($sezione=="D"){
        $_SESSION['set_sectionD'] = isset($_POST['dati'])?$_POST['dati']:"";
    } else if($sezione=="E"){
        $_SESSION['set_sectionE'] = isset($_POST['dati'])?$_POST['dati']:"";
    } else if($sezione=="F"){
        $_SESSION['set_sectionF'] = isset($_POST['dati'])?$_POST['dati']:"";
    } else if($sezione=="G"){
        $_SESSION['set_sectionG'] = isset($_POST['dati'])?$_POST['dati']:"";
    }  else if($sezione=="H"){
        $_SESSION['set_sectionH'] = isset($_POST['dati'])?$_POST['dati']:"";
    }  else if($sezione=="I"){
        $_SESSION['set_sectionI'] = isset($_POST['dati'])?$_POST['dati']:"";
    }  else if($sezione=="L"){
        $_SESSION['set_sectionL'] = isset($_POST['dati'])?$_POST['dati']:"";
    }  else if($sezione=="M"){
        $_SESSION['set_sectionM'] = isset($_POST['dati'])?$_POST['dati']:"";
    }
    
    $_SESSION['nome'] = isset($_POST['nome'])?$_POST['nome']:"";
    $_SESSION['cognome'] = isset($_POST['cognome'])?$_POST['cognome']:"";
    $_SESSION['action'] = isset($_POST['action'])?$_POST['action']:"";

}

function salvaDati($table_name){
    
    salva();
    
        
    if($_SESSION['set_sectionA']!="") {
        $datafin =$_SESSION['set_sectionA'];
    }
    
    if($_SESSION['set_sectionB']!="") {
        if($datafin!=""){
        $datafin = array_merge($datafin,$_SESSION['set_sectionB']);
        } else {
            $datafin =$_SESSION['set_sectionB'];
        }
    }
    
    if($_SESSION['set_sectionC']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionC']);
        } else {
            $datafin =$_SESSION['set_sectionC'];
        }
    }
    
    if($_SESSION['set_sectionD']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionD']);
        } else {
            $datafin =$_SESSION['set_sectionD'];
        }
    }
    
    if($_SESSION['set_sectionE']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionE']);
        } else {
            $datafin =$_SESSION['set_sectionE'];
        }
    }
    
    if($_SESSION['set_sectionF']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionF']);
        } else {
            $datafin =$_SESSION['set_sectionF'];
        }
    }
    
    if($_SESSION['set_sectionG']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionG']);
        } else {
            $datafin =$_SESSION['set_sectionG'];
        }
    }
    
    if($_SESSION['set_sectionH']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionH']);
        } else {
            $datafin =$_SESSION['set_sectionH'];
        }
    }
    
    if($_SESSION['set_sectionI']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionI']);
        } else {
            $datafin =$_SESSION['set_sectionI'];
        }
    }
    
    if($_SESSION['set_sectionL']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionL']);
        } else {
            $datafin =$_SESSION['set_sectionL'];
        }
    }
    
    if($_SESSION['set_sectionM']!="") {
        if($datafin!=""){
            $datafin = array_merge($datafin,$_SESSION['set_sectionM']);
        } else {
            $datafin =$_SESSION['set_sectionM'];
        }
    }
    
    //return $datafin;
    
 
    
    $Prog = Progetto::getInstance();


    
    /*if(isset($_SESSION['codiceProgetto'])){
        if($_SESSION['codiceProgetto']!=""){
            $sql = "DELETE FROM progetto WHERE CodProj = ".$_SESSION['codiceProgetto'];
            $rows = $Prog->query($sql, [""],true);
            if(!array_key_exists("CodProj",$datafin)){
                $datafin['CodProj'] = $_SESSION['codiceProgetto'];
            }
        }
    } else */
    if (isset($_POST['dati']['codProg'])){
        if($_POST['dati']['codProg']!=""){
            $sql = "DELETE FROM progetto WHERE CodProj = ".$_POST['dati']['codProg'];
            $rows = $Prog->query($sql, [""],true);
            if(!array_key_exists("CodProj",$datafin)){
                $datafin['CodProj'] = $_POST['dati']['codProg'];
            }
        }
    } else {
        if(isset($_SESSION['set_sectionA']['codProg'])){
            if($_SESSION['set_sectionA']['codProg']!=""){
                $sql = "DELETE FROM progetto WHERE CodProj = ".$_SESSION['set_sectionA']['codProg'];
                $rows = $Prog->query($sql, [""],true);
                if(!array_key_exists("CodProj",$datafin)){
                    $datafin['CodProj'] = $_SESSION['set_sectionA']['codProg'];
                }
            }
        }
    }
  
    print $datafin;
    
    $Prog->insert($table_name,$datafin);
    
    $sql = $Prog->getSQL();
    
    $result_response = $Prog->table("response")->get()->toArray();
    
    //aggiorno la tabella anagrafica in caso di invalidit� riconosciuta
    $CodiceAnagrafico = $_SESSION['set_sectionA']['codAnag'];
    
    if($_POST['dati']['invalidita']=="3"){
        $Prog->update("anagrafico", "CatProtetta", "1", $CodiceAnagrafico);
    } else {
        $Prog->update("anagrafico", "CatProtetta", "2", $CodiceAnagrafico);
    }
    
    
    
    
    
    if(isset($_SESSION['codiceProgetto'])){
        if($_SESSION['codiceProgetto']==""){
            $_SESSION['codiceProgetto'] = $result_response[0]['id'];
        }
    } else {
        $_SESSION['codiceProgetto'] = $result_response[0]['id'];
    }
    
    insertTabelle($result_response[0]['id'],$Prog);
    
    print $result_response;
    
    }

    
    function insertTabelle($result_response,$Prog){
        
        //definisco la funzione che inserisce i dati della tabella valutazione funzionale (Sezione A)
        if(isset($_SESSION['Table_VF'])){
            
            if($_SESSION['Table_VF']!=""){
            
                    //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                    if(isset($result_response)){
                        $sql = "DELETE FROM valutazionefunzionale WHERE CodProj = ".$result_response;
                        $rows = $Prog->query($sql, [""],true);
                    }
                    
                    $num_righe = $_SESSION['num_Table_VF']-1;
                    
                    for ($i = 1; $i <= $num_righe; $i++) {
                        $keys = [];
                        $values = [];
                        $risfinale = [];
                        
                        //inserisco il primo campo relativo al codice progetto
                        array_push($keys,'CodProj');
                        array_push($values,$result_response);
                        
                        $risfinale[$keys[0]] = $values[0];
                        
                        foreach ($_SESSION['Table_VF'] as $key => $value){
                            if (strpos($key, '_'.$i) !== false) {
                                $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                            }
                        }
                        
                        //qui metto l'insert alla tabella
                        $Prog->insert('valutazionefunzionale',$risfinale);
                    }
             }
        }
        
        
        //definisco la funzione che inserisce i dati della tabella Esami strumentali (Sezione B)
        if(isset($_SESSION['Table_ES'])){
            if($_SESSION['Table_ES']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM esamistrumentali WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                $num_righe = $_SESSION['num_Table_ES']-1;
                
                for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['Table_ES'] as $key => $value){
                        if (strpos($key, '_'.$i) !== false) {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                        }
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('esamistrumentali',$risfinale);
                }
            }
        }
        
        //definisco la funzione che inserisce i dati della tabella Indicazioni (Sezione C)
        if(isset($_SESSION['Table_IND'])){
            if($_SESSION['Table_IND']!=""){
                    //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                    if(isset($result_response)){
                        $sql = "DELETE FROM indicazioni WHERE CodProj = ".$result_response;
                        $rows = $Prog->query($sql, [""],true);
                    }
                    
                    $num_righe = $_SESSION['num_Table_IND']-1;
                    
                    for ($i = 1; $i <= $num_righe; $i++) {
                        $keys = [];
                        $values = [];
                        $risfinale = [];
                        
                        //inserisco il primo campo relativo al codice progetto
                        array_push($keys,'CodProj');
                        array_push($values,$result_response);
                        
                        $risfinale[$keys[0]] = $values[0];
                        
                        foreach ($_SESSION['Table_IND'] as $key => $value){
                            if (strpos($key, '_'.$i) !== false) {
                                $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                            }
                        }
                        
                        //qui metto l'insert alla tabella
                        $Prog->insert('indicazioni',$risfinale);
                    }
            }
        }
        
        //definisco la funzione che inserisce i dati della tabella Figure Professionali Inizio (Sezione D)
        if(isset($_SESSION['FigureProfInizio'])){
            if($_SESSION['FigureProfInizio']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM figureprofinizio WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                //$num_righe = $_SESSION['FigureProfInizio']-1;
                
                //for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodpROJ');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['FigureProfInizio'] as $key => $value){
                       
                            $risfinale[$key] = $value;
                       
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('figureprofinizio',$risfinale);
                //}
            }
        }
        
        
        //definisco la funzione che inserisce i dati della tabella Patologie Oggetto (Sezione E)
        if(isset($_SESSION['Table_PAT1'])){
            if($_SESSION['Table_PAT1']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM patologieoggetto WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                    
                    
                    $sql = "DELETE FROM altrepatologie WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                $num_righe = $_SESSION['num_Table_PAT1']-1;
                
                for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['Table_PAT1'] as $key => $value){
                        if (strpos($key, '_'.$i) !== false) {
                            if(substr($key,0, strripos($key,"_"))=="CodICD9value"|substr($key,0, strripos($key,"_"))=="AltraPatologia"){
                                if(substr($key,0, strripos($key,"_"))=="AltraPatologia"){
                                    $altrapatologia = $value;
                                }
                            } else {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                            }
                        }
                    }
                    
                    $num_keys = count($risfinale);
                    
                    //qui metto l'insert alla tabella
                    if($altrapatologia=="0"){
                        if($num_keys<2){
                            
                        } else {
                        $Prog->insert('patologieoggetto',$risfinale);
                        }
                    } else {
                        unset($risfinale['DataInizioPatologia']);
                        $Prog->insert('altrepatologie',$risfinale);
                    }
                }
            }
        }
        
        
        //definisco la funzione che inserisce i dati della tabella Disabilit� (Sezione E)
        if(isset($_SESSION['Table_PAT2'])){
            if($_SESSION['Table_PAT2']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM profilodisabn WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                $num_righe = $_SESSION['num_Table_PAT2']-1;
                
                for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['Table_PAT2'] as $key => $value){
                        if (strpos($key, '_'.$i) !== false) {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                        }
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('profilodisabn',$risfinale);
                }
            }
        }
        
        
        //definisco la funzione che inserisce i dati della tabella Accessi Previsti(Sezione G)
        if(isset($_SESSION['AccessiPrev'])){
            if($_SESSION['AccessiPrev']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM interventiprevisti WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                //$num_righe = $_SESSION['num_Table_PAT2']-1;
                
                //for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['AccessiPrev'] as $key => $value){
                        if (strpos($key, '_prev') !== false) {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                        }
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('interventiprevisti',$risfinale);
                //}
            }
        }
        
        
        //definisco la funzione che inserisce i dati della tabella Diaria (Sezione H)
        if(isset($_SESSION['Table_Day'])){
            if($_SESSION['Table_Day']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM Diaria WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                $num_righe = $_SESSION['num_Table_Day']-1;
                
                for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['Table_Day'] as $key => $value){
                        if (strpos($key, '_'.$i) !== false) {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                        }
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('Diaria',$risfinale);
                }
            }
        }
        
        //definisco la funzione che inserisce i dati della tabella Diaria (Sezione H)
        if(isset($_SESSION['Table_Cons'])){
            if($_SESSION['Table_Cons']!=""){
                //qui ci devo mettere la delete di tutte le righe che hanno quel codice progetto
                if(isset($result_response)){
                    $sql = "DELETE FROM Consulenze WHERE CodProj = ".$result_response;
                    $rows = $Prog->query($sql, [""],true);
                }
                
                $num_righe = $_SESSION['num_Table_Cons']-1;
                
                for ($i = 1; $i <= $num_righe; $i++) {
                    $keys = [];
                    $values = [];
                    $risfinale = [];
                    
                    //inserisco il primo campo relativo al codice progetto
                    array_push($keys,'CodProj');
                    array_push($values,$result_response);
                    
                    $risfinale[$keys[0]] = $values[0];
                    
                    foreach ($_SESSION['Table_Cons'] as $key => $value){
                        if (strpos($key, '_'.$i) !== false) {
                            $risfinale[substr($key,0, strripos($key,"_"))] = $value;
                        }
                    }
                    
                    //qui metto l'insert alla tabella
                    $Prog->insert('Consulenze',$risfinale);
                }
            }
        }
        
}


    
    
function cancella(){
    
    if(isset($_POST['source'])){
    if($_POST['source']=="cancellaVariabile") {
        session_start();
    }
    }
    //session_start();
    $_SESSION['set_sectionA'] = "";
    $_SESSION['set_sectionB'] = "";
    $_SESSION['set_sectionC'] = "";
    $_SESSION['set_sectionD'] = "";
    $_SESSION['set_sectionE'] = "";
    $_SESSION['set_sectionF'] = "";
    $_SESSION['set_sectionG'] = "";
    $_SESSION['set_sectionH'] = "";
    $_SESSION['set_sectionI'] = "";
    $_SESSION['set_sectionL'] = "";
    $_SESSION['set_sectionM'] = "";
    $_SESSION['vecchioPr'] = "";
    $_SESSION['action'] = "";
    $_SESSION['rowData'] = "";
    $_SESSION['progTer']="";
    $_SESSION['nome'] ="";
    $_SESSION['cognome'] = "";
    $_SESSION['action'] = "";
    $_SESSION['vecchioPr'] = "";
    $_SESSION['durataProgetto'] = "";
    $_SESSION['feste'] = "";
    $_SESSION['domeniche'] = "";
    $_SESSION['modInt'] = "";
    $_SESSION['impRiab'] = "";
    $_SESSION['freqSett'] = "";
    $_SESSION['codiceProgetto'] = "";
    
    if(isset($_SESSION['Table_ES'])){
        $_SESSION['Table_ES'] ="";
        $_SESSION['num_Table_ES']="";
        
    } 
    
    if(isset($_SESSION['Table_VF'])){
        $_SESSION['Table_VF'] ="";
        $_SESSION['num_Table_VF']="";
        
    } 
    
    if(isset($_SESSION['Table_IND'])){
        $_SESSION['Table_IND'] ="";
        $_SESSION['num_Table_IND']="";
        
    }  
    
    if(isset($_SESSION['Table_PAT1'])){
        $_SESSION['Table_PAT1'] ="";
        $_SESSION['num_Table_PAT1']="";
        
    }  
    
    if(isset($_SESSION['Table_PAT2'])){
        $_SESSION['Table_PAT2'] ="";
        $_SESSION['num_Table_PAT2']="";
    }
    
    if(isset($_SESSION['Table_PT1'])){
        $_SESSION['Table_PT1'] ="";
        $_SESSION['num_Table_PT1']="";
        
    }
    
    if(isset($_SESSION['Table_PT2'])){
        $_SESSION['Table_PT2'] ="";
        $_SESSION['num_Table_PT2']="";
    }
    
    if(isset($_SESSION['FigureProfInizio'])){
        $_SESSION['FigureProfInizio'] ="";
    }
    
    if(isset($_SESSION['AccessiPrev'])){
        $_SESSION['AccessiPrev'] ="";
    }
    
    if(isset($_SESSION['Table_Day'])){
        $_SESSION['Table_Day'] ="";
    }
    
    if(isset($_SESSION['Table_Cons'])){
        $_SESSION['Table_Cons'] ="";
    }
    
}

function invia(){
    session_start();
    $_SESSION['inviaDati'] = $_POST['dati'];
}

function salvaMedico(){
    
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $nomeCognome = $_POST['Nome_Cognome'];
    $oreSett = $_POST['OreSett'];
    $specializzazione = $_POST['Specializzazione'];
    $qualifica = $_POST['Qualifica'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
       $DBa->insert($tableName,
            [
                'Nome_Cognome' => $nomeCognome,
                'OreSett' => $oreSett,
                'Specializzazione'	=> $specializzazione,
                'Qualifica' => $qualifica
            ]);
    }else{//action Update
        $DBa->update($tableName,
            [
                'Nome_Cognome' => $nomeCognome,
                'OreSett' => $oreSett,
                'Specializzazione'	=> $specializzazione,
                'Qualifica' => $qualifica
            ])->where('Id','=',$Id)->exec();
        
    }
    
    
    
}

function salvaIcd9cmn(){
    
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $codice = $_POST['codice'];
    $descrizione = $_POST['descrizione'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'codice' => $codice,
                'descrizione' => $descrizione
            ]);
    }else{//action Update
        
        if($tableName == "icd9cmn"){
            $DBa->update($tableName,
                [
                    'codice' => $codice,
                    'descrizione' => $descrizione
                ])->where('ID1','=',$Id)->exec();
        }else{
            $DBa->update($tableName,
                [
                    'codice' => $codice,
                    'descrizione' => $descrizione
                ])->where('Id','=',$Id)->exec();
        }
        
        
            
    }
    
    
    
}

function updatePaziente(){
    
    $tableName = $_POST['tableName'];
    $codAnagr = $_POST['codAnagr'];
    $action = $_POST['action'];
    $data=$_POST['jsonData'];
    $DBa = Anagrafica::getInstance();
    
        $sql = "DELETE FROM anagrafico WHERE CodAnagr = ".$codAnagr;
        $rows = $DBa->query($sql, [""],true);
        $DBa->insert($tableName,$data);
    
    
}
function eliminaPaziente(){
    $db = MysqliDb::getInstance();
    $tableName = $_POST['tableName'];
    $codAnagr = $_POST['codAnagr'];
    //$DBa = DB::getInstance();
    // elimina
    //echo  $DBa->delete($tableName, ['CodAnagr', $codAnagr]);
    $db->where('CodAnagr', $codAnagr);
    if($db->delete($tableName)) echo 'successfully deleted';
    
}

function eliminaFromID(){
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $DBa = DB::getInstance();
    $DBa->delete($tableName, $Id);
    
}

function eliminaFromIDFestivita(){
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $DBa = DB::getInstance();
    $DBa->delete($tableName, $Id);
        
    $progetti = $DBa->table('progetto')->select("CodProj, DataFine, DataInizio")->where('DataInizio','<',"'".$_POST['dataEliminata']."'")->where('DataFine','>=',"'".$_POST['dataEliminata']."'")->get()->toArray();
    $dataFine = $dataInizio;
    if(sizeof($progetti)!=0){
        
        
        for($i = 0; $i < sizeof($progetti); $i++){
            
            $dataFine = $progetti[$i]["DataFine"];
            $dataInizio = $progetti[$i]["DataInizio"];
            
            $festivit� = $DBa->table('festivita')->select("DataFestivita")->where('DataFestivita','>=',"'".$dataInizio."'")->get()->toArray();
            $dataFine =  date('Y-m-d', strtotime($dataFine. ' - 1 days'));
            
            for($k = 0; $k < sizeof($festivit�);$k++){
                if($festivit�[$k] == $dataFine){
                    $dataFine =  date('Y-m-d', strtotime($dataFine. ' - 1 days'));
                }
                if(giorno($dataFine) == "domenica"){
                    $dataFine =  date('Y-m-d', strtotime($dataFine. ' - 1 days'));
                }
                
            }
            
            if($progetti[$i]["DataFine"]!=$dataFine){
                $DBb = DB::getInstance();
                $DBb->query("UPDATE progetto SET `DataFine` = '".$dataFine."' WHERE `CodProj` = ".$progetti[$i]['CodProj']);
                $sql = $DBb->getSQL();
            }
            
        }
        
        
    }
    
    
}
function salvaTerapista(){
    
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $nomeCognome = $_POST['Nominativo'];
    $username = $_POST['username'];
    $specializzazione = $_POST['Specializzazione'];
    $oreAmb = $_POST['oreAmb'];
    $pazAmb = $_POST['pazAmb'];
    $oreDom = $_POST['oreDom'];
    $pazDom = $_POST['pazDom'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'Nominativo' => $nomeCognome,
                'specializzazione'	=> $specializzazione,
                'oreAmb' => $oreAmb,
                'pazAmb'	=> $pazAmb,
                'oreDom' => $oreDom,
                'pazDom' => $pazDom
            ]);
        salavProfileLight($nomeCognome, $username, $username, '4');
    }else{//action Update
        $DBa->update($tableName,
            [
                'Nominativo' => $nomeCognome,
                'specializzazione'	=> $specializzazione,
                'oreAmb' =>  $oreAmb,
                'pazAmb' =>  $pazAmb,
                'oreDom' =>  $oreDom,
                'pazDom' =>  $pazDom
               
            ])->where('Id','=',$Id)->exec();
            
    }
    
   
    
}


function salvaFestivita(){
    
    $tableName = $_POST['tableName'];
    $id = $_POST['Id'];
    $DataFestivita = $_POST['DataFestivita'];
    $Commento = $_POST['Commento'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'DataFestivita' => $DataFestivita,
                'Commento'	=> $Commento
            ]);
    }else{//action Update
        $DBa->update($tableName,
            [
                'DataFestivita' => $DataFestivita,
                'Commento'	=> $Commento
                
            ])->where('id','=',$id)->exec();
            
    }
    
    $progetti = $DBa->table('progetto')->select("CodProj, DataFine, DataInizio")->where('DataInizio','<',"'".$DataFestivita."'")->where('DataFine','>=',"'".$DataFestivita."'")->get()->toArray();
    $dataFine = $dataInizio;
    if(sizeof($progetti)!=0){
        
        
        for($i = 0; $i < sizeof($progetti); $i++){
            
            $dataFine = $progetti[$i]["DataFine"];
            $dataInizio = $progetti[$i]["DataInizio"];
            
            $festivit� = $DBa->table('festivita')->select("DataFestivita")->where('DataFestivita','>=',"'".$dataInizio."'")->get()->toArray();
            $dataFine =  date('Y-m-d', strtotime($dataFine. ' + 1 days'));
            
            for($k = 0; $k < sizeof($festivit�);$k++){
                if($festivit�[$k] == $dataFine){
                    $dataFine =  date('Y-m-d', strtotime($dataFine. ' + 1 days'));
                }
                if(giorno($dataFine) == "domenica"){
                    $dataFine =  date('Y-m-d', strtotime($dataFine. ' + 1 days'));
                }
                                    
            }
            
            if($progetti[$i]["DataFine"]!=$dataFine){
                $DBb = DB::getInstance();
                $DBb->query("UPDATE progetto SET `DataFine` = '".$dataFine."' WHERE `CodProj` = ".$progetti[$i]['CodProj']);
                $sql = $DBb->getSQL();
            }
            
        }
        
        
    }
    
}

function giorno($d){
    
    //attento la data deve essere nel formato yyyy-mm-gg
    //anche come separatori (se altri separatori devi modificare)
    $d_ex=explode("-", $d);//attento al separatore
    $d_ts=mktime(0,0,0,$d_ex[1],$d_ex[2],$d_ex[0]);
    $num_gg=(int)date("N",$d_ts);//1 (for Monday) through 7 (for Sunday)
    //per nomi in italiano
    $giorno=array('','luned�','marted�','mercoled�','gioved�','venerd�','sabato','domenica');//0 vuoto
    return $giorno[$num_gg];
}

function salavProfileLight($nomeCognome, $username, $pwd, $role){
    
    $tableName = 'profile';
    $DBa = DB::getInstance();
    $DBa->insert($tableName,
        [
            'nome_cognome'	=> $nomeCognome,
            'email'	=> 'noreplay@igea.online',
            'username'	=> $username,
            'role'	=> $role,
            'notifiche'	=> 0,
            'password' => md5($username)
        ]);
    $query = $DBa->getSQL();
    
}


function salvaProfile(){
    
    $tableName = $_POST['tableName'];
    $id = $_POST['Id'];
    if($_POST['password']!=""){
        $action = 3;
        $password = $_POST['password'];
        $nomeCognome = $_POST['nome_cognome'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $role = $_POST['role'];
        $notifiche = $_POST['notifiche'];
    }else{
        $nomeCognome = $_POST['nome_cognome'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $role = $_POST['role'];
        $notifiche = $_POST['notifiche'];
        $action = $_POST['action'];
    }
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'nome_cognome'	=> $nomeCognome,
                'email'	=> $email,
                'username'	=> $username,
                'role'	=> $role,
                'notifiche'	=> $notifiche,
                'password' => md5($username)
            ]);
        $query = $DBa->getSQL();
        
    }else{//action Update
        if($action==3){
            $DBa->update($tableName,
                [
                    'nome_cognome'	=> $nomeCognome,
                    'email'	=> $email,
                    'username'	=> $username,
                    'role'	=> $role,
                    'notifiche'	=> $notifiche,
                    'password' => md5($password)
                    
                ])->where('id','=',$id)->exec();
                
        }else{
        
            $DBa->update($tableName,
                [
                    'nome_cognome'	=> $nomeCognome,
                    'email'	=> $email,
                    'username'	=> $username,
                    'role'	=> $role,
                    'notifiche'	=> $notifiche
                    //'password' => md5($username)
                    
                ])->where('id','=',$id)->exec();
                
        }
                
    }
    
    
    
}

function salvaBanca(){
    
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $banca = $_POST['banca'];
    $agenzia = $_POST['agenzia'];
    $via = $_POST['via'];
    $CAP = $_POST['CAP'];
    $citta = $_POST['citta'];
    $iban = $_POST['iban'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'Banca' => $banca,
                'Via'	=> $via,
                'CAP' => $CAP,
                'citta'	=> $citta,
                'iban' => $iban,
                'agenzia' => $agenzia
            ]);
    }else{//action Update
        $DBa->update($tableName,
            [
                'Banca' => $banca,
                'Via'	=> $via,
                'CAP' =>  $CAP,
                'citta' =>  $citta,
                'iban' =>  $iban,
                'agenzia' =>  $agenzia
                
            ])->where('Id','=',$Id)->exec();
            
    }
    
    
    
}

function salvaAmministrativo(){
    
    $tableName = $_POST['tableName'];
    $Id = $_POST['Id'];
    $nomeCognome = $_POST['nomeCognome'];
    $oreSett = $_POST['oreSett'];
    $action = $_POST['action'];
    $DBa = DB::getInstance();
    if($action == '1'){//action Salva
        $DBa->insert($tableName,
            [
                'nomeCognome' => $nomeCognome,
                'OreSett'	=> $oreSett
            ]);
    }else{//action Update
        $DBa->update($tableName,
            [
                'nomeCognome' => $nomeCognome,
                'OreSett'	=> $oreSett
            ])->where('Id','=',$Id)->exec();
            
    }
    
    
    
}

function salvaDati_anagrafico($table_name,$data=[]){
    
    $Anagr = Anagrafica::getInstance();
    
    $Anagr->insert($table_name,$data);
}


function salvaCodProg(){
    session_start();
    $_SESSION['codiceProgetto'] = $_POST['codProg'];
}

function setPostDataCartClinic(){
    
    $DBa = DB::getInstance();
    
}

?>