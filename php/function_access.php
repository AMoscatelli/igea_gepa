<?php 

include 'config.inc.php';
include 'MSACCESS_MYSQL.php';

function retriveCampiPresenze(){
    
    $DBaccess = MSACCESS_MYSQL::getInstance();
    
    
    $query='SELECT Presenze.CodProj, Presenze.DataInizio, Presenze.DataFine, Presenze.GGPCTeor, Presenze.Ass80, Presenze.Ass00 FROM Presenze WHERE (((Presenze.[Anno])=2018) AND ((Presenze.[Mese])=2));';
    
    $result = $DBaccess->executeQuery($query);
    $result->setFetchMode(PDO::FETCH_ASSOC);
    return  array_keys($result->fetch());
}
function retriveValoriPresenze($anno, $mese){
    
    $DBaccess = MSACCESS_MYSQL::getInstance();
    $DBmysql = DB::getInstance();
    
    $query='SELECT Presenze.CodProj, Presenze.DataInizio, Presenze.DataFine, Presenze.GGPCTeor, Presenze.Ass80, Presenze.Ass00 FROM Presenze WHERE (((Presenze.[Anno])=2018) AND ((Presenze.[Mese])=2));';
    
    $result = $DBaccess->executeQuery($query);
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $i=0;
    while($row = $result->fetch()) {
        
           $record['CodProj'] = $row['CodProj'];
           $record['DataInizio'] = $row['DataInizio'];
           $record['DataFine'] = $row['DataFine'];
           $record['GGPCTeor'] = $row['GGPCTeor'];
           $record['Ass80'] = $row['Ass80'];
           $record['Ass00'] = $row['Ass00'];
           $record['anno'] = $anno;
           $record['mese'] = $mese;
           
           $riga[$i] = $record;
           $i++;
    }
    return $riga;
}


?>