<?php
//include_once 'config.php';
include 'function.php';
include 'function_access.php';



//controllo le chimate fatte da JQuery
if(isset($_POST['source'])){
    //session_start();
    
    if(isset($_POST['source'])){
        $source = $_POST['source'];
        switch ($source) {
            case 'saveConatbAssenze':
                saveConatbAssenze();
                break;
            case 'login':
                login();
                break;
            case 'cercaProgetto':
                cercaProgetto();
                break;
            case 'mandaDati':
                salva();
                break;
            case 'cancellaVariabile':
                cancella();
                break;
			case 'inviaDati':
                invia();
                break;
			case 'salvaDatiSession':
			    session_start();
			    $_SESSION[$_POST['nomeVariabile']] = $_POST['dati'];
			    break;
			case 'salvaMedico':
			    salvaMedico();
			    break;
			case 'eliminaMedico':
			    eliminaFromID();
			    break;
			case 'eliminaProfile':
			    eliminaFromID();
			    break;
			case 'salvaIcd9cmn':
			    salvaIcd9cmn();
			    break;
			case 'eliminaIcd9cmn':
			    eliminaFromID();
			    break;
			case 'salvaTerapista':
			    salvaTerapista();
			    break;
			case 'eliminaTerapista':
			    eliminaFromID();
			    break;
			case 'salvaFestivita':
			    salvaFestivita();
			    break;
			case 'salvaProfile':
			    salvaProfile();
			    break;
			case 'eliminaFestivita':
			    eliminaFromIDFestivita();
			    break;
			case 'salvaBanca':
			    salvaBanca();
			    break;
			case 'eliminaBanca':
			    eliminaFromID();
			    break;
			case 'salvaAmministrativo':
			    salvaAmministrativo();
			    break;
			case 'eliminaAmministrativo':
			    eliminaFromID();
			    break;
			case 'updatePaziente':
			    updatePaziente();
			    break;
			case 'eliminaPaziente':
			    eliminaPaziente();
			    break;
			case 'salvaTabelle':
			    salvaTabelle();
			    break;
			case 'salvaDatiProgetto':
			    salvaDati('progetto');
			    break;
			case 'cercaValutazioneFunzionale':
			    cercaPaziente1('ValutazioneFunzionale','DataVF, FaseVF, Medico, Terapista, ValutazioneFunzionale,Obbiettivi, TempiPrevisti, InterventiProposti','CodProj',$_POST['codProg']);
			    break;
			case 'cercaEsamiStrumentali':
			    cercaPaziente1('EsamiStrumentali','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaAccessiPrev':
			    cercaPaziente1('interventiprevisti','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaFigureProfIniz':
			    cercaPaziente1('figureprofinizio','','CodpROJ',$_POST['codProg']);
			    break;
			case 'cercaIndicazioni':
			    cercaPaziente1('Indicazioni','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaProfilo':
			    cercaPaziente1('ProfiloDisabN','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaPatologie':
			    cercaPaziente1('PatologieOggetto','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaAltrePatologie':
			    cercaPaziente1('altrepatologie','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaProgrammaT':
			    cercaPaziente1('ProgrammaTerapeutico','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaProgettoR':
			    cercaPaziente1('ProgettoRiabilitativo','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaDiaria':
			    cercaPaziente1('Diaria','','CodProj',$_POST['codProg']);
			    break;
			case 'cercaConsulenza':
			    cercaPaziente1('Consulenze','','CodProj',$_POST['codProg']);
			    break;
			case 'getPresenze':
			    $anno = $_POST['selectAnno'];
			    $mese = $_POST['selectMese'];
			    controllerMethod($source, $mese.'|'.$anno);
			    break;
			case 'cercaContabDomAmb':
			    $index = $_POST['index'];
			    controllerMethod($source, $index);
			    break;
			case 'getRette':
			    getRette();
			    break;
			case 'stampaCartClinic':
			    setPostDataCartClinic();
			case 'richiediCodProgetto':
			    selectAll('response','');
			    break;
			case 'salvaCodProg':
			    salvaCodProg();

        }
    }
}

//gestisce le chiamate fatte da JQuery o dirette da PHP
function controller(){
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $source = $_POST['source'];
        switch ($source) {
            case 'saveConatbAssenze':
                saveConatbAssenze();
                break;
            case 'login':
                login();
                break;
        }
    }     
}


function controllerMethod($method, $input){
    switch ($method) {
        case 'cercaContabDomAmb':
            cercaContabResNonRes($input);
            break;
        case 'unUsed':
            break;
        case 'selectAll':
            selectAll($input, "*");
            break;
        case 'getPresenze':
            $data = explode("|",$input);
            
            getPresenze($data[0], $data[1]);
            break;
    }
    
}

?>