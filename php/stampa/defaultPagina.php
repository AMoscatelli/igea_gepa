<?php
include '../../php/fpdf.php';


class defaultPagina extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('../../images/airri.png',75,6,60);
        // Arial bold 15
        $this->SetFont('Arial','B',12);
        // Move Next Row
        $this->Cell(80,30,'',0,1);
        // Title
        $this->Cell(190,10,'ASSOCIAZIONE ITALIANA RIABILITAZIONE REINSERIMENTO INVALIDI',0,0,'C');
        // Line break
        $this->Ln(20);
    }
    
    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-25);
        // Arial italic 8
        $this->SetFont('Arial','B',12);
        //Footer infos
        $this->Cell(0,10,'CENTRO "CLODIO" di RIABILITAZIONE',0,1,'C');
        $this->SetFont('Arial','',8);
        $this->Cell(0,4,'00195 ROMA - VIA ENRICO ACCINI, 20 - TEL. 06.39741144 - 06.39741153 - FAX 06.3974137',0,1,'C');
        $this->Cell(0,4,'email: airriclodio@airri.it - P.IVA 01200791000 - Cod. Fisc. 03469660587',0,1,'C');
        // Page number
        $this->Cell(0,4,'Pag. '.$this->PageNo().'/{nb}',0,0,'R');
    }
}

?>