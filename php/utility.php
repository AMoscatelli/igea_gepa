<?php

function getnomeMese($numeroMese){
    
    if($numeroMese==1){
        return 'Gennaio';
    }
    if($numeroMese==2){
        return 'Febbraio';
    }
    if($numeroMese==3){
        return 'Marzo';
    }
    if($numeroMese==4){
        return 'Aprile';
    }
    if($numeroMese==5){
        return 'Maggio';
    }
    if($numeroMese==6){
        return 'Giugno';
    }
    if($numeroMese==7){
        return 'Luglio';
    }
    if($numeroMese==8){
        return 'Agosto';
    }
    if($numeroMese==9){
        return 'Settembre';
    }
    if($numeroMese==10){
        return 'Ottobre';
    }
    if($numeroMese==11){
        return 'Novembre';
    }
    if($numeroMese==12){
        return 'Dicembre';
    }
    
}
?>