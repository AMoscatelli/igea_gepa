<?php
//====================== Marei DB Class V 1.0 ======================
//include_once '../config.inc.php';

class Anagrafica{
    private static $instance = null;
    private $dbh = null, $table, $columns, $sql, $bindValues, $getSQL,
    $where, $orWhere, $whereCount=0, $isOrWhere = false,
    $rowCount=0, $limit, $orderBy, $SQLString, $lastIDInserted = 0;
    private $reset = true;
    // Initial values for pagination array
    private $pagination = ['previousPage' => null,'currentPage' => 1,'nextPage' => null,'lastPage' => null, 'totalRows' => null];

    
    private function __construct()
    {
        global $db_config;
        
        if ($db_config['env'] == "development") {
            $config = $db_config['development'];
        }elseif ($db_config['env'] == "production") {
            $config = $db_config['production'];
        }else{
            die("Environment must be either 'development' or 'production'.");
        }
        
        try {
            $this->dbh = new PDO("mysql:host=".$config['host'].";dbname=".$config['database'].";charset=utf8", $config['username'], $config['password'] );
            $this->dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $db_config = null;
        } catch (Exception $e) {
            die("Error establishing a database connection.");
        }
        
    }
    
    public function query($query, $args = [], $quick = false)
    {
        $this->resetQuery();
        $query = trim($query);
        $this->getSQL = $query;
        $this->bindValues = $args;
        
        if ($quick == true) {
            $stmt = $this->dbh->prepare($query);
            $stmt->execute($this->bindValues);
            $this->rowCount = $stmt->rowCount();
            return $stmt->fetchAll();
        }else{
            if (strpos( strtoupper($query), "SELECT" ) === 0 ) {
                $stmt = $this->dbh->prepare($query);
                $stmt->execute($this->bindValues);
                $this->rowCount = $stmt->rowCount();
                
                $rows = $stmt->fetchAll(PDO::FETCH_CLASS,'MareiObj1');
                $collection= [];
                $collection = new MareiCollection1;
                $x=0;
                foreach ($rows as $key => $row) {
                    $collection->offsetSet($x++,$row);
                }
                
                return $collection;
                
            }else{
                $this->getSQL = $query;
                $stmt = $this->dbh->prepare($query);
                $stmt->execute($this->bindValues);
                return $stmt->rowCount();
            }
        }
    }
    
    public function delete($table_name, $id=null)
    {
        $this->resetQuery();
        
        $this->sql = "DELETE FROM `{$table_name}`";
        
        if (isset($id)) {
            // if there is an ID
            if (is_numeric($id)) {
                if($table_name == "icd9cmn"){
                    $this->sql .= " WHERE `ID1` = ?";
                    $this->bindValues[] = $id;
                }else{
                    $this->sql .= " WHERE `id` = ?";
                    $this->bindValues[] = $id;
                }
                
                // if there is an Array
            }elseif (is_array($id)) {
                $arr = $id;
                $count_arr = count($arr);
                $x = 0;
                
                foreach ($arr as  $param) {
                    if ($x == 0) {
                        $this->where .= " WHERE ";
                        $x++;
                    }else{
                        if ($this->isOrWhere) {
                            $this->where .= " Or ";
                        }else{
                            $this->where .= " AND ";
                        }
                        
                        $x++;
                    }
                    $count_param = count($param);
                    
                    if ($count_param == 1) {
                        $this->where .= "`id` = ?";
                        $this->bindValues[] =  $param[0];
                    }elseif ($count_param == 2) {
                        $operators = explode(',', "=,>,<,>=,>=,<>");
                        $operatorFound = false;
                        
                        foreach ($operators as $operator) {
                            if ( strpos($param[0], $operator) !== false ) {
                                $operatorFound = true;
                                break;
                            }
                        }
                        
                        if ($operatorFound) {
                            $this->where .= $param[0]." ?";
                        }else{
                            $this->where .= "`".trim($param[0])."` = ?";
                        }
                        
                        $this->bindValues[] =  $param[1];
                    }elseif ($count_param == 3) {
                        $this->where .= "`".trim($param[0]). "` ". $param[1]. " ?";
                        $this->bindValues[] =  $param[2];
                    }
                    
                }
                //end foreach
            }
            // end if there is an Array
            $this->sql .= $this->where;
            
            $this->getSQL = $this->sql;
            $stmt = $this->dbh->prepare($this->sql);
            $stmt->execute($this->bindValues);
            return $stmt->rowCount();
        }// end if there is an ID or Array
        // $this->getSQL = "<b>Attention:</b> This Query will update all rows in the table, luckily it didn't execute yet!, use exec() method to execute the following query :<br>". $this->sql;
        // $this->getSQL = $this->sql;
        return $this;
    }
    
    public function update($table_name, $fields = [], $id=null)
    {
        $this->resetQuery();
        $set ='';
        $x = 1;
        
        foreach ($fields as $column => $field) {
            $set .= "`$column` = '$field'";
            $this->bindValues[] = $field;
            if ( $x < count($fields) ) {
                $set .= ", ";
            }
            $x++;
        }
        
        $this->sql = "UPDATE `{$table_name}` SET $set";
        
        if (isset($id)) {
            // if there is an ID
            if (is_numeric($id)) {
                $this->sql .= " WHERE `id` = ?";
                $this->bindValues[] = $id;
                // if there is an Array
            }elseif (is_array($id)) {
                $arr = $id;
                $count_arr = count($arr);
                $x = 0;
                
                foreach ($arr as  $param) {
                    if ($x == 0) {
                        $this->where .= " WHERE ";
                        $x++;
                    }else{
                        if ($this->isOrWhere) {
                            $this->where .= " Or ";
                        }else{
                            $this->where .= " AND ";
                        }
                        
                        $x++;
                    }
                    $count_param = count($param);
                    
                    if ($count_param == 1) {
                        $this->where .= "`id` = ?";
                        $this->bindValues[] =  $param[0];
                    }elseif ($count_param == 2) {
                        $operators = explode(',', "=,>,<,>=,>=,<>");
                        $operatorFound = false;
                        
                        foreach ($operators as $operator) {
                            if ( strpos($param[0], $operator) !== false ) {
                                $operatorFound = true;
                                break;
                            }
                        }
                        
                        if ($operatorFound) {
                            $this->where .= $param[0]." ?";
                        }else{
                            $this->where .= "`".trim($param[0])."` = ?";
                        }
                        
                        $this->bindValues[] =  $param[1];
                    }elseif ($count_param == 3) {
                        $this->where .= "`".trim($param[0]). "` ". $param[1]. " ?";
                        $this->bindValues[] =  $param[2];
                    }
                    
                }
                //end foreach
            }
            // end if there is an Array
            $this->sql .= $this->where;
            
            $this->getSQL = $this->sql;
            $stmt = $this->dbh->prepare($this->sql);
            $stmt->execute($this->bindValues);
            return $stmt->rowCount();
        }// end if there is an ID or Array
        // $this->getSQL = "<b>Attention:</b> This Query will update all rows in the table, luckily it didn't execute yet!, use exec() method to execute the following query :<br>". $this->sql;
        // $this->getSQL = $this->sql;
        return $this;
    }
    
    
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Anagrafica();
        }
        return self::$instance;
    }
    
    public function exec()
    {
        //assimble query
        $this->sql .= $this->where;
        $this->getSQL = $this->sql;
        $stmt = $this->dbh->prepare($this->sql);
        $stmt->execute($this->bindValues);
        return $stmt->rowCount();
    }
    
    private function resetQuery()
    {
        $this->table = null;
        $this->columns = null;
        $this->sql = null;
        $this->bindValues = null;
        $this->limit = null;
        $this->orderBy = null;
        $this->getSQL = null;
        $this->where = null;
        $this->SQLString = null;
        $this->orWhere = null;
        $this->whereCount = 0;
        $this->isOrWhere = false;
        $this->rowCount = 0;
        $this->lastIDInserted = 0;
        $this->reset = false;
    }
    
    private function getCampi($fields){
        
        static $campiArray = [
            'codAnag' => 'CodAnagr',
            'codProg' => 'CodProj',
            'stato' => 'Stato',
            //'stato_view' => 'Stato',
            //'stato' => 'stato_view',
            'dataInizio' => 'DataInizio',
            'dataFine' => 'DataFine',
            'dateStato' => 'DataStato',
            'durataProgetto' => 'DurataProgetto',
            'intervento' => 'Intervento',
            'regAssist' => 'RegAssist',
            'modInt' => 'ModInt',
            'impRiab' => 'ImpRiab',
            'medico' => 'Medico',
            'terapista1' => 'Terapista1',
            'terapista2' => 'Terapista2',
            'terapista3' => 'Terapista3',
            'anamnesi' => 'AnamnesiPatologicaprossima',
            'esameObbiettivoS' => 'EsameObbiettivoSpecialistico',
            'esameObbiettivoG' => 'EsameObbiettivoGenerale',
            'invalidita' => 'Invalidita',
            'percInvalidita' => 'InvaliditaPerc',
            'modTrasp' => 'ModaTrasporto',
            'altraModTrasp' => 'altraModaTrasporto',
            'ricov12mesi' => 'Ricov12mesi',
            'strProvenienza' => 'CodProvenienza',
            'altroProv' => 'altroprov',
            'condVita' => 'CodCondiVita',
            'inviante' => 'CodInviante',
            'altroCondVita' => 'altrocondivita',
            'altroInviante' => 'Altroinv',
            'perTrattamento' => 'PeriodoTrattamento',
            'obRiabRagg' => 'ObbiettiviRiabilitativiRaggiunti',
            'freqSett' => 'freqsett1',
            'scalaCod' => 'ScalaCodificata',
            'scalaCod1' => 'ScalaCodificata1'];
            
            // $keys1 = implode('`, `', array_keys(Progetto::getCampi()));
        $a = 0;
        $field_nuovo = [];
        // $chiavi_campi = array_keys($campiArray);
        foreach ($fields as $field => $key) {
            
            if ($campiArray[$field]) {
                $field_nuovo[$campiArray[$field]] = $fields[$field];
            } else if ($campiArray[$field]=='nuovoPr'|($campiArray[$field])=='vecchioPr'){
                $field_nuovo[$field] = $fields[$field];
            }
        }
        return $field_nuovo;
    }
    
    
    public function insert( $table_name, $fields1 = [] )
    {
        //$fields1 = Progetto::getCampi($fields);
        
        $this->resetQuery();
        
        $keys = implode('`, `', array_keys($fields1));
        $values = '';
        $x=1;
        foreach ($fields1 as $field => $value) {
            
            $this->bindValues[] =  escapeLocal($value);
            $values .="'".$value;
            if ($x < count($fields1)) {
                $values .="', ";
            }
            $x++;
        }
        
        $this->sql = "INSERT INTO `{$table_name}` (`{$keys}`) VALUES ({$values}')";
        $this->getSQL = $this->sql;
        $stmt = $this->dbh->prepare($this->sql);
        $stmt->execute($this->bindValues);
        $this->lastIDInserted = $this->dbh->lastInsertId();
        
        return $this->lastIDInserted;
    }//End insert function
    
    public function select($columns)
    {
        $columns = explode(',', $columns);
        foreach ($columns as $key => $column) {
            $columns[$key] = trim($column);
        }
        
        $columns = implode(', ', $columns);
        
        
        $this->columns = "{$columns}";
        return $this;
    }
    
    public function where()
    {
        if ($this->whereCount == 0) {
            $this->where .= " WHERE ";
            $this->whereCount+=1;
        }else{
            $this->where .= " AND ";
        }
        
        $this->isOrWhere= false;
        
        // call_user_method_array('where_orWhere', $this, func_get_args());
        //Call to undefined function call_user_method_array()
        //echo print_r(func_num_args());
        $num_args = func_num_args();
        $args = func_get_args();
        if ($num_args == 1) {
            if (is_numeric($args[0])) {
                $this->where .= "`id` = ?";
                $this->bindValues[] =  $args[0];
            }elseif (is_array($args[0])) {
                $arr = $args[0];
                $count_arr = count($arr);
                $x = 0;
                
                foreach ($arr as  $param) {
                    if ($x == 0) {
                        $x++;
                    }else{
                        if ($this->isOrWhere) {
                            $this->where .= " Or ";
                        }else{
                            $this->where .= " AND ";
                        }
                        
                        $x++;
                    }
                    $count_param = count($param);
                    if ($count_param == 1) {
                        $this->where .= "`id` = ?";
                        $this->bindValues[] =  $param[0];
                    }elseif ($count_param == 2) {
                        $operators = explode(',', "=,>,<,>=,>=,<>");
                        $operatorFound = false;
                        
                        foreach ($operators as $operator) {
                            if ( strpos($param[0], $operator) !== false ) {
                                $operatorFound = true;
                                break;
                            }
                        }
                        
                        if ($operatorFound) {
                            $this->where .= $param[0]." ?";
                        }else{
                            $this->where .= "`".trim($param[0])."` = ?";
                        }
                        
                        $this->bindValues[] =  $param[1];
                    }elseif ($count_param == 3) {
                        $this->where .= "`".trim($param[0]). "` ". $param[1]. " ?";
                        $this->bindValues[] =  $param[2];
                    }
                }
            }
            // end of is array
        }elseif ($num_args == 2) {
            $operators = explode(',', "=,>,<,>=,>=,<>");
            $operatorFound = false;
            foreach ($operators as $operator) {
                if ( strpos($args[0], $operator) !== false ) {
                    $operatorFound = true;
                    break;
                }
            }
            
            if ($operatorFound) {
                $this->where .= $args[0]." ?";
            }else{
                $this->where .= "`".trim($args[0])."` = '".trim($args[1]."'");
            }
            
            $this->bindValues[] =  $args[1];
            
        }elseif ($num_args == 3) {
            
            $this->where .= "".trim($args[0]). " ". $args[1]. " " .trim($args[2])."";
            $this->bindValues[] =  $args[2];
        }
        
        return $this;
    }
    
    public function orWhere()
    {
        if ($this->whereCount == 0) {
            $this->where .= " WHERE ";
            $this->whereCount+=1;
        }else{
            $this->where .= " OR ";
        }
        $this->isOrWhere= true;
        // call_user_method_array ( 'where_orWhere' , $this ,  func_get_args() );
        
        $num_args = func_num_args();
        $args = func_get_args();
        if ($num_args == 1) {
            if (is_numeric($args[0])) {
                $this->where .= "`id` = ?";
                $this->bindValues[] =  $args[0];
            }elseif (is_array($args[0])) {
                $arr = $args[0];
                $count_arr = count($arr);
                $x = 0;
                
                foreach ($arr as  $param) {
                    if ($x == 0) {
                        $x++;
                    }else{
                        if ($this->isOrWhere) {
                            $this->where .= " Or ";
                        }else{
                            $this->where .= " AND ";
                        }
                        
                        $x++;
                    }
                    $count_param = count($param);
                    if ($count_param == 1) {
                        $this->where .= "`id` = ?";
                        $this->bindValues[] =  $param[0];
                    }elseif ($count_param == 2) {
                        $operators = explode(',', "=,>,<,>=,>=,<>");
                        $operatorFound = false;
                        
                        foreach ($operators as $operator) {
                            if ( strpos($param[0], $operator) !== false ) {
                                $operatorFound = true;
                                break;
                            }
                        }
                        
                        if ($operatorFound) {
                            $this->where .= $param[0]." ?";
                        }else{
                            $this->where .= "`".trim($param[0])."` = ?";
                        }
                        
                        $this->bindValues[] =  $param[1];
                    }elseif ($count_param == 3) {
                        $this->where .= "`".trim($param[0]). "` ". $param[1]. " ".$param[2];
                        $this->bindValues[] =  $param[2];
                    }
                }
            }
            // end of is array
        }elseif ($num_args == 2) {
            $operators = explode(',', "=,>,<,>=,>=,<>");
            $operatorFound = false;
            foreach ($operators as $operator) {
                if ( strpos($args[0], $operator) !== false ) {
                    $operatorFound = true;
                    break;
                }
            }
            
            if ($operatorFound) {
                $this->where .= $args[0]." ?";
            }else{
                $this->where .= "`".trim($args[0])."` = ?";
            }
            
            $this->bindValues[] =  $args[1];
            
        }elseif ($num_args == 3) {
            
            $this->where .= "`".trim($args[0]). "` ". $args[1].  " " .trim($args[2])."";
            $this->bindValues[] =  $args[2];
        }
        
        return $this;
    }
    
    // private function where_orWhere()
    // {
    
    // }
    
    public function get()
    {
        $this->assimbleQuery();
        $this->getSQL = $this->sql;
        
        $stmt = $this->dbh->prepare($this->sql);
        $stmt->execute($this->bindValues);
        $this->rowCount = $stmt->rowCount();
        
        $rows = $stmt->fetchAll(PDO::FETCH_CLASS,'MareiObj');
        $collection= [];
        $collection = new MareiCollection;
        $x=0;
        foreach ($rows as $key => $row) {
            $collection->offsetSet($x++,$row);
        }
        
        return $collection;
    }


    public function getSQL()
    {
        return $this->getSQL;
    }    
    
}
// End Marei DB Class
