<?php
include 'config.inc.php';
session_start();

if(!isset($_SESSION['login_user'])){
    $_SESSION['url_redirect'] = $_SERVER['REQUEST_URI'];
    header("location: ".$baseurl."/login.php?result=10");//session expired or Logout
    exit;
}else{
    $request = floatval(str_replace('.', '',$_SERVER['REQUEST_TIME_FLOAT']));
    $request= substr($request, 0, 11);
    if(($request-$_SESSION['session_started'])>600000){
        session_destroy();
        header("location: ".$baseurl."/login.php?result=10");//session expired or Logout
        exit;
    }else{
        $_SESSION['session_started'] = $request;
        $login_session = $_SESSION['login_user'];
        $login_role = $_SESSION['login_role'];
        
        if(checkAccessLevel()<$login_role && !strpos($_SERVER['REQUEST_URI'],'index.php')){
            header("location: ".$baseurl."/index.php");//session ok
            exit;
        }
    }     
}


// Access Level (MySql):
// 1: Admin
// 2: Amministrazione
// 3: Dottori
// 4: Terapista
// 4: Utenti
// Corrispondenza Access Level (PHP):
// 2: Admin
// 3: Amministrazione
// 4: Dottori
// 5: Terapista
// 6: Utenti


function checkAccessLevel(){
    $token = explode('/',$_SERVER['SCRIPT_NAME']);
    switch ($token[sizeof($token)-2]) {
        case 'igea': //Admin
            return 5;
            break;
        case 'contabilita': //Amministrazione
            return 3;
            break;
        case 'anagrafica': //Medici
            return 5;
            break;
        case 'utility': //Utenti
            return 4;
            break;
        case 'progetto': //Utenti
            return 2;
            break;
        case 'stampe': //Utenti
            return 3;
            break;
        case 'calendar': //Utenti
            return 6;
            break;
        case 'lista_attesa': //Utenti
            return 6;
            break;
        case 'cartClinic': //Utenti
            return 6;
            break;
    }
    
}
?>