<?php
class MSACCESS_MYSQL{
    private static $instance = null;
    private $dbName = null;
    private $protetto = null;
    private $tableName;
    private $whereCondition;
    private $MSAccessdb = null;
    private $MySqlconn = null;
    
    
    private function __construct()
    {
        $servername = "lhcp1129.webapps.net";
        $username= "h42njki8_igea";
        $password = "h42njki8_igea";
        $db = 'h42njki8_igea';
        $dbName = $_SERVER["DOCUMENT_ROOT"] . "/igea/ClodioDB.mdb";
        $protetto = $_SERVER["DOCUMENT_ROOT"] . "/igea/Protetto.mdw";
        try {
            if (!file_exists($dbName)) {
                die("Could not find database file.");
            }else{
                $this->MSAccessdb = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; SystemDB=$protetto; Uid=attilio; Pwd='&!x%;");
                $this->MSAccessdb->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                $this->MySqlconn = new mysqli($servername, $username, $password, $db);
            }
            
        } catch (Exception $e) {
            die("Error establishing a database connection.");
        }
        
    }
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new MSACCESS_MYSQL();
        }
        return self::$instance;
    }
    
    public function updateTableByID($tableName, $whereField, $whereCondition, $whereValue){
        
        $sql = "SELECT * FROM " . $tableName;
        $sql = $sql." where ".$whereField." ".$whereCondition." ".$whereValue;
 //        $sql = "SELECT * FROM Invalidita";
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $campi =  array_keys($STH->fetch());
        
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $servername = "lhcp1129.webapps.net";
        $username= "h42njki8_igea";
        $password = "h42njki8_igea";
        $db = 'h42njki8_igea';
        //$conn = new mysqli($servername, $username, $password, $db);
        while($row = $STH->fetch()) {
            if($row[$whereField] == $whereValue){
                for($i=1; $i<sizeof($campi); $i++){
                    $sql  = "UPDATE ".$tableName;
                    $sql .= " SET ".$campi[$i]." = '".$row[$campi[$i]]."'";
                    //$escaped_values = array_map('mysql_real_escape_string', array_values($row));
                    $sql .= " WHERE ".$whereField." = ".$whereValue;
                    $this->MySqlconn->query($sql);
                }
            }
        }
        
        if($tableName == 'progetto'){
            //Aggiorno la colonna stato con gli ID
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 1 WHERE `stato` = "ATTESA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 2 WHERE `stato` = "AUTORIZZATO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 3 WHERE `stato` = "TRATTAMENTO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 3 WHERE `stato` = "ATTIVO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESO/A"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTO/A"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTA"');
            //Aggiorno la interventi stato con gli ID
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 1 WHERE `intervento` = "NEUROMOTORIO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 1 WHERE `intervento` like "%neuro%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 2 WHERE `intervento` = "FISIOTERAPICO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 5 WHERE `intervento` = "OSTEOPATA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 8 WHERE `intervento` LIKE "%psicomot%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 9 WHERE `intervento` LIKE "%fisiatr%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 10 WHERE `intervento` LIKE "%ortoped%"');
        }
    }
    
    public function importBigTable($tableName){
        
        $sql = "SELECT * FROM " . $tableName;
        //$sql = $sql." where ".$whereField." ".$whereCondition." ".$whereValue;
        //        $sql = "SELECT * FROM Invalidita";
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $campi =  array_keys($STH->fetch());
        
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        
        //$conn = new mysqli($servername, $username, $password, $db);
        while($row = $STH->fetch()) {
            
                for($i=1; $i<sizeof($campi); $i++){
                    $sql  = "UPDATE ".$tableName;
                    $sql .= " SET ".$campi[$i]." = '".$row[$campi[$i]]."'";
                    //$escaped_values = array_map('mysql_real_escape_string', array_values($row));
                    $sql .= " WHERE ".$campi[0]." = ".$row[$campi[0]];
                    $this->MySqlconn->query($sql);
                }
            
        }
        
        if($tableName == 'progetto'){
            //Aggiorno la colonna stato con gli ID
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 1 WHERE `stato` = "ATTESA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 2 WHERE `stato` = "AUTORIZZATO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 3 WHERE `stato` = "TRATTAMENTO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 3 WHERE `stato` = "ATTIVO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESO/A"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 4 WHERE `stato` = "SOSPESA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTO/A"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `stato`= 5 WHERE `stato` = "DECEDUTA"');
            //Aggiorno la interventi stato con gli ID
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 1 WHERE `intervento` = "NEUROMOTORIO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 1 WHERE `intervento` like "%neuro%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 2 WHERE `intervento` = "FISIOTERAPICO"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 5 WHERE `intervento` = "OSTEOPATA"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 8 WHERE `intervento` LIKE "%psicomot%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 9 WHERE `intervento` LIKE "%fisiatr%"');
            $this->MySqlconn->query('UPDATE `progetto` SET `intervento`= 10 WHERE `intervento` LIKE "%ortoped%"');
        }
    }
    
    public function importTable($tableName){
        $sql = "SELECT * FROM ".$tableName;
        
        //preparo i record da rimettere nel DB
        //$sql = "SELECT * FROM Intervento";
        
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        
        //recupero prima i nomi dei campi
        $campi =  " (`".implode("`, `",array_keys($STH->fetch()))."`)";
        
        //rieseguo la query perche altrimenti mi perdo il primo record
        $STH = $this->MSAccessdb->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        
        while($row = $STH->fetch()) {
            //scrivo i dati in Mysql
            $sql  = "INSERT INTO ".$tableName;
            $sql .= " ".$campi."";
            $sql .= " VALUES ('".implode("', '", $row)."') ";
            $result = $this->MySqlconn->query($sql);
        }
        
        
    }


    
    public function executeQuery($query){
       
        $STH = $this->MSAccessdb->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        
        //recupero prima i nomi dei campi
        $campi =  " (`".implode("`, `",array_keys($STH->fetch()))."`)";
        
        //rieseguo la query perche altrimenti mi perdo il primo record
        $STH = $this->MSAccessdb->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        
        return $STH;
        
        
    }
    
    private function old(){
        
        $dbName = $_SERVER["DOCUMENT_ROOT"] . "/igea/ClodioDB.mdb";
        $protetto = $_SERVER["DOCUMENT_ROOT"] . "/igea/Protetto.mdw";
        $tableName = 'intervento';
        if (!file_exists($dbName)) {
            die("Could not find database file.");
        }
        
        
        $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; SystemDB=$protetto; Uid=attilio; Pwd='&!x%;");
        
        // $sql = "SELECT `CodProj`, `VecchiaChiave`, `CodAnagr`, `NumCart`, `Intervento`, `Medico`,  `GGdaIntervAcuto`,
        //                                 `DataIntervPrec`,
        //                                 `Stato`,
        //                                 `DataStato`,
        //                                 `DataInizio`,
        //                                 `DurataProgetto`,
        //                                 `DataFine`,
        //                                 `Regassist`,
        //                                 `ModInt`,
        //                                 `ImpRiab`,
        //                                 `Invalidita`,
        //                                 `InvaliditaPerc`,
        //                                 `ModaTrasporto`,
        //                                 `altraModaTrasporto`,
        //                                 `InSostegno`,
        //                                 `ServAssEl`,
        //                                 `CodInviante`,
        //                                 `Altroinv`,
        //                                 `StruttureProvenienza`,
        //                                 `CodProvenienza`,
        //                                 `altroprov`,
        //                                 `CodCondiVita`,
        //                                 `altrocondivita`,
        //                                 `Ricov12mesi`,
        //                                 `Proj12mesi`,
        //                                 `DataValutaz`,
        //                                 `nomeAltraFig`,
        //                                 `nomeAltraFig1`,
        //                                 `Terapista1`,
        //                                 `Terapista2`,
        //                                 `Terapista3` FROM ".$tableName;
        
        $sql = "SELECT * FROM ".$tableName;
        
        $STH = $db->query($sql);
        
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        //$columns = implode(", ",array_keys($STH->fetch()));
        // // build query...
        
        // // implode values of $array...
        // $sql .= " VALUES ('".implode("', '", $STH->fetch())."') ";
        // //$sql = "INSERT INTO `progetto` (`CodProj`, `VecchiaChiave`, `CodAnagr`, `NumCart`, `Intervento`, `Medico`, `GGdaIntervAcuto`, `DataIntervPrec`, `Stato`, `DataStato`, `DataInizio`, `DurataProgetto`, `DataFine`, `Regassist`, `ModInt`, `ImpRiab`, `Invalidita`, `InvaliditaPerc`, `ModaTrasporto`, `altraModaTrasporto`, `InSostegno`, `ServAssEl`, `CodInviante`, `Altroinv`, `StruttureProvenienza`, `CodProvenienza`, `altroprov`, `CodCondiVita`, `altrocondivita`, `Ricov12mesi`, `Proj12mesi`, `DataValutaz`, `nomeAltraFig`, `nomeAltraFig1`, `nomeAltraFig2`, `modulo`, `MedAltraSpecialita`, `CodiceAltraFig`, `CodiceAltraFig1`, `MedAltraSpecialitaf`, `CodiceAltraFigf`, `CodiceAltraFigf1`, `CodprotOrtAusili`, `P1`, `P2`, `P3`, `P4`, `P5`, `P6`, `P7`, `P8`, `P9`, `P10`, `CodOrt1`, `CodOrt2`, `CodOrt3`, `BarthelInizio`, `SPMSQInizio`, `SVAM_Lc`, `SVAM_Lp`, `SVAM_AU`, `SVAM_V`, `ScalaCodificata`, `ScalaCodificataPunteggio`, `ScalaCodificata1`, `ScalaCodificataPunteggio1`, `NomeAltraScalaInizio1`, `PuntAltraScalaInizio1`, `NomeAltraScalaInizio2`, `PuntAltraScalaInizio2`, `NomeAltraScalaInizio3`, `PuntAltraScalaInizio3`, `RisultatoAtteso`, `DurataGG`, `CicloSN`, `NumCicli`, `PdaAgg`, `PAggto`, `SospRicoveroNG`, `SospRicovero100`, `SospRicovero80`, `SospRicovero60`, `SospAssenzaTempNG`, `SospAssenzaTemp100`, `SospAssenzaTemp80`, `SospAssenzaTemp00`, `GGEffettiviPC`, `SoggEstivoSN`, `SoggEstivoOrg`, `SoggEstivoAltroSN`, `SoggEstivoAltro`, `SoggEstivoDurata`, `DataConclusione`, `nomeAltraFigfine`, `nomeAltraFig1fine`, `nomeAltraFig2fine`, `Barthelfine`, `SPSMSQfine`, `SVAM_Lcfine`, `SVAM_Lpfine`, `SVAM_AUfine`, `SVAM_Vfine`, `ScalaCodificataf`, `ScalaCodificataPunteggiof`, `ScalaCodificataf1`, `ScalaCodificataPunteggiof1`, `NomeAltraScalafine1`, `PuntAltraScalafine1`, `NomeAltraScalafine2`, `PuntAltraScalafine2`, `NomeAltraScalafine3`, `PuntAltraScalafine3`, `ProtOrtAusiliFine`, `P1f`, `P2f`, `P3f`, `P4f`, `P5f`, `P6f`, `P7f`, `P8f`, `P9f`, `P10f`, `MotivoConclusione`, `AltroMotivo`, `freqsett1`, `Terapista1`, `Terapista2`, `Terapista3`, `freqset1`, `freqset2`, `freqset3`, `AnamnesiPatologicaprossima`, `PeriodoTrattamento`, `ObbiettiviRiabilitativiRaggiunti`, `EsameObbiettivoSpecialistico`, `Terapiafarmacologica`, `EsamiEmatochimici`, `PropProgEquipeRiabIniz`, `DataProgIni`, `PropProgEquipeRiabInter`, `DataProgInt`, `AspettativeAssistito`, `AspettativeFamiliari`, `DimissioniEquipe`, `Feste`, `Chiu`, `Terapie`, `VisiteMed`, `ValTer`, `ValMed`, `InterTer`, `InterMed`, `CounsTer`, `CounsMed`, `IntInferm`, `ElabDirS`, `ElabTer`, `ElabMed`, `RequTer`, `RequMed`, `AdemTer`, `AdemMed`, `PsiMed`, `OK`, `Ortot`, `VisitaMed`, `OsInTer`, `ValTest`, `SomTestTer`, `ValTdr`, `ValLogo`, `Psicol`, `NeurPsi`, `Npi`, `Ortop`, `Neuro`, `Fonia`, `Ocul`, `Otor`, `Fisiatra`, `TecAusili`, `CounsFam`, `CounsIns`, `CounsEst`, `ElabIniz`, `VerInt`, `ElabFine`, `MedIniz`, `MedInt`, `MedFine`, `CounsFamM`, `CounsInsM`, `CounsEstM`, `EsameObbiettivoGenerale`, `DataDF`, `Diagnosifunzionale`, `TerRiab`, `DiagCli`, `AreaCog`, `AreaAff`, `AreaMot`, `AreaLing`, `AreaAppr`, `AreaAut`, `Interv`) VALUES";
        $servername = "lhcp1129.webapps.net";
        $username= "h42njki8_igea";
        $password = "h42njki8_igea";
        $db = 'h42njki8_igea';
        
        $campi =  " (`".implode("`, `",array_keys($STH->fetch()))."`)";
        echo $campi;
        
        // Create connection
        $conn = new mysqli($servername, $username, $password, $db);
        
        
        
        while($row = $STH->fetch()) {
            $sql  = "INSERT INTO ".$tableName;
            
            // // implode keys of $array...
            $sql .= " ".$campi."";
            
            //$escaped_values = array_map('mysql_real_escape_string', array_values($row));
            $sql .= " VALUES ('".implode("', '", $row)."') ";
            echo $sql;
            //echo "\n";
            $result = $conn->query($sql);
            //     $row['Interv'];
            //     # output query results
            //     echo $row['citta'];
            //     echo "\n";
        }
        
        # close the connection
        $db = null;
        
    }
    
}

?>
