var obj1 = [];
var rowData_old = 0;
var rowData = 0;

// JQGrid contabilità
function JQGRIDinitGrid(obj, GridName, width) {
	obj1 = obj
	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "Cognome",
			label : "Cognome",
			align : "left",
			width : 160

		}, {
			name : "Nome",
			label : "Nome",
			align : "left",
			width : 160

		}, {
			name : "IndPri",
			label : "Indice Priortà",
			align : "center",
			width : 170
		} ],
		width : width,
		iconSet : "fontAwesome",
		idPrefix : "g1_",
		caption : 'Cerca',
		data : obj,
		rownumbers : true,
		sortname : "invdate",
		sortorder : "desc",
		caption : "Lista Attesa --> ADULTI",
		onSelectRow : function(rowid) {
			rowData = $(this).jqGrid("getLocalRow", rowid);

			if (rowData_old == rowData) {// doppio Click

				showPaziente();

			} else {// Primo Click

				rowData_old = rowData;
			}
		}
	});
	$(GridName).jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});

}

function showPaziente() {
	if (rowData_old === 0) {
		toastr.warning('Non hai selezionato alcun Paziente!');
	} else {
		showComponent('userSection');
		showComponent('rowNominativo');
		hideComponent('gridSection');
		hideComponent('show');
		$('#nominativo_cognome').val(rowData_old.Cognome);
		$('#nominativo_nome').val(rowData_old.Nome);
		riempiElementi("userSection", "select", rowData_old, "2");
		riempiElementi("userSection", "input", rowData_old, "1");
		var eta = setDataEta('DataDiNascita', 'eta');
		AdultoMinore(eta, 'adultoMinore');
		// abilitaCampiCustom('userSection');
		$('#posrecuperoVal').val(
				query_laposrecupero[$('#Priorita1').val() - 1]['punti']);
		$('#gravitaVal').val(query_gravita[$('#Priorita2').val()]['punti']);
		$('#collaborativitaVal').val(
				query_collab[$('#Priorita3').val() - 1]['punti']);
		$('#socioambientaliVal').val(
				query_consoc[$('#Priorita4').val() - 1]['punti']);
		$('#urgenzaVal').val(query_urgenza[$('#Priorita5').val() - 1]['punti']);
		calcolaIndPri();
		$('#giorniAttesa').val(getGiorniInAttesa());
		$('#evPat').val(getGiorniEventoPatologico());

	}
}
function showAnagrafica(baseUrl) {
	if (rowData_old === 0) {
		toastr.warning('Non hai selezionato alcun Paziente!');
	} else {
		var url = baseUrl + '/anagrafica/anagrafica_detail.php?CodAnagr='
				+ rowData_old.CodAnagr;
		window.open(url, "_self")
	}
}
function showProgetto(baseUrl) {
	if (rowData_old === 0) {
		toastr.warning('Non hai selezionato alcun Paziente!');
	} else {
		var url = baseUrl + '/progetto/progetto_sezA.php?CodProj='
				+ rowData_old.CodProj + '&nome=' + rowData_old.Nome
				+ '&cognome=' + rowData_old.Cognome;
		window.open(url, "_self")
	}
}
function showListPazienti() {

	hideComponent('userSection');
	hideComponent('rowNominativo');
	showComponent('gridSection');
	showComponent('show');

}

function calcolaIndPri() {
	a = parseInt($('#urgenzaVal').val());
	b = parseInt($('#socioambientaliVal').val());
	c = parseInt($('#collaborativitaVal').val());
	d = parseInt($('#gravitaVal').val());
	e = parseInt($('#posrecuperoVal').val());
	$('#IndPri').val(a + b + c + d + e);

}

function getGiorniInAttesa() {
	var a = DATEUTILgetDateNow(true);
	var b = new Date(formatISODate($('#AttesaDal').val())).getTime();
	var diff = new moment.duration(a - b);
	return diff.asDays().toFixed(2);

}
function getGiorniEventoPatologico() {
	var a = DATEUTILgetDateNow(true);
	var b = new Date(formatISODate($('#DataEvPat').val())).getTime();
	var diff = new moment.duration(a - b);
	return diff.asDays().toFixed(2);

}