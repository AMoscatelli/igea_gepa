<?php
// Da includere per la gestione della sessione
include '../php/session.php';
include '../php/controller.php';
include '../php/config.inc.php';

$action = 0;

// if($_SERVER["REQUEST_METHOD"] == "POST") {
// $action = 1;
// }

if (isset($_GET['child'])) {
    $action = 1; // NPI LISTA ATTESA
} else {
    $action = 2; // ADULTI LISTA ATTESA
}
if (isset($_POST['source'])) {
    if ($_POST['source'] == 'child') {
        $action = 1; // NPI LISTA ATTESA
    } else {
        $action = 2; // ADULTI LISTA ATTESA
    }
}

?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - Lista d'attesa</title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">

<!-- Select2 CSS -->
<link href="../css/select2.css" rel="stylesheet">
<!-- JqGrid Css -->
<link rel="stylesheet" href="../css/jquery-ui.min.css">
<!-- https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css -->
<link rel="stylesheet" href="../css/ui.jqgrid.min.css">
<!-- https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css -->
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
</head>


<body>

	<!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
	<!--/Main Navigation-->

	<!--Main Layout-->
	<main>
	<div id="spinner" style='height: 100%; width: 100%'></div>
	<div class="container" id="completed" style="visibility: hidden;">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center" id="row-lenght">
					<!--Sezione bottoni shares-->
					<div class="social-counters ">
						<form action="" method="post">
							<button type="button" name="" value="" class="btn btn-default" id="back"    onclick="showListPazienti()"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button>
							<button type="submit" name="source" value="adult"
								class="btn btn-default" id="adult" onclick=""><i class="fa fa-male left">  Lista attesa
								Adulti</i></button>
							<button type="submit" name="source" value="child"
								class="btn btn-default" id="child" onclick=""><i class="fa fa-odnoklassniki left">  Lista attesa
								Bambini</i></button>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!--Section: Grid-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="gridSection">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<button type="button" name="" value="" class="btn btn-default mb-3" id="show"    onclick="showPaziente()"><i class="fa fa-eye left">  MOSTRA DETTAGLIO</i></button>
					<button type="button" name="" value="" class="btn btn-default mb-3" id="showAnag"    onclick="showAnagrafica(baseUrl);"><i class="fa fa-eye left">  MOSTRA ANAGRAFICA</i></button>
					<button type="button" name="" value="" class="btn btn-default mb-3" id="showAnag"    onclick="showProgetto(baseUrl);"><i class="fa fa-eye left">  MOSTRA PROGETTO</i></button>
					<table id="jqGrid"></table>
				</div>
			</div>
		</section>
		<!--/Section: Grid-->
		<div id="showAnag">
		<!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="rowNominativo" style="display:none" name="anagrafica">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						<div class="col-2">
						<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
    					<div class="col-3 align-middle">
    					<input type="text" class="form-control text-center mt-2 " id="nominativo_cognome" readonly>
						</div>
						
    					<div class="col-3 align-middle">
    					<input type="text" class="form-control text-center mt-2 " id="nominativo_nome" readonly>
						</div>
    					<div class="col-4">
    					<button id="dettaglio"  class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapsable" aria-expanded="false" aria-controls="collapseExample">
                            Mostra Dettagli <i class="fa fa-angle-down rotate-icon"></i>
                        </button>
    					</div>
				</div>
			</div>
		</section>

		<!--Section: -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="userSection">
			<div class="card card-cascade wider reverse">
				<div class="collapse card-body text-center" id="collapsable">
				    <!-- ROW 1 -->
					<div class="row">
						<div class="col">
							<!-- Codice progetto input -->
							<label for="CodAnagr">Codice Anagrafica</label> <input type="text"
								class="form-control text-center" data-toggle="tooltip"
								data-placement="top" id="CodAnagr" readonly>
						</div>
						<div class="col">
							<!-- Vecchia chiave input -->
							<label for="Centro">Centro</label> <input type="text"
								class="form-control text-center" id="Centro"  readonly>
						</div>
						<div class="col">
							<!-- Codice Anagrafica input -->
							<label for="CodiceStruttura">Codice Struttura</label> <input type="text"
								class="form-control text-center" id="CodiceStruttura" readonly>
						</div>
						<div class="col-2">
							<!-- Num Cart input -->
							<label for="ASL">ASL</label> <input type="text"
								class="form-control text-center" id="ASL"readonly>
						</div>
						<div class="col-1" id="statoDiv">
							<!-- Stato input -->
							<label for="Distretto">Distretto</label> <input
								class="form-control text-center"
								id="Distretto" name="Distretto"readonly>
						</div>
						<!-- Sesso select input -->
						<div class="col-1">
							<label for="Sesso">Sesso</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="Sesso" readonly>
								<option value="">Sel</option>
								<option value="1">M</option>
								<option value="2">F</option>
							</select>
						</div>
					</div>
					<!-- /ROW 1 -->
					
					<!-- INIZIO ROW 2 -->
					<div class="row">
						<div class="col-2">
							<!-- Istat Residenza input -->
							<label for="IstatResidenza">Istat Residenza</label> <input type="text"
								class="form-control text-center" 
								id="IstatResidenza" readonly>
						</div>
						<div class="col-5">
							<!-- Indirizzo redidenza -->
							<label for="IndirizzoResidVia">Indirizzo Resid.</label> <input type="text"
								class="form-control text-center" id="IndirizzoResidVia" readonly>
						</div>
						<div class="col-1">
							<!-- civico -->
							<label for="IndirizzoResidNVia">Numero</label> <input type="text"
								class="form-control text-center" id="IndirizzoResidNVia" readonly>
						</div>
						<div class="col-2">
							<!-- Luogo di Nascita -->
							<label for="LuogoDiNascita">Luogo di Nascita</label> <input type="text"
								class="form-control text-center" id="LuogoDiNascita" readonly>
						</div>
						<div class="col-2">
							<!-- Data Nascita input -->
							<label for="DataDiNascita">Data Di Nascita</label> <input type="text"
								class="form-control text-center dateFormat datepicker" id="DataDiNascita" readonly>
						</div>
					</div>
					<!-- /ROW 2 -->
					
					<!-- INIZIO ROW 3 -->
					<div class="row">
						<div class="col-3">
							<!-- CodiceFiscale -->
							<label for="CodiceFiscale">Codice Fiscale</label> <input type="text"
								class="form-control text-center" 
								id="CodiceFiscale" readonly>
						</div>
						<div class="col-2">
							<!-- Telefono -->
							<label for="Telefono">Telefono</label> <input type="text"
								class="form-control text-center" id="Telefono" readonly>
						</div>
						<div class="col-2">
							<!-- Cellulare -->
							<label for="Cellulare">Cellulare</label> <input type="text"
								class="form-control text-center" id="Cellulare" readonly>
						</div>
						<div class="col-2">
							<!-- ETA -->
							<label for="empty"></label> <input type="text"
								class="form-control text-center" style="visibility: hidden;" id="empty" readonly>
						</div>
						<div class="col-1">
							<!-- ETA -->
							<label for="eta">ETA</label> <input type="text"
								class="form-control text-center" style="background-color: #f3f39a" id="eta" readonly>
						</div>
						<div class="col-2">
							<!-- ADULTO/MINORE -->
							<label for="adultoMinore">Tipologia</label> <input type="text"
								class="form-control text-center" style="background-color: #f3f39a" id="adultoMinore" readonly>
						</div>
					</div>
					<!-- /ROW 3 -->
					
					<!-- INIZIO ROW 4 -->
					<div class="row">
						<div class="col-2">
							<!-- Data Evento Patologico -->
							<label for="DataEvPat">Data Evento Patologico</label> <input type="text"
								class="form-control text-center dateFormat" 
								id="DataEvPat" readonly>
						</div>
						<!-- Diagnosi select input -->
						<div class="col-4">
							<label for="Diagnosi">Diagnosi</label> 
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%" id="Diagnosi" readonly></select>
						</div>
						<div class="col-6">
							<!-- Cod Inviante -->
							<label for="PCodInviante">Codice Inviante</label> 
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%" id="PCodInviante" readonly></select>
						</div>
					</div>
					<!-- /ROW 4 -->
					
					<!-- INIZIO ROW 5 -->
					<div class="row">
						<div class="col-4">
							<!-- Regime Assistenziale -->
							<label for="PRegAssist">Regime assistenziale</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%" id="PRegAssist" readonly></select>
						</div>
						<div class="col-4">
							<!-- Tipo Intervento -->
							<label for="PIntervento">Tipo Intervento</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="PIntervento" readonly></select> 
						</div>
						<div class="col-4">
							<!-- Regime Assistenziale -->
							<label for="PIntervento1">Tipo intervento 1</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="PIntervento1" readonly></select>
						</div>
					</div>
					<!-- /ROW 5 -->
					<!-- INIZIO ROW 6 -->
					<div class="row">
						<div class="col-2">
						  <!-- Fatto Ciclo -->
							<div class="form-check checkbox-teal">
								<input type="checkbox" class="form-check-input" id="CicloAltri"readonly> <label
									class="form-check-label" for="CicloAltri">Fatto ciclo?</label>
							</div>
						</div>
						<div class="col-2">
							<!-- Data Intervento Precedente -->
							<label for="CicloDimil">Intervento Precedente</label> <input type="text"
								class="form-control text-center dateFormat" 
								id="CicloDimil" readonly>
						</div>
						<div class="col-4">
							<!-- Tipo Intervento -->
							<label for="Zona">Zona</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Zona" readonly></select> 
						</div>
						<div class="col-4">
							<!-- Tipo Intervento -->
							<label for="Valutazione">Valutazione</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width: 100%" id="Valutazione" readonly></select> 
						</div>
					</div>
					<!-- /ROW 6 -->
					<!-- INIZIO ROW 7 -->
					<div class="row">
    					<div class="col-2">
    						  <!-- Fatto Ciclo -->
    							<div class="form-check checkbox-teal">
    								<input type="checkbox" class="form-check-input" id="MinAltroCiclo" readonly> <label
    									class="form-check-label" for="MinAltroCiclo">Serve altro??</label>
    							</div>
						</div>
						<div class="col-3">
							<!-- CicloAltroDal -->
							<label for="CicloAltroDal">Dal</label> <input type="text"
								class="form-control text-center dateFormat" 
								id="CicloAltroDal" readonly>
						</div>
						<div class="col-2">
							<!-- Tipo Intervento -->
							<label for="asl">ASL</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="asl" readonly></select> 
						</div>
						<div class="col-3">
							<!-- Tipo Intervento -->
							<label for="tabvalutazione">Trattamenti Fatti</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="tabvalutazione" readonly></select> 
						</div>
					</div>
					<!-- /ROW 7 -->
					<!-- INIZIO ROW 8 -->
					<div class="row">
    					<div class="col-12">
							<!-- Data Intervento Precedente -->
							<label for="note">Note</label> 
							<textarea class="form-control" id="note" rows="7" readonly></textarea>
						</div>
					</div>
					<!-- /ROW 8  -->
				</div>
				<div class="card-body text-center" id="nonCollapsable">
				    <!-- ROW riepilogo -->
					<div class="row justify-content-md-center mb-3">
						<div class="col-2">
							<!-- Codice Attesa Dal input -->
							<label for="AttesaDal">In ripresa dal:</label> <input type="text"
								class="form-control text-center dateFormat datepicker" style="background-color: #f3f39a" id="AttesaDal" readonly>
						</div>
						<div class="col-2">
							<!-- Codice Inserito In Terapia input -->
							<label for="InserInTerapia">Inserito in terapia il:</label> <input type="text"
								class="form-control text-center dateFormat datepicker" style="background-color: #f3f39a" id="InserInTerapia" readonly>
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="giorniAttesa">Giorni in attesa:</label> <input type="text"
								class="form-control text-center" style="background-color: #f3f39a" id="giorniAttesa" readonly>
						</div>
						<div class="col-3">
							<!-- Codice Anagrafica input -->
							<label for="evPat">Giorni dall'evento Patologico</label> <input type="text"
								class="form-control text-center" style="background-color: #f3f39a" id="evPat" readonly>
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="IndPri">Indice Priorit&agrave</label> <input type="text"
								class="form-control text-center" style="background-color: #f3f39a" id="IndPri" readonly>
						</div>
					</div>
					<!-- /ROW riepilogo -->
					<hr>
				    <!-- ROW 1 -->
					<div class="row">
						<div class="col-10">
							<!-- Tipo Intervento -->
							<label for="posrecupero">Possibilit&agrave di recupero:</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="Priorita1" ></select> 
						</div>
						<div class="col-2 mb-2">
							<!-- Codice Anagrafica input -->
							<label for="posrecuperoVal">Valore</label> <input type="text"
								class="form-control text-center" id="posrecuperoVal" >
						</div>
					</div>
					<!-- /ROW 1 -->
					<hr>
					<!-- ROW 2 -->
					<div class="row">
						<div class="col-10">
							<!-- Tipo Intervento -->
							<label for="Priorita2">Gravit&agrave</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="Priorita2" ></select> 
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="gravitaVal">Valore</label> <input type="text"
								class="form-control text-center" id="gravitaVal" readonly>
						</div>
					</div>
					<!-- /ROW 2 -->
					<hr>
					<!-- ROW 2 bis -->
					<div class="row">
						<div class="col-10">
							<!-- Tipo Intervento -->
							<label for="Priorita3">Collaborativit&agrave</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="Priorita3" ></select> 
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="collaborativitaVal">Valore</label> <input type="text"
								class="form-control text-center" id="collaborativitaVal" readonly>
						</div>
					</div>
					<!-- /ROW 2 bis-->
					<hr>
					<!-- ROW 3 -->
					<div class="row">
						<div class="col-10">
							<!-- Tipo Intervento -->
							<label for="Priorita4">Condizioni socio-ambiantali</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="Priorita4" ></select> 
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="socioambientaliVal">Valore</label> <input type="text"
								class="form-control text-center" id="socioambientaliVal" readonly>
						</div>
					</div>
					<!-- /ROW 3 -->
					<hr>
					<!-- ROW 4 -->
					<div class="row">
						<div class="col-10">
							<!-- Tipo Intervento -->
							<label for="Priorita5">Tempistivit&agrave del trattamento</label>
							<select class="colorful-select dropdown-primary border_custom_select search" style="width:100%"id="Priorita5" ></select> 
						</div>
						<div class="col-2">
							<!-- Codice Anagrafica input -->
							<label for="urgenzaVal">Valore</label> <input type="text"
								class="form-control text-center" id="urgenzaVal" readonly>
						</div>
					</div>
					<!-- /ROW 4 -->
			</div>
		</section>
		<!--/Section: -->
		</div>
	</div>

	</main>
	<!--Main Layout-->

</body>
<!--  SCRIPTS  -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- JQGRDI core files -->
<script src="../js/jquery.jqgrid.min.js"></script>
<!--  https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js-->
<script type="text/javascript" src="js/lista_attesa.js"></script>
<!-- Menu Navigazione -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- Spinner -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- Select2 -->
<script type="text/javascript" src="../js/select2.js"></script>
<script>
//Inizializzo la NavBar
//initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>');
initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');

$(document).ready(function () {
	$('.collapsible-body').css({
		"display" : "none"
	});
	$('#listaAttesaMenuBody').css({
		"display" : "block"
	});
	hideComponent('spinner');
	hideComponent('userSection');
	hideComponent('rowNominativo');
	showComponent('completed');
	$('#urgenzaVal').val("0");
	$('#socioambientaliVal').val("0");
	$('#collaborativitaVal').val("0");
	$('#gravitaVal').val("0");
	$('#posrecuperoVal').val("0");
	//window.location.hash == '#anagrafica'
	$('#spinner').LoadingOverlay("hide");
		
});
$('#spinner').LoadingOverlay("show");
new WOW().init();
var baseUrl = '<?php echo $baseurl;?>';
//Aggiorno sulla NavBar il nome utente loggato
$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');

//var query_terapisti = <?php selectAll('terapista','Nominativo as terapista,ID as Id');?>;
var query_stato = <?php selectAll('stato','Stato,Id');?>;
var query_diagnosi = <?php selectAll('diagnosi','Diagnosi,Id');?>;
var query_inviante = <?php selectAll('inviante','Inviante as PCodInviante,Id');?>;
var query_regAssist = <?php selectAll('regimeassistenziale','RegAssist as PRegAssist,Id');?>;
var query_Intervento = <?php selectAll('intervento','Intervento,Id');?>;
//var query_Intervento1 = <?php //selectAll('intervento1','PIntervento1,Id');?>;
var query_Valutazione = <?php selectAll('tabvalutazione','descrizione as Valutazione,codice as Id');?>;
var query_Zona = <?php selectAll('Zona','Id,Zona');?>;
var query_ASL = <?php selectAll('asl','Id,ASL as asl');?>;
var query_tabVal = <?php selectAll('tabvalutazione','codice as Id,descrizione as tabvalutazione');?>;
var query_modInt = <?php selectAll('modalitaintervento','ModalitaIntervento as modInt,Id');?>;
var query_impRiab = <?php selectAll('impegnoriabilitativo','ImpRiab as impRiab,Id');?>;

var query_laposrecupero = <?php selectAll('laposrecupero','va as Id,posrecupero as Priorita1,punti');?>;
var query_gravita = <?php selectAll('lagravita','va as Id,gravita as Priorita2,punti');?>;
var query_collab = <?php selectAll('lacollabor','va as Id,collaborativita as Priorita3,punti');?>;
var query_consoc = <?php selectAll('lacondsocioamb','va as Id,socioambientali as Priorita4,punti');?>;
var query_urgenza = <?php selectAll('purgenza','va as Id,urgenza as Priorita5,punti');?>;





$('#collapsable').on('hidden.bs.collapse', function () {
	  $('#dettaglio').html('Mostra Dettagli   <i class="fa fa-angle-down rotate-icon"></i>');
});
$('#collapsable').on('shown.bs.collapse', function () {
	  $('#dettaglio').html('Nascondi Dettagli  <i class="fa fa-angle-up rotate-icon"></i>');
});


//::STATO
creaOption('userSection',query_stato);
creaOption('userSection',query_diagnosi);
creaOption('userSection',query_inviante);
creaOption('userSection',query_regAssist);
creaOption('userSection',query_Intervento);
//creaOption('userSection',query_Intervento1);
creaOption('userSection',query_Valutazione);
creaOption('userSection',query_Zona);
creaOption('userSection',query_ASL);
creaOption('userSection',query_tabVal);
creaOption('userSection',query_urgenza);
creaOption('userSection',query_consoc);
creaOption('userSection',query_collab);
creaOption('userSection',query_gravita);
creaOption('userSection',query_laposrecupero);

$('#Priorita5').on('change', function() {
	$('#urgenzaVal').val(query_urgenza[this.value-1]['punti']);
	calcolaIndPri();
})
$('#Priorita4').on('change', function() {
	$('#socioambientaliVal').val(query_consoc[this.value-1]['punti']);
	calcolaIndPri()
})
$('#Priorita3').on('change', function() {
	$('#collaborativitaVal').val(query_collab[this.value-1]['punti']);
	calcolaIndPri()
})
$('#Priorita2').on('change', function() {
	$('#gravitaVal').val(query_gravita[this.value-1]['punti']);
	calcolaIndPri()
})
$('#Priorita1').on('change', function() {
	$('#posrecuperoVal').val(query_laposrecupero[this.value-1]['punti']);
	calcolaIndPri()
})

//inizializzo le select
$('.search').select2();
$('.mdb-select').material_select();



if(<?php echo $action;?>===1){
	hideComponent('child');
    var obj = <?php echo controllerMethod('selectAll','lista_attesa_child_less_18_vista');?>;
	JQGRIDinitGrid(obj, '#jqGrid', $("#row-lenght").innerWidth()-80);
}else{
	hideComponent('adult');
	var obj = <?php echo controllerMethod('selectAll','lista_attesa_adult_over_18_vista');?>;
	JQGRIDinitGrid(obj, '#jqGrid', $("#row-lenght").innerWidth()-80);
} 


</script>

</html>



