<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

$action = 0;
$nome = "";
$codProg = "";
$cognome = "";
$cognome1 = "";
$nome1 = "";
$sezioneA = "";
$tab_sezA = "";
$jObj = "";
$codAnagr = "";


if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET['codAnag'])) {
        
        $nome = $_GET['nome'];
        $codAnagr = $_GET['codAnag'];
        $cognome = $_GET['cognome'];
        $action = 4;
        
    } else if (isset($_GET['CodProj'])) {
        $action = 2;
        $nome = '';
        $codProg = $_GET['CodProj'];
        $cognome = '';
    }
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $source = $_POST['buttonPr'];
    switch ($source) {
        case 'nuovo':
            $action = 1;
            $_SESSION['set_sectionA'] = "";
            break;
        case 'cerca':
            $action = 2;
            $nome = $_POST['cerca_nome'];
            $codProg = $_POST['cerca_codProg'];
            $cognome = $_POST['cerca_cognome'];
            break;
    }
}


?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - Cartella clinica</title>

<!-- Font Awesome -->
<!-- JQGrid CSS -->
<link rel="stylesheet"
	href="../css/jquery-ui.min.css">
<link rel="stylesheet"
	href="../css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- SELECT2 -->
<link href="../css/select2.css" rel="stylesheet" />




</head>


<body>

	<!--Main Navigation-->
	<header>

		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>

	<!-- Definisco la modal di richiesta salvataggio Progetto utilizzando la classe modal.js -->
	<div id="salvaProgetto"></div>


	<!-- Definisco la modal per la ricerca del progetto -->

	<div class="modal fade show" id="centralModalWarningDemo" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel"padding-right: 17px;">
		<form method="post">
			<div class="modal-dialog modal-notify modal-info" role="document">
				<!--Content-->
				<div class="modal-content">
					<!--Header-->
					<div class="modal-header">
						<p class="heading">Cerca Progetto</p>

						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true" class="white-text">�</span>
						</button>
					</div>

					<!--Body-->
					<div class="modal-body">
						<div class="row">
							<div class="col-4">
								<span class="align-middle"> <img src="../img/lente.png"
									class="img-fluid z-depth-1-half rounded-circle modal_custom_cercaProgetto"></img>
								</span>
							</div>
							<div class="col-8">
								<div class="row text-center mr-2 ml-2">
									<p class="card-text">
										<strong>Per cercare il progetto, inserisci il nome o il
											cognome del paziente o il codice progetto</strong>
									</p>
								</div>
								<div class="row mt-2  mr-1 ml-1">
									<div class="col">
										<label for="cerca_nome">Nome</label> <input type="text"
											class="form-control text-center" id="cerca_nome"
											name="cerca_nome"></input>
									</div>
									<div class="col">
										<label for="cerca_nome">Cognome</label> <input type="text"
											class="form-control text-center" id="cerca_cognome"
											name="cerca_cognome"></input>
									</div>
								</div>
								<div class="row mt-2 mr-2 ml-2">
									<label for="cerca_codProg">Codice Progetto</label> <input
										type="text" class="form-control text-center"
										id="cerca_codProg" name="cerca_codProg"></input>
								</div>

							</div>
						</div>
					</div>


					<!--Footer-->
					<div class="modal-footer justify-content-center">

						<button type="submit" name="buttonPr"
							class="btn btn-info waves-effect waves-light" value="cerca">Cerca</button>
						<button type="button" class="btn btn-outline-info waves-effect"
							data-dismiss="modal">Chiudi</button>

					</div>
				</div>
				<!--/.Content-->
			</div>
		</form>
	</div>

	<!-- Fine MODAL -->

	<div class="container">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">

				<div class="card-body text-center">

					<!--Sezione bottoni shares-->
					<div class="social-counters ">
						<form method="post">
							<!--Facebook-->
							<button type="button" class="btn btn-default" id="backBtn"
								onclick="gestisciIndietro();" style="display: none">
								<i class="fa fa-arrow-circle-left left"> INDIETRO</i>
							</button>
							<!-- Pag. click="window.location.href='../progetto/progetto.php'" style="display:none"><i class="fa fa-arrow-circle-left left">   INDIETRO</i></button> -->
							<button type="submit" name="source" value=""
								class="btn btn-default" onclick="" style="display: none">
								<i class="fa fa-print left"> STAMPA MESE</i>
							</button>
							<button type="button" class="btn btn-default" id="salvaBtn"
								onclick="salvaDati('sezioni','A','<?php echo $action ?>');"
								style="display: none">
								<i class="fa fa-save left"> SALVA</i>
							</button>
							<button type="button" class="btn btn-default" id="eliminaBtn"
								onclick="" style="display: none">
								<i class="fa fa-trash left"> ELIMINA</i>
							</button>
							<button type="button" class="btn btn-default" id="modificaBtn"
								onclick="abilitaCampi();" style="display: none">
								<i class="fa fa-edit left"> MODIFICA</i>
							</button>
							<button type="submit" name="buttonPr" value="nuovo"
								class="btn btn-default" id="nuovoPrBtn" onclick=""
								style="display: none">
								<i class="fa fa-list-alt left"> ELENCO PAZIENTI</i>
							</button>
							<button type="button" name="buttonPr" class="btn btn-default"
								id="cercaPrBtn" onclick="$('#centralModalWarningDemo').modal();"
								style="display: none">
								<i class="fa fa-search left"> CERCA PROGETTO</i>
							</button>
							<button type="button" name="buttonPr" value="mostra"
								class="btn btn-default" id="mostraPrBtn"
								onclick="mostraProgetti();" style="display: none">
								<i class="fa fa-eye left"> MOSTRA PROGETTO</i>
							</button>
							<span class="counter" id="counterPr" style="display: none">0</span>
							<button type="button" name="buttonPr" value="anagr"
								class="btn btn-default" id="mostraAnagr"
								onclick="showComponent('gridPazienti');hideComponent('gridProgetti');showComponent('mostraPrBtn');hideComponent('mostraAnagr');showComponent('counterPr');"
								style="display: none">
								<i class="fa fa-eye left"> MOSTRA ANAGRAFICA</i>
							</button>
						</form>
					</div>


				</div>
				<!--Post data-->
			</div>
		</section>

		<!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="rowNominativo" style="display: none">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						
						<div class="col-2">
							<label for="codProj">Cod. Progetto</label>
							<input type="text" class="form-control text-center"
								id="codProj" readonly>
						</div>
						
						<div class="col-5">
						<label for="nominativo_cognome">Cognome</label>
							<input type="text" class="form-control text-center"
								id="nominativo_cognome" readonly>
						</div>

						<div class="col-5">
						<label for="nominativo_nome">Nome</label>
							<input type="text" class="form-control text-center"
								id="nominativo_nome" readonly>
						</div>
					</div>
					<div class="row">
						
						<div class="col-2">
							<label for="stato">Stato</label>
							<input type="text" class="form-control text-center"
								id="stato" readonly>
						</div>
						
						<div class="col-5">
						<label for="data_inizio">Data Inizio</label>
							<input type="text" class="form-control text-center"
								id="data_inizio" readonly>
						</div>

						<div class="col-5">
						<label for="nominativo_nome">Data Fine</label>
							<input type="text" class="form-control text-center"
								id="data_fine" readonly>
						</div>
					</div>
				</div>
		
		</section>
		
		<!-- Definisco la riga dove verranno visualizzate le pagine della cartella clinica da Stampare -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="rowPagineStampa" style="display: none">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<div class="row">
						
						<div class="col-12">
							<label for="title"><h1 class="text-primary">Stampa Cartella Clinica</h1></label>
						</div>

					</div>
					<div class="row mb-2">

						<div class="col-6">
							<select
								class="mdb-select colorful-select dropdown-primary text-center border_custom_select"
								id="selectReport">
								<option value="0" selected>Seleziona il report da stampare...</option>
								<option value="1">Visita Iniziale</option>
								<option value="2">Riunione Equipe iniziale</option>
								<option value="3">Riunione Equipe finale</option>
							</select>
						</div>
						<div class="col-6">
							<div class="form-check">
								<input type="checkbox" class="form-check-input"
									id="selectAll"> <label class="form-check-label"
									for="selectAll">Seleziona tutti</label>
							</div>
						</div>
						<hr>
					</div>
					<div class="row">
						<div class="row">

							<div class="col-4">
								<div class="row">
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="1"> <span
											class="lever"></span> Pag. 01 - Copertina </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="2"> <span
											class="lever"></span> Pag. 02 - Diagnosi </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="3"> <span
											class="lever"></span> Pag. 03 - Dati sociosanitari / Anamnesi
										</label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="4"> <span
											class="lever"></span> Pag. 04 - APR / APP </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="5"> <span
											class="lever"></span> Pag. 05 - Trattamenti Riab Pregressi </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="6"> <span
											class="lever"></span> Pag. 06 - E.O. / Esami </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="7"> <span
											class="lever"></span> Pag. 07 - ICF </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="8"> <span
											class="lever"></span> Pag. 08 - Videat </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="9"> <span
											class="lever"></span> Pag. 09 - Valutazione Funzionale Iniz </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="10"> <span
											class="lever"></span> Pag. 10 - Progetto Equipe </label>
									</div>
								</div>
							</div>

							<div class="col-4">
								<div class="row">
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="11"> <span
											class="lever"></span> Pag. 11 - Aspettative </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="12"> <span
											class="lever"></span> Pag. 12 - Programma Terapeutico </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="13"> <span
											class="lever"></span> Pag. 13 - Indicazioni Varie </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="14"> <span
											class="lever"></span> Pag. 14 - Progetto Riabilitativo /
											Prognosi </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="15"> <span
											class="lever"></span> Pag. 15 - Accessi e Scale </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="16"> <span
											class="lever"></span> Pag. 16 - Valutazione Funzionale
											Verifica </label>
									</div>
									<div
										class="col-12 switch teal-switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="17"> <span
											class="lever"></span> Pag. 17 - Diaria </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="18"> <span
											class="lever"></span> Pag. 18 - Diaria Consulenze </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="19"> <span
											class="lever"></span> Pag. 19 - Progetto Equipe Intermedio </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="20"> <span
											class="lever"></span> Pag. 20 - Aggiornamento Prog.
											Terapeutico </label>
									</div>
								</div>
							</div>

							<div class="col-4">
								<div class="row">
									<div class="col-12 switch teal-switch mt-2 text-left text-left">
										<label><input type="checkbox" unchecked id="21"> <span
											class="lever"></span> Pag. 21 - Indicazioni Varie </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="22"> <span
											class="lever"></span> Pag. 22 - Aggiornamento Progetto
											Riabilitativo </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left text-left">
										<label><input type="checkbox" unchecked id="23"> <span
											class="lever"></span> Pag. 23 - Valutazione Funzionale
											Iniziale </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="24"> <span
											class="lever"></span> Pag. 24 - Valutazione Funzionale Finale
										</label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="25"> <span
											class="lever"></span> Pag. 25 - Dimissioni d' Equipe </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="26"> <span
											class="lever"></span> Pag. 26 - Codici ICF Dimissione </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="27"> <span
											class="lever"></span> Pag. 27 - Prestazioni e Scale di
											Chiusura </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="28"> <span
											class="lever"></span> Pag. 28 - Obbiettivi Raggiunti </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="29" class="pagina"> <span
											class="lever"></span> Pag. 29 - </label>
									</div>
									<div class="col-12 switch teal-switch mt-2 text-left">
										<label><input type="checkbox" unchecked id="30"> <span
											class="lever"></span> Pag. 30 - Indicazioni Finali </label>
									</div>
								</div>
							</div>
						</div>

					</div>
					<hr>
					<div class="row">

						<div class="col-12 text-center">

							<button class="btn blue-gradient" onclick="stampaCartellaClinica($('#codProj').val(),$('#codAnag').val())">Stampa Cartella Clinica</button>

						</div>
						
					</div>
					</section>
		
		
		<!--Section: GRID pazienti-->

		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="gridPazienti" style="display: none">
			<table id="grid1">
				<div id="jqGridPazientiPager"></div>
			</table>
		</section>

		<!--Section: GRID progetti-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="gridProgetti" style="display: none">
			<table id="grid2">
				<div id="jqGridProgettiPager"></div>
			</table>
		</section>



	</div>


</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!--  Custom JS -->
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- JQGrid JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="js/cartClinicJQGrid.js"></script>
<!-- PROGETTO CUSTOM JS -->
<script type="text/javascript" src="js/cartClinic.js"></script>
<!-- Modal JS -->
<script type="text/javascript" src="../jsCustom/modal.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- SELECT2 -->
<script src="../js/select2.js"></script>



<script>
		var query_stato = <?php selectAll('stato','Stato as stato,Id');?>;
		var path = '<?php echo $baseurl;?>';
		//inizializzo lo spinner
		$.LoadingOverlay("show");

		HTMLTOOLnumberOnlyClass("number-only");

		$('#nuovoPr').val("0");
		$('#vecchioPr').val("0");
		
        new WOW().init();
      //---------------------
		//inizializzo la navBar
		initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
		
        $(document).ready(function () {
        	$('.collapsible-body').css({
        		"display" : "none"
        	});
        	$('#stampeMenuBody').css({
        		"display" : "block"
        	});
        	var action = <?php echo $action;?>; 

        	HTMLTOOLvalidateTextNumber('sezioni');
        	
        	showComponent('cercaPrBtn');
            showComponent('nuovoPrBtn');
            
            if(action==1){
            	hideComponent('cercaPrBtn');
                hideComponent('nuovoPrBtn');
                }
            var sezione = [];
            
			
			$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_profile'];?>');
			 
			//inizializzo le select
			$('.search').select2();
			$('.mdb-select').material_select();
			
		    //inizializzo i tooltip
        	$('[data-toggle="tooltip"]').tooltip();

        	//---------------------
         $('#selectAll').change(function(){
             if($('#selectAll').prop("checked")==true){
				selectAll();
				$('#selectReport').val(0);
				 
             }else{
            	unSelectAll();
            	$('#selectReport').val('0');
             }
         });	
        	
        	
        $('#selectReport').change(function(){

    		switch ($('#selectReport').val()){

    		case "0": //Deseleziona tutto

    			$("#1").prop("checked", false);
    			$("#2").prop("checked", false);
    			$("#3").prop("checked", false);
    			$("#4").prop("checked", false);
    			$("#5").prop("checked", false);
    			$("#6").prop("checked", false);
    			$("#7").prop("checked", false);
    			$("#8").prop("checked", false);
    			$("#9").prop("checked", false);
    			$("#10").prop("checked", false);
    			$("#11").prop("checked", false);
    			$("#12").prop("checked", false);
    			$("#13").prop("checked", false);
    			$("#14").prop("checked", false);
    			$("#15").prop("checked", false);
    			$("#16").prop("checked", false);
    			$("#17").prop("checked", false);
    			$("#18").prop("checked", false);
    			$("#19").prop("checked", false);
    			$("#20").prop("checked", false);
    			$("#21").prop("checked", false);
    			$("#22").prop("checked", false);
    			$("#23").prop("checked", false);
    			$("#24").prop("checked", false);
    			$("#25").prop("checked", false);
    			$("#26").prop("checked", false);
    			$("#27").prop("checked", false);
    			$("#28").prop("checked", false);
    			$("#29").prop("checked", false);
    			$("#30").prop("checked", false);
    			
    			
    			break;

    		case "1": //Visita Iniziale
    			$("#1").prop("checked", true);
    			$("#2").prop("checked", true);
    			$("#3").prop("checked", true);
    			$("#4").prop("checked", true);
    			$("#5").prop("checked", true);
    			$("#6").prop("checked", true);
    			$("#7").prop("checked", false);
    			$("#8").prop("checked", false);
    			$("#9").prop("checked", true);
    			$("#10").prop("checked", false);
    			$("#11").prop("checked", true);
    			$("#12").prop("checked", false);
    			$("#13").prop("checked", false);
    			$("#14").prop("checked", false);
    			$("#15").prop("checked", false);
    			$("#16").prop("checked", false);
    			$("#17").prop("checked", false);
    			$("#18").prop("checked", false);
    			$("#19").prop("checked", false);
    			$("#20").prop("checked", false);
    			$("#21").prop("checked", false);
    			$("#22").prop("checked", false);
    			$("#23").prop("checked", false);
    			$("#24").prop("checked", false);
    			$("#25").prop("checked", false);
    			$("#26").prop("checked", false);
    			$("#27").prop("checked", false);
    			$("#28").prop("checked", false);
    			$("#29").prop("checked", false);
    			$("#30").prop("checked", false);
				
    			break;
    		case "2": //Riunione Equipe iniziale
    			$("#1").prop("checked", false);
    			$("#2").prop("checked", false);
    			$("#3").prop("checked", false);
    			$("#4").prop("checked", false);
    			$("#5").prop("checked", false);
    			$("#6").prop("checked", false);
    			$("#7").prop("checked", true);
    			$("#8").prop("checked", false);
    			$("#9").prop("checked", false);
    			$("#10").prop("checked", true);
    			$("#11").prop("checked", false);
    			$("#12").prop("checked", true);
    			$("#13").prop("checked", false);
    			$("#14").prop("checked", true);
    			$("#15").prop("checked", true);
    			$("#16").prop("checked", false);
    			$("#17").prop("checked", true);
    			$("#18").prop("checked", false);
    			$("#19").prop("checked", false);
    			$("#20").prop("checked", false);
    			$("#21").prop("checked", false);
    			$("#22").prop("checked", false);
    			$("#23").prop("checked", false);
    			$("#24").prop("checked", false);
    			$("#25").prop("checked", false);
    			$("#26").prop("checked", false);
    			$("#27").prop("checked", false);
    			$("#28").prop("checked", false);
    			$("#29").prop("checked", false);
    			$("#30").prop("checked", false);
     			break;

    		case "3": //Riunione Equipe finale
    			$("#1").prop("checked", false);
    			$("#2").prop("checked", false);
    			$("#3").prop("checked", false);
    			$("#4").prop("checked", false);
    			$("#5").prop("checked", false);
    			$("#6").prop("checked", false);
    			$("#7").prop("checked", false);
    			$("#8").prop("checked", false);
    			$("#9").prop("checked", false);
    			$("#10").prop("checked", false);
    			$("#11").prop("checked", false);
    			$("#12").prop("checked", false);
    			$("#13").prop("checked", false);
    			$("#14").prop("checked", false);
    			$("#15").prop("checked", false);
    			$("#16").prop("checked", false);
    			$("#17").prop("checked", true);
    			$("#18").prop("checked", false);
    			$("#19").prop("checked", false);
    			$("#20").prop("checked", false);
    			$("#21").prop("checked", false);
    			$("#22").prop("checked", false);
    			$("#23").prop("checked", false);
    			$("#24").prop("checked", true);
    			$("#25").prop("checked", true);
    			$("#26").prop("checked", true);
    			$("#27").prop("checked", false);
    			$("#28").prop("checked", true);
    			$("#29").prop("checked", false);
    			$("#30").prop("checked", false);
				break;

    		}
        	
    	});	
                    	
        	//chiudo lo spinner
        	$.LoadingOverlay("hide");  	

        });

</script>


<!-- Datepicker files: lingua-->
<script type="text/javascript" src="../js/it_IT.js"></script>

<!-- Funzioni della pagina -->
<script>



	var action = <?php echo $action;?>; 
	//nella ricerca della modal se compilo un campo l'altro automaticamente si svuota
	$('#cerca_nome').keyup(function() {
	      
		$('#cerca_codProg').val("");
	      
	 });

	$('#cerca_cognome').keyup(function() {
	      
    	$('#cerca_codProg').val("");
 
	});
	
	$('#cerca_codProg').keyup(function() {
	      
        $('#cerca_nome').val("");
        $('#cerca_cognome').val("");
 
	});
	// fine ricerca modal	

	if(action == 1){
    	
    	$.LoadingOverlay("show");

    	showComponent('backBtn');
    	showComponent('mostraPrBtn');
        hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
    	
		var obj = <?php if($action==1){echo selectAll('anagrafico','CodAnagr,Nome,Cognome,CodiceFiscale');}else{echo "0";}?>;

		var query_festivita1 = <?php selectAll('festivita','DataFestivita');?>;
    	
    	JQGRIDinitPazientiGrid(obj, '#grid1', $("#rowButton").innerWidth(),query_festivita1);

    	showComponent('gridPazienti');
    	
    	$.LoadingOverlay("hide");
    	
    } else if (action == 2) {

    	$.LoadingOverlay("show");

		var nome = "<?php echo $nome; ?>";

		var cognome = "<?php echo $cognome; ?>";

		var codProg1 = "<?php echo $codProg; ?>";

		if(nome==""&cognome==""&codProg1==""){

			toastr.error('Inserisci nome, cognome o numero progetto!!');

		} else {

		var query_festivita1 = <?php selectAll('festivita','DataFestivita');?>;

		var result = <?php if ($action == 2 & $nome != "" | $action == 2 & $codProg != "" | $action == 2 & $cognome != "") { cercaPazienteModal(); } else {    echo "0"; } ?>;

		if(result.length>0){
			
				if(nome!==""|cognome!==""){
        
        		JQGRIDinitPazientiGrid(result, '#grid1', $("#rowButton").innerWidth(),query_festivita1);
        
        		showComponent('gridPazienti');
        
        		showComponent('mostraPrBtn');
        
        		showComponent('backBtn');
        
        		hideComponent('nuovoPrBtn');
        
        		hideComponent('cercaPrBtn');
        		
        		} else if (codProg1!==""){
        			
        		JQGRIDinitProgettiGrid(result,'#grid2',$("#rowButton").innerWidth(),result[0].codAnagr,query_festivita1,result[0].NomeP,result[0].CognomeP);
        		
        		showComponent('gridProgetti');
        
        		}
		} else {
				// se la query mi restituisce un risultato vuoto mostro il messaggio
				toastr.error('Nessun progetto trovato!');
		}
		}
		$.LoadingOverlay("hide");
		
    } else if(action==3){
        hideComponent('cercaPrBtn');
        hideComponent('nuovoPrBtn');
    } else if(action==4){
    	hideComponent('gridPazienti');
		
		hideComponent('mostraPrBtn');
		
		showComponent('sezioni');

		showComponent('VF_section');
		
		showComponent('salvaBtn');
		
		showComponent('rowNominativo');
		
		$('#nominativo_nome').val("<?php echo $nome; ?>");
		
		$('#nominativo_cognome').val("<?php echo $cognome; ?>");
		
		$('#codAnag').val("<?php echo $codAnagr; ?>");
		
		hideComponent('counterPr');
		
		

		$('#nuovoPr').val("1");
    }

	function selectAll(){

		$("#1").prop("checked", true);
		$("#2").prop("checked", true);
		$("#3").prop("checked", true);
		$("#4").prop("checked", true);
		$("#5").prop("checked", true);
		$("#6").prop("checked", true);
		$("#7").prop("checked", true);
		$("#8").prop("checked", true);
		$("#9").prop("checked", true);
		$("#10").prop("checked", true);
		$("#11").prop("checked", true);
		$("#12").prop("checked", true);
		$("#13").prop("checked", true);
		$("#14").prop("checked", true);
		$("#15").prop("checked", true);
		$("#16").prop("checked", true);
		$("#17").prop("checked", true);
		$("#18").prop("checked", true);
		$("#19").prop("checked", true);
		$("#20").prop("checked", true);
		$("#21").prop("checked", true);
		$("#22").prop("checked", true);
		$("#23").prop("checked", true);
		$("#24").prop("checked", true);
		$("#25").prop("checked", true);
		$("#26").prop("checked", true);
		$("#27").prop("checked", true);
		$("#28").prop("checked", true);
		$("#29").prop("checked", true);
		$("#30").prop("checked", true);

	}


	function unSelectAll(){

		$("#1").prop("checked", false);
		$("#2").prop("checked", false);
		$("#3").prop("checked", false);
		$("#4").prop("checked", false);
		$("#5").prop("checked", false);
		$("#6").prop("checked", false);
		$("#7").prop("checked", false);
		$("#8").prop("checked", false);
		$("#9").prop("checked", false);
		$("#10").prop("checked", false);
		$("#11").prop("checked", false);
		$("#12").prop("checked", false);
		$("#13").prop("checked", false);
		$("#14").prop("checked", false);
		$("#15").prop("checked", false);
		$("#16").prop("checked", false);
		$("#17").prop("checked", false);
		$("#18").prop("checked", false);
		$("#19").prop("checked", false);
		$("#20").prop("checked", false);
		$("#21").prop("checked", false);
		$("#22").prop("checked", false);
		$("#23").prop("checked", false);
		$("#24").prop("checked", false);
		$("#25").prop("checked", false);
		$("#26").prop("checked", false);
		$("#27").prop("checked", false);
		$("#28").prop("checked", false);
		$("#29").prop("checked", false);
		$("#30").prop("checked", false);

	}
</script>


</html>