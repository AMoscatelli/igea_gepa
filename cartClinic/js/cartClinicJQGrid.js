var obj = [];
var project = [];
var rowData_old = 0;
var rowDataAnag;
var rowDataProj;

// JQGrid Progetti --> Pazienti
function JQGRIDinitPazientiGrid(obj, GridName, width, query, action) {
	$grid = $(GridName);

	var click_count = 0;
	if (action == "3") {

	} else {
		// definisco due valori per determinare se si tratta di un nuovo
		// progetto o la visualizzazione di uno vecchio
		$('#nuovoPr').val("0");
		$('#vecchioPr').val("0");
	}

	$(GridName).jqGrid('GridUnload');

	"use strict";
	$(GridName)
			.jqGrid(
					{
						colModel : [ {
							name : "CodAnagr",
							label : "Codice Anagrafica",
							align : "center",
							searchoptions : {
								// show search options
								sopt : [ "eq" ]
							// ge = greater or equal to, le = less or equal to,
							// eq = equal to
							}
						}, {
							name : "Nome",
							label : "Nome",
							align : "center"
						}, {
							name : "Cognome",
							label : "Cognome",
							align : "center"
						}, {
							name : "CodiceFiscale",
							label : "Codice Fiscale",
							align : "center"
						}, ],
						rowNum : 30,
						height : "auto",
						pager : "#jqGridPazientiPager",
						width : width,
						iconSet : "fontAwesome",
						caption : "Cerca:",
						idPrefix : "g1_",
						data : obj,
						rownumbers : true,
						sortname : "CodAnagr",
						sortorder : "asc",
						caption : "Selezionare il paziente desiderato",
						ignoreCase : true,
						onSelectRow : function(rowid) {

							rowDataAnag = $(this).jqGrid("getLocalRow", rowid);

							if (rowData_old == rowDataAnag) {

								if (Object.keys(project).length == 0) {
									toastr
											.warning('Oops! L\'utente selezionato non ha progetti');
								} else {
									hideComponent('gridPazienti');

									hideComponent('counterPr');

									hideComponent('mostraAnagr');

									showComponent('rowNominativo');

									showComponent('rowPagineStampa');

									$('#nominativo_nome').val(rowDataAnag.Nome);

									$('#nominativo_cognome').val(
											rowDataAnag.Cognome);

									$('#codProj').val(project['0']['codProg']);

									for (var i = 0; i < Object
											.keys(query_stato).length; i++) {

										if (project['0']['stato'] == query_stato[i]["Id"]) {
											$('#stato').val(
													query_stato[i]['stato']);
											break;
										}

									}

									$('#data_inizio').val(
											project['0']['dataInizio']);
									$('#data_fine').val(
											project['0']['dataFine']);

									rowData_old = 0;
								}

							} else {

								click_count = 0;
								$.LoadingOverlay("show");
								$('#nominativo_nome').val(rowDataAnag.Nome);

								$('#nominativo_cognome').val(
										rowDataAnag.Cognome);

								rowData_old = rowDataAnag;

								rowDataAnag = rowDataAnag.CodAnagr;

								showComponent('counterPr');

								$
										.ajax({
											type : 'post',
											url : '../php/controller.php',
											data : {
												'source' : "cercaProgetto",
												'whereValue' : rowDataAnag,
												'whereCond' : 'CodAnagr',
												'tableName' : 'progetto',
												'fieldName' : "p.CodProj as codProg,"
														+ "st.Stato as stato_view,"
														+ "p.Stato as stato,"
														+ "p.DataInizio as dataInizio,"
														+ "p.DataFine as dataFine,"
														+ "p.DataStato as dataStato,"
														+ "p.DurataProgetto as durataProgetto,"
														+ "p.Intervento as intervento,"
														+ "p.RegAssist as regAssist,"
														+ "p.ModInt as modInt,"
														+ "p.ImpRiab as impRiab,"
														+ "p.Medico as medico,"
														+ "p.Terapista1 as terapista1,"
														+ "p.Terapista2 as terapista2,"
														+ "p.Terapista3 as terapista3,"
														+ "p.AnamnesiPatologicaprossima as anamnesi,"
														+ "p.EsameObbiettivoSpecialistico as esameObbiettivoS,"
														+ "p.EsameObbiettivoGenerale as esameObbiettivoG,"
														+ "p.Invalidita as invalidita,"
														+ "p.InvaliditaPerc as percInvalidita,"
														+ "p.ModaTrasporto as modTrasp,"
														+ "p.altraModaTrasporto as altraModTrasp,"
														+ "p.Ricov12mesi as ricov12mesi,"
														+ "p.CodProvenienza as strProvenienza,"
														+ "p.altroprov as altroProv,"
														+ "p.CodCondiVita as condVita,"
														+ "p.CodInviante as inviante,"
														+ "p.altrocondivita as altroCondVita,"
														+ "p.Altroinv as altroInviante,"
														+ "p.PeriodoTrattamento as perTrattamento,"
														+ "p.ObbiettiviRiabilitativiRaggiunti as obRiabRagg,"
														+ "p.freqsett1 as freqSett,"
														+ "p.CodprotOrtAusili,"
														+ "p.ScalaCodificata as scalaCod,"
														+ "p.ScalaCodificata1 as scalaCod1,"
														+ "p.*"

											},
											success : function(response) {

												project = JSON.parse(response);

												var a = 0;
												for (i = 0; i < project.length; i++) {
													a = i + 1;

												}
												$.LoadingOverlay("hide");
												JQGRIDinitProgettiGrid(project,
														'#grid2', $(
																"#rowButton")
																.innerWidth(),
														rowDataAnag, query);
												setComponentText('counterPr', a);

											},
											error : function() {
												toastr
														.error('Opps! Si è verificato un problema!');
											}
										});

							}
						}

					});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridPazientiPager').css({
		"height" : "35px"
	});
}

function mostraProgetti() {

	var counter = $("#counterPr").text();

	if (counter != 0) {
		showComponent('gridProgetti');
		hideComponent('rowNominativo');
		hideComponent('rowPagineStampa');
		hideComponent('gridPazienti');
		showComponent('mostraAnagr');
		hideComponent('mostraPrBtn');
		hideComponent('counterPr');
		rowData_old = 0;
	} else {
		toastr
				.error('Non hai selezionato un paziente oppure non ci sono progetti associati al paziente!');
	}
}

function JQGRIDinitProgettiGrid(project, GridName, width, codAnagr, query,
		nome, cognome) {
	$grid = $(GridName);
	$(GridName).jqGrid('GridUnload');
	"use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "codProg",
			label : "Codice Progetto",
			align : "center",
			searchoptions : {
				// show search options
				sopt : [ "eq" ]
			// ge = greater or equal to, le = less or equal to,
			// eq = equal to
			}
		}, {
			name : "stato_view",
			label : "Stato",
			align : "center"
		}, {
			name : "dataInizio",
			label : "Data Inizio",
			align : "center"
		}, {
			name : "dataFine",
			label : "Data Fine",
			align : "center"
		}, {
			name : "dataStato",
			label : "Data Stato",
			align : "center"
		}, ],
		rowNum : 30,
		height : "auto",
		pager : "#jqGridProgettiPager",
		width : width,
		iconSet : "fontAwesome",
		caption : "Cerca:",
		idPrefix : "g1_",
		data : project,
		rownumbers : true,
		sortname : "CodProj",
		sortorder : "asc",
		caption : "Progetti associati",
		ignoreCase : true,
		onSelectRow : function(rowid) {

			rowDataProj = $(this).jqGrid("getLocalRow", rowid);

			if (rowData_old == rowDataProj) {

				hideComponent('gridPazienti');

				hideComponent('gridProgetti');

				hideComponent('counterPr');

				hideComponent('mostraAnagr');

				showComponent('rowNominativo');

				showComponent('rowPagineStampa');

				$('#codProj').val(rowDataProj.codProg);

				for (var i = 0; i < Object.keys(query_stato).length; i++) {

					if (rowDataProj.stato == query_stato[i]["Id"]) {
						$('#stato').val(query_stato[i]['stato']);
						break;
					}

				}

				$('#data_inizio').val(rowDataProj.dataInizio);
				$('#data_fine').val(rowDataProj.dataFine);

				rowData_old = 0;

			} else {

				click_count = 0;
				hideComponent('gridPazienti');

				hideComponent('gridProgetti');

				hideComponent('counterPr');

				hideComponent('mostraAnagr');

				showComponent('rowNominativo');

				showComponent('rowPagineStampa');

				$('#codProj').val(rowDataProj.codProg);

				for (var i = 0; i < Object.keys(query_stato).length; i++) {

					if (rowDataProj.stato == query_stato[i]["Id"]) {
						$('#stato').val(query_stato[i]['stato']);
						break;
					}

				}

				$('#data_inizio').val(rowDataProj.dataInizio);
				$('#data_fine').val(rowDataProj.dataFine);

				rowData_old = 0;

				rowData_old = rowDataProj;

			}
		}
	});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid("filterToolbar", {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridProgettiPager').css({
		"height" : "35px"
	});
}