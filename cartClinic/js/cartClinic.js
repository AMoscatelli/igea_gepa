var modalShow = false;

function gestisciIndietro() {

	var url = path + '/cartClinic/cartClinica.php';
	window.location = url;

	if ($('#gridPazienti').is(":visible")) {
		// quando la grid pazienti è visibile si torna alla card di creazione
		hideComponent('gridPazienti');
		hideComponent('mostraPrBtn');
		hideComponent('backBtn');
		hideComponent('salvaBtn');
		hideComponent('eliminaBtn');
		showComponent('nuovoPrBtn');
		showComponent('cercaPrBtn');
		hideComponent('counterPr');

	} else if ($('#gridProgetti').is(":visible")) {
		// quando la grid pazienti è visibile si torna alla card di creazione
		showComponent('gridPazienti');
		hideComponent('gridProgetti');
		showComponent('mostraPrBtn');
		hideComponent('mostraAnagr');
		// showComponent('counterPr');
		hideComponent('rowNominativo');
		hideComponent('counterPr');
		$('#grid1').jqGrid('resetSelection');

	} else if ($('#sezioni').is(":visible")) {
		var stato = $('#stato').prop('disabled');
		if (stato == false && !modalShow) {
			doModal("salvaProgetto", "Salva Progetto",
					"Vuoi salvare le modifiche?", "salva()", "Salva",
					"gestisciIndietro()", "Esci senza salvare", 2, "fa-save");
			modalShow = true;// $('#salvaProgetto').modal();
		} else {
			// rinizializzo la variabile che mostra la modal
			modalShow = false;

			// quando la grid sezioni è visibile si torna alla tabella pazienti
			// se è un nuovo progetto, altrimenti si torna alla grid pazienti
			if ($('#vecchioPr').val() == "1") {
				hideComponent('sezioni');
				hideComponent('VF_section');
				showComponent('gridProgetti');
				showComponent('rowNominativo');
				hideComponent('salvaBtn');
				hideComponent('modificaBtn');
				resetSezioni("A");
				$('#vecchioPr').val("0");
			} else {
				hideComponent('sezioni');
				hideComponent('VF_section');
				showComponent('gridPazienti');
				showComponent('mostraPrBtn');
				showComponent('counterPr');
				hideComponent('rowNominativo');
				hideComponent('salvaBtn');
				hideComponent('modificaBtn');
				hideComponent('counterPr');
				$('#grid1').jqGrid('resetSelection');
				resetCampi();
				// $('.mdb-select').material_select();
				$('#nuovoPr').val("0");

			}
		}
	}
}
function stampaCartellaClinica(codProj, codAnag) {
	console.log(rowDataAnag);
	console.log(rowDataProj);
	console.log(project[0]);

	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : "stampaCartClinic",
			'codAnag' : codAnag,
			'codProj' : codProj
		},
		success : function(response) {

			var a = 0;
			var url = path + "/cartClinic/stampe/AcartClinic_init.php?";
			// controllo gli switch che sono stati accesi e compongo la Url per
			// stampare
			// il report
			for (var i = 0; i <= 30; i++) {
				if ($('#' + i).prop('checked') == true) {
					url += i + "=" + this.value + "&";
				}
			}

			window.open(url, '_blank');

		},
		error : function() {
			toastr.error('Opps! Si è verificato un problema!');
		}
	});

}
