<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$nazionalita = 'nazione';
$lingua = "lingua";
$statoCivile = "statoCivile";
$tutelaLegale = "tutelaLegale";
$scolarita = "scolarita";
$professione = "professione";
$categoriaProtetta = "categoriaProtetta";
$praticaSport = "praticaSport";
$condizioniVita = "condizioniVita";
$destinazioneDimissione = "destinazioneDimissione";
$insegnanteSostegno = "insegnanteSostegno";
$abitudiniVita = "abitudiniVita";
$allergie = "allergie";
$menarca = "menarca";
$gravidanza = "gravidanza";
$menopuasa = "menopausa";
$anamnesiFamiliare = "anamnesiFamiliare";
$anamnesiFisiologica = "anamnesiFisiologica";

$giorni = "2";
// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */



$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LTR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','',15);
$pdf->Cell(170, $line, 'DATI SOCIOSANITARI',0,"","L");
$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"R",1);

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'NazionalitÓ:',0,0);
$pdf->Cell(60, $line, $nazionalita,0,0); //ToDo, riportare la nazionalita
$pdf->Cell(30, $line, 'Lingua:',0,0);
$pdf->Cell(60, $line, $lingua,"R",1); //ToDo, riportare la lingua

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Stato Civile:',0,0);
$pdf->Cell(130, $line, $statoCivile,"R",1); //ToDo, riportare la statoCivile

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Tutela Legale:',0,0);
$pdf->Cell(130, $line, $tutelaLegale,"R",1); //ToDo, riportare la statoCivile

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'ScolaritÓ:',0,0);
$pdf->Cell(130, $line, $scolarita,"R",1); //ToDo, riportare la ScolaritÓ

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Professione:',0,0);
$pdf->Cell(130, $line, $professione,"R",1); //ToDo, riportare la professione

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Categorie Protette:',0,0);
$pdf->Cell(130, $line, $categoriaProtetta,"R",1); //ToDo, riportare la $categoriaProtetta

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Pratica sport pre ricovero:',0,0);
$pdf->Cell(130, $line, $praticaSport,"R",1); //ToDo, riportare la $praticaSport

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Destinazoine dimissione:',0,0);
$pdf->Cell(130, $line, $destinazioneDimissione,"R",1); //ToDo, riportare la $destinazioneDimissione

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Condizioni di vita:',0,0);
$pdf->Cell(130, $line, $condizioniVita,"R",1); //ToDo, riportare la $condizioniVita

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Insegnante di sostegno:',0,0);
$pdf->Cell(130, $line, $insegnanteSostegno,"R",1); //ToDo, riportare la $insegnanteSostegno

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LBR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(50, $line, 'Abitudini di vita:',0,0);
$pdf->Cell(130, $line, $abitudiniVita,"",1); //ToDo, riportare la $abitudiniVita

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(50, $line, 'Allergie:',0,0);
$pdf->Cell(130, $line, $allergie,"",1); //ToDo, riportare la $allergie

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Menarca:',0,0);
$pdf->Cell(40, $line, $menarca,0,0); //ToDo, riportare la Menarca
$pdf->Cell(20, $line, 'Gravidanza:',0,0);
$pdf->Cell(40, $line, $gravidanza,0,0); //ToDo, riportare la Gravidanza
$pdf->Cell(20, $line, 'Menopausa:',0,0);
$pdf->Cell(40, $line, $menopuasa,"",1); //ToDo, riportare la Menopausa

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(180, $line, 'ANAMNESI FAMILIARE:',0,1);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(180, 30, $anamnesiFamiliare,"",1); //ToDo, riportare la $anamnesiFamiliare

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(180, $line, 'ANAMNESI FISIOLOGICA:',0,1);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(180, 30, $anamnesiFisiologica,"",1); //ToDo, riportare la $anamnesiFisiologica










//$pdf->Output();
?>