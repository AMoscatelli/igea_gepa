<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$modalitaRichiesta = 'modalitaRichiesta';
$modalitaIntervento = "modalita intervento";
$tipologiaAssistenziale = "tipologia assistenziale";
$patologiaOggetto = 'patoogia oggetto';
$dataInizioPatologiaOggetto = "00/00/00";
$codiceInizioPatologiaOggetto = "123456";
$diagnosiInizioPatologiaOggetto = "diagnosiInizioPatologiaOggetto";
$giorni = "2";
// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota*/

$pdf->Cell(10, $line, '',"",0);
$pdf->SetFont('Times','',20);
$pdf->Cell(170, $line, 'INTERVENTO RICHIESTO',0,"","C");
$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',0,1); // riga vuota*/
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota*/


$pdf->Cell(190, $halfLine, '',"LTR",1); // riga vuota


$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','U',10);
$pdf->Cell(30, $line, 'MODALITA DI RICHIESTA:',0,0);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, "" ,"R",1); //ToDo, riportare il cognome


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(180, $line, $modalitaRichiesta ,"R",1); //ToDo, riportare il $modalitaRichiesta

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','U',10);
$pdf->Cell(30, $line, 'MODALITA D\'INTERVENTO:',0,0);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, "" ,"R",1); //ToDo, riportare il cognome


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(180, $line, $modalitaIntervento ,"R",1); //ToDo, riportare il $modalita intervento

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','U',10);
$pdf->Cell(30, $line, 'TIPOLOGIA ASSISTENZIALE:',0,0);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, "" ,"R",1); //ToDo, riportare il cognome


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(180, $line, $tipologiaAssistenziale ,"R",1); //ToDo, riportare il $tipologiaAssistenziale

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','U',10);
$pdf->Cell(30, $line, 'PATOLOGIA OGGETTO:',0,0);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, "" ,"R",1); //ToDo, riportare il cognome

$pdf->Cell(190, $line, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(35, $line, "Data inizio Patologia:" ,"",0); 
$pdf->Cell(15, $line, $dataInizioPatologiaOggetto ,"",0); //ToDo, riportare il $dataInizioPatologiaOggetto
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(83, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(35, $line, "Data inizio Patologia:" ,"",0);
$pdf->Cell(15, $line, $dataInizioPatologiaOggetto ,"",0); //ToDo, riportare il $dataInizioPatologiaOggetto
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(83, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(35, $line, "Data inizio Patologia:" ,"",0);
$pdf->Cell(15, $line, $dataInizioPatologiaOggetto ,"",0); //ToDo, riportare il $dataInizioPatologiaOggetto
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(83, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $line, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->SetFont('Times','U',10);
$pdf->Cell(30, $line, 'ALTRE PATOLOGIE:',0,0);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, "" ,"R",1); //ToDo, riportare il cognome

$pdf->Cell(190, $line, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(133, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(133, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(133, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(133, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(12, $line, "Codice:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(20, $line, $codiceInizioPatologiaOggetto ,"",0); //ToDo, riportare il $codiceInizioPatologiaOggetto
$pdf->Cell(15, $line, "Diagnosi:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(133, $line, $diagnosiInizioPatologiaOggetto ,"R",1); //ToDo, riportare il $diagnosiInizioPatologiaOggetto

$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LBR",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90, $line, "Nr. giorni trascorsi dall'evento acuto:" ,"",0); //ToDo, riportare il $patologiaOggetto
$pdf->Cell(100, $line, $giorni ,"",1); //ToDo, riportare il $codiceInizioPatologiaOggetto








//$pdf->Output();
?>