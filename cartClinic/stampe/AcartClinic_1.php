<?php

$line = 5;
$halfLine = 2;

$cart_numero = '123456789';
$nome_centro = 'A.I.R.R.I';
$cod_struttura = '030';
$asl_competenza = '10';
$codice_progetto = '20523';
$data_inizio = '01/01/2018';
$data_fine = '31/01/2018';
$cognome = "cognome";
$nome = "nome";
$sesso = "M";
$luogoNascita = "luogonascita";
$dataNascita = "datanascita";
$codFiscale = "XXXXXXXXXXXXX";
$codRegionale = "XXXXXXXXXXXXX";
$residenza = "residenza";
$municipio = "Municipio";
$indirizzo = "indirizzo";
$ASLDomicilio = "ASLDomicilio";
$telefono = "telefono";
$cellulare = "cellulare";
$invalidita = "Invalidita";
$perc_invalidita = "perc_invalidita";
$ausili = "SI";
$codOrt = "12.32.20";
$codOrt1 = "12.32.20";
$medicoCurante = "medicoCurane";
$telMedicoCurante = "0666181286";
$referenteFamiliare = "referente Famil";
$telReferenteFamiliare = "referente Famil";
$email = "noreply@igea.online.it";

$numero = '2';
$data = '01/01/2019';

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

$pdf->SetLineWidth(1);
$pdf->Cell(10, $line, '',"LT",0);
$pdf->Cell(170, $line, '',"T",0);
$pdf->Cell(10, $line, '',"TR",1);
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(170, $line, 'CARTELLA  C L I N I C A  RIABILITATIVA ESTENSIVA E DI MANTENIMENTO',0,0,"C");
$pdf->Cell(10, $line, '',"R",1);
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(170, $line, 'ESTENSIVA E DI MANTENIMENTO',0,0,"C");
$pdf->Cell(10, $line, '',"R",1);
$pdf->Cell(10, $line, '',"LB",0);
$pdf->Cell(170, $line, '',"B",0);
$pdf->Cell(10, $line, '',"BR",1);

$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

$pdf->Cell(90, $line, '',0,0);
$pdf->Cell(60, $line, 'CARTELLA NR:',0,0);
$pdf->Cell(40, $line, $cart_numero ,0,1,"L"); //ToDo, riportare il numero della Cartella

$pdf->Cell(190, $line, '',0,1); // riga vuota
//$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

$pdf->Cell(90, $line, '',0,0);
$pdf->Cell(60, $line, 'DENOMINAZIONE DEL CENTRO:',0,0);
$pdf->Cell(40, $line, $nome_centro ,0,1); //ToDo, riportare il nome del Centro

$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

$pdf->Cell(90, $line, '',0,0);
$pdf->Cell(60, $line, 'CODICE STRUTTURA',0,0);
$pdf->Cell(40, $line, $cod_struttura ,0,1); //ToDo, riportare il codice struttura

$pdf->Cell(190, $halfLine, '',0,1); // riga vuota


$pdf->Cell(90, $line, '',0,0);
$pdf->Cell(60, $line, 'ASL DI COMPETENZA',0,0);
$pdf->Cell(40, $line, $asl_competenza ,0,1); //ToDo, riportare l'ASL di riferiemnto

$pdf->Cell(190, $halfLine, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

$pdf->Cell(10, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(30, $line, $codice_progetto ,0,0); //ToDo, riportare il codProj

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'Data Inizio:',0,0);
$pdf->Cell(30, $line, $data_inizio ,0,0); //ToDo, riportare data inizio

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'Data Fine:',0,0);
$pdf->Cell(30, $line, $data_fine ,0,1); //ToDo, riportare data fine

$pdf->Cell(190, $line, '',0,1); // riga vuota
//$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

//$pdf->Cell(190, $line, '',0,1); // riga vuota
//$pdf->Cell(190, $halfLine, '',0,1); // riga vuota

$pdf->Cell(190, $halfLine, '',"LTR",1); // riga vuota
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'COGNOME:',0,0);
$pdf->Cell(30, $line, $cognome ,0,0); //ToDo, riportare il cognome

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'NOME:',0,0);
$pdf->Cell(30, $line, $nome ,0,0); //ToDo, riportare nome

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'SESSO:',0,0);
$pdf->Cell(30, $line, $sesso ,"R",1); //ToDo, riportare sesso

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(50, $line, 'Luogo e Data di nascita:',0,0);
$pdf->Cell(130, $line, $luogoNascita." - ".$dataNascita ,"R",1); //ToDo, riportare il luogo e data nascita

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'Codice Fiscale:',0,0);
$pdf->Cell(50, $line, $codFiscale ,0,0); //ToDo, riportare il cod. fiscale

$pdf->Cell(30, $line, 'Codice Regionale:',0,0);
$pdf->Cell(70, $line, $codRegionale ,"R",1); //ToDo, riportare il luogo e data nascita

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'Residenza:',0,0);
$pdf->Cell(50, $line, $residenza ,0,0); //ToDo, riportare il resdenza

$pdf->Cell(30, $line, 'Municipio:',0,0);
$pdf->Cell(70, $line, $municipio ,"R",1); //ToDo, riportare il municipio

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota


$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'Indirizzo:',0,0);
$pdf->Cell(150, $line, $indirizzo ,"R",1); //ToDo, riportare l'indirizzo


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->SetFont('Times','B',10);
$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(180, $line, 'Domicilio:' ,"R",1); //email
$pdf->SetFont('Times','',10);

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'ASL:',0,0);
$pdf->Cell(30, $line, $ASLDomicilio ,0,0); //ToDo, riportare l'ASl di domicilio

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'Telefono:',0,0);
$pdf->Cell(30, $line, $telefono ,0,0); //ToDo, riportare telefono

//$pdf->Cell(10, $line, '',1,0);
$pdf->Cell(30, $line, 'Cellulare:',0,0);
$pdf->Cell(30, $line, $cellulare ,"R",1); //ToDo, riportare cellulcare

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota


$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(34, $line, 'Invaliditā Civile:',0,0);
$pdf->Cell(146, $line, $invalidita ,"R",1); //ToDo, riportare invalidita


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota
    

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(35, $line, 'Percentuale invaliditā: ',0,0);
$pdf->Cell(145, $line, $perc_invalidita." %" ,"R",1); //ToDo, riportare % invalidita


$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'Ausili:',0,0);
$pdf->Cell(50, $line, $ausili ,0,0); //ToDo, riportare il resdenza

$pdf->Cell(5, $line, '',0,0);
$pdf->Cell(45, $line, $codOrt ,"",0); //ToDo, riportare il cod ort

$pdf->Cell(5, $line, '',0,0);
$pdf->Cell(45, $line, $codOrt1 ,"R",1); //ToDo, riportare il cod ort 1

$pdf->Cell(190, $line, '',"LR",1); // riga vuota
$pdf->Cell(190, $halfLine, "","LR",1); // riga vuota

$pdf->Cell(10, $line, '',"L",0);
$pdf->Cell(30, $line, 'E-mail:',0,0);
$pdf->Cell(150, $line, $email ,"R",1); //ToDo, riportare il email


$pdf->Cell(190, $line, '',"LBR",1); // riga vuota
$pdf->Cell(190, $line, '',0,1); // riga vuota

$pdf->Cell(10, $line, '',0,0);
$pdf->Cell(30, $line, 'Medico curante:',0,0);
$pdf->Cell(50, $line, $medicoCurante ,0,0); //ToDo, riportare il medico curante

$pdf->Cell(15, $line, 'Telefono:',0,0);
$pdf->Cell(80, $line, $telMedicoCurante,0,1); //ToDo, riportare il tel medico curante

$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota


$pdf->Cell(10, $line, '',0,0);
$pdf->Cell(30, $line, 'Medico curante:',0,0);
$pdf->Cell(50, $line, $referenteFamiliare ,0,0); //ToDo, riportare il referetne

$pdf->Cell(15, $line, 'Telefono:',0,0);
$pdf->Cell(80, $line, $telReferenteFamiliare ,0,1); //ToDo, riportare il tel mrefeternet

$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',0,1); // riga vuota


$pdf->Cell(10, $line, '',0,0);
$pdf->Cell(30, $line, 'Medico curante:',0,0);
$pdf->Cell(50, $line, $referenteFamiliare ,0,0); //ToDo, riportare il referetne

$pdf->Cell(15, $line, 'Telefono:',0,0);
$pdf->Cell(80, $line, $telReferenteFamiliare ,0,1); //ToDo, riportare il tel mrefeternet








//$pdf->Output();
?>