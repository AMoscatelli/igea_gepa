<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$dataFinale = '01/01/2019';
$dimissioniEquipe = 'Carla';
$nome = 'Carla';
$cognome = "Cora";
$dataNascita = '01/01/2019';
$luogoNascita = 'Roma';
$dataValutazioneFunzionale = '01/01/2019';
$ICF = 'prova';
$ICF_S = 'prova';
$ICFDescrizione = 'prova';
$ICFDescrizione_S = 'prova';
$ICFQualF1 = 'prova';
$ICFQualF1_S = 'prova';
$ICFQualF2_S = 'prova';
$ICFQualF3_S = 'prova';
$QualDeso = 'prova';
$S1QualDes = 'prova';
$S2QualDes = 'prova';
$S3QualDes = 'prova';
$ICF_D = 'prova';
$ICFDescrizione_D = 'prova';
$ICFQualF1_D = 'prova';
$D1QualDes_D = 'prova';
$ICFQualF2_D = 'prova';
$D2QualDes_D = 'prova';
$ICF_E = 'prova';
$ICFDescrizione_E = 'prova';
$ICFQualF1_E = 'prova';
$QualDeso_E = 'prova';
$Residente_A = 'Roma';
$indirizzo = 'Via della Mortella, 13';
$terRiab = 'terriab';
$diagCli = '';
$areaCog = '';
$areaAff = '';
$areaAppr = '';
$areaMot = '';
$areaLing = '';
$dataConclusione = '01/01/2019';

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Scheda di dimissione',0,"","C");
$pdf->Cell(10, $line, '',"",1);


$pdf->SetFont('Times','',10);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Cognome e Nome:',0,"","L");
$pdf->Cell(85, $line, $cognome." ".$nome,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Luogo e Data Nasc.:',0,"","L");
$pdf->Cell(85, $line, $luogoNascita." ".$dataNascita,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Residente a:',0,"","L");
$pdf->Cell(85, $line, $Residente_A,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Indirizzo:',0,"","L");
$pdf->Cell(85, $line, $indirizzo,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(5, $line,'dal ',0,"","C");
$pdf->Cell(30, $line,$dataInizio,0,"","C");
$pdf->Cell(5, $line,'al ',0,"","C");
$pdf->Cell(30, $line,$dataFine,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Diagnosi di ammissione',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$terRiab,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Quadro funzionale iniziale',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$diagCli,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Trattamento effettuato',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$areaCog,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Obiettivi raggiunti',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$areaAff,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Scale di valutazione utilizzate',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$areaAppr,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Indicazioni e suggerimenti',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$areaMot,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Programmi futuri',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$areaLing,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Data:',0,"","L");
$pdf->Cell(20, $line, $dataConclusione,0,"","L");
$pdf->Cell(80, $line, '',0,"","L");
$pdf->Cell(50, $line, 'Il Medico Responsabile',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, '',0,"","L");
$pdf->Cell(20, $line, '',0,"","L");
$pdf->Cell(80, $line, '',0,"","L");
$pdf->Cell(50, $line, '_________________________',0,"","C");
$pdf->Cell(10, $line, '',"",1);


?>