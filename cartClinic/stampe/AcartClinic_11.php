<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$aspettativeFamiliari = '01/01/2019';
$aspettativeAssistito = '01/01/2019';



// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Aspettative Assistito',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$aspettativeAssistito,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(60, $line, 'Firma:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Aspettative Familiare',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$aspettativeFamiliari,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(60, $line, 'Firma:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);



//$pdf->Output();
?>