<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$data = '01/01/2019';
$nome = 'Carla';
$cognome = "Cora";
$area = '';
$medico_terapista = '';
$qualifica = '';
$valutazioneFunzinale = '';
$obiettivi = '';
$tempiPrevisti = '';
$interventiProposti = '';


// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'VALUTAZIONE  FUNZIONALE',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(170,$line,"(di verifica)",'0',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->SetFont('Times','',10);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//data e nome paziente:
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Data:',0,"","L");
$pdf->cell(40,$line,$data,'0',"","L");
$pdf->Cell(30, $line, 'Paziente:',0,"","L");
$pdf->cell(50,$line,$cognome." ".$nome,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Area intervento Operatore e qualifica:
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Area d\'intervento:',0,"","L");
$pdf->cell(35,$line,$area,'0',"","L");
$pdf->Cell(20, $line, 'Operatore:',0,"","L");
$pdf->cell(40,$line,$medico_terapista,'0',"","L");
$pdf->Cell(20, $line, 'Qualifica:',0,"","L");
$pdf->cell(35,$line,$qualifica,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//valutazione funzionale
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Valutazione funzionale:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$valutazioneFunzinale,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Proposte operative:   OBBIETTIVI
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Proposte operative: OBBIETTIVI:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$obiettivi,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Tempi Previsti:
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Tempi Previsti:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$tempiPrevisti,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Interventi Proposti:
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Interventi Proposti:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$interventiProposti,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetXY(10, 260);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Firma:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);



//$pdf->Output();
?>