<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$data = '01/01/2019';
$periodo = '01/01/2019';
$frequenza = "";
$GG_Presa_carico = "";
$operatoreResponsabile = "";
$procedure = 'Procedure di tera�pisa';

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'P R O G R A M M A   T E R A P E U T I C O',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Data:',1,"","L");
$pdf->Cell(20, $line, 'Periodo:',1,"","L");
$pdf->Cell(20, $line, 'Frequenza:',1,"","L");
$pdf->Cell(30, $line, 'GG Presa in carico:',1,"","L");
$pdf->Cell(80, $line, 'Operatore responsabile:',1,"","C");
$pdf->Cell(10, $line, '',"",1);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, $data,1,"","L");
$pdf->Cell(20, $line, $periodo,1,"","L");
$pdf->Cell(20, $line, $frequenza,1,"","L");
$pdf->Cell(30, $line, $GG_Presa_carico,1,"","L");
$pdf->Cell(80, $line, $operatoreResponsabile,1,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Procedure',"B","","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$procedure,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetXY(10, 200);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Il sottoscritto dichiara :',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Di aver preso visione della proposta del programma riabilitativo su cui concorda.',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Firma del Paziente',0,"","L");
$pdf->Cell(60, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Firma del Medico Referente',0,"","L");
$pdf->Cell(60, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);
?>