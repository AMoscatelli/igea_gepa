<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$dataFinale = '01/01/2019';
$dimissioniEquipe = 'Carla';
$cognome = "Cora";



// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'DIMISSIONI D\'EQUIPE',0,"","C");
$pdf->Cell(10, $line, '',"",1);


$pdf->SetFont('Times','',10);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Dimissioni d\'Equipe:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $line, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$dimissioniEquipe,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Gli Operatori Responsabili:',0,"","L");
$pdf->Cell(85, $line, 'Il Medico Responsabile:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, '___________________________',0,"","L");
$pdf->Cell(85, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, '___________________________',0,"","L");
$pdf->Cell(85, $line, '',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, '___________________________',0,"","L");
$pdf->Cell(85, $line, '',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Data:',0,"","L");
$pdf->Cell(85, $line, 'il paziente:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, $dataFinale,0,"","L");
$pdf->Cell(85, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);

//$pdf->Output();
?>