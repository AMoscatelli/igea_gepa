<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$Proj12mesi = 'proj12mesi';

$anamnesiProssima = "lingua";
$provenienza = "proveniencza";
$trattamento = "tutelaLegale";
$obiettiviRaggiunti = "obiettivi raggiunti";


$giorni = "2";
// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Trattamenti Riabilitativi negli ultimi 12 mesi',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(170,$line,$Proj12mesi,'0',"","C");
$pdf->Cell(10, $line, '',"",1);


$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(60, $line, 'Provenienza (eventuale trattamento):',0,"","L");
$pdf->cell(110,$line,$provenienza,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(60, $line, 'Periodo di trattamento:',0,"","L");
$pdf->cell(110,$line,$trattamento,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(190, $line, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Obiettivi riabilitativi raggiunti:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$obiettiviRaggiunti,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->SetXY(10, 200);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Il sottoscritto dichiara :',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'di non essere attualmente in trattamento presso altre strutture riabilitative accreditate.',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20, $line, 'Data',0,"","L");
$pdf->Cell(60, $line, '___________________________',0,"","L");
$pdf->Cell(40, $line, 'Firma del Paziente',0,"","L");
$pdf->Cell(50, $line, '___________________________',0,"","L");
$pdf->Cell(10, $line, '',"",1);


//$pdf->Output();
?>