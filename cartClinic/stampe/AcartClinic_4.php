<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$anamnesiRemota = 'fsffdfg dfg g dfg dfg \n df gdf gdf gdf gdf gdf gd fgdf gdf g \n  h  hk h h kh jk hk  k h jkh kh h kh jkh kh kh h khk h khk h k h khk hk hk hk h k h kh kh kh kh kh k h kh kh khkgdf ger gerter fger ger ger ger ge rg dg g dg dg';
$anamnesiProssima = "lingua";
$provenienza = "statoCivile";
$altraProvenienza = "tutelaLegale";


$giorni = "2";
// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'ANAMNESI PATOLOGICA REMOTA',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$anamnesiRemota,'',"L",0);
$pdf->Cell(10, $line, '',"",1);


$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'ANAMNESI PATOLOGICA PROSSIMA',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(170,$line,$anamnesiProssima,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'PROVENIENZA',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, 10, '',"",0);
$pdf->Cell(170, 10,$provenienza,0,"","L");
$pdf->Cell(10, 10, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(80, $line, 'Altra provenienza',0,"","L");
$pdf->Cell(100, $line, $altraProvenienza,"",1);


$pdf->Cell(190, $line, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


//$pdf->Output();
?>