<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$nome='Nome';
$cognome='COgnome';
$intervento = 'intervento';
$area = 'area';
$obbiettivo = 'obbiettivo obbiettivo obbiettivo obbiettivo obbiettivoobbiettivoobbiettivo obbiettivo obbiettivo obbiettivo obbiettivoobbiettivoobbiettivo ';
$operatore = 'operatore';
$risultatoAtteso = 'risultato atteso';


$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO


$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'PROGETTO  RIABILITATIVO: Obiettivi Raggiunti',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(20,$line,"PAZIENTE",'0',"","C");
$pdf->cell(150,$line,$cognome." ".$nome,'0',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(20,$line,"Disabilità",'0',"","C");
$pdf->cell(150,$line,$intervento,'0',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(40,$line,"AREA",'LTR',"","C");
$pdf->cell(40,$line,"OBBIETTIVO",'LTR',"","C");
$pdf->cell(40,$line,"OPERATORE",'LTR',"","C");
$pdf->cell(40,$line,"Raggiungibilità'",'LTR',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->cell(40,$line,"",'LBR',"","C");
$pdf->cell(40,$line,"",'LBR',"","C");
$pdf->cell(40,$line,"",'LBR',"","C");
$pdf->cell(40,$line,"OBBIETTIVO",'LBR',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(40,$line,$area,'0',"","C");
    $pdf->cell(40,$line,$operatore,'0',"","C");
    $pdf->cell(40,$line,$risultatoAtteso,'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    $x=$pdf->GetX();
    $y=$pdf->GetY();
    $pdf->SetXY($x+130,$y-5);
    
    $pdf->MultiCell(40,$line,$obbiettivo,'',"L",0);
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(170,$line,"",'B',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
}


?>