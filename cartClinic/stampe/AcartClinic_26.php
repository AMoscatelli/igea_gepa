<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$dataFinale = '01/01/2019';
$dimissioniEquipe = 'Carla';
$nome = 'Carla';
$cognome = "Cora";
$dataNascita = '01/01/2019';
$luogoNascita = 'Roma';
$dataValutazioneFunzionale = '01/01/2019';
$ICF = 'prova';
$ICF_S = 'prova';
$ICFDescrizione = 'prova';
$ICFDescrizione_S = 'prova';
$ICFQualF1 = 'prova';
$ICFQualF1_S = 'prova';
$ICFQualF2_S = 'prova';
$ICFQualF3_S = 'prova';
$QualDeso = 'prova';
$S1QualDes = 'prova';
$S2QualDes = 'prova';
$S3QualDes = 'prova';
$ICF_D = 'prova';
$ICFDescrizione_D = 'prova';
$ICFQualF1_D = 'prova';
$D1QualDes_D = 'prova';
$ICFQualF2_D = 'prova';
$D2QualDes_D = 'prova';
$ICF_E = 'prova';
$ICFDescrizione_E = 'prova';
$ICFQualF1_E = 'prova';
$QualDeso_E = 'prova';



// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'RELAZIONE DI DIMISSIONE',0,"","C");
$pdf->Cell(10, $line, '',"",1);


$pdf->SetFont('Times','',10);
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Cognome e Nome:',0,"","L");
$pdf->Cell(85, $line, $cognome." ".$nome,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Luogo e Data Nasc.:',0,"","L");
$pdf->Cell(85, $line, $luogoNascita." ".$dataNascita,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(85, $line, 'Data V.F.:',0,"","L");
$pdf->Cell(85, $line, $dataValutazioneFunzionale,0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'DISABILITA\'  (codici ICF) area B',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(40,$line,$ICF,'0',"","C");
    $pdf->cell(40,$line,$ICFDescrizione,'0',"","C");
    $pdf->cell(40,$line,$ICFQualF1,'0',"","C");
    $pdf->cell(40,$line,$QualDeso,'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}

$pdf->Cell(190, $halfLine, '0',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '0',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'DISABILITA\'  (codici ICF) area S',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(40,$line,$ICF_S,'0',"","C");
    $pdf->cell(40,$line,$ICFDescrizione_S,'0',"","C");
    $pdf->cell(10,$line,$ICFQualF1_S,'0',"","C");
    $pdf->cell(20,$line,$S1QualDes,'0',"","C");
    $pdf->cell(10,$line,$ICFQualF2_S,'0',"","C");
    $pdf->cell(20,$line,$S2QualDes,'0',"","C");
    $pdf->cell(10,$line,$ICFQualF3_S,'0',"","C");
    $pdf->cell(20,$line,$S3QualDes,'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}


$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'DISABILITA\'  (codici ICF) area D',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(40,$line,$ICF_D,'0',"","C");
    $pdf->cell(40,$line,$ICFDescrizione_D,'0',"","C");
    $pdf->cell(10,$line,$ICFQualF1_D,'0',"","C");
    $pdf->cell(20,$line,$D1QualDes_D,'0',"","C");
    $pdf->cell(10,$line,$ICFQualF2_D,'0',"","C");
    $pdf->cell(20,$line,$D2QualDes_D,'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'DISABILITA\'  (codici ICF) area E',0,"","L");
$pdf->Cell(10, $line, '',"",1);

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(40,$line,$ICF_E,'0',"","C");
    $pdf->cell(40,$line,$ICFDescrizione_E,'0',"","C");
    $pdf->cell(40,$line,$ICFQualF1_E,'0',"","C");
    $pdf->cell(40,$line,$QualDeso_E,'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota



?>