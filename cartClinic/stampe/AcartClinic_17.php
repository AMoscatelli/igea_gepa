<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$dataInizio = '2019-01-01';
$codanagr='10101';
$Codproj = '010101';
$diaria=[]; //array che ha tutte le diarie associate al progetto.

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(20, $line, $codanagr."-".$Codproj."-".substr($dataInizio, 0,4),0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'D I A R I A',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(50,$line,"DATA",'0',"","C");
$pdf->cell(70,$line,"DIARIA",'0',"","C");
$pdf->cell(50,$line,"FIRMA",'0',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(50,$line,"01/01/2019",'0',"","C");
    $pdf->cell(70,$line,"testo della diaria",'0',"","C");
    $pdf->cell(50,$line,"____________________",'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}

?>