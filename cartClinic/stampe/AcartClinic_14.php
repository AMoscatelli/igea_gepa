<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$nomeCognome = 'Carlo Cori9';
$dataInizio = '01/01/2019';
$disabilita = "malato terminale";
$area = "";
$operatore = "";
$obiettivo = "prova";
$risultatoAtteso="prova";


// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'PROGETTO  RIABILITATIVO E PROGNOSI',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(30,$line,"PAZIENTE","",'',"L",0);
$pdf->Cell(50,$line,$nomeCognome,"",'',"L",0);
$pdf->Cell(10,$line,"Data Inizio",'',"L",0);
$pdf->Cell(70,$line,$dataInizio,"",'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(30,$line,"Disabilità","",'',"L",0);
$pdf->Cell(140,$line,$disabilita,"",'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20,$line,"AREA",'1',"", "L",0);
$pdf->Cell(30,$line,"OPERATORE",'1',"", "L",0);
$pdf->Cell(40,$line,"",'',"L",0);
$pdf->Cell(80,$line,"Raggiungibilità obiettivo",'TLR',"","C",0);
$pdf->Cell(10, $line, '',"",1);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90,$line,"OBIETTIVO",'1',"", "C",0);
$pdf->Cell(80,$line,"",'BLR',"","C",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(20,$line,$area,'1',"", "L",0);
$pdf->Cell(30,$line,$operatore,'1',"", "L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->MultiCell(85,$line,$obiettivo,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(100, $line, '',"",0);
$pdf->MultiCell(85,$line,$risultatoAtteso,'',"L",0);
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetXY(10, 250);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90, $line, 'Firma (Medico-Operatori):',0,"","L");
$pdf->Cell(80, $line, 'Firma (Paziente):',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90, $line, '_______________________________',0,"","L");
$pdf->Cell(80, $line, "_______________________________",0,"","L");
$pdf->Cell(10, $line, '',"",1);




















$pdf->Cell(10, $line, '',"",1);



//$pdf->Output();
?>