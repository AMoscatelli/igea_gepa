<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$riabilitazione_logopedica=0;
$valutazione_osservazione=0;
$riabilitazione_cardioogica=0;
$riabilitazione_respiratoria=0;
$riabilitazione_meuromotoria=0;
$riabilitazione_psicomotoria=0;
$riabilitazione_cognitiva=0;
$terapia_psicologica=0;
$terapia_occupazionale=0;
$intervento_ortottico=0;
$intervento_educativo=0;
$revisione_progetti=0;
$counseling=0;
$riunione_equipe=0;
$ademp_integr_scolastici=0;
$impRiab=0;
$barthelInizio=0;
$spmsqInizio=0;
$SVAM_Lc=0;
$SVAM_Lp=0;
$SVAM_AU=0;
$SVAM_V=0;
$NomeAltraScalaInizio1=0;
$PuntAltraScalaInizio1=0;
$Scala_Codificata=0;
$ScalaCodificataPunteggio=0;
$Scala_Codificata1=0;
$ScalaCodificataPunteggio1=0;
$dataInizio=0;
$durataProgetto=0;

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO

//$pdf->Cell(190, $line, '',0,1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Tipologia e numero accessi previsti',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);

//Valutazione Osservazione Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Valutazione Osservazione Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$valutazione_osservazione,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Logopedica Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Logopedica Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_logopedica,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Cardiologica Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Cardiologica Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_cardioogica,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Respiratoria Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Respiratoria Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_respiratoria,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Neuromorotia Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Neuromorotia Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_meuromotoria,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Psicomotoria Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Psicomotoria Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_psicomotoria,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riabilitazione Cognitiva Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riabilitazione Cognitiva Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riabilitazione_cognitiva,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Terapia Psicologica Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Terapia Psicologica Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$terapia_psicologica,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Terapia Occupazionale Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Terapia Occupazionale Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$terapia_occupazionale,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Intervento Ortottico Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Intervento Ortottico Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$intervento_ortottico,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Intervento Educativo Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Intervento Educativo Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$intervento_educativo,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


//Elaborazione Revisione Progetti Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Elaborazione Revisione Progetti Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$revisione_progetti,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Counseling Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Counseling Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$counseling,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Riunione Equipe Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Riunione Equipe Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$riunione_equipe,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

//Ademp Integr Scolastica Numero Accessi
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(150, $line, 'Ademp Integr Scolastica Numero Accessi:',0,"","L");
$pdf->cell(20,$line,$ademp_integr_scolastici,'0',"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(120, $line, 'IMPEGNO RIABILITATIVO ASSISTENZIALE',0,"","C");
$pdf->Cell(50, $line, $impRiab,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'Tipologia e numero accessi previsti',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(50, $line, '',"",0);
$pdf->Cell(120, $line, 'Barthel Index %',0,"","L");
$pdf->Cell(150, $line, $barthelInizio,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);

$pdf->Cell(50, $line, '',"",0);
$pdf->Cell(120, $line, 'S.P.M.S.Q.',0,"","L");
$pdf->Cell(150, $line, $spmsqInizio,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->SetFont('Times','',10);
$pdf->Cell(50, $line, '',"",0);
$pdf->Cell(20, $line, 'S.V.A.M.A.',0,"","L");
$pdf->Cell(10, $line,'Lc',0,"","L");
$pdf->Cell(5, $line,$SVAM_Lc,0,"","C");
$pdf->Cell(10, $line,'Lp',0,"","L");
$pdf->Cell(5, $line,$SVAM_Lp,0,"","");
$pdf->Cell(10, $line,'U',0,"","L");
$pdf->Cell(5, $line,$SVAM_AU,0,"","C");
$pdf->Cell(10, $line,'V',0,"","L");
$pdf->Cell(5, $line,$SVAM_V,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Altre eventuali scale utilizzate',0,"","L");
$pdf->Cell(60, $line,$NomeAltraScalaInizio1,0,"","C");
$pdf->Cell(40, $line,$PuntAltraScalaInizio1,0,"","");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Scala Codificata',0,"","L");
$pdf->Cell(60, $line,$Scala_Codificata,0,"","C");
$pdf->Cell(40, $line,$ScalaCodificataPunteggio,0,"","");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Scala Codificata 1',0,"","L");
$pdf->Cell(60, $line,$Scala_Codificata1,0,"","C");
$pdf->Cell(40, $line,$ScalaCodificataPunteggio1,0,"","");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Data presa in carico del paziente',0,"","L");
$pdf->Cell(60, $line,$dataInizio,0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(40, $line, 'Durata prevista dell\'intervento',0,"","L");
$pdf->Cell(60, $line,$durataProgetto,0,"","C");
$pdf->Cell(10, $line, '',"",1);

?>