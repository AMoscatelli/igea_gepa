<?php

$line = 5;
$halfLine = 2;

$codice_progetto = '20523';
$dataInizio = '2019-01-01';
$codanagr='10101';
$Codproj = '010101';
$diaria=[]; //array che ha tutte le diarie associate al progetto.

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');

$pdf->SetFont('Times','',10);

/*Bordi dellel celle:
 * L bordo Sinistro
 * R bordo Destro
 * T bordo superire
 * B bordo inferiore
 */

//$pdf->Image('../../images/airri.png', 10, 10, 100, 100);
$pdf->SetFont('Times','',10);
$pdf->Cell(150, $line, '',0,0);
$pdf->Cell(30, $line, 'Codice Progetto:',0,0);
$pdf->Cell(10, $line, $codice_progetto ,0,1,"L"); //ToDo, riportare il numero del PROGETTO


$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',15);
$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(170, $line, 'AGGIORNAMENTO PROGRAMMA TERAPEUTICO',0,"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota


$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, '',"",0);
$pdf->cell(20,$line,"DATA",'LTR',"","C");
$pdf->cell(30,$line,"PERIODO",'LTR',"","C");
$pdf->cell(30,$line,"PROCEDURE",'LTR',"","C");
$pdf->cell(30,$line,"MODALITA'",'LTR',"","C");
$pdf->cell(30,$line,"TEMPI",'LTR',"","C");
$pdf->cell(30,$line,"OPERATORE",'LTR',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->cell(20,$line,"",'LBR',"","C");
$pdf->cell(30,$line,"",'LBR',"","C");
$pdf->cell(30,$line,"",'LBR',"","C");
$pdf->cell(30,$line,"(frequenza)",'LBR',"","C");
$pdf->cell(30,$line,"d'esecuzione",'LBR',"","C");
$pdf->cell(30,$line,"responsabile",'LBR',"","C");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

for ($i = 0; $i<5;$i++){
    
    $pdf->Cell(10, $line, '',"",0);
    $pdf->cell(20,$line,"01/01/2019",'0',"","C");
    $pdf->cell(30,$line,"periodo",'0',"","C");
    $pdf->cell(30,$line,"procedura",'0',"","C");
    $pdf->cell(30,$line,"frequenza",'0',"","C");
    $pdf->cell(30,$line,"esecuzione",'0',"","C");
    $pdf->cell(30,$line,"responsabile",'0',"","C");
    $pdf->Cell(10, $line, '',"",1);
    
    $pdf->Cell(190, $halfLine, '',"",1); // riga vuota
    
}


$pdf->SetXY(10, 230);

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(190, $line, 'Il sottoscritto dichiara:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(190, $line, 'Di aver preso visione della proposta del programma riabilitativo su cui concorda e di non',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(190, $line, 'essere attualmente in trattamento presso altre strutture riabilitative accreditate.',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90, $line, 'Firma del Paziente:',0,"","L");
$pdf->Cell(80, $line, 'Firma del Medico Referente:',0,"","L");
$pdf->Cell(10, $line, '',"",1);

$pdf->Cell(190, $halfLine, '',"",1); // riga vuota
$pdf->Cell(190, $halfLine, '',"",1); // riga vuota

$pdf->Cell(10, $line, '',"",0);
$pdf->Cell(90, $line, '_______________________________',0,"","L");
$pdf->Cell(80, $line, "_______________________________",0,"","L");
$pdf->Cell(10, $line, '',"",1);




?>