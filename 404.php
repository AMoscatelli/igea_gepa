<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>IGEA 404!</title>
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  
  <script src="js/jquery-3.2.1.min.js"></script>
  
  <script src="js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="css/404.css">

</head>

<body>
<!--Main Navigation-->
	<header>
	    <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
    <!--/Main Navigation-->
  <div class="container">
  <div>
	
  </div>
  <div class="404">
  <h1 class="login-heading mt-4">IGEA - Gestione pazienti</h1>
  <img src="images/igea.png" class="img-responsive" alt="IGEA">
  	<h1 class="login-heading">Opps! Pagina non trovata.</h1>
	<a href="index.php" class="lnk"><h1 class="login-heading">Torna alla HOME</h1></a>
          <div class="login-footer">
             <a href="#" class="lnk">
              <i class="fa fa-life-ring login-heading" aria-hidden="true"></i>
              Invia un avviso di Malfunzionamento.
             </a>
          </div>
  </div>
</div>
  <script>

</script>
</body>
</html>
