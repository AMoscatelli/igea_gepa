<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');

$action = 0;
$export = 0;
// controller($db);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['export'])){
        $export = 1;   
    }
    $action = 1;
    $selectMese = $_POST['selectMese'];
    $selectAnno = $_POST['selectAnno'];
    
}
?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - Contabilita</title>

<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">

<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">

<!-- JqGrid Css -->
<link rel="stylesheet" href="../css/jquery-ui.min.css"> <!-- https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css -->
<link rel="stylesheet" href="../css/ui.jqgrid.min.css"> <!-- https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/css/ui.jqgrid.min.css -->

<!-- Select Search Css -->
<link rel="stylesheet" href="../css/select2.css" rel="stylesheet" />
</head>


<body>

	<!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
		
	</header>
	<!--Main Navigation-->
	<!--Main Layout-->
	
	<div class="container">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="searchSection">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<form method="post">
						<div class="row mt-4 text-center" id="row-lenght">
							<div class="col-md-6">
							<label>Anno:</label>
								<select class="mdb-select colorful-select dropdown-primary search"
									id="selectAnno" name="selectAnno" style="width: 100%">
									<option value="" disabled selected>Scegli l'Anno...</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
								</select> 
								
							</div>
							<div class="col-md-6">
							<label>Mese:</label>
								<select class="mdb-select colorful-select dropdown-primary search"
									id="selectMese" name="selectMese" style="width: 100%">
									<option value="" disabled selected>Scegli il Mese...</option>
									<option value="1">Gennaio</option>
									<option value="2">Febbraio</option>
									<option value="3">Marzo</option>
									<option value="4">Aprile</option>
									<option value="5">Maggio</option>
									<option value="6">Giugno</option>
									<option value="7">Luglio</option>
									<option value="8">Agosto</option>
									<option value="9">Settembre</option>
									<option value="10">Ottobre</option>
									<option value="11">Novembre</option>
									<option value="12">Dicembre</option>
								</select> 
								
							</div>
						</div>
						
						<div class="">
						
							<button type="submit" class="btn btn-default mt-4" id="inizializzaBtn" onclick="" name="source" value="cercaContab"><i class="fa fa-search left "> INIZAILIZZA MESE</i></button>
							<!-- <a href="../contabilita/contabilita.php"><button type="button" class="btn btn-default" id="indietroBtn"    onclick=""><i class="fa fa-arrow-circle-left leftt "> INDIETRO</i></button></a>
							<button type="button" class="btn btn-default" id="modificaBtn"    onclick="showComponent('salvaBtn');hideComponent('modificaBtn');JQGRIDstartEdit('#grid1', '#grid2');"><i class="fa fa-edit left"> MODIFICA</i></button>
							<button type="button" class="btn btn-default" id="salvaBtn"       onclick="$.LoadingOverlay('show'); showComponent('modificaBtn'); hideComponent('salvaBtn'); JQGRIDsaveRows('#grid1', '#grid2');"><i class="fa fa-save left"> SALVA</i></button>
							<button type="button" class="btn btn-success" id="stampaBtn"      onclick="$.LoadingOverlay('show'); STAMPEstampaFattura('#grid1', '#grid2', 10, '01/0172018', $('#selectMese').val(), $('#selectAnno').val(), )"><i class="fa fa-print left"> STAMPA FATTURA ASL</i></button>                                            
						      -->
						</div>
					</form>
				</div>
				<!--Post data-->
			</div>
		</section>
		
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="editSection">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
					<form method="post">
						<div class="row mt-4 text-center" id="row-lenght">
							<div class="col-md-6">
								<h2>Anno: <span class="badge cyan"  id="annoBadge"></span></h2>
							</div>
							<div class="col-md-6">
								<h2>Mese: <span class="badge cyan" id="meseBadge"></span></h2>
							</div>
						</div>
						
						<div class="">
							<!--  <button type="submit" class="btn btn-default" id="inizializzaBtn" onclick="" name="source" value="cercaContab"><i class="fa fa-search left "> INIZAILIZZA MESE</i></button>-->
							<a href="../contabilita/contabilita.php"><button type="button" class="btn btn-default" id="indietroBtn"    onclick=""><i class="fa fa-arrow-circle-left leftt "> INDIETRO</i></button></a>
							<button type="button" class="btn btn-default" id="modificaBtn"    onclick="showComponent('salvaBtn');hideComponent('modificaBtn');JQGRIDstartEdit('#grid1', '#grid2');"><i class="fa fa-edit left"> MODIFICA</i></button>
							<button type="button" class="btn btn-default" id="salvaBtn"       onclick="$.LoadingOverlay('show'); showComponent('modificaBtn'); hideComponent('salvaBtn'); JQGRIDsaveRows('#grid1', '#grid2');"><i class="fa fa-save left"> SALVA</i></button>
						</div>
					</form>
				</div>
				<!--Post data-->
			</div>
		</section>
		
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="printSection">
			<div class="card card-cascade wider reverse">
				<div class="card-body text-center">
				
					<div class="row col-md-12">
						<div class="md-form col-md-6">
						<!-- Default input -->
                        <label class="text-center" for="numeroFatt">  Numero Fattura:</label>
                        <input type="number" id="numeroFatt" class="form-control text-center req">
                        </div>
                        <div class="md-form col-md-6">
                            <input placeholder="" type="text" id="dataFatt" class="form-control datepicker text-center req">
                            <label for="dataFatt">  Data Fattura:</label>
                        </div>
                        <div class="md-form col-md-6">
						<!-- Default input -->
                        <select class="mdb-select colorful-select dropdown-primary req"
							id="Banca" name="selectBanca">
						</select> <label>Banca:</label>
						<hr class="select">
                        </div>
                        <div class="md-form col-md-6">
                            <input placeholder="" type="text" id="bancaDtl" class="form-control text-center" readonly="readonly">
                            <label for="bancaDtl"></label>
                        </div>
                        <div class="md-form col-md-6">
                            <input placeholder="" type="number" id="giorniAp" class="form-control text-center req">
                            <label for="giorniAp">  Numero Giorni Apertura:</label>
                        </div>
					</div>
					<div class="mt-1">
					<form  method="post" action="contabilita.php" id="form">
						<button type="submit" name="export" value="export" class="btn btn-success" id="exportBtn" onclick="mostraDati('form');"><i class="fa fa-print left"> ESPORTA DATI</i></button>
						<button type="button" class="btn btn-success" id="stampaBtn"      onclick="$.LoadingOverlay('show'); STAMPEvalidate();"><i class="fa fa-print left"> STAMPA FATTURA ASL</i></button>                                            
					</form>
					</div>
				
				</div>
				<!--Post data-->
			</div>
			
			<div class="mt-4" id="tabPanel">
    			<ul class="nav nav-tabs nav-justified" id="ul-panel">
    				<li class="nav-item"><a class="nav-link active" data-toggle="tab"
    					href="#ambulatoriale" role="tab">Ambulatoriale</a></li>
    				<li class="nav-item"><a class="nav-link" data-toggle="tab"
    					href="#domiciliare" role="tab">Domiciliare</a></li>
    			</ul>
    			<!-- Tab panels -->
    			<div class="tab-content card" id="card">
    				<!--Panel Residenziale-->
    				<div class="tab-pane fade in show active" id="#ambulatoriale"
    					role="tabpanel">
    					<table id="grid1"></table>
    				</div>
    				<!--/.Panel Residenziale-->
    				<!--Panel NonResidenziale-->
    				<div class="tab-pane fade" id="domiciliare" role="tabpanel">
    					<table id="grid2"></table>
    				</div>
    				<!--/.Panel NonResidenziale-->
    			</div>
			</div>
		</section>
		
		<!--Section: CSV Export DIV-->
		<section class="mb-5 mt-2" style="display: none" >
			<div id="exportCSV"></div>
		</section>
		<!--Section: CSV Export DIV-->
		
	
	<!--Main Layout-->
</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Jquery To Excel JavaScript -->
<script type="text/javascript" src="../js/excelexportjs.js"></script>



<script	src="../js/jquery.jqgrid.min.js"></script> <!--  https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js-->

<script type="text/javascript" src="../js/moment.js"></script>	
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<script type="text/javascript" src="js/JQGrid.js"></script>
<script type="text/javascript" src="../jsCustom/exportCSV.js"></script>
<script type="text/javascript" src="../jsCustom/textNumber.js"></script>
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="js/stampe.js"></script>
<script type="text/javascript" src="../js/select2.js"></script>
<!-- Menu Navigazione -->
    <script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>


<!-- Datepicker files: lingua-->
<script type="text/javascript" src="../js/it_IT.js"></script>

<script>
	var baseUrl='<?php echo $baseurl;?>';
	$('#dataFatt').change(function() {
    	if($('#dataFatt').val()==""){
    		toastr.error('Non � stato inserita una data valida!');
    	}
	});
	$('#Banca').on('change', function() {
		console.log(this.value);
		for(var i = 0; i<Object.keys(json).length;i++){
			if(json[i]['Id']==this.value){
			 	 $('#bancaDtl').val(json[i]['Agenzia']+" - "+json[i]['Iban']);
			 	 break;
			}
		}		
		
	});

	initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
	//initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>');
	var user = 'Benvenuto <?php echo $_SESSION['login_user'];?>';
	
    $(document).ready(function () {
    	$('.collapsible-body').css({
    		"display" : "none"
    	});
    	$('#contabilitaMenuBody').css({
    		"display" : "block"
    	});
    	$('#loginUser').html(user);
    	$('.search').select2();
    	
    	$('.datepicker').pickadate({format: 'dd/mm/yyyy'});
    	HTMLTOOLvalidateTextNumber('printSection')
    	$.LoadingOverlay("hide"); 	
    });

	$.LoadingOverlay("show");
	var json = jQuery.parseJSON('<?php echo selectAll('banche','Banca, Id, Agenzia, Iban');?>');
	creaOption('printSection',json);
	$('#Banca').material_select();		
	hideComponent('editSection');
	hideComponent('printSection');
	hideComponent('salvaBtn');

	new WOW().init();
	
    var anno = DATEUTILgetAnno();
	var mese = DATEUTILgetMese();
    	
	var action = <?php echo $action;?>;
	var esporta = <?php if($action == 1) echo $export; else echo 1;?>;  
    	 

	if(action == 0){
		$("#selectAnno").val(anno);
    	$("#selectMese").val(mese);
	}else if(action == 1){ //Eseguo la ricerca delle Presenza
		$.LoadingOverlay("show");

		$.ajax({
			type : 'post',
			url : '../php/controller.php',
			data : {
				'source' : "getPresenze",
				'selectMese'   : <?php if($action == 1) echo $selectMese; else echo 1;?>,
				'selectAnno'   : <?php if($action == 1) echo $selectAnno; else echo 1;?> 
			},
			success : function(response) {
				var obj = $.parseJSON(response);
				$.ajax({
					type : 'post',
					url : '../php/controller.php',
					data : {
						'source' : "cercaContabDomAmb",
						'index'   : "5",
						'selectMese'   : <?php if($action == 1) echo $selectMese; else echo 1;?>,
						'selectAnno'   : <?php if($action == 1) echo $selectAnno; else echo 1;?>
					},
					success : function(response) {
						var obj1 = $.parseJSON(response);
						showComponent('modificaBtn');
				    	JQGRIDinitGrid(obj, '#grid1', 'Ambulatoriale', $("#row-lenght").innerWidth()+60);
				    	JQGRIDinitGrid(obj1, '#grid2', 'Domiciliare', $("#row-lenght").innerWidth()+60);
				    	var grid = $('#grid1');
				    	var ids = grid.jqGrid('getDataIDs');
				    	var grid1 = $('#grid2');
				    	var ids1 = grid1.jqGrid('getDataIDs');
				    	$("#selectAnno").val(<?php if($action==1) echo $selectAnno; else 1 ?>);
				    	$("#selectMese").val(<?php if($action==1) echo $selectMese; else 1 ?>);
				    	if(Object.keys(ids).length !== 0 || Object.keys(ids1).length !== 0){
				    		showComponent('editSection');
				    		showComponent('printSection');
				    		hideComponent('searchSection');
				    		$("#annoBadge").html($("#selectAnno").val());
				    		$("#meseBadge").html(getNomeMese($("#selectMese").val()));
				    		$('#tabPanel').show();
				    	}else{
				    		hideComponent('editSection');
				    		hideComponent('printSection');
				    		showComponent('searchSection');
				    		toastr.info('Non sono stati trovati risultati');
				        }
				        if(esporta == '1'){
				        	JQGRIDexportGrid(obj, 'exportCSV', 'export.csv');
				        	JQGRIDexportGrid(obj1, 'exportCSV', 'export.csv');
				        }
				    	$.LoadingOverlay("hide");
					},
					error : function() {
						$.LoadingOverlay("hide");
						toastr.error('Opps! Si � verifiato un problema!');
					}
				});
				
			},
			error : function() {
				$.LoadingOverlay("hide");
				toastr.error('Opps! Si � verifiato un problema!');
			}
		});
	
		
 
    }

	function mostraDati(form){
        
        $('#'+form).submit( function() {
           $('<input />').attr('type', 'hidden')
           .attr('name', "selectMese")
           .attr('value',  $("#selectMese").val())
           .appendTo('#'+form);
           $('<input />').attr('type', 'hidden')
           .attr('name', "selectAnno")
           .attr('value', $("#selectAnno").val())
           .appendTo('#'+form);
       		return true;
  		 }); 
                      
	}
	

    </script>
</html>



