var obj = [];
function STAMPEvalidate() {

	if (HTMLTOOLareRequiredFilled('printSection', 'req')) {
		$.ajax({
			type : 'post',
			url : '../php/controller.php',
			data : {
				'source' : "getRette"
			},
			success : function(response) {
				obj = JSON.parse(response);
				STAMPEstampaFattura('#grid1', '#grid2', $('#numeroFatt').val(),
						$('#dataFatt').val(), $('#selectMese').val(), $(
								'#selectAnno').val());
				$.LoadingOverlay("hide");
			},
			error : function() {
				$.LoadingOverlay("hide");
				toastr.error('Opps! Si è verifiato un problema!');
			}
		});

	} else {
		$.LoadingOverlay("hide");
	}

}

function STAMPEstampaFattura(GridName, GridName1, numero, data, mese, anno) {
	var resid = [];
	var nonResid = [];
	var grid = $(GridName);
	var ids = grid.jqGrid('getDataIDs');
	var grid1 = $(GridName1);
	var ids1 = grid1.jqGrid('getDataIDs');
	var tot = 0;
	if (ids1.length != 0) {
		for (var i = 0; i < ids1.length; i++) {
			grid1.jqGrid('saveRow', ids1[i]);
			nonResid[i] = grid1.jqGrid("getRowData", ids1[i]);
		}
	}
	if (ids.length != 0) {
		for (var i = 0; i < ids.length; i++) {
			grid.jqGrid('saveRow', ids[i]);
			resid[i] = grid.jqGrid("getRowData", ids[i]);
		}
	}

	var ids = JQGRDIgetGrid(GridName);
	var ids1 = JQGRDIgetGrid(GridName1);

	var meseNome = getNomeMese(mese);

	var ee = 0; // nr. progetti estensivo - elevato
	var em = 0; // nr. progetti estensivo - medio
	var el = 0; // nr. progetti estensivo - lieve
	var me = 0; // nr. progetti mantenimento - elevato
	var mm = 0; // nr. progetti mantenimento - medio
	var ml = 0; // nr. progetti mantenimento - lieve

	var ggee = 0; // giornate piene estensivo - elevato
	var ggem = 0; // giornate piene estensivo - medio
	var ggel = 0; // giornate piene estensivo - lieve
	var ggme = 0; // giornate piene mantenimento - elevato
	var ggmm = 0; // giornate piene mantenimento - medio
	var ggml = 0; // giornate piene mantenimento - lieve

	var ggee80 = 0; // giornate 80 estensivo - elevato
	var ggem80 = 0; // giornate 80 estensivo - medio
	var ggel80 = 0; // giornate 80 estensivo - lieve
	var ggme80 = 0; // giornate 80 mantenimento - elevato
	var ggmm80 = 0; // giornate 80 mantenimento - medio
	var ggml80 = 0; // giornate 80 mantenimento - lieve

	var rettaEE = 0; // retta per Reg. Assist. 3 o 5 Estensivo Elevato
	var rettaEM = 0; // retta per Reg. Assist. 3 o 5 Estensivo Medio
	var rettaEL = 0; // retta per Reg. Assist. 3 o 5 Estensivo Lieve
	var rettaME = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Elevato
	var rettaMM = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Medio
	var rettaML = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Lieve

	var rettaEE80 = 0; // retta per Reg. Assist. 3 o 5 Estensivo Elevato 80
	var rettaEM80 = 0; // retta per Reg. Assist. 3 o 5 Estensivo Medio 80
	var rettaEL80 = 0; // retta per Reg. Assist. 3 o 5 Estensivo Lieve 80
	var rettaME80 = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Elevato 80
	var rettaMM80 = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Medio 80
	var rettaML80 = 0; // retta per Reg. Assist. 3 o 5 Mantenimento Lieve 80

	for (var i = 0; i < Object.keys(obj).length; i++) {
		if (obj[i].RegaAssist == "3"
				&& obj[i].ModalitaIntervento == "ESTENSIVO") {
			if (obj[i].ImpRiab == "ELEVATO") {
				rettaEE = obj[i].PrezzoEuro;
				rettaEE80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "MEDIO") {
				rettaEM = obj[i].PrezzoEuro;
				rettaEM80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "LIEVE") {
				rettaEL = obj[i].PrezzoEuro;
				rettaEL80 = obj[i].AssenzaEuro;
			}
		}
		if (obj[i].RegaAssist == "3"
				&& obj[i].ModalitaIntervento == "MANTENIMENTO") {
			if (obj[i].ImpRiab == "ELEVATO") {
				rettaME = obj[i].PrezzoEuro;
				rettaME80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "MEDIO") {
				rettaMM = obj[i].PrezzoEuro;
				rettaMM80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "LIEVE") {
				rettaML = obj[i].PrezzoEuro;
				rettaML80 = obj[i].AssenzaEuro;
			}
		}
		if (obj[i].RegaAssist == "5"
				&& obj[i].ModalitaIntervento == "ESTENSIVO") {
			if (obj[i].ImpRiab == "ELEVATO") {
				rettaEE = obj[i].PrezzoEuro;
				rettaEE80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "MEDIO") {
				rettaEM = obj[i].PrezzoEuro;
				rettaEM80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "LIEVE") {
				rettaEL = obj[i].PrezzoEuro;
				rettaEL80 = obj[i].AssenzaEuro;
			}
		}
		if (obj[i].RegaAssist == "5"
				&& obj[i].ModalitaIntervento == "MANTENIMENTO") {
			if (obj[i].ImpRiab == "ELEVATO") {
				rettaME = obj[i].PrezzoEuro;
				rettaME80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "MEDIO") {
				rettaMM = obj[i].PrezzoEuro;
				rettaMM80 = obj[i].AssenzaEuro;
			}
			if (obj[i].ImpRiab == "LIEVE") {
				rettaML = obj[i].PrezzoEuro;
				rettaML80 = obj[i].AssenzaEuro;
			}
		}
	}

	if (ids.length != 0 && ids1.length != 0) {

		for (var i = 0; i < ids1.length; i++) {

			if (ids1[i].ModalitaIntervento == "ESTENSIVO") {
				if (ids1[i].ImpRiab == "ELEVATO") {
					ee++;
					ggee += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggee80 += parseInt(ids1[i].Ass80);
				} else if (ids1[i].ImpRiab == "MEDIO") {
					em++;
					ggem += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggem80 += parseInt(ids1[i].Ass80);
				} else if (ids1[i].ImpRiab == "LIEVE") {
					el++;
					ggel += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggel80 += parseInt(ids1[i].Ass80);
				}

			} else if (ids1[i].ModalitaIntervento == "MANTENIMENTO") {
				if (ids1[i].ImpRiab == "ELEVATO") {
					me++;
					ggme += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggme80 += parseInt(ids1[i].Ass80);
				} else if (ids1[i].ImpRiab == "MEDIO") {
					mm++;
					ggmm += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggmm80 += parseInt(ids1[i].Ass80);
				} else if (ids1[i].ImpRiab == "LIEVE") {
					ml++;
					ggml += ids1[i].GGPCTeor - ids1[i].Ass00 - ids1[i].Ass80;
					ggml80 += parseInt(ids1[i].Ass80);
				}

			}

		}
		for (var i = 0; i < ids.length; i++) {
			var Ass00 = ids[i].Ass00 == null ? 0 : ids[i].Ass00;
			var Ass80 = ids[i].Ass80 == null ? 0 : ids[i].Ass80;
			var GGPCTeor = ids[i].GGPCTeor == null ? 0 : ids[i].GGPCTeor;

			if (ids[i].ModalitaIntervento == "ESTENSIVO") {
				if (ids[i].ImpRiab == "ELEVATO") {
					ee++;
					ggee += GGPCTeor - Ass00 - Ass80;
					ggee80 += parseInt(Ass80);
				} else if (ids[i].ImpRiab == "MEDIO") {
					em++;
					ggem += GGPCTeor - Ass00 - Ass80;
					ggem80 += parseInt(Ass80);
				} else if (ids[i].ImpRiab == "LIEVE") {
					el++;
					ggel += GGPCTeor - Ass00 - Ass80;
					ggel80 += parseInt(Ass80);
				}

			} else if (ids[i].ModalitaIntervento == "MANTENIMENTO") {
				if (ids[i].ImpRiab == "ELEVATO") {
					me++;
					ggme += GGPCTeor - Ass00 - Ass80;
					ggme80 += parseInt(Ass80);
				} else if (ids[i].ImpRiab == "MEDIO") {
					mm++;
					ggmm += GGPCTeor - Ass00 - Ass80;
					ggmm80 += parseInt(Ass80);
				} else if (ids[i].ImpRiab == "LIEVE") {
					ml++;
					ggml += GGPCTeor - Ass00 - Ass80;
					ggml80 += parseInt(Ass80);
				}

			}

		}

		var totEE = ggee * rettaEE;
		var totEE80 = ggee80 * rettaEE80;

		var totEM = ggem * rettaEM;
		var totEM80 = ggem80 * rettaEM80;

		var totEL = ggel * rettaEL;
		var totEL80 = ggel80 * rettaEL80;

		var totME = ggme * rettaME;
		var totME80 = ggme80 * rettaME80;

		var totMM = ggmm * rettaMM;
		var totMM80 = ggmm80 * rettaMM80;

		var totML = ggml * rettaML;
		var totML80 = ggml80 * rettaML80;

		totEE += totEE80;
		tot += totEE;
		totEE = formatComma(totEE.toFixed(2));

		totEM += totEM80;
		tot += totEM;
		totEM = formatComma(totEM.toFixed(2));

		totEL += totEL80;
		tot += totEL;
		totEL = formatComma(totEL.toFixed(2));

		totME += totME80;
		tot += totME;
		totME = formatComma(totME.toFixed(2));

		totMM += totMM80;
		tot += totMM;
		totMM = formatComma(totMM.toFixed(2));

		totML += totML80;
		tot += totML;
		totML = formatComma(totML.toFixed(2));

		tot = formatComma(tot.toFixed(2));

		$.LoadingOverlay("hide");

		var url = baseUrl + "/contabilita/stampe/contabilitaStampa.php?totEE="
				+ totEE + "&totEM=" + totEM + "&totEL=" + totEL + "&totME="
				+ totME + "&totMM=" + totMM + "&totML=" + totML + "&numero="
				+ numero + "&data=" + data + "&mese=" + meseNome + "&ee=" + ee
				+ "&em=" + em + "&el=" + el + "&me=" + me + "&mm=" + mm
				+ "&ml=" + ml + "&ggee=" + ggee + "&ggem=" + ggem + "&ggel="
				+ ggel + "&ggme=" + ggme + "&ggmm=" + ggmm + "&ggml=" + ggml
				+ "&anno=" + anno + "&tot=" + tot;
		window.open(url, "newPage");

	}
}

function formatComma(amount) {
	var delimiter = ".";
	var a = amount.split('.', 2)
	var d = a[1];
	var i = parseInt(a[0]);
	if (isNaN(i)) {
		return '';
	}
	var minus = '';
	if (i < 0) {
		minus = '-';
	}
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while (n.length > 3) {
		var nn = n.substr(n.length - 3);
		a.unshift(nn);
		n = n.substr(0, n.length - 3);
	}
	if (n.length > 0) {
		a.unshift(n);
	}
	n = a.join(delimiter);
	if (d.length < 1) {
		amount = n;
	} else {
		amount = n + ',' + d;
	}
	amount = minus + amount;
	return amount;
}
