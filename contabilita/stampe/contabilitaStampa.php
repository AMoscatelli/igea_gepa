<?php
include '../../php/session.php';
include '../../php/stampa/defaultPagina.php';
include '../../php/utility.php';

$line = 5;
$halfLine = 2;

$pdf = new FPDF();

// Italian national format with 2 decimals`
//setlocale(LC_MONETARY, 'it_IT');


//da sostituire con le variabili
$numero = isset($_GET['numero']) ? $_GET['numero'] : 0;//isset($_GET('numeroFattura'));
$data = isset($_GET['data']) ? $_GET['data'] : 0;//isset($_GET('dataFattura'));
$mese = isset($_GET['meseNome']) ? $_GET['meseNome'] : 0;//isset($_GET('meseFattura'));
$anno = isset($_GET['anno']) ? $_GET['anno'] : 0;//isset($_GET('annoFattura'));

$ee = isset($_GET['ee']) ? $_GET['ee'] : 0; //nr. progetti estensivo - elevato
$em = isset($_GET['em']) ? $_GET['em'] : 0; //nr. progetti estensivo - medio
$el = isset($_GET['el']) ? $_GET['el'] : 0; //nr. progetti estensivo - lieve
$me = isset($_GET['me']) ? $_GET['me'] : 0; //nr. progetti mantenimento - elevato
$mm = isset($_GET['mm']) ? $_GET['mm'] : 0; //nr. progetti mantenimento - medio
$ml = isset($_GET['ml']) ? $_GET['ml'] : 0; //nr. progetti mantenimento - lieve

$ggee = isset($_GET['ggee']) ? $_GET['ggee'] : 0; //giornate piene estensivo - elevato
$ggem = isset($_GET['ggem']) ? $_GET['ggem'] : 0; //giornate piene estensivo - medio
$ggel = isset($_GET['ggel']) ? $_GET['ggel'] : 0; //giornate piene estensivo - lieve
$ggme = isset($_GET['ggme']) ? $_GET['ggme'] : 0; //giornate piene mantenimento - elevato
$ggmm = isset($_GET['ggmm']) ? $_GET['ggmm'] : 0; //giornate piene mantenimento - medio
$ggml = isset($_GET['ggml']) ? $_GET['ggml'] : 0; //giornate piene mantenimento - lieve

$tot = isset($_GET['tot']) ? $_GET['tot'] : 0;

$totEE = isset($_GET['totEE']) ? $_GET['totEE'] : 0;

$totEM = isset($_GET['totEM']) ? $_GET['totEM'] : 0;

$totEL = isset($_GET['totEL']) ? $_GET['totEL'] : 0;

$totME = isset($_GET['totME']) ? $_GET['totME'] : 0;

$totMM = isset($_GET['totMM']) ? $_GET['totMM'] : 0;

$totML = isset($_GET['totML']) ? $_GET['totML'] : 0;



// Instanciation of inherited class
$pdf = new defaultPagina();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',10);

$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, 'Spett.le',0,1);
$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, 'Azienda Sanitaria Locale ROMA 1',0,1);
$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, 'U.O. Gest. Attivit� Econ. e Fin.rie',0,1);
$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, 'B.GO S.SPIRITO, 3',0,1);
$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, '00193 - Roma (RM)',0,1);
$pdf->Cell(120, $line, '',0,0);
$pdf->Cell(70, $line, 'P.IVA: 13664791004',0,1);

//SPAZIO VUOTO
$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);

//RIGA CON NUMERO E DATA FATTURA
$pdf->SetFont('Times','BI',12);
$pdf->Cell(50, $line, 'Fattura N. ');
$pdf->SetFont('Times','B',10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40, $line, $numero."/RE");
$pdf->SetFont('Times','BI',12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(20, $line, 'del ');
$pdf->SetFont('Times','B',10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(70, $line, $data,0,1);

//SPAZIO VUOTO
$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);

//RIGA CON TESTO DELLA FATTURA
$pdf->SetFont('Times','',10);
$pdf->Cell(10, $line, 'Vi rimettiamo fattura relativa a prestazioni di riabilitazione estensiva e di mantenimento',0,1);
$pdf->Cell(10, $line, 'in regime NON Residenziale erogate nel mese di '.$mese.' '.$anno,0,1);

//RIGA INTESTAZIONE CORPO DELLA FATTURA

$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);
$pdf->Cell(120, $line, '',0,1);

$pdf->SetFont('Times','B',10);
$pdf->Cell(40,$line,'Regime Assistenziale',0,0);
$pdf->Cell(40,$line,'Modalita di',0,0);
$pdf->Cell(30,$line,'Impegno',0,0);
$pdf->Cell(20,$line,'Nr.',0,0,'C');
$pdf->Cell(20,$line,'Giorni di presa',0,0,'C');
$pdf->Cell(30,$line,'TOTALE',0,1,'R');

$pdf->Cell(40,$halfLine,'',0,0);
$pdf->Cell(40,$halfLine,'Intervento',0,0);
$pdf->Cell(30,$halfLine,'Assistenziale',0,0);
$pdf->Cell(20,$halfLine,'Progetti',0,0,'C');
$pdf->Cell(20,$halfLine,'in carico',0,0,'C');
$pdf->Cell(30,$halfLine,'',0,1,'R');

$pdf->Cell(120, $line, '',0,1);
$pdf->SetFont('Times','',10);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'ESTENSIVO',0,0);
    $pdf->Cell(30,$line,'ELEVATO',0,0);
    $pdf->Cell(20,$line, ' '.$ee,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggee,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totEE,0,1,'R');
  
    $pdf->Cell(120, $halfLine, '',0,1);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'ESTENSIVO',0,0);
    $pdf->Cell(30,$line,'MEDIO',0,0);
    $pdf->Cell(20,$line, ' '.$em,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggem,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totEM,0,1,'R');
  
    $pdf->Cell(120, $halfLine, '',0,1);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'ESTENSIVO',0,0);
    $pdf->Cell(30,$line,'LIEVE',0,0);
    $pdf->Cell(20,$line, ' '.$el,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggel,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totEL,0,1,'R');
    
    $pdf->Cell(120, $halfLine, '',0,1);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'MANTENIMENTO',0,0);
    $pdf->Cell(30,$line,'ELEVATO',0,0);
    $pdf->Cell(20,$line, ' '.$me,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggme,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totME,0,1,'R');
    
    $pdf->Cell(120, $halfLine, '',0,1);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'MANTENIMENTO',0,0);
    $pdf->Cell(30,$line,'MEDIO',0,0);
    $pdf->Cell(20,$line, ' '.$mm,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggmm,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totMM,0,1,'R');
    
    $pdf->Cell(120, $halfLine, '',0,1);
    
    $pdf->Cell(40,$line,'NON RESIDENZIALE',0,0);
    $pdf->Cell(40,$line,'MANTENIMENTO',0,0);
    $pdf->Cell(30,$line,'LIEVE',0,0);
    $pdf->Cell(20,$line, ' '.$ml,0,0,'C');
    $pdf->Cell(20,$line, ' '.$ggml,0,0,'C');
    $pdf->Cell(30,$line, ' '.$totML,0,1,'R');
    
    $pdf->Cell(120, $line, '',0,1);

    //TOTLALE FATTURA
    $pdf->Cell(140,$line,'TOTALE FATTURA:',0,0);
    $pdf->Cell(40,$line,$tot,0,1,'R');
    
    $pdf->Cell(120, $line, '',0,1);
    $pdf->Cell(120, $line, '',0,1);
    $pdf->Cell(120, $line, '',0,1);
    
    //ESENZIONE IVA
    $pdf->Cell(70,$line,'Esente IVA art.10 combinato disposto punti 18-19-27Ter DPR 633 del 26/10/72',0,1);
    $pdf->Cell(70,$line,'e successive modificazioni.',0,1);
    
    $pdf->Output('I','Fattura.pdf');
    
    















//$pdf->Output();
?>