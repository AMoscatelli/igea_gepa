<?php
// Da includere per la gestione della sessione
include ('../php/session.php');
include ('../php/controller.php');
//$query=0;
$action = 0;
$codAnagr='';
$queryPaziente='';
//$pippo=$_POST['query'];

    
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //if(isset($_POST['query'])){
        //$action = 1;
        //$query=$_POST['query'];
    //}
}
if (isset($_GET['new'])){
    $action=2;
}
if (isset($_GET['CodAnagr'])){
    $queryPaziente=cercaPaziente1('anagrafico','CodAnagr,Nome,Cognome','CodAnagr',$_GET['CodAnagr']);
    $action=1;
}

?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - Anagrafica</title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
<!-- Select 2 Css -->
<link href="../css/select2.css" rel="stylesheet">
<!-- Google Css -->
<link href="../css/google.css" rel="stylesheet">

</head>


<body>

	<!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
	<!--/Main Navigation-->
	<!--Main Layout-->
	<main>

	<div id="spinner" style='height: 100%; width: 100%'></div>
	<div class="container" id="completed" style="visibility: hidden;">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse card-custom">
				<div class="card-body text-center">
					<!--Sezione bottoni shares-->
					<form method="post" id="dati" action="anagrafica_detail.php">
						<button style="float: right" type="submit" name="buttonPr"
							value="nuovo" class="btn btn-default" id="nuovoPrBtn" onclick="">
							<i class="fa fa-plus left"> NUOVO PAZIENTE</i>
						</button>
						<button type="submit" name="buttonPr" value="anagr"
							class="btn btn-default" id="mostraAnagr"
							onclick="mostraDati('dati')" style="display: none">
							<i class="fa fa-eye left"> MOSTRA ANAG.</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="salvaAnagrafica()" style="display: none">
							<i class="fa fa-save left"> SALVA</i>
						</button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="modalEliminaPaziente()" style="display: none">
							<i class="fa fa-trash left"> ELIMINA</i>
						</button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="sbloccaCampi()" style="display: none">
							<i class="fa fa-edit left"> MODIFICA</i>
						</button>
						<button type="button" name="buttonPr" value="cerca"
							class="btn btn-default" id="cercaPrBtn"
							onclick="tornaAnagrafica();">
							<i class="fa fa-search left "> CERCA PAZIENTE</i>
						</button>
						
					</form>
				</div>
				<!--Post data-->
			</div>
		</section>
		<!-- Definisco la riga dove verra visualizzato il nome del paziente selezionato -->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn"
			id="rowNominativo">
			<div class="card card-cascade wider reverse card-custom">
				<div class="card-body text-center">
					<div class="row">
						<!-- <div class="col-1"></div> -->
						<div class="col-2">
							<img src="../img/user_icon.png" width="50%" id="icona">
						</div>
						<div class="col-4">
							<label class="labelRequired" for="Cognome">Cognome</label>
							 <input type="text"
								class="form-control text-center" id="Cognome" required >
						</div>
						<div class="col-4">
							<label class="labelRequired"  for="Nome">Nome</font></label> 
							<input type="text" class="form-control text-center" id="Nome" required >
						</div>
						<div class="col-2">
						<button style="float: right" type="" name=""
							value="progetoNew" class="btn btn-outline-info btn-rounded waves-effect" id="progettoNew" onclick="vaiProgetto()">
							<i class="fa fa-plus left"> NUOVO PROGETTO</i>
						</button>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Section: A-->
		<section id="campi"
			class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<!-- Nav tabs -->
			<div class="tabs-wrapper">
				<ul class="nav classic-tabs tabs-cyan" role="tablist">
					<li class="nav-item"><a class="nav-link waves-light active"
						data-toggle="tab" href="#anagrafico" role="tab">Anagrafico</a></li>
					<li class="nav-item"><a class="nav-link waves-light"
						data-toggle="tab" href="#cartClinica_p2" role="tab">Cart. Clinica</a></li>
				</ul>
			</div>
			<!-- Tab panels -->
			<div class="tab-content card">
				<!--Panel 1-->
				<div class="tab-pane fade in show active" id="anagrafico"
					role="tabpanel">
					<!-- Extended default form grid -->
					<!-- Prima row -->
					<div class="row">
						<!-- Codice Anagrafica input -->
						<div class="col-md-1">
							<label for="CodAnagr">Codice</label> <input type="text"
								class="form-control" id="CodAnagr" disabled>
						</div>
						<!-- Centro input -->
						<div class="col-md-2">
							<label for="Centro">Centro</label> <input type="text"
								class="form-control" id="Centro" disabled>
						</div>
						<!-- Codice Struttura input -->
						<div class="col-md-2">
							<label for="CodiceStruttura">Codice Struttura</label> <input
								type="text" class="form-control" id="CodiceStruttura" disabled>
						</div>
						<!-- ASL input -->
						<div class="col-md-2">
							<label class="labelRequired"  for="ASL">ASL</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="ASL" required></select>
						</div>
						<!-- Distretto input -->
						<div class="col-md-1">
							<label class="labelRequired"  for="Distretto">Distretto</label> <input type="text"
								class="form-control" id="Distretto" required>
						</div>
						<!-- Cod Muni input -->
						<div class="col-md-2">
							<label class="labelRequired" for="CodMuni">Codice Municipio</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="CodMuni" required></select>
						</div>
						<!-- Sesso select input -->
						<div class="col-md-1 labelRequired">
							<label for="Sesso">Sesso</font></label><select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="Sesso" required>
								<option value="">Sel</option>
								<option value="1">M</option>
								<option value="2">F</option>
							</select>
						</div>
					</div>
					<!-- Fine prima row -->
					<br>
					<!-- row Residenza -->
					<div class="row">
						<div class="col-md-5"><hr>
						</div>
						<div class="col-md-2"><label for="Residenza">Residenza</label>
						</div>
						<div class="col"><hr>
						</div>
					</div>
					<!-- Fine row Residenza -->
					<!-- Seconda row -->
					<div class="row">
						
						<!-- IndirizzoResidenza input -->
						<div class="col-md-3"id="locationField">
							<label class="labelRequired" for="IndirizzoResidVia">Indirizzo di Residenza</label> 
							<input id="IndirizzoResidVia" placeholder="Digita indirizzo" type="text" class="form-control" required>
							<!--select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="IndirizzoResidVia"></select-->
						</div>
						<!-- Num Via input -->
						<div class="col-md-1">
							<label class="labelRequired" for="IndirizzoResidNVia">Civico</label> <input
								type="text" class="form-control" id="IndirizzoResidNVia" required>
						</div>
						<!-- IstatResidenza input id="Codice ISTAT" -->
						<div class="col">
							<label class="labelRequired" for="luogoResidenza">Luogo di Residenza</label> <input
								type="text" class="form-control capitalize" id="luogoResidenza" required>
						</div>
						<!-- Res Prov input -->
						<div class="col">
							<label for="Zona">Zona</label> <input type="text"
								class="form-control" id="Zona">
						</div>
						<!-- Res Prov input -->
						<div class="col-md-1">
							<label class="labelRequired"  for="ResProv">Provincia</label> <input type="text"
								class="form-control" id="ResProv" required>
						</div>
						<!-- Res CAP input -->
						<div class="col">
							<label class="labelRequired" for="ResCAP">CAP</label> <input type="text"
								class="form-control" id="ResCAP" required>
						</div>
					</div>
					<!-- Fine seconda row -->
					<br>
										<!-- row Dati di Nascita -->
					<div class="row">
						<div class="col-md-5"><hr>
						</div>
						<div class="col-md-2 custom-nascita" onclick='riportaDatiResidenza()' data-toggle="tooltip" title="Clicca se uguale a residenza"><i class="fas fa-angle-double-down"></i> <span >Dati di Nascita </span><i class="fas fa-angle-double-down"></i>
						</div>
						<div class="col"><hr>
						</div>
					</div>
					<!-- Fine row Dati di nascita 
				
					<!-- Quarta row -->
					<div class="row">
						<!-- Luogo di Nascita input -->
						<div class="col">
							<label class="labelRequired" for="LuogoDiNascita">Luogo di Nascita</label> 
								<input id="LuogoDiNascita"  type="text" class="form-control capitalize" required>
						</div>
						<!-- DNprov input -->
						<div class="col-1">
							<label class="labelRequired" for="DNProv">Provincia</label> <input type="text"
								class="form-control" id="DNProv" required>
						</div>
						<!-- DNCAP input -->
						<div class="col-2">
							<label class="labelRequired" for="DNCAP">CAP</label> <input type="text"
								class="form-control" id="DNCAP" required>
						</div>
						<!-- Data Di Nascita input -->
						<div class="col">
							<label class="labelRequired" for="DataDiNascita">Data di Nascita</label> <input type="text"
								class="form-control text-center dateFormat datepicker" id="DataDiNascita" required>
						</div>
						
						<!-- Et� input -->
						<div class="col-1">
							<label class="text-uppercase labelRequired" for="eta">et&agrave</label> <input
								type="text" class="form-control text-center custom-yellow" id="eta" required disabled>
						</div>
						
						<!-- Nazionalit� input -->
						<div class="col-2">
							<label class="labelRequired" for="Nazionalita">Nazionalit&agrave</label> <input
								type="text" class="form-control" id="Nazionalita" required>
						</div>
						<!-- Lingua input -->
						<div class="col">
							<label for="Lingua">Lingua</label> <input type="text"
								class="form-control" id="Lingua">
						</div>
					</div>
					<!-- Fine Quarta row -->
					<!-- row hr -->
					<div class="row">
						<hr>
					</div>
					<!-- Fine row hr -->
					<!-- Terza row -->
					<div class="row">
						<!-- Stato civ select input -->
						<div class="col">
							<label class="labelRequired" for="StatoCiv">Stato Civile</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="StatoCiv" required>
							</select>
						</div>
						<!-- Titolo Studio select input -->
						<div class="col">
						
							<label class="labelRequired" for="TitoloStudio">Titolo di Studio</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="TitoloStudio" required>
							
							</select>
						</div>
						<!-- Occupazione select input -->
						<div class="col-2">
							<label class="labelRequired" for="Occupazione">Occupazione</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="Occupazione" required>
							</select>
						</div>
						
					</div>
					<!-- Fine terza row -->
					<br>
					<!-- Terza row -->
					<div class="row">
						
						<!-- CatProtetta select input -->
						<div class="col">
							<label class="labelRequired"  for="CatProtetta">CatProtetta</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="CatProtetta" required>
								
							</select>
						</div>
						<!-- Tut Legale select input -->
						<div class="col">
							<label class="labelRequired" for="Tutlegale" id="labelTutelaLegale">Tutela Legale</label> <select
								class="mdb-select colorful-select dropdown-primary border_custom_select"
								id="Tutlegale" required>
							</select>
						</div>
					</div>
					<!-- Fine terza row -->
						<br>
	
	
					<!-- Quinta row -->
					<div class="row">
						<!-- Telefono input -->
						<div class="col-2">
							<label for="Telefono">Telefono</label> <input type="text"
								class="form-control" id="Telefono">
						</div>
						<!-- Cellulare input -->
						<div class="col-2">
							<label for="Cellulare">Cellulare</label> <input type="text"
								class="form-control" id="Cellulare">
						</div>
						<!-- Email input -->
						<div class="col">
							<label for="Fax">Indirizzo email</label> <input type="email"
								class="form-control" id="Fax">
						</div>
						<!-- Codice Fiscale input -->
						<div class="col-3">
							<label class="labelRequired"  for="CodiceFiscale">Codice Fiscale</label> <input
								type="text" class="form-control" id="CodiceFiscale" required>
						</div>
					</div>
					<!-- Fine Quinta row -->
					<br>
					<!-- Sesta row -->
					<div class="row">
						<!-- Domicilio textarea -->
						<div class="col">
							<label for="Email">Domicilio</label>
							<textarea class="form-control" id="Email" rows="3"></textarea>
						</div>
						<!-- Codice Regionale input -->
						<div class="col-3 custom-blu">
							<label for="CodiceRegionale">Codice Regionale</label> <input
								type="text" class="form-control" id="CodiceRegionale">
						</div>
					</div>
					<!-- Fine Sesta row -->
					<br>
					<!-- Settima row -->
					<div class="row">
						<!-- Medico Curante input -->
						<div class="col-3 custom-blu">
							<label for="MedicoCurante">Medico Curante</label> <input
								type="text" class="form-control" id="MedicoCurante">
						</div>
						<!-- Medico Curante Tel input -->
						<div class="col-3 custom-blu">
							<label for="MedicoCuranteTelefono">Medico Curante Telefono</label>
							<input type="text" class="form-control"
								id="MedicoCuranteTelefono">
						</div>
						<!-- Referenti Familiari input -->
						<div class="col-3 custom-blu">
							<label for="ReferentiFamiliari">Referenti Familiari</label> <input
								type="text" class="form-control " id="ReferentiFamiliari">
						</div>
						<!-- ISTAT Nascita input -->
						<div class="col">
							<label class="labelRequired" for="IstatNascita">ISTAT Nascita</label> <input
								type="text" class="form-control custom-yellow" id="IstatNascita"   required disabled>
						</div>
						<!-- ISTAT residenza input -->
						<div class="col">
							<label class="labelRequired" for="IstatResidenza">ISTAT Residenza</label> <input
								type="text" class="form-control custom-yellow" id="IstatResidenza"  required disabled>
						</div>
					</div>
					<!-- Fine Settima row -->
					<br>
					<!-- Ottava row -->
					<div class="row">
						<!-- Referenti Familiari input -->
						<div class="col custom-blu">
							<label for="ReferentiFamiliariTelefono">Referenti Familiari
								Telefono</label> <input type="text" class="form-control "
								id="ReferentiFamiliariTelefono">
						</div>
						<!-- Servizio ASL Riferimento input -->
						<div class="col custom-blu">
							<label for="ServizioASLRiferimento">Servizio ASL Riferimento</label>
							<input type="text" class="form-control "
								id="ServizioASLRiferimento">
						</div>
						<!-- Servizio ASL Riferimento Telefono input -->
						<div class="col custom-blu">
							<label for="ServizioASLRiferimentoTelefono">Servizio ASL
								Riferimento Telefono</label> <input type="text"
								class="form-control " id="ServizioASLRiferimentoTelefono">
						</div>
						<!-- Castale Nascita input -->
						<div class="col">
							<label class="labelRequired" for="CatastaleNascita">Catastale Nascita</label> <input
								type="text" class="form-control custom-yellow" id="CatastaleNascita" required disabled>
						</div>
					</div>
					<!-- Fine Ottava row -->
					<br>
					<!-- Nona row -->
					<div class="row">
						<!-- Servizio ASL Riferimento Fax input -->
						<div class="col custom-blu">
							<label for="ServizioASLRiferimentoFax">Servizio ASL Riferimento
								Fax</label> <input type="text" class="form-control "
								id="ServizioASLRiferimentoFax">
						</div>
						<!-- Condizioni Abitative input -->
						<div class="col custom-blu">
							<label for="CondizioniAbitative">Condizioni Abitative</label> <input
								type="text" class="form-control " id="CondizioniAbitative">
						</div>
						<!-- Condizioni Socio Economiche input -->
						<div class="col custom-blu">
							<label for="CondizioneSocioEconomica">Condizioni Socio Economiche</label>
							<input type="text" class="form-control "
								id="CondizioneSocioEconomica">
						</div>
						<!-- Comune di Nascita input -->
						<div class="col">
							<label class="labelRequired" for="comuneDiNascita">Comune di Nascita</label> <input
								type="text" class="form-control custom-yellow" id="comuneDiNascita" required disabled>
						</div>
					</div>
					<!-- Extended default form grid -->
				</div>
				<!--/.Panel 1-->
				<!--Panel 2-->
				<div class="tab-pane fade in show" id="cartClinica_p2"
					role="tabpanel">
					<!-- Extended default form grid -->
					<!-- Prima row -->
					<div class="row">
						<!-- Pratica di Sport pre_ricovero input -->
						<div class="col custom-blu">
							<label for="SportPreRicovero">Pratica di Sport pre_ricovero</label>
							<input type="text" class="form-control " id="SportPreRicovero">
						</div>
						<!-- Abitudini di vita (fumo, alcool, droghe..) input -->
						<div class="col custom-blu">
							<label for="AbitVita">Abitudini di vita (fumo, alcool, droghe..)</label>
							<input type="text" class="form-control " id="AbitVita">
						</div>
						<!-- Allergie input -->
						<div class="col custom-blu">
							<label for="Allergie">Allergie</label> <input type="text"
								class="form-control " id="Allergie">
						</div>
					</div>
					<!-- Fine Prima row -->
					<br>
					<!-- Seconda row -->
					<div class="row">
						<!-- Menarca input -->
						<div class="col custom-blu">
							<label for="Menarca">Menarca</label> <input type="text"
								class="form-control req" id="Menarca">
						</div>
						<!-- Gravidanza input -->
						<div class="col custom-blu">
							<label for="Gravidanza">Gravidanza</label> <input type="text"
								class="form-control " id="Gravidanza">
						</div>
						<!-- Menopausa input -->
						<div class="col custom-blu">
							<label for="Menopausa">Menopausa</label> <input type="text"
								class="form-control " id="Menopausa">
						</div>
					</div>
					<!-- Fine Seconda row -->
					<br>
					<!-- Terza row -->
					<div class="row">
						<!-- ANAMNESI FAMILIARE textarea -->
						<div class="col custom-blu">
							<label for="AnamnesiFamiliare">ANAMNESI FAMILIARE</label>
							<textarea class="form-control" id="AnamnesiFamiliare" rows="3"></textarea>
						</div>
					</div>
					<!-- Fine Terza row -->
					<br>
					<!-- Quarta row -->
					<div class="row">
						<!-- ANAMNESI FISIOLOGICA textarea -->
						<div class="col custom-blu">
							<label for="AnamnesiFisiologica">ANAMNESI FISIOLOGICA</label>
							<textarea class="form-control" id="AnamnesiFisiologica" rows="3"></textarea>
						</div>
					</div>
					<!-- Fine Quarta row -->
					<br>
					<!-- Quinta row -->
					<div class="row">
						<!-- ANAMNESI PATOLOGICA REMOTA textarea -->
						<div class="col custom-blu">
							<label for="AnamnesiPatologicaRemota">ANAMNESI PATOLOGICA REMOTA</label>
							<textarea class="form-control" id="AnamnesiPatologicaRemota"
								rows="3"></textarea>
						</div>
					</div>
					<!-- Fine Quinta row -->
					<!-- Extended default form grid -->
				</div>
			</div>
	</section>	
	</main>
	<!--Lista div Modal-->
	<div id="modalElimina"></div>
	<!--Fine Lista div Modal-->
	<!--Main Layout-->
</body>
<!--  SCRIPTS  -->
<!-- JQuery -->


<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.12.1.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Select 2 Js -->
<script type="text/javascript" src="../js/select2.js"></script>
<!-- Custom JS-->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- Modal JS -->
<script type="text/javascript" src="../jsCustom/modal.js"></script>
<!-- Navigation JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- JQGrid JS -->
<script	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="js/JQGrid_anagrafica.js"></script>
<script type="text/javascript" src="js/codiceFiscale.js"></script>
<!-- Autocomplete Address JS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3kJ9VZiRc-C55SsrkshBbQG4gzwD1E2M&libraries=places&callback=initAutocomplete&language=it-IT"
        async defer></script>
<script type="text/javascript" src="../js/google.js"></script>



<script>
$(document).ready(function () {


	initAutocomplete();	

});


$('#spinner').LoadingOverlay("show");
var queryPaziente1;
new WOW().init(); 
//initAutocomplete('LuogoDiNascita');

var baseUrl = '<?php echo $baseurl;?>';
//Inizializzo la NavBar

initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
//Aggiorno sulla NavBar il nome utente loggato
$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');

$(document).ready(function () {

	var dncap = $('#DNCAP').val();

	if (dncap.length!=6){

		for(var i = dncap.length; i<5; i++){
			$('#DNCAP').val("0"+$('#DNCAP').val());
		}
		
	}


	var rescap = $('#ResCAP').val();

	if (rescap.length!=6){

		for(var i = rescap.length; i<5; i++){
			$('#ResCAP').val("0"+$('#ResCAP').val());
		}
		
	}
	
	
	$('.collapsible-body').css({
		"display" : "none"
	});
	$('#anagraficaMenuBody').css({
		"display" : "block"
	});
	$('[data-toggle="tooltip"]').tooltip();

    HTMLTOOLvalidateTextNumber('anagrafico');
    hideComponent('spinner');
	showComponent('completed');
	$('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 150, // Creates a dropdown of 50 years to control year
        min: new Date(1900,1,20),
        max: true,
        format: 'dd/mm/yyyy',
        onClose: function() {
    		setDataEta(),
    		$('#CodiceFiscale').val(codiceFiscale());  		
    	  }
        //formatSubmit: 'dd/mm/aaaa'
    });
    $('#spinner').LoadingOverlay("hide");
    //compila in automatico i dati di nascita


	$('#DataDiNascita').css('background-color', '#e0f5c7');
   	 
});
var act=<?php echo $action;?>;
var centro = '<?php echo $_SESSION['cod_centro'];?>';
var struttura = '<?php echo $_SESSION['cod_struttura'];?>';

//costruisco select da tabelle

var json_codMuni = <?php echo selectAll('codmuni','CodMuni,Id');?>;


var json_catProtetta= <?php echo selectAll('catProtette','Id,CatProtette as CatProtetta');?>;
var json_titoloStudio= <?php echo selectAll('titolostudio','Id,TitoloStudio');?>;
var json_occupazione= <?php echo selectAll('occupazione','Id,Occupazione');?>;
var json_statoCivile= <?php echo selectAll('statocivile','Id,StatoCivile as StatoCiv');?>;
var json_TutLegale= <?php echo selectAll('tutelalegale','Id,TutelaLegale as Tutlegale');?>;
var json_ASL= <?php echo selectAll('asl','Id,ASL');?>;

//CodMuni
creaOption_rug('CodMuni', json_codMuni,'CodMuni');

//Popolo le select
creaOption('campi', json_catProtetta);
creaOption('campi', json_titoloStudio);
creaOption('campi', json_occupazione);
creaOption('campi', json_statoCivile);
creaOption('campi', json_TutLegale);
creaOption('campi', json_ASL);

			
if (act=='1'){
    //$.LoadingOverlay("show");
    showComponent('eliminaBtn');
    showComponent('modificaBtn');
    hideComponent('mostraAnagr');
    showComponent('cercaPrBtn');
    //query del paziente selezionato
    //valorizza i campi di anagrafica.php
    var queryPaziente='<?php echo $queryPaziente;?>';
    queryPaziente = sitemaApostrofo(queryPaziente);

    //csv read
   //var csvISTAT= '<?php //echo getISTATcsv('../csv/istat_31_03_2018.csv');?>';
   //console.log(csvISTAT); 
    queryPaziente1=jQuery.parseJSON(queryPaziente);
    if (queryPaziente!==''){
        //valorizza sezione nominativo
        riempiElementi('rowNominativo', 'input', queryPaziente1[0],'1');
        //valorizza tutte le input
         riempiElementi('campi', 'input', queryPaziente1[0],'1');
        //valorizza tutte le select
        riempiElementi('campi', 'select', queryPaziente1[0],'1');
        ////valorizza tutte le textarea
        riempiElementi('campi', 'textarea', queryPaziente1[0],'1');
        //format data
       setDataEta();
       bloccaCampi();
       showComponent('modificaBtn');
       hideComponent('salvaBtn');
        //pushElementi('campi',queryPaziente1);
    }			
	//$.LoadingOverlay("hide");
	//showComponent('gridPazienti');
}else if (act='2'){
	$('#Centro').val(centro);
	$('#CodiceStruttura').val(struttura);
	sbloccaCampi();
	hideComponent('progettoNew');
}

//Gestione google.js Indirizzo

$("#IndirizzoResidVia").on('focusin', function(e) {
    	object=1;
    	$('#IndirizzoResidVia').val(''); 
    	$('#IndirizzoResidNVia').val('');
    	$('#luogoResidenza').val(''); 
    	$('#Zona').val('');     
    	$('#ResProv').val(''); 
    	$('#ResCAP').val(''); 
    	$('#IstatResidenza').val(''); 
});
$("#LuogoDiNascita").on('focusin', function(e) {
	object=2;
	$('#LuogoDiNascita').val(''); 
	$('#DNProv').val('');    
	$('#DNCAP').val(''); 
	$('#comuneDiNascita').val(''); 
	$('#CatastaleNascita').val(''); 
	$('#IstatNascita').val(''); 
});


//Focus cod. fiscale

 $('#Nome').blur(function() {
			
	$('#CodiceFiscale').val(codiceFiscale());
});
$('#Cognome').blur(function() {
	
	$('#CodiceFiscale').val(codiceFiscale());
});
$('#Sesso').blur(function() {
	
	$('#CodiceFiscale').val(codiceFiscale());
});
$('#eta').blur(function() {
	
	$('#CodiceFiscale').val(codiceFiscale());
});

$('#LuogoDiNascita').blur(function() {
	
	$('#CodiceFiscale').val(codiceFiscale());
});
$('#DNProv').blur(function() {
	
	$('#CodiceFiscale').val(codiceFiscale());
});
$('#luogoResidenza').blur(function() {

	VisualizzaIstat('IstatResidenza',$('#luogoResidenza').val());	
});

$('#LuogoDiNascita').blur(function() {

	VisualizzaIstat('IstatNascita',$('#LuogoDiNascita').val());	
}); 

function riportaDatiResidenza(){
	$('#DNCAP').val($('#ResCAP').val());
	$('#DNProv').val($('#ResProv').val());
	$('#LuogoDiNascita').val($('#luogoResidenza').val());
	
	VisualizzaIstat('IstatNascita',$('#LuogoDiNascita').val());	
	
	
}

</script>