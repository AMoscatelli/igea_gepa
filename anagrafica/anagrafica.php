<?php
// Da includere per la gestione della sessione

include ('../php/session.php');
include ('../php/controller.php');
include ('../php/config.inc.php');


$action=0;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $source = $_POST['buttonPr'];
    switch ($source) {
        case 'nuovo':
            $action = 1;
            break;
        case 'cerca':
            $action = 2;
            break;
        case 'anagr':
            $action = 3;
            break;
    }
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if(isset($_GET['source'])){
        $source = $_GET['source'];
        switch ($source) {
            case 'cerca':
                $action = 2;
                break;
        }
    }
    elseif(isset($_GET['success'])){
        if( $_GET['success']=='true'){
            //operazione ok
            $action=4;
           
        }else{
            //operazione ko
            $action=5;
            
            }
        
    }
}

?>
<html>

<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>Igea - page name</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="../css/jquery-ui.min.css">
<link rel="stylesheet"
	href="../css/ui.jqgrid.min.css">
	
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="../css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="../css/style.css" rel="stylesheet">
</head>
<body>
	<!--Main Navigation-->
	<header>
	    <!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
    <!--/Main Navigation-->
	<div id="spinner" style='height: 100%; width: 100%'></div>
    <div class="container" id="completed" style="visibility: hidden;">
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn">
			<div class="card card-cascade wider reverse" id="rowButton">
				<div class="card-body text-center">
					<!--Sezione bottoni shares-->
					<form method="post" id="dati">
						<button type="button" name="buttonPr" value="anagr"
							class="btn btn-default" id="mostraAnagr" style="display: none"
							onclick="mostraDati(baseUrl);">
							<i class="fa fa-eye left"> MOSTRA ANAG.</i>
						</button>
						<button type="button" name="buttonPr" value="nuovo"
							class="btn btn-default" id="nuovoPrBtn"
							onclick="nuovoPaziente()">
							<i class="fa fa-plus left"> NUOVO PAZIENTE</i>
						</button>
						<button type="submit" name="buttonPr" value="cerca"
							class="btn btn-default" id="cercaPrBtn">
							<i class="fa fa-search left "> CERCA PAZIENTE</i>
						</button>
						<button type="button" class="btn btn-default" id="salvaBtn"
							onclick="" style="display: none">
							<i class="fa fa-save left"> SALVA</i>
						</button>
						<button type="button" class="btn btn-default" id="eliminaBtn"
							onclick="eliminaPazienteFromGrid();" style="display: none">
							<i class="fa fa-trash left"> ELIMINA</i>
						</button>
						<button type="button" class="btn btn-default" id="modificaBtn"
							onclick="" style="display: none">
							<i class="fa fa-edit left"> MODIFICA</i>
						</button>
					</form>
				</div>
				<!--Post data-->
			</div>
		</section>
		<!--Section: GRID pazienti-->
		<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="gridPazienti" style="display:none">
    		<table id="gridAnagrPazienti">
    			<div id="jqGridAnagrPazientiPager"></div>
    		</table>
		</section>
		<!--Section: /GRID pazienti-->
	</div>
	<!--Lista div Modal-->
	<div id="modalElimina"></div>
	<div id="modalSuccess"></div>
	<!--Fine Lista div Modal-->
</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Custom JS-->
<script type="text/javascript" src="../jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="../jsCustom/dateUtil.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="../js/loadingoverlay.min.js"></script>
<!-- DB JS -->
<script type="text/javascript" src="../jsCustom/DB.js"></script>
<!-- Modal JS -->
<script type="text/javascript" src="../jsCustom/modal.js"></script>
<!-- Navigation JS -->
<script type="text/javascript" src="../jsCustom/navigationSideBar.js"></script>
<!-- JQGrid JS -->
<script	src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.2/jquery.jqgrid.min.js"></script>
<script type="text/javascript" src="js/JQGrid_anagrafica.js"></script>
<script>
	$('#spinner').LoadingOverlay("show");
	$(document).ready(function () {
		$('.collapsible-body').css({
    		"display" : "none"
    	});
    	$('#anagraficaMenuBody').css({
    		"display" : "block"
    	});
		hideComponent('spinner');
		showComponent('completed');
    	$('.mdb-select').material_select();
    	$('#spinner').LoadingOverlay("hide");      	
    });
    
	new WOW().init();
	var baseUrl = '<?php echo $baseurl;?>';
	//Inizializzo la NavBar
	//initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>');
	initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');
	//Aggiorno sulla NavBar il nome utente loggato
	$('#loginUser').html('Benvenuto <?php echo $_SESSION['login_user'];?>');
    var action = <?php echo $action;?>;  
   	if(action == 0){
        	
    } else if (action == 1) {
			
        	
    } else if (action == 2) {
		//Cerca un paziente dalla griglia
    	showComponent('eliminaBtn');
    	hideComponent('modificaBtn');
    	showComponent('mostraAnagr');
    	hideComponent('cercaPrBtn');
    	var obj = <?php if($action==2){echo selectAll('anagrafico','CodAnagr,Nome,Cognome,CodiceFiscale');}else{echo "0";}?>;
    	obj = sitemaApostrofo(obj);
		JQGRIDinitAnagraficaGrid(obj, '#gridAnagrPazienti', $("#rowButton").innerWidth());
		showComponent('gridPazienti');
    } else if (action == 3) {
		//Mostra l'anagrafica del paziente
     	//$.LoadingOverlay("show");
     	showComponent('eliminaBtn');
     	hideComponent('modificaBtn');
     	hideComponent('mostraAnagr');
     	showComponent('cercaPrBtn');
     	var codAnagr = <?php if($action==3){echo cercaPaziente('anagrafico','CodAnagr,Nome,Cognome','CodAnagr',$query);}else{echo "0";}?>;
 		//JQGRIDinitAnagraficaGrid(obj, '#gridAnagrPazienti', $("#rowButton").innerWidth());
 		//var data = JSON.stringify(results);
 		$.ajax({
       	 		type: 'post',
        		url: '../php/controller.php',
        		data: { 
             			'source': 'inviaDati',
             			'dati': data
        		},
        		success: function (response) {
        			//$.LoadingOverlay("hide");
            		window.location="../anagrafica/anagrafica_detail.php";
        		},
        		error: function () {
        			toastr.warning('Attenzione si � verificato un Errore !');
        		}
		});
 	} else if (action==4){
		//mostra toast dopo operazione ok
 		 toastr.success('Operazione avvenuta con successo');
		
 	}else if (action==5){
 		//mostra toast dopo operazione ko
 		toastr.warning('Errore');
 	}



  	

</script>

