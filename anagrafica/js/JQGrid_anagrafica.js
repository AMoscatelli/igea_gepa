var obj = [];
var rowData_old = 0;
var rowData;
var p = 0;
var action = 2;

// JQGrid Anagrafica --> Pazienti
function JQGRIDinitAnagraficaGrid(obj, GridName, width) {
	$grid = $(GridName);

	var click_count = 0;

	$(GridName).jqGrid('GridUnload');
	// "use strict";
	$(GridName).jqGrid({
		colModel : [ {
			name : "CodAnagr",
			label : "Codice Anagrafica",
			align : "center",
			searchoptions : {
				// show search options
				sopt : [ "eq" ]
			// ge = greater or equal to, le = less or equal to, eq = equal to
			}
		}, {
			name : "Nome",
			label : "Nome",
			align : "center"
		}, {
			name : "Cognome",
			label : "Cognome",
			align : "center"
		}, {
			name : "CodiceFiscale",
			label : "Codice Fiscale",
			align : "center"
		}, ],
		rowNum : 30,
		height : "auto",
		pager : "#jqGridAnagrPazientiPager",
		width : width,
		iconSet : "fontAwesome",
		caption : "Cerca:",
		idPrefix : "g1_",
		data : obj,
		rownumbers : true,
		sortname : "CodAnagr",
		sortorder : "asc",
		caption : "Selezionare il paziente desiderato",
		ignoreCase : true,
		onSelectRow : function(rowid) {

			rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;

			if (rowData_old == rowData) {

				mostraDati(baseUrl);
				rowData_old = 0;

				// $('.mdb-select').material_select();

			} else {

				click_count = 0;

				rowData_old = rowData;

			}
		}

	});

	$grid.jqGrid("navGrid", "#packagePager", {
		add : false,
		edit : false,
		del : false
	}, {}, {}, {}, {
		multipleSearch : true,
		multipleGroup : true
	});
	$grid.jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false,
		defaultSearch : "cn"
	});
	$('#jqGridAnagrPazientiPager').css({
		"height" : "35px"
	});
}

function creaOption_rug(componentID, json, opzione) {

	var component = document.getElementById(componentID);

	var option = document.createElement("option");
	option.value = "Vuoto";
	option.text = "Seleziona";
	component.add(option);

	for (var i = json.length - 1; i >= 0; i--) {
		option = document.createElement("option");

		switch (opzione) {
		case "CodMuni":
			option.text = json[i].CodMuni;
			option.value = json[i].Id;
			break;
		case "CODICE ISTAT":
			// var codIstat='Codice ISTAT';
			option.text = json[i]['COMUNE'];
			option.value = json[i]['CODICE ISTAT'];
			break;
		case "citta":
			option.text = json[i].comune;
			option.value = json[i].comune;
			break;
		case "IndirizzoResidVia":
			option.text = json[i].IndirizzoResidVia;
			option.value = json[i].IndirizzoResidVia;
			break;
		// case "Intervento":
		// option.text = json[i].Intervento;
		// option.value = json[i].Intervento;
		// break;
		// case "RegAssist":
		// option.text = json[i].RegAssist;
		// option.value = json[i].RegAssist;
		// break;
		// case "ModInt":
		// option.text = json[i].ModalitaIntervento;
		// option.value = json[i].ModalitaIntervento;
		// break;
		// case "ImpRiab":
		// option.text = json[i].ImpRiab;
		// option.value = json[i].ImpRiab;
		// break;
		// case "NomeCognome":
		// option.text = json[i].Nome_Cognome;
		// option.value = json[i].Nome_Cognome;
		// break;
		// case "NoTag":
		// option.text = json[i];
		// break;
		}

		// option.value = i;

		component.add(option);
	}
}

// }

/*
 * function cercaAll(data, location) {
 * 
 * $.ajax({ type : 'post', url : '../php/controller.php', data : { 'source' :
 * 'mandaDati', 'dati' : data }, success : function(response) { window.location =
 * location; }, error : function() { alert("error"); } }); }
 */

// tasto modifica in anagrafica_detail che abilita la modifica dei campi
// function modificaCampi(elemento) {
function modificaCampi(sectionID) {
	action = 1;

	// abilica Onclick dati nascita= residenza
	$('#compilaNascita').on('click', function(e) {
		e.preventDefault();
		compila();
		return false;
	});

	$('#' + sectionID + ' input').each(function() {
		var campo = this.id;
		if (campo !== "") {
			if (campo == "CodAnagr" | campo == "eta") {
				// non faccio nulla perchè devono rimanere disabilitate
			} else {
				$('#' + campo).prop('disabled', false);
			}
		}
	});

	$('#' + sectionID + ' select').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	$('#' + sectionID + ' textarea').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	/*
	 * $('#' + elemento + ' input').each(function() { if (this.id !== "") {
	 * $("#" + this.id).prop("disabled", false); } });
	 * $("#CodAnagr").prop("disabled", true); $("#eta").prop("disabled", true);
	 * $('#' + elemento + ' textarea').each(function() { if (this.id !== "") {
	 * $("#" + this.id).prop("disabled", false); } }); $('#' + elemento + '
	 * select').each(function() { if (this.id !== "") { $("#" +
	 * this.id).prop("disabled", false); } });
	 */

	// reinizializzo le select!
	$('.mdb-select').material_select('destroy');

	$('.mdb-select').material_select();

	showComponent('salvaBtn');
	hideComponent('modificaBtn');
	showComponent('compilaNascita');
	showComponent('compilaNascita1');

}

// tasto torna anagrafica (cerca paziente) in anagrafica_detail
function tornaAnagrafica() {

	var newURL = baseUrl + "/anagrafica/anagrafica.php?source=cerca";
	location.href = newURL;

}

// funzione che riabilita la modalità di visualizzazione
function visualizzaCampi(elemento, action) {
	if (action == '2') {

		$('#rowNominativo input').each(function() {
			if (this.id !== "") {
				$("#" + this.id).prop("disabled", true);
			}
		});
	}

	$('#' + elemento + ' input').each(function() {
		if (this.id !== "") {
			$("#" + this.id).prop("disabled", true);
		}
	});
	$('#' + elemento + ' select').each(function() {
		if (this.id !== "") {
			$("#" + this.id).prop("disabled", true);
		}
	});
	// $("#CodAnagr").prop("readonly", true);
	$('#' + elemento + ' textarea').each(function() {
		if (this.id !== "") {
			$("#" + this.id).prop("disabled", true);
		}
	});
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
	hideComponent('salvaBtn');
	showComponent('modificaBtn');
	hideComponent('compilaNascita1');
	hideComponent('compilaNascita');
}

function sitemaApostrofo(obj1) {
	obj2 = JSON.stringify(obj1);
	obj2 = obj2.replace(/ó/g, "'");
	obj2 = JSON.parse(obj2);
	return obj2;
}

// funzione per il recupero dei valori di Anagrafica (tasto SALVA)
function recuperaElementiAnagr(elemento, action) {
	var result = {};
	var nome;
	var cognome;
	var action = action;

	// var vecchioPr = $('#vecchioPr').val();

	// if (vecchioPr=="1"){
	// action="3";
	// }

	// recupero il nomitavo del paziente
	$('#rowNominativo input').each(function() {
		if (this.id == "nominativo_cognome") {
			cognome = this.value;
			if (action == '2') {
				if (this.value.includes("'")) {
					result[this.id] = this.value.replace(/'/g, 'ó');
					;
				} else {
					result[this.id] = this.value;
				}
			}
		} else {
			nome = this.value;
			if (action == '2') {
				if (this.value.includes("'")) {
					result[this.id] = this.value.replace(/'/g, 'ó');
					;
				} else {
					result[this.id] = this.value;
				}
			}
		}
	});

	// per ogni elemento input recupero il valore (dove non è vuoto)
	$('#' + elemento + ' input').each(function() {
		var campo = this.id;
		if (campo != "" & this.value != "" & campo != "eta") {
			// results.push({
			// id : campo,
			// value : this.value
			if (campo.includes("'")) {
				result[this.id] = this.value.replace(/'/g, 'ó');
				;
			} else {
				result[this.id] = this.value;
			}

			// });
		}
		if (campo !== "" & this.value !== "") {
			if ($("#" + this.id).attr("class").includes("dateFormat")) {
				result[this.id] = formatISODate(this.value);
			}
		}
	});

	// per ogni elemento select recupero il valore (dove non è vuoto)
	$('#' + elemento + ' select').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result[this.id] = this.value;
			// });
		}
	});

	// per ogni elemento textarea recupero il valore (dove non è vuoto)
	$('#' + elemento + ' textarea').each(function() {
		var campo = this.id
		if (campo !== "" & this.value !== "") {
			// results.push({
			// id : campo,
			// value : this.value
			result[this.id] = this.value;
			// });
		}
	});

	// return result;
	// popolo la variabile result con i valori dei campi non vuoti;
	// var data = JSON.stringify(result);

	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : 'updatePaziente',
			'tableName' : 'anagrafico',
			'jsonData' : result,
			'action' : action,
			'codAnagr' : $('#CodAnagr').val(),
		},
		success : function(response) {
			console.log(response)
			if (action == '2') {
				$('#CodAnagr').val(response);
			}
			setDataEta();
			$.LoadingOverlay("hide");
			toastr.success('Salvataggio avvenuto con successo!');
			// window.location = "../progetto/progetto_sez" + sezioneGoTo
			// + ".php";
		},
		error : function() {
			$.LoadingOverlay("hide");
			toastr.error('Attenzione si &egrave; verificato un errore!!!');
		}
	});

}

// restituisce true se almeno un campo della row nominativo non è valorizzato
function valida(sectionID) {
	var a = false;
	sectionID_n = sectionID.split(",");
	for (i = 0; i < sectionID_n.length; i++) {
		$('#' + sectionID_n[i] + ' input').each(
				function() {
					var campo = this.id;
					if (campo !== "") {
						if ($('#' + campo).prop('required')) {
							console.log(campo);
							if ($('#' + campo).val() == ""
									| $('#' + campo).val() == null) {
								a = true;
							}
						}
					}

				});
		$('#' + sectionID_n[i] + ' select').each(
				function() {
					var campo = this.id;
					if (campo !== "") {
						if ($('#' + campo).prop('required')) {
							console.log(campo);
							if ($('#' + campo).val() == ""
									| $('#' + campo).val() == null) {
								a = true;
							}
						}
					}

				});
	}

	return a;
}

// tasto salva
function salvaAnagrafica() {

	// $.LoadingOverlay("show");

	if (!valida('rowNominativo,campi')) {
		// recupero elementi e azione di salva
		recuperaElementiAnagr('campi', action);
		// mostra bottone modifica e nascondi salva
		visualizzaCampi('campi', action);
		// $("#test").removeAttr("onclick");
		bloccaCampi();
	} else {
		// $.LoadingOverlay("hide");
		toastr.error('E\' necessario completare i campi obbligatori!');
	}
}

function setDataEta() {
	// format data di nascita
	// var old_date = $('#DataDiNascita').val();

	// var new_date = formatCustomHTMLDate(old_date, '-');
	// $('#DataDiNascita').val(new_date);
	// ricava età
	// var data= $('#DataDiNascita').val();

	var data1 = formatISODate($('#DataDiNascita').val());
	var data = data1.replace(new RegExp('/', 'g'), '-');
	var birthDate = new Date(data);
	var ageDifMs = Date.now() - birthDate.getTime();
	var ageDate = new Date(ageDifMs); // miliseconds from epoch
	var eta = Math.abs(ageDate.getUTCFullYear() - 1970);
	$('#eta').val(eta);

	if (eta < 18) {

		$('#Tutlegale').val('Seleziona');
		$('#Tutlegale').prop('disabled', 'disabled');
		$('#Tutlegale').prop('required', false);
		$('#labelTutelaLegale').removeClass('labelRequired');
		$('.mdb-select').material_select('destroy');

		$('.mdb-select').material_select();

	} else {
		$('#Tutlegale').prop('disabled', false);
		$('#Tutlegale').prop('required', 'required');
		$('#labelTutelaLegale').addClass('labelRequired');
		$('.mdb-select').material_select('destroy');

		$('.mdb-select').material_select();
	}

}

function nuovoPaziente() {
	var newURL = baseUrl + "/anagrafica/anagrafica_detail.php?new=true";
	location.href = newURL;
}

// Funzione che mostra il dettaglio del paziente selezionato
function mostraDati() {

	// controllo
	if (rowData_old !== 0) {

		var newURL = baseUrl + "/anagrafica/anagrafica_detail.php?CodAnagr="
				+ rowData_old.CodAnagr;
		location.href = newURL;

	} else {
		toastr.warning('Non hai selezionato alcun Paziente!');
	}

}
// attiva la modal per eliminare un paziente
function modalEliminaPaziente() {
	doModal("modalElimina", "Paziente " + $('#Cognome').val() + " "
			+ $('#Nome').val(), "Sei sicuro di voler eliminare il paziente "
			+ $('#Cognome').val() + " " + $('#Nome').val() + "?",
			"eliminaPaziente(" + $("#CodAnagr").val() + ")", "Elimina", 1,
			"Annulla", 4);
}
function eliminaPaziente(codAnagr) {

	$.ajax({
		type : 'post',
		url : '../php/controller.php',
		data : {
			'source' : 'eliminaPaziente',
			'tableName' : 'anagrafico',
			'action' : action,
			'codAnagr' : codAnagr,
		},
		success : function(response) {
			var newURL = baseUrl + "/anagrafica/anagrafica.php?success=true";
			location.href = newURL;
			// window.location = "../progetto/progetto_sez" + sezioneGoTo
			// + ".php";
		},
		error : function() {
			var newURL = baseUrl + "/anagrafica/anagrafica.php?success=false";
			location.href = newURL;
		}
	});
}

function eliminaPazienteFromGrid() {
	if (rowData != undefined) {
		doModal("modalElimina", "Paziente " + rowData_old.Nome + " "
				+ rowData_old.Cognome,
				"Sei sicuro di voler eliminare il paziente " + rowData_old.Nome
						+ " " + rowData_old.Cognome + "?", "eliminaPaziente("
						+ rowData_old.CodAnagr + ")", "Elimina", 1, "Annulla",
				4);
	} else {
		toastr.warning('Non hai selezionato alcun Paziente!');
	}
}

// visualizza codice istat a partire dalla residenza
function VisualizzaIstat(input, data) {
	$.ajax({
		type : "GET",
		url : "../csv/istat_31_03_2018.csv",
		dataType : "text",
		success : function(response) {

			var array = response.split('\n');
			controllo = 0;
			for (i = 0; i < array.length; i++) {
				temp = array[i].split(';');
				if (data == temp[5]) {
					$('#' + input).val(temp[4]);
					i = array.length - 1;
					controllo = 1;
					if (input == 'IstatNascita') {
						$('#CatastaleNascita').val(temp[18]);
						$('#comuneDiNascita').val(temp[5]);
					}
				}
			}
			if (controllo == 0) {
				toastr.warning(input + ' Errato. Reinserirlo!');
			}
		},
		error : function(request, status, error) {
			alert(request.responseText);
		}
	});
}

function compila() {
	$('#LuogoDiNascita').val($('#luogoResidenza').val());
	$('#DNProv').val($('#ResProv').val());
	$('#DNCAP').val($('#ResCAP').val());
	VisualizzaIstat('IstatNascita', $('#LuogoDiNascita').val());
}

function vaiProgetto() {

	var url = "../progetto/progetto_sezA.php?codAnag=" + $('#CodAnagr').val()
			+ "&nome=" + $('#Nome').val() + "&cognome=" + $('#Cognome').val();
	window.location.href = url;
}

function sbloccaCampi() {

	$('#campi input').each(
			function() {
				var campo = this.id;
				if (campo !== "") {
					if (campo == "CodAnagr" | campo == "Centro"
							| campo == "CodiceStruttura"
							| campo == "luogoResidenza" | campo == "eta"
							| campo == "IstatNascita"
							| campo == "IstatResidenza"
							| campo == "CatastaleNascita"
							| campo == "comuneDiNascita") {
						// non faccio nulla perchè devono rimanere disabilitate
					} else {
						$('#' + campo).prop('disabled', false);
					}
				}
			});

	$('#rowNominativo input').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	$('#campi select').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	$('#campi textarea').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', false);
		}
	});

	hideComponent('modificaBtn');
	showComponent('salvaBtn');
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
	setDataEta();
}

function bloccaCampi() {

	$('#campi input').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', 'disabled');
		}
	});

	$('#rowNominativo input').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', 'disabled');
		}
	});

	$('#campi select').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', 'disabled');
		}
	});

	$('#campi textarea').each(function() {
		var campo = this.id;
		if (campo !== "") {
			$('#' + campo).prop('disabled', 'disabled');
		}
	});

	hideComponent('modificaBtn');
	showComponent('salvaBtn');
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
}
