<?php 
//Da includere per la gestione della sessione
//include('php/config.php');
include('php/session.php');
include('php/config.inc.php');
include('php/function.php');
$index_result = 9;

if(isset($_GET['result'])){
    $index_result = $_GET['result'];
}

if($_SESSION['login_role']=='1'){}

?>
<html>
   
   <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Igea - HOME</title>
    
    <!-- JQGrid CSS -->
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/ui.jqgrid.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Custom Css Select -->
<link href="css/style.css" rel="stylesheet">
<!-- Calendar -->
<link href='calendar/core/main.css' rel='stylesheet' />
<link href='calendar/bootstrap/main.css' rel='stylesheet' />
<link href='calendar/timegrid/main.css' rel='stylesheet' />
<link href='calendar/daygrid/main.css' rel='stylesheet' />
<link href='calendar/list/main.css' rel='stylesheet' />
	
<style>

.select-wrapper input.select-dropdown {
    margin: 6px 9px 15px;
    border-bottom-width: 1px;
}

#legend{
    margin-left: 20%;
    margin-right: 20%;
    }

#chartdiv {
  width: 100%;
  height: 400px;
}

#chartdiv1 {
  width: 100%;
  height: 400px;
}

#chartdiv2 {
  width: 100%;
  height: 400px;
}



    .side-nav .logo-sn {
    padding-bottom: 1rem;
    padding-top: 1rem;
}

.side-nav .logo-sn img {
    height: 38px;
}

.side-nav .search-form input[type=text] {
    margin-top: 0;
    padding-top: 12px;
    padding-bottom: 12px;
    border-top: 1px solid rgba(255, 255, 255, 0.3);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}

body {
    background-color: #eee;
}

.accordion .card {
    margin-bottom: 1.2rem;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}

.accordion .card .card-body {
    border-top: 1px solid #eee;
}

</style>

</head>


<body>

    <!--Main Navigation-->
	<header>
		<!-- Standard TOP Nav BAR -->
	    <!--  <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>-->
	    <div id="navBar"></div>
	</header>
    <!--/Main Navigation-->

    <!--Main layout-->
    <main>
      <div class="container mt-5" id="container">

        <!--Section: Main panel-->
        <section class="card card-cascade narrower mb-5" id="gradimento" style="display:none">
        
        <div id="modal"></div>

          <!--Grid row-->
          <div class="row">

            <!--Grid column-->
            <div class="col-md-5">

              <!--Panel Header-->
              <div class="view view-cascade py-3 gradient-card-header info-color-dark">
                <h5 class="mb-0">% Gradimento Totale</h5>
              </div>
              <!--/Panel Header-->

              <!--Panel content-->
              <div class="card-body">
                <!--Grid row-->
                <div class="row">
                  <!--Grid column-->
                  <div class="col-md-12 text-center">
                    <!--Change chart-->
                    <div id="indicatorContainer" class="mt-4"></div>
                  </div>
                  <!--Grid column-->
                </div>
                <!--Grid row-->
              </div>
              <!--Panel content-->
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-5">
              <!--Panel Header-->
                <div id="chartdiv"></div>
              <!--/Card image-->
            </div>
            <!--Grid column-->
            <div class="col-md-2 align-middle">
            
			

			<div class="align-middle mt-5">
				<img class="align-middle" src="images/verygood.png" width="30px"> - Ottimo
			</div>
			<div class="align-middle mt-2">
				<img class="align-middle" src="images/good.png" width="30px"> - Buono
			</div>
			<div class="align-middle mt-2">
				<img class="align-middle" src="images/neutral.png" width="30px"> - Sufficiente
			</div>
			<div  class="align-middle mt-2">
				<img class="align-middle" src="images/bad.png" width="30px"> - Mediocre
			</div>
			<div class="align-middle mt-2">
				<img class="align-middle" src="images/verybad.png" width="30px"> - Scarso
			</div>

			

          
            </div>
          </div>
          <!--Grid row-->

        </section>
        <!--Section: Main panel-->
        
        <!--Section: Main panel-->
        <section class="card card-cascade narrower mt-3 mb-5" id="charts" style="display:none">

          <!--Grid row-->
          <div class="row">

            <!--Grid column-->
            <div class="col-md-4">

              <!--Panel Header-->
              <div class="view view-cascade py-3 gradient-card-header info-color-dark">
                <h5 class="mb-0">Dettaglio Sondaggi</h5>
              </div>
              <!--/Panel Header-->

              <!--Panel content-->
              <div class="card-body">

                <!--Grid row-->
                <div class="row">
                 <!--Grid column-->
                  <div class="col mb-4 mt-4">
                  <div>
                        <!--Blue select-->
                        <select class="mdb-select md-form colorful-select dropdown-primary" id="idStruttura"></select>
                        <label class="mdb-main-label">Struttura:</label>
                        <!--/Blue select-->
                       </div>
                  </div>
                  <!--Grid column-->
                 </div>
                <div class="row ml-2">

                  <!--Grid column-->
                  <div class="col-md-6 mb-1">
                    <!--Date pickers-->
                    <div class="md-form">
                      <input placeholder="Seleziona data" type="text" id="dateStart" class="form-control datepicker">
                      <label for="dateStart">Da:</label>
                    </div>
                  </div>
                  <!--Grid column-->
	               <!--Grid column-->
                  <div class="col-md-6 mb-4">
                    <div class="md-form">
                      <input placeholder="Seleziona data" type="text" id="dateEnd" class="form-control datepicker">
                      <label for="date-picker-example">A:</label>
                    </div>
                  </div>
                  <!--Grid column-->
                </div>
                <!--Grid row-->
                
                <!-- Grid row -->
                <div class="row d-flex justify-content-end">
                	<div>
                	<a type="button" class="btn-floating light-blue" id="cercaStat" data-toggle="tooltip" data-placement="top" title="Cerca"><i class="fas fa-search" aria-hidden="true"></i></a>
                	</div>
                	<div>
                	<a type="button" class="btn-floating light-blue" id="resetStat" data-toggle="tooltip" data-placement="top" title="Reset date"><i class="far fa-trash-alt"></i></a> 
                	</div>
                </div>

              </div>
              <!--Panel content-->

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-8">
              <!--Panel Header-->
              <div class="mt-4 mr-3">
               <div id="chartdiv1"></div>
              </div>
              <!--/Card image-->
            </div>
            <!--Grid column-->
          </div>
          <!--Grid row-->

        </section>
        <!--Section: Main panel-->
        
                <!--Section: Dettaglio giornaliero-->
        <section class="card card-cascade narrower mt-3 mb-5" id="charts1" style="display:none">

          <!--grid row-->
          <div class="row">

            <!--grid column-->
            <div class="col-md-4">

              <!--panel header-->
              <div class="view view-cascade py-3 gradient-card-header info-color-dark">
                <h5 class="mb-0">Dettaglio gradimento giornaliero</h5>
              </div>
              <!--/panel header-->
            </div>
            <!--grid column-->
			</div>
			
			<div class="row ml-2 mr-2">
              <!--Panel Header-->
               <div id="chartdiv2"></div>
              <!--/Card image-->
			</div>
          <!--Grid row-->

        </section>
        <!--Section: Main panel-->
        
      </div>
    </main>
    <!--Main layout-->

  </body>
  	
</body>
<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>
<!-- Moment JS-->
<script type="text/javascript" src="js/moment.js"></script>
<!-- Calendar -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src=" https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

<script src='calendar/core/main.js'></script>
<script src='calendar/core/locales-all.js'></script>
<script src='calendar/interaction/main.js'></script>
<script src='calendar/bootstrap/main.js'></script>
<script src='calendar/daygrid/main.js'></script>
<script src='calendar/timegrid/main.js'></script>
<script src='calendar/list/main.js'></script>
<!--  Custom JS -->
<script type="text/javascript" src="jsCustom/textNumber.js"></script>
<script type="text/javascript" src="jsCustom/htmlTool.js"></script>
<script type="text/javascript" src="jsCustom/dateUtil.js"></script>
<script type="text/javascript" src="jsCustom/calendar.js"></script>
<!-- Spinner JS -->
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<!-- NavBar JS -->
<script type="text/javascript" src="jsCustom/navigationSideBar.js"></script>

<script type="text/javascript" src="jsCustom/radialIndicator.js"></script>
    
 <!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/frozen.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>

//Mostro lo Spinner
$.LoadingOverlay("show");
//Inizializzo MDBootstrap 
new WOW().init();


//SideNav Button Initialization
initNavBar('<?php echo $baseurl;?>','<?php echo $version;?>', '<?php echo $_SESSION['login_role'];?>');

var centro = <?php echo $centro; ?>;
var result = <?php echo $index_result; ?>;
var user = 'Benvenuto <?php echo $_SESSION['login_user'];?>';
var role = '<?php echo $_SESSION['login_role'];?>';

var d = new Date();
var numberDay = getDays(d.getFullYear(),d.getMonth()+1);
var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);

var dataUpdated;

// definisco la variabile per il calendario
var calendarEl = document.getElementById('calendar');

if(result===1){//ho appena fatto login e quindi mostro il msg di Benvenuto
	toastr.success('Benvenuto <?php echo $login_session; ?>!');
}

var obj = <?php echo selectAll('anagrafico','Nome,Cognome');?>;

var result = [];

for(var i in obj){
    result.push([obj [i].Nome,obj[i].Cognome]);
}

//Material Select Initialization
$(document).ready(function () {

	
	
	// Show sideNav
	$('.button-collapse').sideNav('show');
	// Hide sideNav
	$('.button-collapse').sideNav('hide');
	
	$('#loginUser').html(user);


	if(role=="1"){
		//funzione per il refresh dei dati
		setInterval(prendiDati, 5000);
	}

	$('.collapsible-body').css({
		"display" : "none"
	});

		
	$.LoadingOverlay("hide");
});

if(role.localeCompare("1")==0){

	//se sono admin
	showComponent("gradimento");
	showComponent("charts");
	showComponent("charts1");

	initCharts();
	initChart2();
	
	$.ajax({ 
	    type: 'GET', 
	    url: 'moduli/questionario/receiveDataApp.php', 
	    data: { 
		    strutture: '0'
		}, 
	    dataType: 'json',
	    success: function (data) { 
	    	creaOptionStruttura(data);
	    }
	});
	

	$.ajax({ 
	    type: 'GET', 
	    url: 'moduli/questionario/receiveDataApp.php', 
	    data: { 
		    dati: '0',
		    idStruttura: centro
		}, 
	    dataType: 'json',
	    success: function (data) {
		    calcolaGradimento(data);
			// Add data
			generatechartData(data);
		}
	});
	
	
	
}else{	//se non sono admin inizializzo il calendario

	showComponent('calendar');
	
		initCalendar(calendarEl);


}

//creo le option per la select della struttura
function creaOptionStruttura(json) {
		
	$('#idStruttura').append($("<option></option>").attr("value", "0").text("TUTTI"));

		for (var i = json.length - 1; i >= 0; i--) {
			$('#idStruttura').append($("<option></option>").attr("value",json[i].id).text(json[i]['strutturaName']));
		}

	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
}

var label;

//Creo il grafico
var chart = am4core.create("chartdiv", am4charts.PieChart);
var chart1 = am4core.create("chartdiv1", am4charts.XYChart);
var chart3 = am4core.create("chartdiv2", am4charts.XYChart);

//Intialiazation indicatore radiale
var radialObj = radialIndicator('#indicatorContainer', {
    barColor: {
        0: '#d97467',
        20: '#e0ab76',
        40: '#eae188',
        60: '#badb84',
        80: '#74af61',
        100: '#74af61'
    },
    barWidth : 10,
    initValue : 0,
    radius: 100,
    percentage: true
}); 

//funzione che inizializza i grafici
function initCharts(){
am4core.ready(function() {

// Add label
chart.innerRadius = 100;
label = chart.seriesContainer.createChild(am4core.Label);
label.horizontalCenter = "middle";
label.verticalCenter = "middle";
label.fontSize = 40;
// var label = chart.seriesContainer.createChild(am4core.Label);
// label.text = "19925";
// label.horizontalCenter = "middle";
// label.verticalCenter = "middle";
// label.fontSize = 40;

// Add and configure Series
var pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.colors.list = [
	  am4core.color("#74af61"),
	  am4core.color("#badb84"),
	  am4core.color("#eae188"),
	  am4core.color("#e0ab76"),
	  am4core.color("#d97467"),
	];

pieSeries.dataFields.value = "size";
pieSeries.dataFields.category = "sector";

pieSeries.ticks.template.disabled = true;
pieSeries.alignLabels = false;
pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
pieSeries.labels.template.radius = am4core.percent(-20);
pieSeries.labels.template.fill = am4core.color("black");


//init bar graph
am4core.useTheme(am4themes_animated);


chart1.hiddenState.properties.opacity = 0; // this creates initial fade-in

//chart1.colors.step = 10;
//chart1.padding(30, 30, 10, 30);
//chart1.legend = new am4charts.Legend();

var categoryAxis = chart1.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart1.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.calculateTotals = true;
valueAxis.renderer.minWidth = 50;

chart1.colors.list = [
	  am4core.color("#d97467"),
	  am4core.color("#e0ab76"),
	  am4core.color("#eae188"),
	  am4core.color("#badb84"),
	  am4core.color("#74af61")
];





var series1 = chart1.series.push(new am4charts.ColumnSeries());
series1.columns.template.width = am4core.percent(80);
series1.name = "Scarso";
series1.columns.template.tooltipText = "{name}: {valueY.totalPercent.formatNumber('#.0')}%";
series1.dataFields.categoryX = "category";
series1.dataFields.valueY = "value5";
series1.dataFields.valueYShow = "totalPercent";
series1.dataItems.template.locations.categoryX = 0.5;
series1.stacked = true;
series1.tooltip.pointerOrientation = "vertical";
series1.columns.color = "#ffffff";

series1.columns.template.events.on("hit", function(ev) {
	showModal(ev.target._dataItem.categories.categoryX);
	}, this);

var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
bullet1.interactionsEnabled = false;
//bullet1.label.text = "{valueY.totalPercent.formatNumber('#.0')}%";
bullet1.label.fill = am4core.color("#ffffff");
bullet1.locationY = 0.5;

var series2 = chart1.series.push(new am4charts.ColumnSeries());
series2.name = "Mediocre";
series2.columns.template.width = am4core.percent(80);
series2.columns.template.tooltipText = "{name}: {valueY.totalPercent.formatNumber('#.0')}%";
series2.dataFields.categoryX = "category";
series2.dataFields.valueY = "value4";
series2.dataFields.valueYShow = "totalPercent";
series2.dataItems.template.locations.categoryX = 0.5;
series2.stacked = true;
series2.tooltip.pointerOrientation = "vertical";
series2.columns.template.events.on("hit", function(ev) {
	showModal(ev.target._dataItem.categories.categoryX);
	}, this);


var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
bullet2.interactionsEnabled = false;
//bullet2.label.text = "{valueY.totalPercent.formatNumber('#.0')}%";
bullet2.locationY = 0.5;
bullet2.label.fill = am4core.color("#ffffff");

var series3 = chart1.series.push(new am4charts.ColumnSeries());
series3.name = "Sufficiente";
series3.columns.template.width = am4core.percent(80);
series3.columns.template.tooltipText = "{name}: {valueY.totalPercent.formatNumber('#.0')}%";
series3.dataFields.categoryX = "category";
series3.dataFields.valueY = "value3";
series3.dataFields.valueYShow = "totalPercent";
series3.dataItems.template.locations.categoryX = 0.5;
series3.stacked = true;
series3.tooltip.pointerOrientation = "vertical";

series3.columns.template.events.on("hit", function(ev) {
	showModal(ev.target._dataItem.categories.categoryX);
	}, this);

var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
bullet3.interactionsEnabled = false;
//bullet3.label.text = "{valueY.totalPercent.formatNumber('#.0')}%";
bullet3.locationY = 0.5;
bullet3.label.fill = am4core.color("#ffffff");

var series4 = chart1.series.push(new am4charts.ColumnSeries());
series4.name = "Buono";
series4.columns.template.width = am4core.percent(80);
series4.columns.template.tooltipText = "{name}: {valueY.totalPercent.formatNumber('#.0')}%";
series4.dataFields.categoryX = "category";
series4.dataFields.valueY = "value2";
series4.dataFields.valueYShow = "totalPercent";
series4.dataItems.template.locations.categoryX = 0.5;
series4.stacked = true;
series4.tooltip.pointerOrientation = "vertical";
series4.columns.template.events.on("hit", function(ev) {
	showModal(ev.target._dataItem.categories.categoryX);
	}, this);


var bullet4 = series4.bullets.push(new am4charts.LabelBullet());
bullet4.interactionsEnabled = false;
//bullet4.label.text = "{valueY.totalPercent.formatNumber('#.0')}%";
bullet4.locationY = 0.5;
bullet4.label.fill = am4core.color("#ffffff");

var series5 = chart1.series.push(new am4charts.ColumnSeries());
series5.name = "Ottimo";
series5.columns.template.width = am4core.percent(80);
series5.columns.template.tooltipText = "{name}: {valueY.totalPercent.formatNumber('#.0')}%";
series5.dataFields.categoryX = "category";
series5.dataFields.valueY = "value1";
series5.dataFields.valueYShow = "totalPercent";
series5.dataItems.template.locations.categoryX = 0.5;
series5.stacked = true;
series5.tooltip.pointerOrientation = "vertical";
series5.columns.template.events.on("hit", function(ev) {
	showModal(ev.target._dataItem.categories.categoryX);
	}, this);


var bullet5 = series5.bullets.push(new am4charts.LabelBullet());
bullet5.interactionsEnabled = false;
//bullet5.label.text = "{valueY.totalPercent.formatNumber('#.0')}%";
bullet5.locationY = 0.5;
bullet5.label.fill = am4core.color("#ffffff");

}); // end am4core.ready()
}


//funzione per la formattazione della data
function formatDate(date) {
	  var monthNames = [
	    "Gen", "Feb", "Mar",
	    "Apr", "Mag", "Giu", "Lug",
	    "Ago", "Set", "Ott",
	    "Nov", "Dicr"
	  ];

	  var day = date.getDate();
	  var monthIndex = date.getMonth();
	  var year = date.getFullYear();

	  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


function initChart2(){
	am4core.ready(function() {

		var am4lang_it_IT = {
				"January": "Gennaio",
				"February": "Febbraio",
				"March": "Marzo",
				"April": "Aprile",
				"May": "Maggio",
				"June": "Giugno",
				"July": "Luglio",
				"August": "Agosto",
				"September": "Settembre",
				"October": "Ottobre",
				"November": "Novembre",
				"December": "Dicembre",
				"Jan": "Gen",
				"Feb": "Feb",
				"Mar": "Mar",
				"Apr": "Apr",
				"May(short)": "Mag",
				"Jun": "Giu",
				"Jul": "Lug",
				"Aug": "Ago",
				"Sep": "Set",
				"Oct": "Ott",
				"Nov": "Nov",
				"Dec": "Dic"
				}

		// Themes begin
		am4core.useTheme(am4themes_animated);
		chart3.language.locale = am4lang_it_IT;
		
		
		// Create axes
		var dateAxis = chart3.xAxes.push(new am4charts.DateAxis());
		dateAxis.startLocation = 0.4;
		dateAxis.endLocation = 0.6;

		// Create value axis
		var valueAxis = chart3.yAxes.push(new am4charts.ValueAxis());
		valueAxis.max = 100;
		valueAxis.min = 0; 

		// Create series
		var series = chart3.series.push(new am4charts.LineSeries());
		series.dataFields.valueY = "media";
		series.dataFields.dateX = "date";
		series.strokeWidth = 3;
		series.tooltipText = "{valueY.value} %";
		series.tooltip.getFillFromObject = false;
		series.tooltip.background.fill = am4core.color("#0199cb");
		series.fillOpacity = 0.1;

		// Create a range to change stroke for values below 0
		var range = valueAxis.createSeriesRange(series);
		range.value = 0;
		range.endValue = 50;
		//range.contents.stroke = chart.colors.getIndex(10);
		range.contents.stroke = am4core.color("#ff0000");
		//#74af60
		range.contents.fill = range.contents.stroke;
		range.contents.strokeOpacity = 0.7;
		range.contents.fillOpacity = 0.1;

		// Create a range to change stroke for values below 0
		var range1 = valueAxis.createSeriesRange(series);
		range1.value = 50;
		range1.endValue = 100;
		//range.contents.stroke = chart.colors.getIndex(10);
		range1.contents.stroke = am4core.color("#74af60");
		//#74af60
		range1.contents.fill = range1.contents.stroke;
		range1.contents.strokeOpacity = 0.7;
		range1.contents.fillOpacity = 0.1;
		



		// Add cursor
		chart3.cursor = new am4charts.XYCursor();
		chart3.cursor.xAxis = dateAxis;
		//chart3.scrollbarX = new am4core.Scrollbar();
		chart3.language.locale = am4lang_it_IT;

		}); // end am4core.ready()
}


//funzione che riempe il chart di gradimento dettagliato
function generatechartData(data) {
//console.log(data);
var day,value;
var count = 0;
var chartData = [];

  //firstDay.setDate(firstDay);
  
  if(data.length!=0){
  
  for(i=0;i<data.length;i++){

    	if(i==0){
        	count++;
    		day = new Date(data[i].dataValutazione).getDate();
    		month = new Date(data[i].dataValutazione).getMonth()+1;
    	  	value = data[i].media;
    	} else {
        	day2 = new Date(data[i].dataValutazione).getDate();
    		if(day2==day){
    			value = value + data[i].media;
    			count++;
    		} else {
    			day_ok = data[i-1].dataValutazione;
    			chartData.push( {
               	    date: day_ok,//day +"-"+month,
               	    media: (value/count)*100/5});
           		count = 1;
      			//console.log(chartData);
        		day = day2;
        		value = data[i].media;
    		}
    	}

    	//console.log(day);
    	//console.log(value);
  }

  chartData.push( {
	       date: data[data.length-1].dataValutazione,//day +"-"+month,
	       media: (value/count)*100/5
	     } );

  	chart3.data = chartData;

 
} else {
	chart3.data = chartData;
}
}

//inizializzo i datepicker
$('.datepicker').pickadate({
	monthsFull: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre','Novembre', 'Dicembre'],
	weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
	today: '',
	clear: '',
	close: '',
	format: 'dd/mm/yyyy',
	formatSubmit: 'yyyy-mm-dd'
	});

// Tooltip Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Minimalist chart
$(function () {
  $('.min-chart#chart-sales').easyPieChart({
    barColor: "#4caf50",
    onStep: function (from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
});

//funzione che calcola quanti giorni ha il mese
function getDays(year, month) {
	  return new Date(year, month, 0).getDate(); // 0 + number of days
}


function calcolaGradimento(json){

	
	
	var scarso = 0;
	var mediocre = 0;
	var sufficiente = 0;
	var buono = 0;
	var ottimo = 0;
	var somma = 0;
	var mediaSomma = 0;
	
	var scarsoq = [0,0,0,0,0,0,0,0,0,0];
	var mediocreq = [0,0,0,0,0,0,0,0,0,0];
	var sufficienteq = [0,0,0,0,0,0,0,0,0,0];
	var buonoq = [0,0,0,0,0,0,0,0,0,0];
	var ottimoq = [0,0,0,0,0,0,0,0,0,0];
	
	for (var i = 0; i<json.length; i++) {

		//calcolo le occorrenze per le varie valutazioni
		var q = [];
		var x = json[i].media;

		q.push(json[i].q1);
		q.push(json[i].q2);
		q.push(json[i].q3);
		q.push(json[i].q4);
		q.push(json[i].q5);
		q.push(json[i].q6);
		q.push(json[i].q7);
		q.push(json[i].q8);
		q.push(json[i].q9);
		q.push(json[i].q10);

		for(var k=0; k<q.length;k++){
	
    			if(q[k]==1){
    				scarsoq[k] = scarsoq[k] + 1;
        		} else if (q[k]==2) {
        			mediocreq[k] = mediocreq[k] + 1;
        		} else if (q[k]==3) {
        			sufficienteq[k] = sufficienteq[k] + 1;
        		} else if (q[k]==4) {
        			buonoq[k] = buonoq[k] + 1;
        		} else if (q[k]==5){
        			ottimoq[k] = ottimoq[k] + 1;
        		} else {
            		//
        		}
 
		}

		 
//calcolo la media
		if(x<2){
				scarso = scarso + 1;
		} else if (x>=2 & x<3) {
				mediocre = mediocre + 1;
		} else if (x>=3 & x<4) {
				sufficiente = sufficiente + 1;
		} else if (x>=4 & x<5) {
				buono = buono + 1;
		} else if (x>=5 & x<6){
				ottimo = ottimo + 1;
		} else {
			//
		}

		somma = somma + x;
	}



	mediaSomma = somma/i;
	
	var gradimento = (mediaSomma * 100)/5;

	// Aggiungo dati al PIE
	chart.data = [
	    { "sector": "Ottimo", "size": ottimo, "color": "#ff0000" },
	    { "sector": "Buono", "size": buono,"color": "#ff0000" },
	    { "sector": "Sufficiente", "size": sufficiente, "color": "#ff0000" },
	    { "sector": "Mediocre", "size": mediocre, "color": "#ff0000" },
	    { "sector": "Scarso", "size": scarso, "color": "#ff0000" }
	];


	
	chart1.data = [
		  {
		    category: "1",
		    value1: ottimoq[0],
		    value2: buonoq[0],
		    value3: sufficienteq[0],
		    value4: mediocreq[0],
			value5: scarsoq[0]
		  },
		  {
		    category: "2",
		    value1: ottimoq[1],
		    value2: buonoq[1],
		    value3: sufficienteq[1],
		    value4: mediocreq[1],
			value5: scarsoq[1]
		  },
		  {
		    category: "3",
		    value1: ottimoq[2],
		    value2: buonoq[2],
		    value3: sufficienteq[2],
		    value4: mediocreq[2],
			value5: scarsoq[2]
		  },
		  {
		    category: "4",
		    value1: ottimoq[3],
		    value2: buonoq[3],
		    value3: sufficienteq[3],
		    value4: mediocreq[3],
			value5: scarsoq[3]
		  },
		  {
		    category: "5",
		    value1: ottimoq[4],
		    value2: buonoq[4],
		    value3: sufficienteq[4],
		    value4: mediocreq[4],
			value5: scarsoq[4]
		  },
		  {
		    category: "6",
		    value1: ottimoq[5],
		    value2: buonoq[5],
		    value3: sufficienteq[5],
		    value4: mediocreq[5],
			value5: scarsoq[5]
		  },
		  {
		    category: "7",
		    value1: ottimoq[6],
		    value2: buonoq[6],
		    value3: sufficienteq[6],
		    value4: mediocreq[6],
			value5: scarsoq[6]
		  },
		  {
		    category: "8",
		    value1: ottimoq[7],
		    value2: buonoq[7],
		    value3: sufficienteq[7],
		    value4: mediocreq[7],
			value5: scarsoq[7]
		  },
		  {
		    category: "9",
		    value1: ottimoq[8],
		    value2: buonoq[8],
		    value3: sufficienteq[8],
		    value4: mediocreq[8],
			value5: scarsoq[8]
		  },
		  {
		    category: "10",
		    value1: ottimoq[9],
		    value2: buonoq[9],
		    value3: sufficienteq[9],
		    value4: mediocreq[9],
			value5: scarsoq[9]
		  }
		];
	
	//Setto i parametri
	label.text = i;
	radialObj.animate(gradimento | 0);

}


$('#dateStart').change( function() {  
	$('#dateEnd').val($('#dateStart').val());
});


$('#dateEnd').change( function() {  
	if($('#dateEnd').val()<$('#dateStart').val()){
		toastr.error('Valore data non valido!');
		$('#dateEnd').val($('#dateStart').val());
	}
});

$( "#cercaStat" ).click(function() {

	if($('#dateStart').val()!="" & $('#dateEnd').val()!=""){
        	$.ajax({ 
        	    type: 'GET', 
        	    url: 'moduli/questionario/receiveDataApp.php', 
        	    data: { 
        		    dati: '0',
        		    dataStart: formatISODate($('#dateStart').val()),
        		    dataEnd: formatISODate($('#dateEnd').val()),
        			idStruttura: $('#idStruttura').val()}, 
        	    dataType: 'json',
        	    success: function (data) {
            	    if(data!=""){
        		    calcolaGradimento(data);
            	    } else {
            	    	toastr.error('Nessun risultato disponibile!');
            	    	generatechartData([]);
            	    	calcolaGradimento([]);
            	    }
        		}
        	});
	} else {
		toastr.error('Data di inizio o fine non inserita!');
	}
	
});

$( "#resetStat" ).click(function() {

	$('#dateStart').val("");
	$('#dateEnd').val("");
	$('#idStruttura').val('0');
	$('.mdb-select').material_select('destroy');
	$('.mdb-select').material_select();
	
	toastr.success('Date resettate');
	
	
});




function prendiDati(){

	if($('#dateStart').val()!=""&&$('#dateEnd').val()!=""){
	$.ajax({ 
	    type: 'GET', 
	    url: 'moduli/questionario/receiveDataApp.php', 
	    data: { 
		    dati: '0',
		    dataStart: formatISODate($('#dateStart').val()),
		    dataEnd: formatISODate($('#dateEnd').val()),
			idStruttura: $('#idStruttura').val()}, 
	    dataType: 'json',
	    success: function (data) {
    	    if(data!=""){
		        if(dataUpdated==data[data.length-1].id){
    		    } else {
    		    	calcolaGradimento(data);
    		    	generatechartData(data);
    		    	dataUpdated=data[data.length-1].id;
    		    }
    	    } else {
    	    	//toastr.error('Nessun risultato disponibile!');
    	    }
		}
	});
	} else {
		$.ajax({ 
		    type: 'GET', 
		    url: 'moduli/questionario/receiveDataApp.php', 
		    data: { 
			    dati: '0',
			    idStruttura: centro
			}, 
		    dataType: 'json',
		    success: function (data) {
	    	    if(data!=""){
			    
    			    if(dataUpdated==data[data.length-1].id){
        		    } else {
        		    	calcolaGradimento(data);
        		    	generatechartData(data);
        		    	dataUpdated=data[data.length-1].id;
        		    }
	    	    } else {
	    	    	//toastr.error('Nessun risultato disponibile!');
	    	    }
			}
		});
	}
}

function showModal(category){

	switch (category) {
	  case "1":
	    var text = "Come valuti la tua esperienza presso i nostri centri?";
	    break;
	  case "2":
		  var text = "Secondo la Sua esperienza, com&rsquo;&eacute; stato individuare e rintracciare il Centro?";
	   	break;
	  case "3":
		  var text = "Secondo la Sua esperienza, com&rsquo;&eacute; stato avere contatti ed ottenere risposte sulla procedura necessaria?";
	   	break;
	  case "4":
		  var text = "Secondo la Sua esperienza, com&rsquo;&eacute; stato completare le pratiche amministrative?";
	   	break;
	  case "5":
		  var text = "Secondo la Sua esperienza, com&rsquo;&eacute; stato definire gli appuntamenti delle visite mediche?";
	   	break;
	  case "6":
		  var text = "Secondo la Sua esperienza, com&rsquo;&eacute; stato definire gli orari delle terapie?";
	   	break;
	  case "7":
		  var text = "Gli orari e gli appuntamenti concordati, sono rispettati in maniera soddisfacente?";
	   	break;
	  case "8":
		  var text = "Il personale medico fornisce prestazioni professionali soddisfacenti ed &eacute; stato esauriente nel darle informazioni sulla Sua patologia e sul piano di trattamento?";
	   	break;
	  case "9":
		  var text = "I terapisti forniscono prestazioni soddisfacenti?";
	   	break;
	  case "10":
		  var text = "Il personale amministrativo fornisce informazioni esaurienti ed &eacute; efficiente nel risolvere eventuali problemi?";
	   	break;
	  default:
	   
	    break;
	}

	
	var html = '<div class="modal fade right show" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="true" style="display: block; padding-right: 17px;" aria-modal="true">';
    html += '<div class="modal-dialog modal-side modal-bottom-right modal-notify modal-info" role="document">';
    html += '  <!--Content-->';
    html += '  <div class="modal-content">';
    html += '    <!--Header-->';
	html += '    <div class="modal-header">';
	html += '<p class="heading">Domanda '+category+'</p>';
    html += '</div>';

	html += '    <!--Body-->';
    html += ' <div class="modal-body">';
	html += ' <div class="row">';
	html += '    <div class="text-center">';
    html += '      <p>'+text+'</p>';
    html += '</div>';
    html += ' </div>';
    html += '</div>';
    html += ' <!--Footer-->';
    html += ' <div class="modal-footer justify-content-center">';
    html += '   <a type="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">Chiudi</a>';
    html += ' </div>';
    html += ' </div>';
    html += ' <!--/.Content-->';
    html += '</div>';

	$("#modal").html(html);
	$('#modalInfo').modal('show');
}


</script>

    
</html>