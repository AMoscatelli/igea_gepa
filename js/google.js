// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
	street_number : 'short_name', // IndirizzoResidNVia
	route : 'long_name', // IndirizzoResidVia
	locality : 'long_name', //LuogoDiNascita 
	administrative_area_level_2 : 'short_name', //DNProv
	administrative_area_level_3 : 'short_name',
	country : 'long_name', // Nazionalita
	postal_code : 'short_name' // DNCAP
};

var place;
var object=0;





//$( "#IndirizzoResidVia" ).is(":focus")

//var pos= ['IndirizzoResidVia','IndirizzoResidNVia','LuogoDiNascita','DNProv','Nazionalita','DNCAP'];
function initAutocomplete() {
	// Create the autocomplete object, restricting the search to geographical
	// location types.
		if (google.maps.places !== undefined){
			autocomplete = new google.maps.places.Autocomplete(
					/** @type {!HTMLInputElement} */
					(document.getElementById('IndirizzoResidVia')), {
						types : [ 'geocode' ]
					});
			autocomplete_new = new google.maps.places.Autocomplete(
					/** @type {!HTMLInputElement} */
					(document.getElementById('LuogoDiNascita')), {
						types : [ 'geocode' ]
					});
			

			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener('place_changed', fillInAddress);
			autocomplete_new.addListener('place_changed', fillInAddress);
		}else console.log("google.maps.place - undefined");
		
}

function fillInAddress() {
	
	// Get the place details from the autocomplete object.
	if ($( "#IndirizzoResidVia" ).is(":focus")|| object==1){
		var place = autocomplete.getPlace();
	}
	else if ($( "#LuogoDiNascita" ).is(":focus")|| object==2){
		var place = autocomplete_new.getPlace();
	}
	//for ( var component in componentForm) {
	//	document.getElementById(component).value = '';
	//	document.getElementById(component).disabled = false;
	//}

	// Get each component of the address from the place details
	// and fill the corresponding field on the form.
	var val_res;
	var val_zone;
	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		
		switch (addressType) {
		 	case  'route':
		 		if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]]; 
					if ($( "#IndirizzoResidVia" ).is(":focus")|| object==1){
						$('#IndirizzoResidVia').val(val);
					}
					
			    }
		 		break;
		 	case "administrative_area_level_3":
		 		if (componentForm[addressType]) {
					 val_res = place.address_components[i][componentForm[addressType]]; 
					 if ($( "#IndirizzoResidVia" ).is(":focus")|| object==1){
						 $('#luogoResidenza').val(val_res);
						 VisualizzaIstat('IstatResidenza',$('#luogoResidenza').val());
						 $('#IndirizzoResidVia').blur();
					 }else if ($( "#LuogoDiNascita" ).is(":focus")|| object==2){
						 $('#LuogoDiNascita').val(val_res);
						 VisualizzaIstat('IstatNascita',$('#LuogoDiNascita').val());
						 $('#LuogoDiNascita').blur();
					 }
					 
					//$('#LuogoDiNascita').val(val);
					//$('#comuneDiNascita').val(val);
			    }
		 		break;
		 	case "locality":
		 		if (componentForm[addressType]) {
					val_zone = place.address_components[i][componentForm[addressType]]; 
			    }
		 		break;
		 	case"street_number":
		 		if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]]; 
					$('#IndirizzoResidNVia').val(val);
			    }
		 		break;
		 	case "administrative_area_level_2":
		 		if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]]; 
					if ($( "#IndirizzoResidVia" ).is(":focus")|| object==1){
						$('#ResProv').val(val);
					}else if ($( "#LuogoDiNascita" ).is(":focus")|| object==2){
						$('#DNProv').val(val);
					}
			    }
		 		break;
		 	case "country":
		 		if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]]; 
					$('#Nazionalita').val(val);
			    }
		 		break;
		 	case "postal_code":
		 		if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]]; 
					if ($( "#IndirizzoResidVia" ).is(":focus")|| object==1){
						$('#ResCAP').val(val);
					}else if ($( "#LuogoDiNascita" ).is(":focus")|| object==2){
						$('#DNCAP').val(val);
					}
			    }
		 		break;
		
		 }
		
		
	}
	if (val_zone!==undefined){
		if (val_res!==undefined){
			if (val_zone.localeCompare(val_res)!==0){
				$('#Zona').val(val_zone);
			}else {
				$('#Zona').val('');
			}
		}
	}
	
	
	
}