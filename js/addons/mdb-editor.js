(function ($) {

  $.fn.mdbEditor = function () {

    return this.each(function () {

      var $selectedRow;
      var $selectedTable = $(this);
      var $selectedTableId = $selectedTable.closest('.wrapper-modal-editor').find('table').attr('id');
      var $selectedTableSharp = $('#' + $selectedTableId);
      var $table = $('#' + $selectedTableId).DataTable();
      var $wrapperModalEditor = $selectedTableSharp.closest('.wrapper-modal-editor');
      var $createShowP = $wrapperModalEditor.find('.createShowP');
      var $buttonEdit = $wrapperModalEditor.find('.buttonEdit');
      var $buttonDelete = $wrapperModalEditor.find('.buttonDelete');
      var $buttonAddFormWrapper = $wrapperModalEditor.find('.buttonAddFormWrapper');
      var $buttonEditWrapper = $wrapperModalEditor.find('.buttonEditWrapper');
      var $editInsideWrapper = $wrapperModalEditor.find('.editInsideWrapper');
      var $deleteButtonsWrapper = $wrapperModalEditor.find('.deleteButtonsWrapper');
      var editInside = $wrapperModalEditor.find('.editInside');
      var trColorSelected = '.tr-color-selected';

      bindEvents();

      function bindEvents() {

        $buttonAddFormWrapper.on('click', '.buttonAdd', addNewRows);
        $selectedTableSharp.on('click', 'tr', addColorToTr);
        $selectedTableSharp.on('click', 'tr', toggleDisabledToButtons);
        $buttonEditWrapper.on('click', $buttonEdit, buttonEditInput);
        $buttonEditWrapper.on('click', $buttonEdit, addClassActiveToLabel);
        $deleteButtonsWrapper.on('click', '.btnYesClass', buttonDeleteYes);
        $editInsideWrapper.on('click', editInside, buttonEditInside);
        $editInsideWrapper.on('click', editInside, removeColorClassFromTr);
        $editInsideWrapper.on('click', editInside, disabledButtons);
        $editInsideWrapper.on('click', editInside, selectedZeroRowsNews);
      }

      function addNewRows() {

        $table.row.add([

          $wrapperModalEditor.find('.addNewInputs input').eq(0).val(),
          $wrapperModalEditor.find('.addNewInputs input').eq(1).val(),
          $wrapperModalEditor.find('.addNewInputs input').eq(2).val(),
          $wrapperModalEditor.find('.addNewInputs input').eq(3).val(),
          $wrapperModalEditor.find('.addNewInputs input').eq(4).val(),
          $wrapperModalEditor.find('.addNewInputs input').eq(5).val()
        ]).draw()
      }

      function addColorToTr() {

        $(this).not('thead tr').not('tfoot tr').toggleClass('tr-color-selected').siblings().removeClass('tr-color-selected');
      }

      function toggleDisabledToButtons() {

        $selectedRow = this;

        if ($(this).not('thead tr').not('tfoot tr').hasClass('tr-color-selected')) {

          $buttonEdit.prop('disabled', false);
          $buttonDelete.prop('disabled', false);
          $createShowP.html('1 row selected');
        } else if (!$('tr').hasClass('tr-color-selected')) {

          $buttonEdit.prop('disabled', true);
          $buttonDelete.prop('disabled', true);
          $createShowP.html('0 row selected');
        }
      }

      function buttonEditInput() {

        $table.row(

          $wrapperModalEditor.find('.modalEditClass input').eq(0).val($table.cell($selectedRow, 0).data()),
          $wrapperModalEditor.find('.modalEditClass input').eq(1).val($table.cell($selectedRow, 1).data()),
          $wrapperModalEditor.find('.modalEditClass input').eq(2).val($table.cell($selectedRow, 2).data()),
          $wrapperModalEditor.find('.modalEditClass input').eq(3).val($table.cell($selectedRow, 3).data()),
          $wrapperModalEditor.find('.modalEditClass input').eq(4).val($table.cell($selectedRow, 4).data()),
          $wrapperModalEditor.find('.modalEditClass input').eq(5).val($table.cell($selectedRow, 5).data())
        )
      }

      function addClassActiveToLabel() {

        $('.modalEditClass label').addClass('active');
      }

      function buttonEditInside() {

        $table.cell($(trColorSelected).find('td').eq(0)).data($wrapperModalEditor.find('.modalEditClass input').eq(0).val());
        $table.cell($(trColorSelected).find('td').eq(1)).data($wrapperModalEditor.find('.modalEditClass input').eq(1).val());
        $table.cell($(trColorSelected).find('td').eq(2)).data($wrapperModalEditor.find('.modalEditClass input').eq(2).val());
        $table.cell($(trColorSelected).find('td').eq(3)).data($wrapperModalEditor.find('.modalEditClass input').eq(3).val());
        $table.cell($(trColorSelected).find('td').eq(4)).data($wrapperModalEditor.find('.modalEditClass input').eq(4).val());
        $table.cell($(trColorSelected).find('td').eq(5)).data($wrapperModalEditor.find('.modalEditClass input').eq(5).val());
      }

      function removeColorClassFromTr() {

        $selectedTable.find('.tr-color-selected').removeClass('tr-color-selected');
      }

      function disabledButtons() {

        $buttonEdit.prop('disabled', true);
        $buttonDelete.prop('disabled', true);
      }

      function selectedZeroRowsNews() {

        $createShowP.html('0 row selected');
        $table.draw(false);
      }

      function buttonDeleteYes() {

        $buttonEdit.prop('disabled', true);
        $buttonDelete.prop('disabled', true);
        $createShowP.html('0 row selected');
        $table.row($(trColorSelected)).remove().draw();
      }
    })
  }

  $.fn.mdbEditorRow = function () {

    return this.each(function () {

      var editRow = '.editRow';
      var saveRow = '.saveRow';
      var tdLast = 'td:last';
      var $removeColumns = $('.removeColumns');
      var $this = $(this);
      var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');
      var $sharpTableId = $('#' + $tableId);
      var $tableData = $sharpTableId.DataTable();
      var addNewColumn = '.addNewColumn';
      var $buttonWrapper = $('.buttonWrapper');
      var $closeByClick = $('.closeByClick')
      var $showForm = $('.showForm');


      bindEvents();

      function bindEvents() {

        $buttonWrapper.on('click', addNewColumn, addNewTr);
        $buttonWrapper.on('click', addNewColumn, removeDisabledButtons);
        $this.on('click', editRow, editRowAndAddClassToTr);
        $this.on('click', editRow, addColorClassAndPy);
        $this.on('click', editRow, addDisabledButtonsByEditBtn);
        $this.on('click', saveRow, saveRowAndRemovePy);
        $this.on('click', saveRow, removeDisabledColorAdnTdYes);
        $('.buttonYesNoWrapper').on('click', '.btnYes', removeColorInTrAndDraw);
        $buttonWrapper.on('click', '.removeColumns', removeSelectedButtonsFromRow);
      }

      function addNewTr() {

        var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');

        $(document).find('#' + $tableId).each(function () {

          $(this).find('tr').each(function () {

            $(this).find(tdLast).not('.td-editor').after('<td class="text-center td-editor" style="border-top: 1px solid #dee2e6; border-bottom:1px solid #dee2e6"><button class="btn btn-sm editRow btn-sm btn-teal"><i class="far fa-edit"></i></button></td>');
          })
        });

      }

      function removeDisabledButtons() {

        var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');
        var $findButton = $('#' + $tableId).closest('.wrapper-row-editor').find('.removeColumns');

        if ($('#' + $tableId).find('td').hasClass('td-editor') == true) {

          $findButton.prop('disabled', false);

        } else {

          $findButton.prop('disabled', true);
        }

        if (!$('#' + $tableId).closest('.wrapper-row-editor').find('td.td-editor').hasClass('td-editor')) {

          $findButton.prop('disabled', true);
        }
      }

      function editRowAndAddClassToTr() {

        var $closestTrTd = $(this).closest('tr').find('td'),
          $closestTrEdit = $(this).closest('tr').find(editRow),

          tdFirst = $closestTrTd.eq(0).html(),
          tdSecond = $closestTrTd.eq(1).html(),
          tdThird = $closestTrTd.eq(2).html(),
          tdFourth = $closestTrTd.eq(3).html(),
          tdFifth = $closestTrTd.eq(4).html(),
          tdSixth = $closestTrTd.eq(5).html(),

          tdFieldFirst = '<input type="text" class="val1 form-control' + '" value="' + tdFirst + '" >',
          tdFieldSecond = '<input type="text" class="val2 form-control' + '" value="' + tdSecond + '" >',
          tdFieldThird = '<input type="text" class="val3 form-control' + '" value="' + tdThird + '" >',
          tdFieldFourth = '<input type="number" class="val4 form-control' + '" value="' + tdFourth + '" >',
          //tdFieldFifth = '<input type="text" class="val5 form-control' + '" value="' + tdFifth + '" >',
          //tdFieldSixth = '<input type="text" class="val6 form-control' + '" value="' + tdSixth + '" >',

          editButton = '<td class="text-center td-editor td-yes" style="border:none"><button class="btn btn-sm btn-danger deleteRow" style="cursor:pointer;"><i class="fas fa-trash-alt"></i></b></td>',
          saveButton = '<td class="text-center td-editor td-yes" style="border:none"><button class="btn btn-sm btn-primary saveRow" style="cursor:pointer;"><i class="fas fa-check"></i></button></td>';

        $closestTrTd.eq(0).html(tdFieldFirst);
        $closestTrTd.eq(1).html(tdFieldSecond);
        $closestTrTd.eq(2).html(tdFieldThird);
        $closestTrTd.eq(3).html(tdFieldFourth);
        //$closestTrTd.eq(4).html(tdFieldFifth);
        //$closestTrTd.eq(5).html(tdFieldSixth);
        $closestTrEdit.after(editButton);
        $closestTrEdit.after(saveButton);


        $($('#' + $tableId)).on('click', '.deleteRow', function () {

          var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');

          $($('#' + $tableId).closest('.wrapper-row-editor').find('.showForm').removeClass('d-none'));

          $($('#' + $tableId)).closest('.wrapper-row-editor').find('.closeByClick').removeClass('d-none');

        })

      }

      if ($closeByClick.hasClass('d-none') === true) {

        $(document).keyup(function (e) {

          if (e.keyCode === 27) {

            $closeByClick.addClass('d-none');
            $showForm.addClass('d-none');
          }
        })
      }

      $closeByClick.on('click', function () {

        $(this).addClass('d-none');
        $showForm.addClass('d-none');
      })


      $showForm.on('click', '.btnYes, .button-x, .btnNo', function () {

        $showForm.addClass('d-none');
        $closeByClick.addClass('d-none');
      })

      function addColorClassAndPy() {

        var $closestTr = $(this).closest('tr');

        $closestTr.addClass('tr-color-selected');
        $closestTr.find('td').not('.td-editor').addClass('py-5');
      }

      function addDisabledButtonsByEditBtn() {

        var $tableId = $(this).closest('.wrapper-row-editor');

        $(this).prop('disabled', true);
        $($tableId).find($removeColumns).prop('disabled', true);
      }

      function saveRowAndRemovePy() {

        var $closestTrTd = $(this).closest('tr').find('td');
        var $closestTr = $(this).closest('tr');

        $tableData.cell($closestTrTd.eq(0)).data($closestTr.find('.val1').val());
        $tableData.cell($closestTrTd.eq(1)).data($closestTr.find('.val2').val());
        $tableData.cell($closestTrTd.eq(2)).data($closestTr.find('.val3').val());
        $tableData.cell($closestTrTd.eq(3)).data($closestTr.find('.val4').val());
        $tableData.cell($closestTrTd.eq(4)).data($closestTr.find('.val5').val());
        $tableData.cell($closestTrTd.eq(5)).data($closestTr.find('.val6').val());
        $closestTr.find('td').removeClass('py-5');
      }

      function removeDisabledColorAdnTdYes() {

        var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');
        var $closestTr = $(this).closest('tr');

        $closestTr.find(editRow).prop('disabled', false);
        $closestTr.removeClass('tr-color-selected');
        $closestTr.find('.td-yes').remove();
        $tableData.draw(false);

        $('#' + $tableId).closest('.wrapper-row-editor').find('.removeColumns').prop('disabled', false);
      }

      function removeColorInTrAndDraw() {

        var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');

        $tableData.row($('#' + $tableId).find('tr.tr-color-selected')).remove().draw(false);

        if (!$('#' + $tableId + ' ' + 'tr').hasClass('td-editor')) {

          $('#' + $tableId).closest('.wrapper-row-editor').find($removeColumns).prop('disabled', false);
        } else {

          $('#' + $tableId).closest('.wrapper-row-editor').find($removeColumns).prop('disabled', true);
        }
      };

      function removeSelectedButtonsFromRow() {

        var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');
        var $closeWrapper = $(this).closest('.wrapper-row-editor');

        if (!$('#' + $tableId).hasClass('td-editor') === true) {

          $closeWrapper.find('.removeColumns').attr('disabled', true);
        }
        if ($('#' + $tableId).hasClass('td-editor') === false && $('#' + $tableId + ' tr').hasClass('tr-color-selected') === false) {

          $('#' + $tableId).find('.td-editor').remove();
          $('#' + $tableId).find('.tr-color-selected').remove();
          $tableData.draw(false);
        }
      }
    })
  }

  $('.buttonWrapper').on('click', '.addNewRows', addNewRowsAndDraw);

  function addNewRowsAndDraw() {

    var $tableId = $(this).closest('.wrapper-row-editor').find('table').attr('id');
    console.log($tableId)
    var $tableData = $('#' + $tableId).DataTable();

    $tableData.row.add([
      $(this).val(),
      $(this).val(),
      $(this).val(),
      $(this).val(),
      $(this).val(),
      $(this).val()
    ]).draw()
  }
}(jQuery));
